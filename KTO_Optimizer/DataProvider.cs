﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;
using Optimizer_Data;

using Logging;

namespace KZO_Data
{
    public class DataProvider_Excel
    {

        protected const bool isSampleDistancesIfMissing = true;
        protected const string KeyElementSeparator="@";
        protected Int32 usedSeed = 0;
        protected Random usedRNG;
        protected const Double simpleDistanceMinVal = 0.5;
        protected const Double simpleDistanceMaxVal = 2.5;
        protected const Double minPctDistMultipleLocs = 0.8;
        protected const Double minPctDistConspointReturn = 0.8;
        protected Int32 startPointColumn = 0;//65;
        protected Int32 startPointLine = 0;//140;
        //protected const bool isIgnoreNotConsignedProducts = true;

        protected LoggerCollection loggers;

        Dictionary<Int32, Int32> lineLanes;
       /*
        "Quantity unit of this shipping level ", 
                                                   "Shipping level qty. per pallet", 
                                                   "Length in mm",
                                                   "Width in mm",
                                                   "Height in mm",
                                                   "Net weight in kg",
                                                   "Gross weight in kg"
        */
        protected class Product {
            public String Id;
            public String Description;
            public String QuantityUnit;
            public Int32 numberOfPicks;
            public double ShippingLevelQuantityPerPallet;
            public double Length;
            public double Width;
            public double Height;
            public double NetWeight;
            public double GrossWeight;
        };

        protected class Point
        {
            public Int32 Lane;
            public Int32 Column;
            public PassageRoute PRoute;
            public String Key
            {
                get { return Convert.ToString(Lane, CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(Column, CultureInfo.InvariantCulture); }
            }
        }
        protected class PassageRoute
        {
            public Int32 StartColNum;
            public Int32 EndColNum;
        }
        protected class Location
        {
            public Int32 Line;
            public Int32 Column;
            public Int32 Level;
            public Int32 PickingSequence;
            public Int32 orderNumber;
            
            public static Int32 compareByPickingSequence(Location l1, Location l2)
            {
                if (l1.PickingSequence < l2.PickingSequence)
                    return -1;
                else if (l1.PickingSequence == l2.PickingSequence)
                    return 0;
                else
                    return 1;
            }

            public static Int32 compareByColumn(Location l1, Location l2)
            {
                if (l1.Column < l2.Column)
                    return -1;
                else if (l1.Column == l2.Column)
                    return 0;
                else
                    return 1;
            }

            public static Int32 compareByOrderNumber(Location l1, Location l2)
            {
                if (l1.orderNumber < l2.orderNumber)
                    return -1;
                else if (l1.orderNumber == l2.orderNumber)
                    return 0;
                else
                    return 1;
            }

            public String Id
            {
                get
                {
                    return Convert.ToString(Line, CultureInfo.InvariantCulture) + KeyElementSeparator +
                           Convert.ToString(Column, CultureInfo.InvariantCulture) + KeyElementSeparator +
                           Convert.ToString(Level, CultureInfo.InvariantCulture);
                    }
            }
        }
        
        protected class MacroLocationProblemData
        {
            public String MacroLocationId;
            public Dictionary<String, Location> Locations;
            public Dictionary<String, Product> AssignedProducts;
            public Dictionary<String, Product> ConsignedProducts;
            public Dictionary<String, ConsignationProblem.ConsignationList> ConsignationLists;
            public Dictionary<String, HashSet<String>> AssociatedLists;
            public Dictionary<String, Double> Distances;
            public Dictionary<String, HashSet<String>> AssignedLocations;
            public List<Location> LocList;
            public HashSet<String> UnAssignedProductsFromConsignationLists;
            public Dictionary<Int32, List<Location>> locationsByLane;
            public HashSet<Int32> passageRouteColNums;
            public HashSet<Int32> relevantColumns;
            public List<PassageRoute> pRoutes;
            public Point [] pArr;
            public double [,] distArr;
            public Dictionary<String, Int32> pointNums;

            public MacroLocationProblemData()
            {
                Locations=new Dictionary<String, Location> ();
                ConsignedProducts=new Dictionary<String, Product> ();
                ConsignationLists = new Dictionary<String, ConsignationProblem.ConsignationList>();
                Distances=new Dictionary<String, Double> ();
                AssignedProducts = new Dictionary<String, Product>();
                AssignedLocations = new Dictionary<String, HashSet<String>>();//new Dictionary<string, Location>();
                LocList = new List<Location>();
                UnAssignedProductsFromConsignationLists = new HashSet<String>();
                locationsByLane = new Dictionary<int, List<Location>>();
                passageRouteColNums = new HashSet<int>();
                pRoutes = new List<PassageRoute>();
                relevantColumns = new HashSet<int>();
                pointNums = new Dictionary<string, int>();
                AssociatedLists = new Dictionary<string, HashSet<string>>();
                //LocationsByOrderNumber = null;
            }

        }

        public class XLSTable
        {
            public DataTable RawTable;
            public int[] ColNums;
            public int[] RowNums;
            public int FirstRow;

            public enum MatchType
            {
                EXACT,
                SUBSTRING
            }
        }

        protected String passageWayArticleName = "DURCHFAHRT";//"Durchfahrt";

        protected String[] reservedArticleNames = { "Pfeiler", 
                                                    "Dummy 1", 
                                                    "Durchfahrt", 
                                                    "I-Punkt", 
                                                    "Leerpaletten", 
                                                    "Schaltschrank", 
                                                    "Übergabezone", 
                                                    "Komm.- Wagen", 
                                                    "RUF Paletten", 
                                                    "Feuerlöscher" 
                                                  };

        protected String[] outputCostColNames = { "Gesamtkosten_Optimal", "Gesamtkosten_Start","Ersparnis", "Verbesserung in %" };
        protected String[] outputSolutionColNames = { "Produkt", "Weight","Anzahl Picks","Line_Optimal", "Column_Optimal", "Level_Optimal","Picking_Sequence_Optimal", "Line_Initial", "Column_Initial", "Level_Inital","Picking_Sequence_Initial","Distanz" };
        protected String[] outputConsListCostColNames = { "NVE Versandpackstück" ,"Kosten_Optimal","Kosten_Start","Ersparnis"};

        protected string locationsFileName = "160412-2FESTPLAT.xlsx";//"160129FESTPLAT.xlsx";
        protected string locationsSheetName = "FESTPLAT";
        protected string[] locationsColNames = { "Pot Kommplatz", "Article No.", "Art. description", "Fixed location", "Picking sequence", "Number of pallet locations" };
        protected XLSTable.MatchType[] locationsColMatchType = { XLSTable.MatchType.EXACT,
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT
                                                               };

        protected string articleDataFileName = "ART_02_Versandstufe_RUF_LEBENS_2016-01-29_1040.xlsx";//"160129FESTPLAT.xlsx";
        protected string articleDataSheetName = "REP_ART_02_Versandstufe_RUF_LEB";
        protected string[] articleDataColNames = { "Article number", 
                                                   "Quantity unit of this shipping level", 
                                                   "Shipping level qty. per pallet", 
                                                   "Length in mm",
                                                   "Width in mm",
                                                   "Height in mm",
                                                   "Net weight in kg",
                                                   "Gross weight in kg"
                                                 };
        protected XLSTable.MatchType[] articleDataColMatchType = {
                                                                 XLSTable.MatchType.EXACT,
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT
                                                               };

        protected string distancesSheetName = "Entfernungsmatrix";
        protected string[] distancesColNames = { "A) Zuordnungsmatrix für Zeilen zu Gassen",
                                                 "Modulmaß von Gasse zu Gasse in [m]:", 
                                                 "Entfernungsmaß von Platz zu Platz in [m] bei Euro-Pal. längs:"
                                               };
        protected XLSTable.MatchType[] distancesColMatchType = { XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT, 
                                                                 XLSTable.MatchType.EXACT
                                                               };

        protected string consignationsFileName = "160204 WALISTE957.csv";
        protected string consignationsSheetName = "160204 WALISTE957";
        protected string[] consignationsColNames = { "Artikelnummer", 
                                                       "Artikelbezeichung",
                                                       "Artikelbezeichnung 2",
                                                     "NVE Versandpackstück",
                                                     "Lagerort"
                                                   };
        
        protected XLSTable.MatchType[] consignationsColMatchType = { XLSTable.MatchType.EXACT, 
                                                                     XLSTable.MatchType.EXACT,
                                                                     XLSTable.MatchType.EXACT, 
                                                                     XLSTable.MatchType.EXACT,
                                                                     XLSTable.MatchType.EXACT
                                                                   };

        protected Dictionary<String, MacroLocationProblemData> problemsByMacroLocation;

        protected Dictionary<String, Product> consProductNameMapping;

        protected String usedPath;

        protected double interLaneDistanceMeasure = 0.0;

        protected double interColumnDistanceMeasure = 0.0;

        protected CultureInfo usedCulture;

        protected double usedTransportCost=1.0;

        public Int32 RNGSeed
        {
            set { usedSeed = value; }
            get { return usedSeed; }
        }

        protected XLSTable loadTableFromExcelDocument(Optimizer_Data.Excel_Document xlsDoc,
                                                       string sheetName,
                                                       string[] colNames,
                                                       XLSTable.MatchType[] colMatchTypes
                                                       )
        {
            string uLC, lRC;
            XLSTable resultXTab;
            int i, j, k;
            string currCellStr;

            uLC = "A1";
            lRC = "ZZ1000000";

            xlsDoc.setSheetData(sheetName, uLC, lRC);

            resultXTab = new XLSTable();
            resultXTab.ColNums = new int[colNames.Length];
            resultXTab.RowNums = new int[colNames.Length];

            resultXTab.RawTable = xlsDoc.getData();

            resultXTab.FirstRow = 0;
            i = j = 0;
            for (k = 0; k < colNames.Length; k++)
            {
                for (i = 0; i < resultXTab.RawTable.Rows.Count; i++)
                {
                    for (j = 0; j < resultXTab.RawTable.Rows[i].ItemArray.Length; j++)
                    {
                        currCellStr = resultXTab.RawTable.Rows[i].ItemArray[j].ToString().Trim();

                        if (
                            (colNames[k] == "*" && currCellStr != "") ||
                            (colMatchTypes[k] == XLSTable.MatchType.EXACT && currCellStr == colNames[k]) ||
                            (colMatchTypes[k] == XLSTable.MatchType.SUBSTRING && currCellStr.Contains(colNames[k]))
                            )
                        {
                            resultXTab.ColNums[k] = j;
                            resultXTab.RowNums[k] = i;
                            if (resultXTab.FirstRow < i)
                                resultXTab.FirstRow = i;

                            break;
                        }
                    }
                    if (j < resultXTab.RawTable.Columns.Count)
                        break;
                }
                if (i == resultXTab.RawTable.Rows.Count ||
                    j == resultXTab.RawTable.Columns.Count)
                {
                    resultXTab.ColNums[k] = -1;
                    resultXTab.RowNums[k] = -1;
                }
            }

            return resultXTab;
        }

        public DataProvider_Excel(CultureInfo currC, LoggerCollection lC)
        {
            problemsByMacroLocation = new Dictionary<string, MacroLocationProblemData>();
            consProductNameMapping = new Dictionary<string, Product>();
            usedRNG = new Random(usedSeed);
            //passageWaysColNames = new HashSet<string>();
            usedCulture = currC;
            loggers = lC;
        }

        public DataProvider_Excel(String path, CultureInfo currC, LoggerCollection lC)
            : this(currC,lC)
        {
            usedPath = path;
        }

        public String UsedPath
        {
            get { return usedPath; }
            set { usedPath = value; }
        }

        public Double UsedTransportCost
        {
            get { return usedTransportCost; }
            set { usedTransportCost = value; }
        }

        protected void loadArticleData()
        {
            XLSTable usedArticleDataTable;
            Optimizer_Data.Excel_Document currDoc;
            String msg;
            Int32 i;
            msg = "Lade Produkteigenschaften";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            currDoc = new Optimizer_Data.Excel_Document(usedPath + Path.DirectorySeparatorChar + articleDataFileName);
            currDoc.OpenDoc();
            usedArticleDataTable = loadTableFromExcelDocument(currDoc, articleDataSheetName, articleDataColNames, articleDataColMatchType);
            currDoc.CloseDoc();
            /*
              protected string[] articleDataColNames = { "Article number", 
                                                   "Quantity unit of this shipping level ", 
                                                   "Shipping level qty. per pallet", 
                                                   "Length in mm",
                                                   "Width in mm",
                                                   "Height in mm",
                                                   "Net weight in kg",
                                                   "Gross weight in kg"
                                                 };
             * */

            for (i = usedArticleDataTable.RowNums[0] + 1; i < usedArticleDataTable.RawTable.Rows.Count; i++)
            {
                String currProductName=usedArticleDataTable .RawTable .Rows [i][usedArticleDataTable .ColNums [0]].ToString ().Replace ("'","").Trim ();
                if (currProductName == "024")
                    Console.WriteLine("P24");
                Product currProduct;
                foreach (KeyValuePair<String, MacroLocationProblemData> currDataKVP in problemsByMacroLocation)
                {

                    //if (consProductNameMapping.ContainsKey (currProductName ))//(currDataKVP .Value //.AssignedProducts.ContainsKey (currProductName ) )
                    //{
                        if (currDataKVP.Value.ConsignedProducts.ContainsKey(currProductName))
                        {
                            currProduct = currDataKVP.Value.ConsignedProducts[currProductName];//AssignedProducts [currProductName ];
                            if (currProduct.QuantityUnit != "KT")
                            {
                                currProduct.QuantityUnit = usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[1]].ToString().Trim();
                                currProduct.ShippingLevelQuantityPerPallet = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[2]].ToString(), usedCulture);
                                currProduct.Length = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[3]].ToString(), usedCulture);
                                currProduct.Width = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[4]].ToString(), usedCulture);
                                currProduct.Height = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[5]].ToString(), usedCulture);
                                currProduct.NetWeight = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[6]].ToString(), usedCulture);
                                currProduct.GrossWeight = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[7]].ToString(), usedCulture);
                            }
                        }

                        if (currDataKVP.Value.AssignedProducts.ContainsKey(currProductName))
                        {
                            currProduct = currDataKVP.Value.AssignedProducts[currProductName];//AssignedProducts [currProductName ];
                            if (currProduct.QuantityUnit != "KT")
                            {
                                currProduct.QuantityUnit = usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[1]].ToString().Trim();
                                currProduct.ShippingLevelQuantityPerPallet = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[2]].ToString(), usedCulture);
                                currProduct.Length = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[3]].ToString(), usedCulture);
                                currProduct.Width = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[4]].ToString(), usedCulture);
                                currProduct.Height = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[5]].ToString(), usedCulture);
                                currProduct.NetWeight = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[6]].ToString(), usedCulture);
                                currProduct.GrossWeight = Convert.ToDouble(usedArticleDataTable.RawTable.Rows[i][usedArticleDataTable.ColNums[7]].ToString(), usedCulture);
                            }
                        }
                    //}
                    
                }
            }

            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        protected void loadLocationDistanceData()
        {
            Int32 i,j,k;
            Optimizer_Data.Excel_Document currDoc;
            XLSTable usedLocationsTab;
            XLSTable usedDistanceTab;
            Location currLocation;
            Product currProduct;
            String currProdDesc;
            MacroLocationProblemData currMProb;
            String currLocId;
            String currCommIndicator;
            String currProdId, currMacroLocId, currLocLineStr, currLocColumnStr, currLocLevelStr, currLocPickingSequence,currNumberPalletLocations;
            Int32 currLocLineInt, currLocColumnInt, currLocLevelInt;
            bool isReservedLoc;
           
            Int32 lineColNum, laneColNum, firstRow,currRow;
            bool isRead;
            String vStr;
            Int32 currLine, currLane,currColumn;
            String currLineStr, currLaneStr;
            List<Location> currLaneLocations;
            List<Int32> pWCols;
            Int32 lastCol;
            PassageRoute currPRoute;
            String msg;
            lineLanes = new Dictionary<Int32, Int32>();

            msg = "Lade Kommissionierplätze, bestehende Zuordnungen von Produkten zu Kommissionierplätzen und Distanzbasisdaten...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            currDoc = new Optimizer_Data.Excel_Document(usedPath + Path.DirectorySeparatorChar + locationsFileName);

            currDoc.OpenDoc();
            usedLocationsTab = loadTableFromExcelDocument(currDoc, locationsSheetName, locationsColNames, locationsColMatchType);
            usedDistanceTab = loadTableFromExcelDocument(currDoc, distancesSheetName, distancesColNames, distancesColMatchType);
            currDoc.CloseDoc();


            //if (usedDistanceTab.ColNums[0] != -1)
            //{
                lineColNum = usedDistanceTab.ColNums[0] + 1;
                laneColNum = usedDistanceTab.ColNums[0] + 2;
                firstRow = usedDistanceTab.RowNums[0] + 3;
                currRow = firstRow;
                isRead = false;
                do
                {
                    currLineStr = usedDistanceTab.RawTable.Rows[currRow][lineColNum].ToString().Trim();
                    currLaneStr = usedDistanceTab.RawTable.Rows[currRow][laneColNum].ToString().Trim();

                    if (currLineStr != "" && currLaneStr != "")
                    {
                        currLine = Convert.ToInt32(currLineStr, usedCulture);
                        currLane = Convert.ToInt32(currLaneStr, usedCulture);

                        if (!lineLanes.ContainsKey(currLine))
                        {
                            lineLanes.Add(currLine, currLane);
                            
                        }
                    }
                    else
                        isRead = true;
                    currRow++;
                }
                while (!isRead);
            //}
            if (usedDistanceTab.ColNums[1] != -1)
            {
                vStr = usedDistanceTab.RawTable.Rows[usedDistanceTab.RowNums[1]][usedDistanceTab.ColNums[1]+11].ToString().Trim();
                if (vStr != "")
                    interLaneDistanceMeasure = Convert.ToDouble(vStr, usedCulture);
            }

            if (usedDistanceTab.ColNums[2] != -1)
            {
                vStr = usedDistanceTab.RawTable.Rows[usedDistanceTab.RowNums[2]][usedDistanceTab.ColNums[2]+11].ToString().Trim();
                if (vStr != "")
                    interColumnDistanceMeasure = Convert.ToDouble(vStr, usedCulture);
            }

            for (i = usedLocationsTab.RowNums[0] + 1; i < usedLocationsTab.RawTable.Rows.Count; i++)
            {
                currCommIndicator = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[0]].ToString().Trim();
                
                currProdId = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[1]].ToString().Trim();
                currProdId = currProdId.Replace("'", "").Trim();
                
                currProdDesc = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[2]].ToString().Trim();

                currMacroLocId = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[3]].ToString().Trim();
                currMacroLocId = currMacroLocId.Replace("'", "").Trim(); ;

                currLocLineStr = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[3] + 1].ToString().Trim();
                currLocLineStr = currLocLineStr.Replace("'", "").Trim();
               
                currLocColumnStr = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[3] + 2].ToString().Trim();
                currLocColumnStr = currLocColumnStr.Replace("'", "").Trim();
                
                currLocLevelStr = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[3] + 3].ToString().Trim();
                currLocLevelStr = currLocLevelStr.Replace("'", "").Trim();
                
                currLocPickingSequence = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[4]].ToString().Trim();
                currLocPickingSequence = currLocPickingSequence.Replace("'", "").Trim();

                currNumberPalletLocations = usedLocationsTab.RawTable.Rows[i].ItemArray[usedLocationsTab.ColNums[5]].ToString().Trim();
                currNumberPalletLocations = currNumberPalletLocations.Replace("'", "").Trim();
                /*
                isReservedLoc = false;

                for (j = 0; j < reservedArticleNames.Length; j++)
                {
                    if (currProdDesc.Contains(reservedArticleNames[i]))
                    {
                        isReservedLoc = true;
                        break;
                    }
                }*/

                /*
                if (currProdDesc.Contains(passageWayArticleName))
                {
                    if (!passageWaysColNames.Contains(currLocColumn))
                        passageWaysColNames.Add(currLocColumn);
                }
                 */

                //Verfügbare Kommissionierplätze mit Indikator !="n"
                if (currCommIndicator!="n")//(!isReservedLoc)
                {
                    
                    if (currMacroLocId != "" && 
                        currLocLineStr != "" && 
                        currLocColumnStr != "" && 
                        currLocLevelStr != "" && 
                        currLocPickingSequence != "" && 
                        currNumberPalletLocations != ""
                        )
                    {
                        if (!problemsByMacroLocation.ContainsKey(currMacroLocId))
                        {
                            currMProb = new MacroLocationProblemData();
                            currMProb.MacroLocationId = currMacroLocId;
                            problemsByMacroLocation.Add(currMacroLocId, currMProb);
                        }
                        else
                            currMProb = problemsByMacroLocation[currMacroLocId];
                        
                        currLocLineInt = Convert.ToInt32(currLocLineStr, CultureInfo.InvariantCulture);
                        currLocColumnInt = Convert.ToInt32(currLocColumnStr, CultureInfo.InvariantCulture);
                        currLocLevelInt = Convert.ToInt32(currLocLevelStr, CultureInfo.InvariantCulture);

                        currLocId = Convert.ToString(currLocLineInt,CultureInfo.InvariantCulture) + KeyElementSeparator 
                            + Convert.ToString(currLocColumnInt,CultureInfo.InvariantCulture) + KeyElementSeparator 
                            + Convert.ToString(currLocLevelInt,CultureInfo.InvariantCulture);

                        if (!currMProb.Locations.ContainsKey(currLocId))
                        {
                            currLocation = new Location();
                            currLocation.Line = currLocLineInt;//Convert.ToInt32(currLocLine, CultureInfo.InvariantCulture);
                            currLocation.Column = currLocColumnInt;//Convert.ToInt32(currLocColumn, CultureInfo.InvariantCulture);

                            if (!currMProb.relevantColumns.Contains(currLocation.Column))
                                currMProb.relevantColumns.Add(currLocation.Column);

                            currLocation.Level = currLocLevelInt;//Convert.ToInt32(currLocLevel, CultureInfo.InvariantCulture);
                            currLocation.PickingSequence = Convert.ToInt32(currLocPickingSequence, CultureInfo.InvariantCulture);

                            currMProb.Locations.Add(currLocId, currLocation);

                            currMProb.LocList.Add(currLocation);
                            currLane = lineLanes[currLocation.Line];
                            if (!currMProb.locationsByLane.ContainsKey(currLane))
                            {
                                currLaneLocations = new List<Location>();
                                currMProb.locationsByLane.Add(currLane, currLaneLocations);
                            }
                            else
                                currLaneLocations = currMProb.locationsByLane[currLane];

                            currLaneLocations.Add(currLocation);
                        }
                        else
                            currLocation = currMProb.Locations[currLocId];

                        if (!currMProb.AssignedProducts.ContainsKey(currProdId))
                        {
                            currProduct = new Product();
                            currProduct.Id = currProdId;
                            currProduct.Description = currProdDesc;
                            currMProb.AssignedProducts.Add(currProdId, currProduct);
                        }
                        else
                        {
                            currProduct = currMProb.AssignedProducts[currProdId];
                           //Console.WriteLine ("Duplikat-Produkt:"+currProdId);
                        }

                        HashSet<String> currLocIds;
                        if (!currMProb.AssignedLocations.ContainsKey(currProdId))
                        {
                            currLocIds = new HashSet<string>();
                            currMProb.AssignedLocations.Add(currProdId, currLocIds);
                        }
                        else
                            currLocIds = currMProb.AssignedLocations[currProdId];

                        if (!currLocIds.Contains(currLocation.Id))
                            currLocIds.Add(currLocation.Id);
                        
                        //currMProb.AssignedLocations.Add(currProdId, currLocation);
                        
                        /*
                        if (!currMProb.AssignedProducts.ContainsKey(currLocId))
                            currMProb.AssignedProducts.Add(currLocId, currProduct);
                        else
                            Console.WriteLine("Artikel:" + currProdId + " Standort:" + currLocId);
                         * */
                    }
                }
                else
                {
                    if (currProdId.Contains (passageWayArticleName))//(currProdDesc.Contains(passageWayArticleName))
                    {
                        if (!problemsByMacroLocation.ContainsKey(currMacroLocId))
                        {
                            currMProb = new MacroLocationProblemData();
                            currMProb.MacroLocationId = currMacroLocId;
                            problemsByMacroLocation.Add(currMacroLocId, currMProb);
                        }
                        else
                            currMProb = problemsByMacroLocation[currMacroLocId];

                        currColumn=Convert.ToInt32 (currLocColumnStr,usedCulture);
                        //if (!currMProb.passageWaysColNames.Contains(currLocColumn))
                          //  passageWaysColNames.Add(currLocColumn);
                        if (!currMProb.passageRouteColNums.Contains(currColumn))
                            currMProb.passageRouteColNums.Add(currColumn);
                    }
                }
            }
           
            currLocId = Convert.ToString(startPointLine, CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(startPointColumn,CultureInfo .InvariantCulture) + KeyElementSeparator + "1";

            foreach (KeyValuePair<String, MacroLocationProblemData> currMProbKVP in problemsByMacroLocation)
            {
                //currLocId = currLocLine + KeyElementSeparator + currLocColumn + KeyElementSeparator + currLocLevel;
                if (!currMProbKVP.Value.Locations.ContainsKey(currLocId))
                {
                    currLocation = new Location();
                    currLocation.Line = Convert.ToInt32(startPointLine, CultureInfo.InvariantCulture);
                    currLocation.Column = Convert.ToInt32(startPointColumn, CultureInfo.InvariantCulture);

                    if (!currMProbKVP.Value.relevantColumns.Contains(currLocation.Column))
                        currMProbKVP.Value.relevantColumns.Add(currLocation.Column);

                    currLocation.Level = 1;// Convert.ToInt32(1, CultureInfo.InvariantCulture);
                    currLocation.PickingSequence = -1;//Convert.ToInt32(currLocPickingSequence, CultureInfo.InvariantCulture);

                    currMProbKVP.Value.Locations.Add(currLocId, currLocation);

                    currMProbKVP.Value.LocList.Add(currLocation);

                    currLane = lineLanes[currLocation.Line];
                    if (!currMProbKVP.Value.locationsByLane.ContainsKey(currLane))
                    {
                        currLaneLocations = new List<Location>();
                        currMProbKVP.Value.locationsByLane.Add(currLane, currLaneLocations);
                    }
                    else
                        currLaneLocations = currMProbKVP.Value.locationsByLane[currLane];

                    currLaneLocations.Add(currLocation);
                }
            }

            foreach (KeyValuePair<String, MacroLocationProblemData> currMProbKVP in problemsByMacroLocation)
            {
                pWCols=currMProbKVP.Value.passageRouteColNums.ToList();
                pWCols.Sort();
                lastCol = -1;
                currPRoute = null;
                foreach (Int32 currCol in pWCols)
                {
                    if (lastCol == -1)
                    {
                        lastCol = currCol;
                        currPRoute = new PassageRoute();
                        currPRoute.StartColNum = currCol;
                        currPRoute.EndColNum = currCol;
                    }
                    else
                    {
                        if (currCol - currPRoute.EndColNum > 1)
                        {
                            currMProbKVP.Value.pRoutes.Add(currPRoute);

                            currPRoute = new PassageRoute();
                            currPRoute.StartColNum = currCol;
                        }
                        currPRoute.EndColNum = currCol;
                    }
                }
                if (currPRoute !=null)
                    currMProbKVP.Value.pRoutes.Add(currPRoute);
            }
            
            //for (KeyValuePair<String,MacroLocationProblemData> currMProbKVP in problemsByMacroLocation)
            //{
                /*
            foreach (KeyValuePair<Int32, List<Location>> laneLocationsKVP in locationsByLane)
            {
                laneLocationsKVP.Value.Sort(Location.compareByColumn);
            }
                 * */
            //}
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            msg = "Berechne Distanzen...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            Int32 numberOfRelevantPoints;
            //Int32 lastCol;

            List<Point> pointList=new List<Point> ();
            foreach (KeyValuePair<String, MacroLocationProblemData> currMPDKV in problemsByMacroLocation)
            {
                pointList.Clear();
                //numberOfRelevantPoints = 0;
                //foreach (KeyValuePair<Int32, List<Location>> laneLocationsKVP in currMPDKV.Value.locationsByLane)//locationsByLane)
                //{
                //    laneLocationsKVP.Value.Sort(Location.compareByColumn);
                //    lastCol = -1;
                    
                //    foreach (Location currLoc in laneLocationsKVP.Value)
                //    {
                //        if (currLoc.Column != lastCol)
                //        {
                //            numberOfRelevantPoints++;
                //        }
                //        lastCol = currLoc.Column;
                //    }
                //    //numberOfRelevantPoints += laneLocationsKVP.Value.Count;
                //    numberOfRelevantPoints += currMPDKV.Value.pRoutes.Count * 2;
                //}

               // currMPDKV.Value.pArr = new Point[numberOfRelevantPoints];
                
                Point currPoint;
                i=0;
                foreach (KeyValuePair<Int32, List<Location>> laneLocationsKVP in currMPDKV.Value.locationsByLane)//locationsByLane)
                {
                    laneLocationsKVP.Value.Sort(Location.compareByColumn);
                    if (laneLocationsKVP.Key == 5)
                    {
                        i += 0;
                    }
                    lastCol = -1;
                    foreach (Location currLoc in laneLocationsKVP.Value)
                    {
                        if (currLoc.Column != lastCol)
                        {
                            currPoint = new Point();
                            currPoint.Lane = laneLocationsKVP.Key;
                            currPoint.Column = currLoc.Column;
                            if (currPoint.Column ==105 && 
                                currPoint .Lane ==5
                                )
                                i+=0;
                            currMPDKV.Value.pointNums.Add(currPoint.Key, i);
                            foreach (PassageRoute currPassageRoute in currMPDKV.Value.pRoutes)
                            {
                                if (currPoint.Column == currPassageRoute.StartColNum || 
                                    currPoint.Column == currPassageRoute.EndColNum
                                    )
                                {
                                    currPoint.PRoute = currPassageRoute;
                                    break;
                                }
                            }
                            pointList.Add(currPoint);
                            /*
                            currMPDKV.Value.pArr[i] = new Point();
                            currMPDKV.Value.pArr[i].Lane = laneLocationsKVP.Key;
                            currMPDKV.Value.pArr[i].Column = currLoc.Column;
                            currMPDKV .Value .pArr [i].PRoute =null;
                            currMPDKV.Value.pointNums.Add(currMPDKV.Value.pArr[i].Key, i);
                             */
                            i++;
                        }
                        lastCol = currLoc.Column;
                    }
                    foreach (PassageRoute currPassageRoute in currMPDKV.Value.pRoutes)
                    {
                        currPoint = new Point();
                        currPoint.Lane = laneLocationsKVP.Key;
                        currPoint.Column = currPassageRoute.StartColNum;
                        currPoint.PRoute = currPassageRoute;
                        if (laneLocationsKVP.Key == 5)
                            i += 0;
                        if (!currMPDKV.Value.pointNums.ContainsKey(currPoint.Key))
                        {
                            currMPDKV.Value.pointNums.Add(currPoint.Key, i);
                            /*currMPDKV.Value.pArr[i] = new Point();
                            currMPDKV.Value.pArr[i].Column = currPassageRoute.StartColNum;
                            currMPDKV.Value.pArr[i].Lane = laneLocationsKVP.Key;
                            currMPDKV .Value.pArr [i].PRoute =currPassageRoute ;
                            currMPDKV.Value.pointNums.Add(currMPDKV.Value.pArr[i].Key, i);
                             * */
                            pointList.Add(currPoint);
                            i++;
                        }
                       
                        /*
                        currMPDKV.Value.pArr[i] = new Point();
                        currMPDKV.Value.pArr[i].Column = currPassageRoute.EndColNum;
                        currMPDKV.Value.pArr[i].Lane = laneLocationsKVP.Key;
                        currMPDKV .Value .pArr [i].PRoute =currPassageRoute;
                        currMPDKV.Value.pointNums.Add(currMPDKV.Value.pArr[i].Key, i);
                         * */
                        currPoint = new Point();
                        currPoint.Lane = laneLocationsKVP.Key;
                        currPoint.Column = currPassageRoute.EndColNum;
                        currPoint.PRoute = currPassageRoute;

                        if (!currMPDKV.Value.pointNums.ContainsKey(currPoint.Key))
                        {
                            currMPDKV.Value.pointNums.Add(currPoint.Key, i);
                            pointList.Add(currPoint);
                            i++;
                        }
                    }
                }

                currMPDKV.Value.pArr = pointList.ToArray();
                currMPDKV.Value.distArr = new double[currMPDKV.Value.pArr.Length, currMPDKV.Value.pArr.Length];

                for (i = 0; i < currMPDKV.Value.pArr.Length ; i++)
                {
                    for (j = 0; j < currMPDKV.Value.pArr.Length; j++)
                        currMPDKV.Value.distArr[i, j] = Double.MaxValue;
                }
                for (i = 0; i < currMPDKV.Value.pArr.Length; i++)
                {
                    for (j = 0; j < currMPDKV.Value.pArr.Length; j++)
                    {
                        if (i == 70 && j == 115)
                            i += 0;

                        if (currMPDKV.Value.pArr[i].Lane == currMPDKV.Value.pArr[j].Lane)
                        {
                            currMPDKV.Value.distArr[i, j] = Math.Abs(currMPDKV.Value.pArr[i].Column - currMPDKV.Value.pArr[j].Column) * interColumnDistanceMeasure;
                            currMPDKV.Value.distArr[j, i] = currMPDKV.Value.distArr[i, j];
                        }
                        else if (
                                 currMPDKV.Value.pArr[i].PRoute!=null && 
                                 currMPDKV.Value.pArr[j].PRoute!=null &&
                                 currMPDKV.Value.pArr[i].PRoute == currMPDKV.Value.pArr[j].PRoute && 
                                 currMPDKV.Value.pArr[i].Column ==currMPDKV.Value.pArr[j].Column
                            )
                        {
                            currMPDKV.Value.distArr[i, j] = Math.Abs(currMPDKV.Value.pArr[i].Lane - currMPDKV.Value.pArr[j].Lane) * interLaneDistanceMeasure;
                            currMPDKV.Value.distArr[j, i] = currMPDKV.Value.distArr[i, j];
                        }
                    }
                }
                double newDistVal;
                for (k = 0; k < currMPDKV.Value.pArr.Length; k++)  
                {
                    for (i = 0; i < currMPDKV.Value.pArr.Length; i++)
                    {
                        //if (currMPDKV.Value.pArr[i].Lane < currMPDKV.Value.pArr[j].Lane)
                        //{
                        for (j = 0; j < currMPDKV.Value.pArr.Length; j++)
                            {
                                if (currMPDKV.Value.distArr[i, k] < Double.MaxValue && currMPDKV.Value.distArr[k, j] < Double.MaxValue)
                                {
                                    newDistVal = currMPDKV.Value.distArr[i, k] + currMPDKV.Value.distArr[k, j];
                                    if (currMPDKV.Value.distArr[i, j] > newDistVal)
                                    {
                                        currMPDKV.Value.distArr[i, j] = newDistVal;
                                    }
                                }
                            }
                        //}
                    }
                }

                currMPDKV.Value.LocList.Sort(Location.compareByPickingSequence);
                i = 0;
                Location firstLoc;
                foreach (Location l in currMPDKV.Value.LocList)
                {
                    l.orderNumber = i;
                    i++;
                }
                
            }
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            //Dictionary<String, double> interLineDistances;

            //interLineDistances = new Dictionary<string, double>();
            //Int32 sLineColNum;
            //Int32 dLineFirstColNum, dLineLastColNum;
            //Int32 linesTitleRowNum,interlineFirstRowNum;
            //String lineName;

            //sLineColNum = usedDistanceTab.ColNums[1] + 1;
            //dLineFirstColNum = usedDistanceTab.ColNums[1] + 2;
            //linesTitleRowNum = usedDistanceTab.RowNums[1] + 5;
            //interlineFirstRowNum = usedDistanceTab.RowNums[1] + 6;

            //dLineLastColNum = dLineFirstColNum;
            //isRead =false;
            //do
            //{
            //    lineName = usedDistanceTab.RawTable.Rows[linesTitleRowNum][i].ToString().Trim();
            //    if (lineName != "")
            //    {
            //        dLineLastColNum++;
            //    }
            //    else
            //        isRead = true;
            //}
            //while (!isRead);

            
        }

        protected void loadConsignationListData()
        {
            Optimizer_Data.Excel_Document currDoc;
            XLSTable usedTab;
            String currProdId, currListId, currMacroLocId, currProdDescr1,currProdDescr2 ;
            ConsignationProblem.ConsignationList currConsList;
            ConsignationProblem.ConsignationList[] relevantConsLists;
            List<ConsignationProblem.ConsignationList> consListsToBeRemoved;
            MacroLocationProblemData currMProb;
            int i,j;
            String msg;

            msg = "Lade Kommissionierlisten...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            consListsToBeRemoved = new List<ConsignationProblem.ConsignationList>();
            
            currDoc = new Optimizer_Data.Excel_Document(usedPath + Path.DirectorySeparatorChar + consignationsFileName);

            currDoc.OpenDoc();
            usedTab = loadTableFromExcelDocument(currDoc, consignationsSheetName, consignationsColNames, consignationsColMatchType);
            currDoc.CloseDoc();

            for (i = usedTab.RowNums[0] + 1; i < usedTab.RawTable.Rows.Count; i++)
            {
                currProdId = usedTab.RawTable.Rows[i].ItemArray[usedTab.ColNums[0]].ToString().Trim();
                currProdId = currProdId.Replace("'", "").Trim();
                currProdDescr1 = usedTab.RawTable.Rows[i].ItemArray[usedTab.ColNums[1]].ToString().Trim();
                currProdDescr1 = currProdDescr1.Replace("'", "").Trim();
                currProdDescr2 = usedTab.RawTable.Rows[i].ItemArray[usedTab.ColNums[2]].ToString().Trim();
                currProdDescr2 = currProdDescr2.Replace("'", "").Trim();
                currListId = usedTab.RawTable.Rows[i].ItemArray[usedTab.ColNums[3]].ToString().Trim();
                currListId = currListId.Replace("'", "").Trim();
                currMacroLocId = usedTab.RawTable.Rows[i].ItemArray[usedTab.ColNums[4]].ToString().Trim();
                currMacroLocId = currMacroLocId.Replace("'", "").Trim();
               
                if (currProdId != "" && currListId != "" && currMacroLocId!="")
                {
                    if (problemsByMacroLocation.ContainsKey(currMacroLocId))
                    {
                        currMProb = problemsByMacroLocation[currMacroLocId];

                        if (!currMProb.ConsignedProducts.ContainsKey(currProdId))
                        {
                            Product currProduct;
                            currProduct = new Product();
                            currProduct.Id = currProdId;
                            currProduct.Description = currProdDescr1 + currProdDescr2;
                            currMProb.ConsignedProducts.Add(currProduct.Id, currProduct);
                        }

                        if (currMProb.AssignedProducts.ContainsKey(currProdId))
                        {
                            if (!currMProb.ConsignationLists.ContainsKey(currListId))
                            {
                                currConsList = new ConsignationProblem.ConsignationList();
                                currConsList.Id = currListId;
                                currConsList.Frequency = 1;
                                currMProb.ConsignationLists.Add(currListId, currConsList);
                            }
                            else
                                currConsList = currMProb.ConsignationLists[currListId];

                            if (!currConsList.Products.Contains(currProdId))
                                currConsList.Products.Add(currProdId);
                        }
                        else
                        {
                            if (!currMProb.UnAssignedProductsFromConsignationLists.Contains(currProdId))
                                currMProb.UnAssignedProductsFromConsignationLists.Add(currProdId);
                        }
                    }
                }
            }


            foreach (KeyValuePair<String, MacroLocationProblemData> currMProbKVP in problemsByMacroLocation)
            {
                //Zusammenfassen identischer Packlisten
                relevantConsLists = new ConsignationProblem.ConsignationList[currMProbKVP.Value.ConsignationLists.Count];

                i = 0;
                foreach (KeyValuePair<string, ConsignationProblem.ConsignationList> currConsListKVP in currMProbKVP.Value.ConsignationLists)
                {
                    relevantConsLists[i] = currConsListKVP.Value;
                    i++;
                }
                HashSet<String> currAssociatedLists;
                consListsToBeRemoved.Clear();
                for (i = 0; i < currMProbKVP.Value.ConsignationLists.Count - 1; i++)
                {
                    if (relevantConsLists[i] != null)
                    {
                        for (j = i + 1; j < currMProbKVP.Value.ConsignationLists.Count; j++)
                        {
                            if (relevantConsLists[j] != null)
                            {
                                if (ConsignationProblem.ConsignationList.compare(relevantConsLists[i], relevantConsLists[j]) == 0)
                                {
                                    relevantConsLists[i].Frequency += relevantConsLists[j].Frequency;
                                    consListsToBeRemoved.Add(relevantConsLists[j]);
                                    if (!currMProbKVP.Value.AssociatedLists.ContainsKey(relevantConsLists[i].Id))
                                    {
                                        currAssociatedLists = new HashSet<string>();
                                        currMProbKVP.Value.AssociatedLists.Add(relevantConsLists[i].Id, currAssociatedLists);
                                    }
                                    else
                                        currAssociatedLists = currMProbKVP.Value.AssociatedLists[relevantConsLists[i].Id];

                                    if (!currAssociatedLists.Contains(relevantConsLists[j].Id))
                                        currAssociatedLists.Add(relevantConsLists[j].Id);

                                    relevantConsLists[j] = null;
                                }
                            }
                        }
                    }
                }

                foreach (ConsignationProblem.ConsignationList currCLR in consListsToBeRemoved)
                {
                    currMProbKVP.Value.ConsignationLists.Remove(currCLR.Id);
                }

                foreach (KeyValuePair<String, Product> currProductKVP in currMProbKVP.Value.ConsignedProducts)
                {
                    //currNumberOfPics = 0;
                    currProductKVP.Value.numberOfPicks = 0;
                    foreach (KeyValuePair<String, ConsignationProblem.ConsignationList> lKVP in currMProbKVP.Value.ConsignationLists)
                    {
                        if (lKVP.Value.Products.Contains(currProductKVP.Key))
                            currProductKVP.Value.numberOfPicks += lKVP.Value.Frequency;
                    }
                }
            }
               
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }
       

        public void load()
        {
            loadLocationDistanceData();
            loadConsignationListData();
            loadArticleData();
        }

        protected String currentProblemId;

        public void setProblemToGet(String problemId,Int32 startingPointLine,Int32 startingPointCol)
        {
            currentProblemId = problemId;
           
            startPointLine = startingPointLine;
            startPointColumn = startingPointCol;
        }
        
        public List<String> getAllProblemIds()
        {
            List<String> pIds;
            pIds = new List<string>();

            foreach (KeyValuePair<String, MacroLocationProblemData> mPDKVP in problemsByMacroLocation)
            {
                pIds.Add(mPDKVP.Key);
            }

            return pIds;
        }

        

        public ConsignationProblem getConsignationProblem(bool isIgnoreNotConsignedProducts,Int32 minimumNumberOfPicks)
        {
            Int32 i;
            MacroLocationProblemData currentData;
            ConsignationProblem currentProblem;
            ConsignationProblem.ConsignationProduct currConsProduct;
            ConsignationProblem.ConsignationLocation currConsLocation;
            ConsignationProblem.ConsignationLocation[] consLocationsByOrderNumber;
            CSV_File distFile;
            String[] distRow = new String[9];
            DataTable distTab = new DataTable();
            distTab.Columns.Add("Start_Zeile");
            distTab.Columns.Add("Start_Säule");
            distTab.Columns.Add("Start_Level");
            distTab.Columns.Add("Start_PickingSequence");
            distTab.Columns.Add("End_Zeile");
            distTab.Columns.Add("End_Säule");
            distTab.Columns.Add("End_Level");
            distTab.Columns.Add("End_PickingSequence");
            distTab.Columns.Add("Distanz");
            currentData = problemsByMacroLocation[currentProblemId];
            consProductNameMapping.Clear();
            currentProblem = new ConsignationProblem();
            Location currAssignedLocation;
            Product cProduct;
            HashSet<String> currAssLocIds;
            //bool isMeetConsignationCriteria;

            foreach (KeyValuePair <String,Product> currProductKVP in currentData.AssignedProducts)
            {
                //currConsProduct = new ConsignationProblem.ConsignationProduct(currProductKVP.Key);
                //currentProblem.addProduct(currConsProduct);

                //isMeetConsignationCriteria = false;
                //if (currentData.ConsignedProducts.ContainsKey(currProductKVP.Key))
                //{
                  //  cProduct = currentData.ConsignedProducts[currProductKVP.Key];
                  //  if (cProduct.numberOfPicks >= minimumNumberOfPicks)
                    //    isMeetConsignationCriteria = true;
                //}
                //else
                  //  cProduct = null;
                //if (isMeetConsignationCriteria)//(currentData.ConsignedProducts.ContainsKey(currProductKVP.Key))
                //{
                if (currentData.ConsignedProducts.ContainsKey(currProductKVP.Key))
                {
                    cProduct = currentData.ConsignedProducts[currProductKVP.Key];
                    currConsProduct = new ConsignationProblem.ConsignationProduct(currProductKVP.Key);
                    
                    currentProblem.addProduct(currConsProduct);
                    //currentProblem.addProduct(currConsProduct);
                    if (!consProductNameMapping.ContainsKey(currProductKVP.Key))
                        consProductNameMapping.Add(currProductKVP.Key, cProduct);//currProductKVP.Value);
                }
                else if (!isIgnoreNotConsignedProducts)
                {
                    currAssLocIds = currentData.AssignedLocations[currProductKVP.Key];

                    foreach (String currLocId in currAssLocIds)
                    {
                        String usedKey;
                        usedKey = currProductKVP.Key + KeyElementSeparator + currLocId;
                        currConsProduct = new ConsignationProblem.ConsignationProduct(usedKey);
                        currentProblem.addProduct(currConsProduct);
                        if (!consProductNameMapping.ContainsKey(usedKey))
                            consProductNameMapping.Add(usedKey, currProductKVP.Value);
                    }
                    //currAssignedLocation =currentData.AssignedLocations [currProductKVP .Key];
                } 
            }

            consLocationsByOrderNumber = new ConsignationProblem.ConsignationLocation[currentData.LocList.Count];
            //currConsLocation = new ConsignationProblem.ConsignationLocation();
            //currConsLocation.Id = "ConsignationPoint";
            //currConsLocation.OrderNumber = 0;
            //currentProblem.addLocation(currConsLocation);
            //consLocationsByOrderNumber[0] = currConsLocation;

            foreach (Location currLocation in currentData.LocList)
            {
                currConsLocation = new ConsignationProblem.ConsignationLocation();
                currConsLocation.Id = currLocation.Id;
                currConsLocation.OrderNumber = currLocation.orderNumber;
                currentProblem.addLocation(currConsLocation);
                consLocationsByOrderNumber[currLocation.orderNumber] = currConsLocation;
            }
            
            foreach (KeyValuePair<String,ConsignationProblem.ConsignationList> cConsListKVP in currentData.ConsignationLists)
            {

                if (cConsListKVP.Value.Products.Contains ("1546"))
                    i=0;
                currentProblem.addConsignmentList(cConsListKVP.Value);
            }

            if (isSampleDistancesIfMissing && currentData.distArr ==null)
            {
                sampleDistances(currentData, currentProblem, consLocationsByOrderNumber);
            }
            else
            {  
                Int32 point1, point2;
                Location l1,l2;
                int j;
                ConsignationProblem.ConsignationDistance currDistance;
                for (i = 0; i < consLocationsByOrderNumber.Length - 1; i++)
                {
                    l1=currentData .Locations[consLocationsByOrderNumber[i].Id];
                    point1 = currentData.pointNums[Convert.ToString(lineLanes[l1.Line], CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(l1.Column, CultureInfo.InvariantCulture)];
                    for (j = i+1; j < consLocationsByOrderNumber.Length; j++)
                    {   
                        l2=currentData .Locations[consLocationsByOrderNumber[j].Id];
                        point2 = currentData.pointNums[Convert.ToString(lineLanes[l2.Line], CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(l2.Column, CultureInfo.InvariantCulture)];
                        currDistance = new ConsignationProblem.ConsignationDistance();
                        currDistance.L1 = consLocationsByOrderNumber[i];
                        currDistance.L2 = consLocationsByOrderNumber[j];
                        currDistance.Cost = currentData.distArr[point1,point2]*usedTransportCost;
                        currentProblem.addDistance(currDistance);
                        distRow[0] = Convert.ToString (l1.Line,usedCulture );
                        distRow[1] = Convert.ToString (l1.Column,usedCulture );
                        distRow[2] = Convert.ToString (l1.Level,usedCulture );
                        distRow[3] = Convert.ToString(l1.PickingSequence, usedCulture);
                        distRow[4] = Convert.ToString(l2.Line, usedCulture);
                        distRow[5] = Convert.ToString(l2.Column, usedCulture);
                        distRow[6] = Convert.ToString(l2.Level, usedCulture);
                        distRow[7] = Convert.ToString(l2.PickingSequence, usedCulture);
                        distRow[8] = Convert.ToString(currDistance.Cost, usedCulture);
                        distTab.Rows.Add(distRow);
                       
                        if (l1.orderNumber ==0 )
                        {
                            currDistance = new ConsignationProblem.ConsignationDistance();
                            currDistance.L1 = consLocationsByOrderNumber[j];
                            currDistance.L2 = consLocationsByOrderNumber[i];
                            currDistance.Cost = currentData.distArr[point2, point1];
                            currentProblem.addDistance(currDistance);
                        }
                    }
                }

                distFile = new CSV_File("distanzen_visualisierung.csv");
                distFile.openForWriting(false);
                distFile.IsHeadLine = true;
                distFile.CSVDelimiter = ";";
                distFile.writeData(distTab);
                distFile.closeDoc();
            }
            return currentProblem;
        }
        
        double getPathSimpleDistances(int l1, int l2, ConsignationProblem currentProblem, ConsignationProblem.ConsignationLocation[] consLocationsByOrderNumber)
        {
            int l;
            double currDistance;
            
            currDistance = 0;
            for (l = l1; l < l2; l++)
            {
                currDistance += currentProblem.getDistance(consLocationsByOrderNumber[l], consLocationsByOrderNumber[l + 1]).Cost;//distances[currKey];
            }

            return currDistance;
        }
        void sampleDistances(MacroLocationProblemData currentData, ConsignationProblem currentProblem, ConsignationProblem.ConsignationLocation[] consLocationsByOrderNumber)
        {
            int l, l1, l2;
            double currDistVal, currDistValConsPointReturn;
            double currDistanceFromConsPoint;
            ConsignationProblem.ConsignationDistance currDistance;
           
            for (l = 0; l < consLocationsByOrderNumber.Length-1; l++)
            {
                currDistVal = usedRNG.NextDouble() * (simpleDistanceMaxVal - simpleDistanceMinVal) + simpleDistanceMinVal;
                currDistance=new ConsignationProblem.ConsignationDistance();
                
                currDistance .L1 =consLocationsByOrderNumber[l];
                currDistance .L2=consLocationsByOrderNumber [l+1];
                currDistance .Cost =currDistVal;
                //currKey = Convert.ToString(l, CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(l + 1, CultureInfo.InvariantCulture);
                currentProblem.addDistance(currDistance);
            }


            for (l1 = 0; l1 < consLocationsByOrderNumber.Length - 2; l1++)
            {
                for (l2 = l1 + 2; l2 < consLocationsByOrderNumber.Length; l2++)
                {
                    currDistanceFromConsPoint = getPathSimpleDistances(l1, l2,currentProblem ,consLocationsByOrderNumber);
                    currDistVal = usedRNG.NextDouble() * (currDistanceFromConsPoint - minPctDistMultipleLocs * currDistanceFromConsPoint) + minPctDistMultipleLocs * currDistanceFromConsPoint;

                    currDistance = new ConsignationProblem.ConsignationDistance();

                    currDistance.L1 = consLocationsByOrderNumber[l1];
                    currDistance.L2 = consLocationsByOrderNumber[l2];
                    currDistance.Cost = currDistVal;
                    currentProblem.addDistance(currDistance);
                    if (l1 == 0)
                    {
                        currDistValConsPointReturn = usedRNG.NextDouble() * (currDistVal - minPctDistConspointReturn * currDistVal) + minPctDistConspointReturn * currDistVal;
                        currDistance = new ConsignationProblem.ConsignationDistance();

                        currDistance.L1 = consLocationsByOrderNumber[l2];
                        currDistance.L2 = consLocationsByOrderNumber[0];
                        currDistance.Cost = currDistVal;
                        currentProblem.addDistance(currDistance);
                    }
                    /*
                    currKey = toString<int>(l1) + "_" + toString<int>(l2);
                    distances.insert(pair<string, double>(currKey, currDistVal));

                    if (l1 == 0)
                    {
                        currDistValConsPointReturn = usedRNG.NextDouble() * (currDistVal - minPctDistConspointReturn * currDistVal) + minPctDistConspointReturn * currDistVal;

                        currKey = toString<int>(l2) + "_0";
                        distances.insert(pair<string, double>(currKey, currDistValConsPointReturn));
                    }
                     */
                }
                if (l1 == 0)
                {
                    currDistanceFromConsPoint = getPathSimpleDistances(0, 1, currentProblem, consLocationsByOrderNumber);
                    currDistValConsPointReturn = usedRNG.NextDouble() * (currDistanceFromConsPoint - minPctDistConspointReturn * currDistanceFromConsPoint) + minPctDistConspointReturn * currDistanceFromConsPoint;

                    currDistance = new ConsignationProblem.ConsignationDistance();

                    currDistance.L1 = consLocationsByOrderNumber[1];
                    currDistance.L2 = consLocationsByOrderNumber[0];
                    currDistance.Cost = currDistValConsPointReturn;
                    currentProblem.addDistance(currDistance);
                    /*
                    currDistanceFromConsPoint = getPathSimpleDistances(0, 1);
                    currDistValConsPointReturn = usedRNG.NextDouble() * (currDistanceFromConsPoint - minPctDistConspointReturn * currDistanceFromConsPoint) + minPctDistConspointReturn * currDistanceFromConsPoint;

                    currKey = "1_0";
                    distances.insert(pair<string, double>(currKey, currDistValConsPointReturn));
                     */
                }
            }
        }

        public ConsignationSolution getExistingSolution(ConsignationProblem prob, bool isIgnoreNotConsignedProducts,Int32 minimumNumberOfPicks)
        {
            ConsignationSolution currSolution;
            MacroLocationProblemData currentData;
            bool isMeetConsignationCriteria,isConsigned;
            currentData = problemsByMacroLocation[currentProblemId];
            currSolution = new ConsignationSolution();

            foreach (KeyValuePair<String, HashSet<String>> currAssignedLocationsKVP in currentData.AssignedLocations)
            {
                //HashSet<String> currLocIds;
                //currLocIds =

                //foreach (String currLocId in currAssignedLocationsKVP.Value)
                //{   
                  //  currSolution.addAssignment(prob.getProduct(currAssignedLocationsKVP.Key), prob.getLocation(currLocId));
    //                    break;
      //          }

                
                foreach (String currLocId in currAssignedLocationsKVP.Value)
                {
                    isMeetConsignationCriteria = false;
                    isConsigned =false;
                    if (currentData.ConsignedProducts.ContainsKey(currAssignedLocationsKVP.Key))
                    {
                        Product cProduct;
                        cProduct = currentData.ConsignedProducts[currAssignedLocationsKVP.Key];
                        if (cProduct.numberOfPicks >=  minimumNumberOfPicks)
                            isMeetConsignationCriteria = true;
                        isConsigned=true;
                    }
                    if (isMeetConsignationCriteria )//(currentData.ConsignedProducts.ContainsKey(currAssignedLocationsKVP.Key))
                    {
                        currSolution.addAssignment(prob.getProduct(currAssignedLocationsKVP.Key), prob.getLocation(currLocId),false);
                    }
                    else if (isConsigned)
                    {
                        currSolution.addAssignment(prob.getProduct(currAssignedLocationsKVP.Key), prob.getLocation(currLocId), true);
                    }
                    else if (!isIgnoreNotConsignedProducts)
                    {
                        if (minimumNumberOfPicks ==0)
                            currSolution.addAssignment(prob.getProduct(currAssignedLocationsKVP.Key + KeyElementSeparator + currLocId), prob.getLocation(currLocId),false);//(currAssignedLocationKVP.Value.Id));
                        else
                            currSolution.addAssignment(prob.getProduct(currAssignedLocationsKVP.Key + KeyElementSeparator + currLocId), prob.getLocation(currLocId), true);
                    }
                }
                
            }

            /*
            foreach (KeyValuePair<String, Location> currAssignedLocationKVP in currentData.AssignedLocations)
            {
                if (currentData.ConsignedProducts.ContainsKey (currAssignedLocationKVP.Key))
                    currSolution.addAssignment(prob.getProduct(currAssignedLocationKVP.Key), prob.getLocation(currAssignedLocationKVP.Value.Id));
                else
                    currSolution.addAssignment(prob.getProduct(currAssignedLocationKVP.Key +"|"+currAssignedLocationKVP.Value.Id), prob.getLocation(currAssignedLocationKVP.Value.Id));
            }
            */
            /*
            foreach (KeyValuePair<String, Product> currAssignedProductsKVP in currentData.AssignedProducts)
            {
                currSolution.addAssignment(prob.getProduct (currAssignedProductsKVP.Value.Id),prob.getLocation (currAssignedProductsKVP.Key));
            }
            */


            currSolution.Cost = 0;

            return currSolution;
        }

        protected double distCost(MacroLocationProblemData currentData, Location l1, Location l2)
        {
            return currentData.distArr[l1.orderNumber, l2.orderNumber] * usedTransportCost;
        }

        protected double getCostForConsList(MacroLocationProblemData currentData, 
                                            ConsignationProblem currentProblem, 
                                            KZO_Data.ConsignationSolution result, 
                                            ConsignationProblem.ConsignationList cList
                                            )
        {
            Location previousLoc,firstLoc;
            double currCost;
            List<Location> completeLocList;
            List<ConsignationProblem.ConsignationLocation> cLocList;
            
            completeLocList = new List<Location>();

            foreach (String pId in cList.Products)
            {
                cLocList = result.getLocationForProduct(pId);

                foreach (ConsignationProblem.ConsignationLocation cLoc in cLocList)
                {
                    completeLocList.Add(currentData.Locations[cLoc.Id]);
                }
            }

            //completeLocList .Add (currentData.LocList.First());
            completeLocList.Sort(Location.compareByOrderNumber );

            currCost = 0;
            firstLoc=previousLoc =currentData.LocList.First();
            foreach (Location currLoc in completeLocList)
            {
                currCost += currentProblem.getDistance(previousLoc.Id, currLoc.Id).Cost;
                previousLoc =currLoc;
            }

            currCost += currentProblem.getDistance(previousLoc.Id, firstLoc.Id).Cost;//currentData.distArr[previousLoc.orderNumber, firstLoc.orderNumber] * usedTransportCost;

            return currCost;
        }

        public void outputResult(String path,ConsignationProblem currentProblem, KZO_Data.ConsignationSolution result,ConsignationSolution initialSolution)
        {
            Optimizer_Data.CSV_File objFunctionFile, assignmentsFile, consListCostFile,usedConsListFile,usedDistanceFile;
            DataTable objTab, assignmentsTab,consListCostTab,usedConsListTab,usedDistanceTab;
            MacroLocationProblemData currentData;
            Location currSLoc,currDLoc;
            DataRow currDRow;
            List<ConsignationProblem.ConsignationLocation> prodLocs,prodLocsInitialSolution;
            List<ConsignationProblem.ConsignationLocation>.Enumerator it;
            CultureInfo outputCulture = CultureInfo.CreateSpecificCulture("de-DE");
            Double cOpt, cInit;
            objTab = new DataTable();
            objTab.Columns.Add(outputCostColNames[0]);
            String msg;
            Int32 i;
            Int32 point1, point2;
            Int32 currNumberOfPics;
            List<ConsignationProblem .ConsignationDistance >usedDistances= currentProblem.getAllDistances();

            msg = "Schreibe Ergebnisse...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
            {
                objTab.Columns.Add(outputCostColNames[1]);
                objTab.Columns.Add(outputCostColNames[2]);
                objTab.Columns.Add(outputCostColNames[3]);
            }
            currDRow = objTab.NewRow();
            currDRow.BeginEdit();
            currDRow[outputCostColNames[0]] = Convert.ToString(result.Cost, outputCulture);//CultureInfo .InvariantCulture);
            if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
            {
                currDRow[outputCostColNames[1]] = Convert.ToString(initialSolution.Cost, outputCulture);
                currDRow[outputCostColNames[2]] = Convert.ToString(result.Cost-initialSolution.Cost, outputCulture);
                currDRow[outputCostColNames[3]] = Convert.ToString((1 - result.Cost / initialSolution.Cost) * 100, outputCulture);
            }
            currDRow.EndEdit();
            objTab.Rows.Add(currDRow);

            objFunctionFile = new Optimizer_Data.CSV_File(path + Path.DirectorySeparatorChar + "result.csv");
            objFunctionFile.CSVDelimiter = ";";
            objFunctionFile.openForWriting(false);
            objFunctionFile.writeData(objTab);
            objFunctionFile.closeDoc();
           // protected String[] outputSolutionColNames = { "Produkt", "Weight","Line_Optimal", "Column_Optimal", "Level_Optimal","Picking_Sequence_Optimal", "Line_Initial", "Column_Initial", "Level_Inital","Picking_Sequence_Initial" };
            assignmentsTab = new DataTable();
            assignmentsTab.Columns.Add(outputSolutionColNames[0]);
            assignmentsTab.Columns.Add(outputSolutionColNames[1]);
            assignmentsTab.Columns.Add(outputSolutionColNames[2]);
            assignmentsTab.Columns.Add(outputSolutionColNames[3]);
            assignmentsTab.Columns.Add(outputSolutionColNames[4]);
            assignmentsTab.Columns.Add(outputSolutionColNames[5]);
            if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
            {
                assignmentsTab.Columns.Add(outputSolutionColNames[6]);
                assignmentsTab.Columns.Add(outputSolutionColNames[7]);
                assignmentsTab.Columns.Add(outputSolutionColNames[8]);
                assignmentsTab.Columns.Add(outputSolutionColNames[9]);
                assignmentsTab.Columns.Add(outputSolutionColNames[10]);
                assignmentsTab.Columns.Add(outputSolutionColNames[11]);
            }

            currentData = problemsByMacroLocation[currentProblemId];
            List<ConsignationProblem.ConsignationProduct> pList;

            pList = currentProblem.getAllProducts();
            Product currFullProductInfo;
            foreach (ConsignationProblem .ConsignationProduct currProduct in pList)//foreach (KeyValuePair<String, Product> prodKVP in currentData.AssignedProducts)
            {
                if (currProduct.Id == "024")
                {
                    i = 0;
                    Console.WriteLine("P24");
                }
                currFullProductInfo=consProductNameMapping[currProduct.Id];
                
                prodLocs = result.getLocationForProduct(currProduct.Id);//(prodKVP.Key);

                if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
                {

                    prodLocsInitialSolution = initialSolution.getLocationForProduct(currProduct.Id);//(prodKVP.Key);

                    it = prodLocsInitialSolution.GetEnumerator();
                    it.MoveNext();

                }
                else
                {
                    prodLocsInitialSolution = null;
                    it = prodLocs.GetEnumerator();
                }
                
                
                foreach (ConsignationProblem.ConsignationLocation l in prodLocs)
                {
                    currSLoc = currentData.Locations[l.Id];

                    currDRow = assignmentsTab.NewRow();
                    
                    currDRow.BeginEdit();
                    currDRow[outputSolutionColNames[0]] = "\"P" + currProduct.Id + "\"";//prodKVP.Key;
                    currDRow[outputSolutionColNames[1]] = Convert.ToString(currFullProductInfo.GrossWeight, usedCulture);
                    currDRow[outputSolutionColNames[2]] = Convert.ToString(currFullProductInfo.numberOfPicks, usedCulture);//(currNumberOfPics, usedCulture);
                    currDRow[outputSolutionColNames[3]] = Convert.ToString(currSLoc.Line, outputCulture);//CultureInfo.InvariantCulture);
                    currDRow[outputSolutionColNames[4]] = Convert.ToString(currSLoc.Column, outputCulture);//CultureInfo.InvariantCulture);
                    currDRow[outputSolutionColNames[5]] = Convert.ToString(currSLoc.Level, outputCulture);//CultureInfo.InvariantCulture);
                    currDRow[outputSolutionColNames[6]] = Convert.ToString(currSLoc.PickingSequence, outputCulture);//CultureInfo.InvariantCulture);
                    if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
                    {
                        currDLoc = currentData.Locations[it.Current.Id];
                        currDRow[outputSolutionColNames[7]] = Convert.ToString(currDLoc.Line, outputCulture);//CultureInfo.InvariantCulture);
                        currDRow[outputSolutionColNames[8]] = Convert.ToString(currDLoc.Column, outputCulture);//CultureInfo.InvariantCulture);
                        currDRow[outputSolutionColNames[9]] = Convert.ToString(currDLoc.Level, outputCulture);//CultureInfo.InvariantCulture);
                        currDRow[outputSolutionColNames[10]] = Convert.ToString(currDLoc.PickingSequence, outputCulture);//CultureInfo.InvariantCulture);
                        
                        point1 = currentData.pointNums[Convert.ToString(lineLanes[currSLoc.Line], CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(currSLoc.Column, CultureInfo.InvariantCulture)];
                        point2 = currentData.pointNums[Convert.ToString(lineLanes[currDLoc.Line], CultureInfo.InvariantCulture) + KeyElementSeparator + Convert.ToString(currDLoc.Column, CultureInfo.InvariantCulture)];
                        currDRow[outputSolutionColNames[11]] = Convert.ToString(currentData.distArr[point1, point2], outputCulture);
                        it.MoveNext();
                    }
                    currDRow.EndEdit();
                    assignmentsTab.Rows.Add(currDRow);
                }
            }

            assignmentsFile = new Optimizer_Data.CSV_File(path + Path.DirectorySeparatorChar + "assignments.csv");
            assignmentsFile.CSVDelimiter = ";";
            assignmentsFile.openForWriting(false);
            assignmentsFile.writeData(assignmentsTab);
            assignmentsFile.closeDoc();

            consListCostTab = new DataTable();
            usedConsListTab = new DataTable();
            consListCostTab.Columns.Add(outputConsListCostColNames[0]);
            usedConsListTab.Columns.Add(outputConsListCostColNames[0]);
            usedConsListTab.Columns.Add("Produkt");
            if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
            {
                consListCostTab.Columns.Add(outputConsListCostColNames[1]);
                consListCostTab.Columns.Add(outputConsListCostColNames[2]);
                consListCostTab.Columns.Add(outputConsListCostColNames[3]);
            }
            double totalOptCost, totalInitCost;

            totalOptCost=0;
            totalInitCost = 0;
            //protected String[] outputConsListCostColNames = { "NVE Versandpackstück" ,"Kosten_Optimal","Kosten_Start","Differenz"};
            foreach (KeyValuePair<String, ConsignationProblem.ConsignationList> cListKVP in currentData.ConsignationLists)
            {
                currDRow = consListCostTab.NewRow();
                currDRow.BeginEdit();
                currDRow[outputConsListCostColNames[0]] = "\"NVEVPS" + cListKVP.Key + "\"";
                //Console.WriteLine("cListKVP.Key:" + cListKVP.Key);
               // if (cListKVP.Key == "00340028098014257988")
                 //   cInit = 0;
                cOpt=getCostForConsList(currentData, currentProblem, result, cListKVP.Value);
                cInit = 0;
                totalOptCost += cOpt;
                currDRow[outputConsListCostColNames[1]] = Convert.ToString(cOpt, usedCulture);
                if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
                {
                    cInit+= getCostForConsList(currentData, currentProblem, initialSolution, cListKVP.Value);
                    totalInitCost += cInit;
                    currDRow[outputConsListCostColNames[2]] = Convert.ToString(cInit, usedCulture);
                    currDRow[outputConsListCostColNames[3]] = Convert.ToString(cInit-cOpt, usedCulture);
                }
                currDRow.EndEdit();
                consListCostTab.Rows.Add(currDRow);

                foreach (String p in cListKVP.Value.Products)
                {
                    currDRow = usedConsListTab.NewRow();
                    currDRow.BeginEdit();
                    currDRow[outputConsListCostColNames[0]] = "\"NVEVPS" + cListKVP.Key + "\"";
                    currDRow["Produkt"] = "\"P" + p + "\"";
                    usedConsListTab.Rows.Add(currDRow);
                }
                if (currentData.AssociatedLists.ContainsKey(cListKVP.Key))
                {
                    HashSet<String> currAssociatedLists;

                    currAssociatedLists = currentData.AssociatedLists[cListKVP.Key];

                    foreach (String listKey in currAssociatedLists)
                    {
                        currDRow = consListCostTab.NewRow();
                        currDRow.BeginEdit();
                        currDRow[outputConsListCostColNames[0]] = "\"NVEVPS" + listKey + "\"";
                        totalOptCost += cOpt;
                        currDRow[outputConsListCostColNames[1]] = Convert.ToString(cOpt, usedCulture);
                        if (initialSolution.Status == ConsignationSolution.StatusType.SOLUTION_FOUND)
                        {  
                            totalInitCost += cInit;
                            currDRow[outputConsListCostColNames[2]] = Convert.ToString(cInit, usedCulture);
                            currDRow[outputConsListCostColNames[3]] = Convert.ToString(cInit - cOpt, usedCulture);
                        }
                        currDRow.EndEdit();
                        consListCostTab.Rows.Add(currDRow);
                        
                        foreach (String p in cListKVP.Value.Products)
                        {
                            currDRow = usedConsListTab.NewRow();
                            currDRow.BeginEdit();
                            currDRow[outputConsListCostColNames[0]] = "\"NVEVPS" + listKey + "\"";
                            currDRow["Produkt"] ="\"P"+ p+"\"";
                            usedConsListTab.Rows.Add(currDRow);
                        }
                    }
                }


            }
            Console.WriteLine("Total_Opt-Kosten:" + totalOptCost + " Total Init Cost:" + totalInitCost);
            consListCostFile = new Optimizer_Data.CSV_File(path + Path.DirectorySeparatorChar + "conslistCost.csv");
            consListCostFile.CSVDelimiter = ";";
            consListCostFile.openForWriting(false);
            consListCostFile.writeData(consListCostTab);
            consListCostFile.closeDoc();

            usedConsListFile = new Optimizer_Data.CSV_File(path + Path.DirectorySeparatorChar + "usedConsLists.csv");
            usedConsListFile.CSVDelimiter = ";";
            usedConsListFile.openForWriting(false);
            usedConsListFile.writeData(usedConsListTab);
            usedConsListFile.closeDoc();

            usedDistanceTab = new DataTable();
            usedDistanceTab.Columns.Add("Start-Zeile");
            usedDistanceTab.Columns.Add("Start-Säule");
            usedDistanceTab.Columns.Add("Start-Ebene");
            usedDistanceTab.Columns.Add("Ziel-Zeile");
            usedDistanceTab.Columns.Add("Ziel-Säule");
            usedDistanceTab.Columns.Add("Ziel-Ebene");
            usedDistanceTab.Columns.Add("Distanz");
            foreach (ConsignationProblem.ConsignationDistance currD in usedDistances)
            {
                currSLoc = currentData.Locations[currD.L1.Id];

                currDRow = usedDistanceTab.NewRow();

                currDRow.BeginEdit();
                //currDRow[outputSolutionColNames[0]] = "\"" + currProduct.Id + "\"";//prodKVP.Key;
                currDRow["Start-Zeile"] = Convert.ToString(currSLoc.Line, outputCulture);//CultureInfo.InvariantCulture);
                currDRow["Start-Säule"] = Convert.ToString(currSLoc.Column, outputCulture);//CultureInfo.InvariantCulture);
                currDRow["Start-Ebene"] = Convert.ToString(currSLoc.Level, outputCulture);//CultureInfo.InvariantCulture);


                currDLoc = currentData.Locations[currD.L2.Id];
                currDRow["Ziel-Zeile"] = Convert.ToString(currDLoc.Line, outputCulture);//CultureInfo.InvariantCulture);
                currDRow["Ziel-Säule"] = Convert.ToString(currDLoc.Column, outputCulture);//CultureInfo.InvariantCulture);
                currDRow["Ziel-Ebene"] = Convert.ToString(currDLoc.Level, outputCulture);//CultureInfo.InvariantCulture);
                currDRow["Distanz"] = Convert.ToString(currD.Cost, outputCulture);
                currDRow.EndEdit();
                usedDistanceTab.Rows.Add(currDRow);
            }

            usedDistanceFile = new Optimizer_Data.CSV_File(path + Path.DirectorySeparatorChar + "usedDistances.csv");
            usedDistanceFile.CSVDelimiter = ";";
            usedDistanceFile.openForWriting(false);
            usedDistanceFile.writeData(usedDistanceTab);
            usedDistanceFile.closeDoc();
            msg = "Fertig.";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }   // assignment
    }
}
