﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Optimizer_Data;
namespace KZO_Data
{
    public class DachserXlsDataLoader
    {
        public class XLSTable
        {
            public DataTable RawTable;
            public int[] ColNums;
            public int[] RowNums;
            public int FirstRow;

            public enum MatchType
            {
                EXACT,
                SUBSTRING
            }
        }

        protected XLSTable loadTableFromExcelDocument(Optimizer_Data.Excel_Document xlsDoc,
                                                       string sheetName,
                                                       string[] colNames,
                                                       XLSTable.MatchType[] colMatchTypes
                                                       )
        {
            string uLC, lRC;
            XLSTable resultXTab;
            int i, j, k;
            string currCellStr;

            uLC = "A1";
            lRC = "ZZ1000000";

            xlsDoc.setSheetData(sheetName, uLC, lRC);

            resultXTab = new XLSTable();
            resultXTab.ColNums = new int[colNames.Length];
            resultXTab.RowNums = new int[colNames.Length];

            resultXTab.RawTable = xlsDoc.getData();

            resultXTab.FirstRow = 0;
            i = j = 0;
            for (k = 0; k < colNames.Length; k++)
            {
                for (i = 0; i < resultXTab.RawTable.Rows.Count; i++)
                {
                    for (j = 0; j < resultXTab.RawTable.Rows[i].ItemArray.Length; j++)
                    {
                        currCellStr = resultXTab.RawTable.Rows[i].ItemArray[j].ToString().Trim();

                        if (
                            (colNames[k] == "*" && currCellStr != "") ||
                            (colMatchTypes[k] == XLSTable.MatchType.EXACT && currCellStr == colNames[k]) ||
                            (colMatchTypes[k] == XLSTable.MatchType.SUBSTRING && currCellStr.Contains(colNames[k]))
                            )
                        {
                            resultXTab.ColNums[k] = j;
                            resultXTab.RowNums[k] = i;
                            if (resultXTab.FirstRow < i)
                                resultXTab.FirstRow = i;

                            break;
                        }
                    }
                    if (j < resultXTab.RawTable.Columns.Count)
                        break;
                }
                if (i == resultXTab.RawTable.Rows.Count ||
                    j == resultXTab.RawTable.Columns.Count)
                {
                    resultXTab.ColNums[k] = -1;
                    resultXTab.RowNums[k] = -1;
                }
            }

            return resultXTab;
        }
    }
}
