﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Logging;
namespace KZO_Data
{
    class Program
    {
        static void Main(string[] args)
        {
            ScreenLogger sL;
            LoggerCollection lC;

            lC = new LoggerCollection();
            
            sL = new ScreenLogger();
            lC.addLogger(sL);
            DataProvider_Excel usedDP;
            CultureInfo usedCulture;
            String usedDataPath = Environment.CurrentDirectory;//"C:\\PM\\KTO\\TestWorkingDir\\Stammdaten";
            usedCulture = CultureInfo.CurrentCulture;
            usedDP = new DataProvider_Excel(usedDataPath,usedCulture,lC);

            usedDP.load();
        }
    }
}
