﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KZO_Data
{
    public class ConsignationSolution
    {
        public enum StatusType
        {
            SOLUTION_FOUND,
            NO_SOLUTION_FOUND
        }
        Dictionary<String, ConsignationProblem.ConsignationProduct> productsByLocation;
        Dictionary<String, List<ConsignationProblem.ConsignationLocation>> locationsByProducts;
        HashSet<String> fixedProductsAssignments;
        public double Cost;
        public StatusType Status;

        public ConsignationSolution()
        {
            locationsByProducts = new Dictionary<string, List<ConsignationProblem.ConsignationLocation>>();
            productsByLocation = new Dictionary<string, ConsignationProblem.ConsignationProduct>();
            fixedProductsAssignments = new HashSet<String>();
        }

        

        public void addAssignment(ConsignationProblem.ConsignationProduct cProduct,ConsignationProblem.ConsignationLocation cLocation,Boolean isFixed)
        {
            String currKey;
            List<ConsignationProblem.ConsignationLocation> currLocList;

            if (!productsByLocation.ContainsKey(cLocation.Id))
            {
                productsByLocation.Add(cLocation.Id, cProduct);
                if (!locationsByProducts.ContainsKey(cProduct.Id))
                {
                    currLocList = new List<ConsignationProblem.ConsignationLocation>();
                    locationsByProducts.Add(cProduct.Id, currLocList);
                }
                else
                    currLocList = locationsByProducts[cProduct.Id];

                currLocList.Add(cLocation);

                if (isFixed)
                {
                    currKey = cProduct.Id + "_" + cLocation.Id;

                    fixedProductsAssignments.Add(currKey);
                }
            }
        }

        public List<ConsignationProblem.ConsignationLocation> getLocationForProduct(String Id)
        {
            return locationsByProducts[Id];
        }

        public Boolean IsFixedAssignment (String productId,String locId)
        {
            if (fixedProductsAssignments .Contains (productId+"_"+locId))
                return true;

            return false;
        }

        public ConsignationProblem.ConsignationProduct getProductForLocation(String Id)
        {
            if (productsByLocation.ContainsKey(Id))
                return productsByLocation[Id];

            return null;
        }
    }

    public class ConsignationProblem
    {
        public class ConsignationProduct
        {
            public String Id;
            protected HashSet<String> admissibleLocations;

            public ConsignationProduct()
            {
                admissibleLocations = new HashSet<string>();
            }

            public ConsignationProduct(String id):this()
            {
                Id = id;
            }

            public void addAdmissibleLocation(String locId)
            {
                if (!admissibleLocations .Contains (locId))
                    admissibleLocations.Add(locId);
            }

            public bool isExistLocationConstraints
            {
                get
                {
                    if (admissibleLocations.Count > 0)
                        return true;
                    else 
                        return false;
                }
            }

            public bool IsLocationAdmisssible(String locId)
            {
                if (admissibleLocations.Count > 0)
                {
                    if (admissibleLocations.Contains(locId))
                        return true;

                    return false;
                }

                return true;
            }
        }

        public class ConsignationLocation
        {
            public String Id;
            public Int32 OrderNumber;
        }

        public class ConsignationList
        {
            public Int32 Frequency;
            public HashSet<String> Products;
            public String Id;

            public ConsignationList()
            {
                Products = new HashSet<string>();
            }

            public static Int32 compare(ConsignationList l1, ConsignationList l2)
            {
                if (l1.Frequency != l2.Frequency)
                    return 1;

                if (l1.Products.Count != l2.Products.Count)
                    return 1;

                foreach (String p in l1.Products)
                {
                    if (!l2.Products.Contains(p))
                        return 1;
                }

                return 0;
            }
        }

        public class ConsignationDistance
        {
            public ConsignationLocation L1;
            public ConsignationLocation L2;
            public double Cost;

            public String Id
            {
                get { return L1.Id + "@" + L2.Id; }
            }
        }

        protected List<ConsignationLocation> locations;
        protected List<ConsignationList> consLists;
        protected List<ConsignationProduct> products;
        protected Dictionary<String,ConsignationDistance> distances;
        protected Dictionary<String, ConsignationProduct> productsDict;
        protected Dictionary<String, ConsignationLocation> locationsDict;
        public ConsignationProblem()
        {
            locations = new List<ConsignationLocation>();
            consLists = new List<ConsignationList>();
            products = new List<ConsignationProduct>();
            distances = new Dictionary<string, ConsignationDistance>();
            productsDict = new Dictionary<string, ConsignationProduct>();
            locationsDict = new Dictionary<string, ConsignationLocation>();
        }

        public void addLocation(ConsignationLocation l)
        {
            locations.Add(l);
            locationsDict.Add(l.Id, l);
        }

        public ConsignationLocation getLocation(String id)
        {
            return locationsDict[id];
        }

        public List<ConsignationLocation> getAllLocations()
        {
            return locations;
        }

        public void addConsignmentList(ConsignationList cList)
        {
            consLists.Add(cList);
        }

        public List<ConsignationList> getAllConsignationLists()
        {
            return consLists;
        }

        public void setKPoint(Int32 row, Int32 col)
        {
        }

        public void addProduct(ConsignationProduct p)
        {
            products.Add(p);
            productsDict.Add(p.Id, p);
        }

        public List<ConsignationProduct> getAllProducts()
        {
            return products;
        }

        public void addDistance(ConsignationLocation l1, ConsignationLocation l2, double d)
        {   
            ConsignationDistance currDist;
            
            currDist = new ConsignationDistance();
            currDist.L1 = l1;
            currDist.L2 = l2;
            currDist.Cost = d;
            distances.Add(currDist .Id, currDist);
        }

        public void addDistance(ConsignationDistance cD)
        {
            distances.Add(cD.Id, cD);
        }

        public ConsignationDistance getDistance(String lId1, String lId2)
        {
            String currKey;
            currKey = lId1 + "@" + lId2;

            if (distances.ContainsKey(currKey))
                return distances[currKey];
            else
                return null;
        }

        public ConsignationDistance getDistance(ConsignationLocation l1, ConsignationLocation l2)
        {
            return getDistance(l1.Id, l2.Id);
        }

        public ConsignationProduct getProduct(String id)
        {
            return productsDict[id];
        }

        public List<ConsignationDistance> getAllDistances()
        {
            List<ConsignationDistance> dList;
            dList = new List<ConsignationDistance>();

            foreach (KeyValuePair<String, ConsignationDistance> cDKVP in distances)
            {
                dList.Add(cDKVP.Value);
            }

            return dList;
        }
    }
}
