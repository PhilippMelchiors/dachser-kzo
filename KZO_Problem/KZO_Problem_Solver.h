// KZO_Problem.h

#pragma once
//#using<mscorlib.dll>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include "KZO_Opt_Core_Heuristic_PS.h"
#include "KZO_Opt_Core_CPLEX_PS.h"
//using namespace System;
//using namespace System::Collections;
using namespace System;
//using namespace System::IO;
using namespace System::Collections::Generic;
//using namespace System::Linq;
//using namespace System::Text;
//using namespace System::Threading::Tasks;
//using namespace System::Data;
//using namespace System::Globalization;

//using namespace KZO_Optimizer;
#pragma once
namespace KZO_Problem {

	public ref class HeuristicParams
	{
	public:
		enum class InitialHeuristicType
		{
			SIMPLE_FREQUENCY_ORDER,
			MODIFIED_CONSIGMENT_LIST_TYPES
		};

		enum class ImprovementHeuristicType
		{
			H2_OPT_BEST,
			H2_OPT_GREEDY,
			H2_OPT_GREEDY_SA
		};

		InitialHeuristicType InitialHeuristic;
		ImprovementHeuristicType ImprovementHeuristic;

		Double MinDistanceForShift;
		Double MaxDistanceForShift;
		Double ShiftCost;

		Int32 MaxNumberOfImprovements;
		Int32 MaxNumberOfThreads;
		HeuristicParams();
	};

	public ref class ConsignmentProblemSolver
	{
	protected:
		//unordered_map <Int32,KZO_Data::ConsignationProblem::ConsignationProduct ^> numProducts;
		//unordered_map <String,Int32> productsNums;
		//Dictionary<Int32,KZO_Data::ConsignationProblem::ConsignationProduct ^>^numProducts;

		bool isWriteData;
		Dictionary<String^,Int32>^productsNums;
		Dictionary<String^,Int32>^distancesById;
		
		array<KZO_Data::ConsignationProblem::ConsignationProduct ^>^products;
		array<KZO_Data::ConsignationProblem ::ConsignationLocation ^>^locations;
		array<KZO_Data::ConsignationProblem::ConsignationDistance ^>^distances;
		array<KZO_Data::ConsignationProblem::ConsignationList ^>^consLists;
/*
		void writeProducts ();
		void writeConsLists();
		void writeDistances();
		void writeLocations();
		*/

		KZO::Product*nativeProducts;
		KZO::ConsignmentLocation*nativeConsLocations;
		KZO::ConsignmentLocationsDistance *nativeConsDistances;
		KZO::ConsignmentListType *nativeConsListTypes;

		KZO::KZO_OptimizationEngine_Heuristic_Params *usedHParams;
		KZO::KZO_OptimizationEngine_Heuristic *hOptimizer;
		KZO::KZO_OptimizationEngine_CPLEX *cplexOptimizer;

		KZO::KZO_OptimizationEngine *usedOptimizer;

		void init();
		std::string buildFullFilename (const char *fName);
		void writeInstance();
		String^ usedPath;
		//KZO::Optimization_Result *usedStartingSolution;
		list<KZO::ProductionLocationAssignment>*initialSolution;
		KZO_Data::ConsignationSolution ^convertSolution(KZO::Optimization_Result *solution);
	public:
		ConsignmentProblemSolver ();
		ConsignmentProblemSolver (String ^path);
		void setHeuristicOptimizer (HeuristicParams ^usedParams);
		void Path_s(String^ path);
		void setProblem (KZO_Data::ConsignationProblem ^prob);
		void setStartingSolution (KZO_Data::ConsignationSolution ^s);
		KZO_Data::ConsignationSolution ^evaluateSolution(KZO_Data::ConsignationSolution ^sol);
		KZO_Data::ConsignationSolution ^optimize();
		KZO_Data::ConsignationSolution ^getStartSolution();
		// TODO: Die Methoden f�r diese Klasse hier hinzuf�gen.
	};
}
