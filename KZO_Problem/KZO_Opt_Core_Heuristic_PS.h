#include "KZO_Opt_Core_PS.h"
#include "Matrix2d.h"
#include<string>
#include<set>
#include <unordered_map>
#include <process.h>
#include <Windows.h>
#pragma once
namespace KZO
{	
	public enum __declspec( dllimport ) OptimizationScheme
	{
		STARTHEURISTIC_IMPROVEMENTHEURISTIC
	};
	public enum __declspec( dllimport ) StartHeuristic
	{
		SIMPLE_FREQUENCY_ORDER,
		MODIFIED_CONSIGMENT_LIST_TYPES
	};
	public enum __declspec( dllimport ) ImprovementHeuristic
	{
		H2_OPT_BEST,
		H2_OPT_GREEDY,
		H2_OPT_GREEDY_SA
	};
	public class __declspec( dllimport ) KZO_OptimizationEngine_Heuristic_Params
	{
	public:
		int maxNumberOfThreads;
		OptimizationScheme usedScheme;
		StartHeuristic usedStartHeuristic;
		ImprovementHeuristic usedImprovementHeuristic;
		int maxNumberOfImprovementIterations;
		double minLocDistanceForShift;
		double maxLocDistanceForShift;
		double shiftCostPerMeter;
		KZO_OptimizationEngine_Heuristic_Params();
		/*
		OptimizationScheme usedScheme;
		StartHeuristic usedStartHeuristic;
		ImprovementHeuristic usedImprovementHeuristic;
		int maxNumberOfImprovementIterations;
		int minLocDistanceForShift;
		KZO_OptimizationEngine_Heuristic_Params();
		*/
	};
	/*
	public class __declspec( dllimport ) KZO_OptimizationEngine_Heuristic : public KZO_OptimizationEngine 
	{
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			std::list<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);
			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
	};*/
	/*
	class __declspec( dllexport ) KZO_OptimizationEngine_Heuristic : public KZO_OptimizationEngine 
	{
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			std::list<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;

		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
	};*/
	/*
	class __declspec( dllimport ) KZO_OptimizationEngine_Heuristic : public KZO_OptimizationEngine 
	{
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			Matrix_2d<int>*sortedProductsByConsListVisit;
			Matrix_2d<int>*productPositionInSortedList;
			Matrix_2d<int>*candidateProductPositionInSortedList;
			
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			
			void checkSortedLists ();
			void sortedListChangePositionSingleProduct(int cL,int p,int newL);
			void sortedListChangePosition(int p1,int newL1,int p2,int newL2);
			void initializeSortedLists();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;
			int numberDiffCalls;
			int totalNumberIts;
			double getAbsDistance (int l1,int l2);
			bool isValidLocDiff(int p,int newLoc);
			bool isValidExchange (int p1,int p2);
			int findNewPositionForProduct(int cL,int p, int l);
		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
		/*
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			//std::list<int>*sortedProductsByLocation;
			Matrix_2d<int>*sortedProductsByConsListVisit;
			Matrix_2d<int>*productPositionInSortedList;
			Matrix_2d<int>*candidateProductPositionInSortedList;
			//Matrix_2d<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			
			void checkSortedLists ();
			void sortedListChangePositionSingleProduct(int cL,int p,int newL);
			void sortedListChangePosition(int p1,int newL1,int p2,int newL2);
			void initializeSortedLists();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;
			int numberDiffCalls;
			int totalNumberIts;

			bool isValidLocDiff(int p,int newLoc);
			bool isValidExchange (int p1,int p2);
			int findNewPositionForProduct(int cL,int p, int l);
		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
			*/
		/*
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			std::list<int>*sortedProductsByLocation;
			Matrix_2d<int>*sortedProductsByConsListVisit;
			Matrix_2d<int>*productPositionInSortedList;
			//Matrix_2d<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			
			void checkSortedLists ();
			void sortedListChangePositionSingleProduct(int cL,int p,int newL);
			void sortedListChangePosition(int p1,int newL1,int p2,int newL2);
			void initializeSortedLists();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;
			int numberDiffCalls;
			int totalNumberIts;
		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
			*/
		/*protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;

			std::list<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void prepareVariables();
			double objFunction();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;

		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
			*/
	//};

	class __declspec( dllimport ) KZO_LocationPositionEntry
	{
	public:
		int iteration;
		int position;
	};
	class __declspec( dllimport ) KZO_ThreadInfos
	{
	public:
		HANDLE threadHandle;
		int p1Start;
		int p1End;
		int p2Start;
		int p2End;
		int p1Min;
		int p2Min;
		double minDiff;
		bool isFinished;
	};

	class __declspec( dllexport ) KZO_OptimizationEngine_Heuristic : public KZO_OptimizationEngine 
	{
		protected:
			KZO_ThreadInfos *findThreads;
			CRITICAL_SECTION crit;
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentLocationsDistance *> cLDistanceList;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			int *locationsByProductBestSolution;
			int *productsByLocationBestSolution;
			int *locationsByProductRefSolution;
			int *productsByLocationRefSolution;
			bool *isFixedAssignmentRefSolutionByProduct;

			Matrix_2d<int>*sortedProductsByConsListVisit;
			Matrix_2d<int>*productPositionInSortedList;
			Matrix_2d<KZO_LocationPositionEntry>*candidateLocationPositionInSortedList;
			
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;
			void storeReferenceSolution();
			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt_Best();
			void improveBy2Opt_Greedy();
			void improveBy2Opt_Greedy_SA();
			void backupCurrentSolution();
			void restoreCurrentSolution();
			void prepareVariables();
			double objFunction();
			
			void checkSortedLists ();
			void sortedListChangePositionSingleProduct(int cL,int p,int newL);
			void sortedListChangePosition(int p1,int newL1,int p2,int newL2);
			void initializeSortedLists();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);

			void findBestExchange (int &p1Min,int &p2Min,double &diff,int p1Start,int p1End,int p2Start,int p2End);
			
			double exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2);

			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
			double initialObjValue;
			
			double **distanceMatrix;
			double *distances;

			bool **consignmentListsBoolean;
			bool *consLBEntries;
			int numberDiffCalls;
			int totalNumberIts;
			int currImprovementIteration;
			
			double getAbsDistance (int l1,int l2);
			bool isValidLocDiff(int p,int newLoc);
			bool isValidExchange (int p1,int p2);
			int findNewPositionForProduct(int cL,int p, int l);
		public:
			unsigned threadProcedure (void *index);
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
	};
}