#include "stdafx.h"
#using <system.dll>
#include "msclr\marshal_cppstd.h"
using msclr::interop::marshal_as;


#include "stringFunctions.h"
#include<iostream>
#include <list>

int numberDigits=4;
int oc=0;
string *charDelete (string s,char c)
{
	const char *sc;
	char *nsc;
	string *nstr;
	int length,i,j;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length];

	j=0;
	for (i=0;i<length;i++)
	{
		if (sc[i]!=c)
		{
			nsc[j]=sc[i];
			j++;
		}
	}
	nsc[j]=0;
	
	nstr=new string;
	nstr->append(nsc);
	return nstr;
}

string *charReplace (string s,char oldChar,char newChar)
{
	const char *sc;
	char *nsc;
	string *nstr;
	int length,i;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length+1];

	for (i=0;i<length;i++)
	{
		if (sc[i]!=oldChar)
		{
			nsc[i]=sc[i];
		}
		else
		{
			nsc[i]=newChar;
		}
	}

	nsc[i]=0;
	
	nstr=new string;
	nstr->append(nsc);
	return nstr;
}

void setNumberDigits(int n)
{
	numberDigits=n;
}

template <> string toString<double> (const double &t)
{
	  ostringstream os;
	    
	  os.precision(numberDigits);
	
		os <<fixed<< t;
	    return os.str();
}

char *fromSystemStringToCChar(String ^s)
{
   pin_ptr<const wchar_t> wch = PtrToStringChars(s);
   //printf_s("%S\n", wch);

   // Conversion to char* :
   // Can just convert wchar_t* to char* using one of the 
   // conversion functions such as: 
   // WideCharToMultiByte()
   // wcstombs_s()
   // ... etc
   size_t convertedChars = 0;
   size_t  sizeInBytes = ((s->Length + 1) * 2);
   errno_t err = 0;
   char    *ch = (char *)malloc(sizeInBytes);

   err = wcstombs_s(&convertedChars, 
                    ch, sizeInBytes,
                    wch, sizeInBytes);
   if (err != 0)
      printf_s("wcstombs_s  failed!\n");
   return ch;
}

void fromSystemStringToStlString(String ^s, string & stlStr)
{
	string stdStr;

	stdStr=marshal_as<string>(s);
	stlStr.append(stdStr);
}

String ^fromStdStringToSystemString (string &stlStr)
{
	String ^s;
	s=gcnew String (stlStr .c_str());

	return s;
}