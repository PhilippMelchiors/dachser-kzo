// Dies ist die Haupt-DLL.

#include "stdafx.h"

#include "KZO_Problem_Solver.h"
#include "KZO_Opt_Core_PS.h"
#include "KZO_Opt_Core_Heuristic_PS.h"
#include "KZO_Opt_Core_CPLEX_PS.h"
#include "CSVFile.h"
#include "stringFunctions.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::IO;
KZO_Problem::HeuristicParams::HeuristicParams()
{
	InitialHeuristic =KZO_Problem ::HeuristicParams::InitialHeuristicType::SIMPLE_FREQUENCY_ORDER;
	ImprovementHeuristic =KZO_Problem::HeuristicParams::ImprovementHeuristicType::H2_OPT_BEST;
	MinDistanceForShift =0;
	MaxDistanceForShift = 1.7976931348623157E+308;
	MaxNumberOfImprovements =2147483647;
	ShiftCost =0;
	MaxNumberOfThreads =1;
}

void KZO_Problem::ConsignmentProblemSolver::init()
{
	isWriteData =true;
	initialSolution =new list<KZO::ProductionLocationAssignment>();
}

KZO_Problem::ConsignmentProblemSolver::ConsignmentProblemSolver ()
{
	init();
	//KZO::KZO_Test *t;

	//t=new KZO::KZO_Test ();
	//KZO::KZO_OptimizationEngine_CPLEX *inst;
	//inst=new KZO::KZO_OptimizationEngine_CPLEX ();
}
KZO_Problem::ConsignmentProblemSolver::ConsignmentProblemSolver (String ^p)
{
	init();
	usedPath =p;
}

void KZO_Problem::ConsignmentProblemSolver::Path_s(String ^p)
{
	usedPath=p+Path::DirectorySeparatorChar;
}

/*
void KZO_Problem::ConsignmentProblemSolver::writeProducts ()
{
	CSVFile *prodFile;
	
	prodFile=new CSVFile (productsFileName,true);

	prodFile->
}

void KZO_Problem::ConsignmentProblemSolver::writeLocations ()
{
}

void KZO_Problem::ConsignmentProblemSolver::writeConsLists ()
{
}
void KZO_Problem::ConsignmentProblemSolver::writeDistances ()
{
}*/
string KZO_Problem::ConsignmentProblemSolver::buildFullFilename (const char *fName)
{
	string fileName;

	fromSystemStringToStlString ( usedPath,fileName );

	fileName+=fName;

	return fileName;
}
void KZO_Problem::ConsignmentProblemSolver::writeInstance ()
{
	int p,l,l1,l2,cL,cD;
	double c;
	int i;
	CSVFile *productFile,*locationsFile,*distancesFile,*consListsFile,*consListsMetaDataFile;
	DatasourceRow *currRow;
	String ^currKey;
	KZO_Data ::ConsignationProblem::ConsignationDistance^ currDistance;
	KZO_Data::ConsignationProblem::ConsignationList ^currConsList;
	char *productsFileName="products.csv";
	char *productsFileHeadLine[]={"Product"};

	char *locationsFileName="locations.csv";
	char *locationsFileHeadLine[]={"Location"};

	char *distancesFileName="distances.csv";
	char *distancesFileHeadLine[]={"Location1","Location2","Distance"};

	char *consignationListsFileName="consignationlists.csv";
	char *consignationFileHeadLine[]={"ConsignationList","Product"};

	char *consignationListsMetaDataFileName="consignationlistsMetaData.csv";
	char *consignationListsMetaDataFileHeadLine[]={"ConsignationList","Frequency"};
	//char *currFileName;

	//currFileName=buildFullFilename(productsFileName);
	productFile=new CSVFile ((char*)buildFullFilename(productsFileName).c_str(),true);
	locationsFile =new CSVFile ((char*)buildFullFilename (locationsFileName).c_str(),true);
	distancesFile =new CSVFile ((char*)buildFullFilename (distancesFileName).c_str(),true);
	consListsFile =new CSVFile ((char*)buildFullFilename (consignationListsFileName).c_str(),true);
	consListsMetaDataFile= new CSVFile((char *) buildFullFilename (consignationListsMetaDataFileName).c_str (),true);

	currRow=new DatasourceRow ();
	currRow->numberRowEls =1;
	currRow->rowEls =new string [3];

	currRow->rowEls[0]=productsFileHeadLine[0];
	productFile->writeRow (currRow);
	for (p=0;p<products->Length;p++)
	{
		currRow->rowEls[0]=toString <int>(p);
		productFile->writeRow (currRow);
	}

	currRow->rowEls[0]=locationsFileHeadLine[0];
	locationsFile->writeRow (currRow);
	for (l=0;l<=locations->Length;l++)
	{
		currRow->rowEls[0]=toString <int>(l);
		locationsFile->writeRow (currRow);
	}
	currRow->numberRowEls=3;
	currRow->rowEls[0]=distancesFileHeadLine[0];
	currRow->rowEls[1]=distancesFileHeadLine[1];
	currRow->rowEls[2]=distancesFileHeadLine[2];
	distancesFile->writeRow (currRow);

	for (l=0;l<distances->Length ;l++)
	{
		currRow->rowEls[0]=toString <int>(nativeConsDistances [l].Loc1_g ());
		currRow->rowEls[1]=toString <int>(nativeConsDistances [l].Loc2_g ());
		currRow->rowEls[2]=toString <double>(nativeConsDistances [l].Cost_g ());
		distancesFile->writeRow (currRow);
	}

	/*
	for (l1=0;l1<locations->Length ;l1++)
	{
		for (l2=l1+1;l2<=locations->Length;l2++)
		{
			//currKey=toString <int>(l1)+"@"+toString <int>(l2);
			

			currKey=fromStdStringToSystemString ( toString <int>(l1)+"@"+toString <int>(l2));
			//currDistance =distancesById[currKey]; //distances[currKey];
			cD=distancesById [currKey];

			c=currDistance->Cost;
			currRow->rowEls[0]=toString <int>(l1);
			currRow->rowEls[1]=toString <int>(l2);
			currRow->rowEls[2]=toString <double>(nativeDis);
			distancesFile->writeRow (currRow);
			
		}
	}

	currRow->rowEls[1]="0";
	for (l=1;l<=locations->Length ;l++)
	{
		currKey=fromStdStringToSystemString (toString<int>(l)+"@0");
		currDistance=distancesById[currKey];
		currRow->rowEls[0]=toString <int>(l);	
		c=currDistance->Cost;
		currRow->rowEls[2]=toString <double>(c);
		distancesFile->writeRow (currRow);
	}*/

	currRow->numberRowEls=2;
	
	currRow->rowEls[0]=consignationFileHeadLine[0];
	currRow->rowEls[1]=consignationFileHeadLine[1];
	consListsFile->writeRow (currRow);
	//Int32 n;
	for (cL=0;cL<consLists->Length;cL++)
	{
		currRow->rowEls[0]=toString <int>(cL);

		for (p=0;p<products->Length;p++)
		{
			//n=p;
			//String ^s;

			//s=productsNums [n]->Id;
			if (consLists[cL]->Products->Contains(products[p]->Id))//(consListsBool->m_A[cL][p])
			{
				currRow->rowEls[1]=toString <int>(p);
				consListsFile ->writeRow (currRow);
			}
		}
	}
	
	currRow->rowEls[0]=consignationListsMetaDataFileHeadLine[0];
	currRow->rowEls[1]=consignationListsMetaDataFileHeadLine[1];
	consListsMetaDataFile->writeRow (currRow);
	for (cL=0;cL<consLists->Length;cL++)
	{
		currRow->rowEls[0]=toString <int>(cL);
		
		currRow->rowEls[1]=toString <int>(nativeConsListTypes[cL].Frequency_g ());//(freqConsLists[cL]);
		consListsMetaDataFile->writeRow (currRow);
	}
	
	delete currRow;
	delete productFile;
	delete locationsFile;
	delete distancesFile;
	delete consListsFile;
}

void KZO_Problem::ConsignmentProblemSolver::setProblem (KZO_Data::ConsignationProblem ^prob)
{
	Int32 i,j;
	List<KZO_Data::ConsignationProblem::ConsignationProduct ^>^pList;
	List<KZO_Data ::ConsignationProblem::ConsignationLocation ^>^locList;
	List<KZO_Data::ConsignationProblem ::ConsignationList ^>^consListList;
	List<KZO_Data::ConsignationProblem ::ConsignationDistance^>^distancesList;

	pList=prob->getAllProducts();
	locList=prob->getAllLocations ();
	consListList=prob->getAllConsignationLists ();
	distancesList=prob->getAllDistances ();

	productsNums=gcnew Dictionary<String^,Int32>();
	products=gcnew array<KZO_Data::ConsignationProblem ::ConsignationProduct ^>(pList->Count);
	nativeConsLocations =new KZO::ConsignmentLocation [locList->Count];
	locations =gcnew array<KZO_Data::ConsignationProblem ::ConsignationLocation ^>(locList->Count );
	
	for each (KZO_Data::ConsignationProblem::ConsignationLocation^l in locList )
	{
		locations[l->OrderNumber]=l;
		nativeConsLocations[l->OrderNumber].Cap_s (1);
		nativeConsLocations [l->OrderNumber].Number_s (l->OrderNumber);
	}

	nativeProducts=new KZO::Product [pList->Count];
	i=0;
	for each (KZO_Data::ConsignationProblem::ConsignationProduct ^p in pList)
	{
		products[i]=p;
		productsNums->Add (p->Id,i);
		nativeProducts [i].Number_s (i);
		
		if (products[i]->isExistLocationConstraints )
		{
			for (j=0;j<locations->Length ;j++)
			{
				if (products[i]->IsLocationAdmisssible (locations[j]->Id))
					nativeProducts[i].addLocation (j);
			}
		}
		i++;
	}
	
	nativeConsListTypes=new KZO::ConsignmentListType [consListList->Count];
	consLists=gcnew array<KZO_Data::ConsignationProblem::ConsignationList ^>(consListList->Count);

	i=0;
	for each (KZO_Data::ConsignationProblem::ConsignationList ^cL in consListList )
	{
		consLists[i]=cL;
		nativeConsListTypes [i].Frequency_s (cL->Frequency);
		nativeConsListTypes[i].Number_s (i);
		
		for (j=0;j<products->Length;j++)
		{
			if (consLists[i]->Products->Contains (products[j]->Id))
			{
				nativeConsListTypes [i].addProduct (j);
			}
		}

		i++;
	}
	
	nativeConsDistances =new KZO::ConsignmentLocationsDistance [distancesList->Count];
	distances =gcnew array<KZO_Data ::ConsignationProblem::ConsignationDistance^>(distancesList->Count );

	distancesById =gcnew Dictionary<String^,Int32>();
	i=0;
	for each (KZO_Data::ConsignationProblem ::ConsignationDistance ^cD in distancesList )
	{	
		distances[i]=cD;
		nativeConsDistances [i].Cost_s (cD->Cost );
		nativeConsDistances [i].Loc1_s (cD->L1->OrderNumber);
		nativeConsDistances [i].Loc2_s (cD->L2->OrderNumber);
		
		distancesById->Add (cD->Id ,i);
		i++;
	}
	
	if (isWriteData )
		writeInstance ();

	usedOptimizer->clear ();

	for (i=0;i<products->Length;i++)
	{
		usedOptimizer->addProduct (&nativeProducts[i]);
	}

	for (i=0;i<locations->Length;i++)
	{
		usedOptimizer->addConsignmentLocation (&nativeConsLocations [i]);
	}

	for (i=0;i<consLists->Length;i++)
	{
		usedOptimizer->addConsignmentList (&nativeConsListTypes[i]);
	}

	for (i=0;i<distances->Length ;i++)
	{
		usedOptimizer->addDistance (&nativeConsDistances [i]);
	}

	usedOptimizer->buildStructures ();

}
void KZO_Problem::ConsignmentProblemSolver::setHeuristicOptimizer (HeuristicParams ^usedParams)
{
	usedHParams =new KZO::KZO_OptimizationEngine_Heuristic_Params ();

	switch (usedParams->InitialHeuristic )
	{
	case HeuristicParams::InitialHeuristicType::SIMPLE_FREQUENCY_ORDER:
		usedHParams->usedStartHeuristic =KZO::StartHeuristic::SIMPLE_FREQUENCY_ORDER;
		break;
		case HeuristicParams::InitialHeuristicType::MODIFIED_CONSIGMENT_LIST_TYPES:
		usedHParams->usedStartHeuristic =KZO::StartHeuristic::MODIFIED_CONSIGMENT_LIST_TYPES;
		break;
	}

	switch (usedParams->ImprovementHeuristic)
	{
	case HeuristicParams::ImprovementHeuristicType::H2_OPT_BEST:
		usedHParams->usedImprovementHeuristic=KZO::ImprovementHeuristic::H2_OPT_BEST;
		break;
	case HeuristicParams::ImprovementHeuristicType::H2_OPT_GREEDY:
		usedHParams->usedImprovementHeuristic=KZO::ImprovementHeuristic::H2_OPT_GREEDY;
		break;
	case HeuristicParams::ImprovementHeuristicType::H2_OPT_GREEDY_SA:
		usedHParams->usedImprovementHeuristic=KZO::ImprovementHeuristic::H2_OPT_GREEDY_SA;
	}
	usedHParams->usedScheme=KZO::OptimizationScheme::STARTHEURISTIC_IMPROVEMENTHEURISTIC;
	//usedHParams->usedStartHeuristic =KZO::StartHeuristic::SIMPLE_FREQUENCY_ORDER;
	//usedHParams->usedImprovementHeuristic =KZO::ImprovementHeuristic::H2_OPT_BEST;//H2_OPT_GREEDY;
	usedHParams->maxNumberOfImprovementIterations =usedParams->MaxNumberOfImprovements;//30000;
	usedHParams->minLocDistanceForShift =usedParams->MinDistanceForShift;//5;
	usedHParams ->maxLocDistanceForShift =usedParams->MaxDistanceForShift;
	usedHParams->shiftCostPerMeter =usedParams->ShiftCost;//0;
	usedHParams->maxNumberOfThreads=usedParams->MaxNumberOfThreads ;
	hOptimizer =new KZO::KZO_OptimizationEngine_Heuristic (usedHParams);
	usedOptimizer =hOptimizer;
}

void KZO_Problem::ConsignmentProblemSolver::setStartingSolution (KZO_Data::ConsignationSolution ^s)
{
	KZO::ProductionLocationAssignment currAssignment;
	String ^currKey;
	List<KZO_Data::ConsignationProblem::ConsignationLocation ^> ^pLocs;
	int i;
	initialSolution->clear ();
	//initialSolution=new list<KZO::ProductionLocationAssignment >();
	//usedStartingSolution=new KZO::Optimization_Result ();

	for (i=0;i<products->Length;i++)
	{
		currAssignment.p=&nativeProducts [i];
		pLocs=s->getLocationForProduct (products[i]->Id);

		for each (KZO_Data::ConsignationProblem::ConsignationLocation ^l in pLocs)
		{
			currAssignment.l =&nativeConsLocations [l->OrderNumber];
			
			//currKey=products[i]->Id+"_"+l->Id ;
			if (s->IsFixedAssignment (products[i]->Id,l->Id))
				currAssignment.isFixed =true;
			else
				currAssignment .isFixed =false;
			//usedStartingSolution->addAssignemnt (currAssignment);
			initialSolution->push_back (currAssignment );
		}
	}

	//usedStartingSolution->Cost_s (s->Cost);
	usedOptimizer->setInitialSolution (*initialSolution);
}
KZO_Data::ConsignationSolution ^KZO_Problem::ConsignmentProblemSolver::evaluateSolution(KZO_Data::ConsignationSolution ^sol)
{
	KZO::ProductionLocationAssignment currNativeAssignment;
	list<KZO::ProductionLocationAssignment>*nativeAssignments;
	List<KZO_Data::ConsignationProblem::ConsignationLocation ^> ^pLocs;
	//KZO::Optimization_Result *nativeEvalResult;
	KZO::Optimization_Result *newNativeResult;
	KZO_Data::ConsignationSolution ^newResult;

	int i;
	double c;

	//usedStartingSolution=new KZO::Optimization_Result ();
	//nativeEvalResult=new KZO::Optimization_Result ();
	nativeAssignments=new list<KZO::ProductionLocationAssignment >();

	for (i=0;i<products->Length;i++)
	{
		currNativeAssignment.p=&nativeProducts [i];
		pLocs=sol->getLocationForProduct (products[i]->Id);

		for each (KZO_Data::ConsignationProblem::ConsignationLocation ^l in pLocs)
		{
			currNativeAssignment.l =&nativeConsLocations [l->OrderNumber];
			//nativeEvalResult->addAssignemnt (currNativeAssignment);
			nativeAssignments->push_back (currNativeAssignment);
		}
	}
	
	newNativeResult =usedOptimizer->evaluateSolution (*nativeAssignments);

	newResult=gcnew KZO_Data::ConsignationSolution();

	for (i=0;i<products->Length;i++)
	{
		pLocs=sol->getLocationForProduct (products[i]->Id);

		for each (KZO_Data::ConsignationProblem::ConsignationLocation ^l in pLocs)
		{
			currNativeAssignment.l =&nativeConsLocations [l->OrderNumber];
			newResult->addAssignment (products[i],l,true);
		}
	}

	newResult->Cost =newNativeResult->Cost_g ();
	
	delete nativeAssignments;
	delete newNativeResult;

	return newResult;
}
KZO_Data::ConsignationSolution ^KZO_Problem::ConsignmentProblemSolver::convertSolution(KZO::Optimization_Result *nativeResult)
{
	KZO_Data::ConsignationSolution ^result;
	
	list<KZO::ProductionLocationAssignment >*assignments;
	list<KZO::ProductionLocationAssignment >::iterator it,itEnd;

	result=gcnew KZO_Data::ConsignationSolution();
	
	if (nativeResult->Status_g ()==KZO::Optimization_Status::SOLUTION_FOUND )
	{
		result->Status =KZO_Data::ConsignationSolution ::StatusType ::SOLUTION_FOUND;
		result->Cost =nativeResult->Cost_g ();
		assignments=nativeResult->Assigments_g();

		it=assignments->begin ();
		itEnd=assignments->end();

		while (it!=itEnd)
		{	
			result->addAssignment (products[it->p->Number_g ()],locations[it->l->Number_g ()],it->isFixed);
			it++;
		}
	}
	else
		result->Status =KZO_Data ::ConsignationSolution ::StatusType ::NO_SOLUTION_FOUND;

	return result;
}
KZO_Data::ConsignationSolution ^KZO_Problem::ConsignmentProblemSolver::optimize()
{
	KZO_Data::ConsignationSolution ^result;
	KZO::Optimization_Result *nativeResult;
	
	nativeResult=usedOptimizer->optimize ();

	result=convertSolution (nativeResult );

	delete nativeResult;

	return result;
}
KZO_Data::ConsignationSolution ^KZO_Problem::ConsignmentProblemSolver::getStartSolution()
{
	KZO::Optimization_Result *nativeResult;
	KZO_Data::ConsignationSolution ^result;

	nativeResult=usedOptimizer->getStartSolution ();

	result=convertSolution (nativeResult );

	delete nativeResult;

	return result;
}