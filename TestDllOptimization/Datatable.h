#include <string>
#include <map>
//using namespace std;

#ifndef DATATABLE

#define DATATABLE
class DatasourceRow {

public:
	std::string *rowEls;
	int numberRowEls;
	DatasourceRow();
	~DatasourceRow();
};

class DatasourceTable {

private:
	bool isFinished;
	std::map<int,DatasourceRow*>*unfinishedDatasourceTableEntries;
	DatasourceRow** finishedDatasourceTableEntries;
	int numberOfRows;
public:
	DatasourceTable ();
	~DatasourceTable ();
	void finishTable ();
	void addRow (DatasourceRow *nTableRow);
	DatasourceRow *getRow (int number);
	std::string *getCellContent(int row, int column);
	int getNumberOfRows();
};

class GeneralDataSource {

public:
	virtual void resetRead()=0; //abstrakte Definition
	virtual DatasourceRow *getRow()=0; //abstrakte Definition
	virtual int writeRow (DatasourceRow *r)=0; //abstrakte Definition
	virtual DatasourceTable *getDatasourceTable()=0; //abstrakte Definition
	virtual bool isDataSourceWithTitleLine ()=0; //abstrakte Definition
};
#endif DATATABLE