#include "stdafx.h"
#include "DataTable.h"

DatasourceRow::DatasourceRow()
{
	rowEls=NULL;
}

DatasourceRow::~DatasourceRow()
{
	if (rowEls!=NULL)
		delete [] rowEls;
}

DatasourceTable::DatasourceTable ()
{
	isFinished=false;
	unfinishedDatasourceTableEntries=new std::map<int,DatasourceRow*>();
	numberOfRows=0;
}

DatasourceTable::~DatasourceTable()
{}

void DatasourceTable::addRow (DatasourceRow *nDatasourceRow)
{
	int currKey;

	currKey=unfinishedDatasourceTableEntries->size();
	unfinishedDatasourceTableEntries->insert(std::pair<int,DatasourceRow *>(currKey,nDatasourceRow));
}

DatasourceRow *DatasourceTable::getRow (int number)
{
	DatasourceRow *r;

	r=(*unfinishedDatasourceTableEntries)[number];
	return r;
}

std::string *DatasourceTable::getCellContent(int row, int column)
{
	if (column>=(*unfinishedDatasourceTableEntries)[row]->numberRowEls)
		return NULL;
	return &(*unfinishedDatasourceTableEntries)[row]->rowEls[column];
}

int DatasourceTable::getNumberOfRows()
{
	return unfinishedDatasourceTableEntries->size();
}