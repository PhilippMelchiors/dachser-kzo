#include "stdafx.h"

#include <set>
#include <unordered_map>
#include <unordered_set>

#ifndef KZO_BASE

#define KZO_BASE

namespace KZO {

	class __declspec( dllexport ) Product
	{
	protected:
		int number;
		bool isConstrainedConsignedLocations;
		std::unordered_set <int> locations;
	public:
		Product();
		void addLocation (int l);
		void removeLocation (int l);
		bool isLocationInConstrainedLocationSet(int l);
		int Number_g();
		void Number_s(int n);
	};

	class __declspec( dllexport ) ConsignmentLocation
	{
	protected:
		int cap;
		int number;
	public:
		ConsignmentLocation ();
		ConsignmentLocation (int c,int n);
		int Cap_g();
		void Cap_s(int v);
		int Number_g();
		void Number_s(int n);
	};

	class __declspec( dllexport ) ConsignmentLocationsDistance 
	{
	protected:
		int loc1;
		int loc2;
		double cost;
	public:
		ConsignmentLocationsDistance ();
		ConsignmentLocationsDistance (int oNL1,int oNL2,double c);
		int Loc1_g();
		void Loc1_s(int l);
		int Loc2_g();
		void Loc2_s(int l);
		double Cost_g();
		void Cost_s(double c);
	};

	class __declspec( dllexport ) ConsignmentListType
	{
	protected:
		std::unordered_set <int> products;
		int frequency;
		int number;
	public:
		void Number_s(int n);
		int Number_g();
		void Frequency_s(int f);
		int Frequency_g();
		void addProduct (int p);
		bool isProductInList(int p);
		//int NumberOfProducts();
		std::list<int> *getProducts();
	};

	class __declspec( dllexport ) ProductionLocationAssignment
	{
	public:
		Product *p;
		ConsignmentLocation *l;
	};

	enum __declspec( dllexport ) Optimization_Status {
		SOLUTION_FOUND,
		NO_SOLUTION_FOUND
	};

	class __declspec( dllexport ) Optimization_Result
	{
	protected:
		std::list<ProductionLocationAssignment> assignments;
		double cost;
		Optimization_Status status;
	public:
		std::list<ProductionLocationAssignment> *Assigments_g();
		
		double Cost_g();
		void Cost_s(double c);

		Optimization_Status Status_g();
		void Status_s(Optimization_Status stat);

		void addAssignemnt (ProductionLocationAssignment a);
	};

	class __declspec( dllexport ) KZO_OptimizationEngine
	{
	public:
		virtual void addProduct(Product *p)=0;
		virtual void addConsignmentLocation (ConsignmentLocation *cL)=0;
		virtual void addDistance (ConsignmentLocationsDistance *d)=0;
		virtual void addConsignmentList(ConsignmentListType *cListType)=0;
		virtual void buildStructures()=0;
		virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments)=0;
		virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments)=0;
		virtual KZO::Optimization_Result *optimize()=0;
		virtual KZO::Optimization_Result *getStartSolution()=0;
		virtual void resetSolution ()=0;;
		virtual void clear()=0;
	};
}
#endif