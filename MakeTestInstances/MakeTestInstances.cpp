// MakeTestInstances.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "CSVFile.h"
#include "stringFunctions.h"
#include "Matrix2d.h"

#include<unordered_map>
#include<unordered_set>
#include<iostream>
#include<cstdlib>
using namespace std;

const char *productsFileName="products.csv";
const char *productsFileHeadLine[]={"Product"};
const char *locationsFileName="locations.csv";
const char *locationsFileHeadLine[]={"Location"};
const char *distancesFileName="distances.csv";
const char *distancesFileHeadLine[]={"Location1","Location2","Distance"};
const char *consignationListsFileName="consignationlists.csv";
const char *consignationFileHeadLine[]={"ConsignationList","Product"};
const char *consignationListsMetaDataFileName="consignationlistsMetaData.csv";
const char *consignationListsMetaDataFileHeadLine[]={"ConsignationList","Frequency"};

const int numberProducts=15;
const int numberLocations=15;

const int numberConsLists=20;
const int maxConsListFreq=5;
const char *path="C:\\PM\\KTO\\TestWorkingDir\\test4\\";//"C:\\PM\\KTO\\TestWorkingDir\\test1\\";//"C:\\dachser\\KTO\\TestWorkingDir\\test1\\";
double simpleDistanceMinVal=0.5;
double simpleDistanceMaxVal=2.5;
double minPctDistMultipleLocs=0.8;
double minPctDistConspointReturn=0.8;

unordered_map <string,double> distances;

//list<bool *>consLists;
Matrix_2d<bool> *consListsBool;
int *freqConsLists;
bool *productsInConsLists;
bool *currConsList;
int currNumberConsLists;

double zeroOneRNG()
{
	int rv1=rand();
	int rv2=(rv1 % 10000);
	double rv=((double)rv2/10000);
	return rv;
}

double getPathSimpleDistances (int l1,int l2)
{
	int l;
	double currDistance;
	string currKey;

	currDistance =0;
	for (l=l1;l<l2;l++)
	{
		currKey=toString <int>(l)+"_"+toString <int>(l+1);
		currDistance+=distances[currKey];
	}

	return currDistance;
}

void sampleDistances ()
{
	int l,l1,l2;
	double currDistVal,currDistValConsPointReturn;
	double currDistanceFromConsPoint;
	string currKey;

	for (l=0;l<numberLocations;l++)
	{
		currDistVal =zeroOneRNG ()*(simpleDistanceMaxVal-simpleDistanceMinVal)+simpleDistanceMinVal;
		currKey =toString<int>(l)+"_"+toString <int>(l+1);
		distances.insert (pair<string,double>(currKey,currDistVal));
	}

	for (l1=0;l1<=numberLocations-2;l1++)
	{
		for (l2=l1+2;l2<=numberLocations;l2++)
		{
			currDistanceFromConsPoint=getPathSimpleDistances (l1,l2);
			currDistVal =zeroOneRNG ()*(currDistanceFromConsPoint-minPctDistMultipleLocs*currDistanceFromConsPoint)+minPctDistMultipleLocs*currDistanceFromConsPoint;

			currKey=toString <int>(l1)+"_"+toString <int>(l2);
			distances.insert(pair<string,double>(currKey,currDistVal));

			if (l1==0)
			{
				currDistValConsPointReturn =zeroOneRNG ()*(currDistVal-minPctDistConspointReturn*currDistVal)+minPctDistConspointReturn*currDistVal;

				currKey=toString <int>(l2)+"_0";
				distances.insert(pair<string,double>(currKey,currDistValConsPointReturn));
			}
		}
		if (l1==0)
		{
			currDistanceFromConsPoint=getPathSimpleDistances (0,1);
			currDistValConsPointReturn =zeroOneRNG ()*(currDistanceFromConsPoint-minPctDistConspointReturn*currDistanceFromConsPoint)+minPctDistConspointReturn*currDistanceFromConsPoint;

			currKey="1_0";
			distances.insert(pair<string,double>(currKey,currDistValConsPointReturn));
		}
	}
}

bool isConslistSampled ()
{
	int i,j;
	//bool isIdentical;
	
	for (i=0;i<currNumberConsLists;i++)
	{
		//isIdentical =true;
		for (j=0;j<numberProducts;j++)
		{
			if (currConsList[j]!=consListsBool->m_A[i][j])
				break;
		}
		if (j==numberProducts)
			return true;
	}
	return false;
}

void sampleConsLists ()
{
	int i,j,k,l,prodPos;
	int consListSize;
	int consListFreq;
	currNumberConsLists=0;
	
	do 
	{
		consListSize =(rand() % (numberProducts-1))+1 ;
		
		for (j=0;j<numberProducts;j++)
			currConsList[j]=false;

		for (k=0;k<consListSize;k++)
		{
			prodPos=rand() % (numberProducts-1-k);

			j=0;
			for (l=0;l<prodPos;l++)
			{
				while (currConsList[j])
					j++;

				j++;
			}
			while (currConsList[j])
					j++;

			currConsList[j]=true;
		}

		if (!isConslistSampled ())
		{
			freqConsLists[currNumberConsLists]=(rand()%(maxConsListFreq-1))+1;
			for (j=0;j<numberProducts;j++)
			{
				consListsBool->m_A[currNumberConsLists][j]=currConsList [j];
				productsInConsLists[j]|=currConsList[j];
			}
			currNumberConsLists++;
		}
	}
	while (currNumberConsLists<numberConsLists);

	for (i=0;i<numberProducts;i++)
	{
		if (!productsInConsLists [i])
		{
			j=(rand() % numberConsLists);
			consListsBool->m_A[j][i]=true;
		}
	}
}

string buildFullFilename (const char *fName)
{
	string fileName;

	fileName=path;

	fileName+=fName;

	return fileName;
}
void writefiles ()
{
	int p,l,l1,l2,cL;
	string currKey;
	double currDistance;
	string fileName;
	CSVFile *productFile,*locationsFile,*distancesFile,*consListsFile,*consListsMetaDataFile;
	DatasourceRow *currRow;
	//char *currFileName;

	//currFileName=buildFullFilename(productsFileName);
	productFile=new CSVFile ((char*)buildFullFilename(productsFileName).c_str(),true);
	locationsFile =new CSVFile ((char*)buildFullFilename (locationsFileName).c_str(),true);
	distancesFile =new CSVFile ((char*)buildFullFilename (distancesFileName).c_str(),true);
	consListsFile =new CSVFile ((char*)buildFullFilename (consignationListsFileName).c_str(),true);
	consListsMetaDataFile= new CSVFile((char *) buildFullFilename (consignationListsMetaDataFileName).c_str (),true);

	currRow=new DatasourceRow ();
	currRow->numberRowEls =1;
	currRow->rowEls =new string [3];

	currRow->rowEls[0]=productsFileHeadLine[0];
	productFile->writeRow (currRow);
	for (p=0;p<numberProducts;p++)
	{
		currRow->rowEls[0]=toString <int>(p);
		productFile->writeRow (currRow);
	}

	currRow->rowEls[0]=locationsFileHeadLine[0];
	locationsFile->writeRow (currRow);
	for (l=0;l<=numberLocations;l++)
	{
		currRow->rowEls[0]=toString <int>(l);
		locationsFile->writeRow (currRow);
	}
	currRow->numberRowEls=3;
	currRow->rowEls[0]=distancesFileHeadLine[0];
	currRow->rowEls[1]=distancesFileHeadLine[1];
	currRow->rowEls[2]=distancesFileHeadLine[2];
	distancesFile->writeRow (currRow);
	for (l1=0;l1<numberLocations;l1++)
	{
		for (l2=l1+1;l2<=numberLocations;l2++)
		{
			currKey=toString <int>(l1)+"_"+toString <int>(l2);
			currDistance =distances[currKey];
			currRow->rowEls[0]=toString <int>(l1);
			currRow->rowEls[1]=toString <int>(l2);
			currRow->rowEls[2]=toString <double>(currDistance);
			distancesFile->writeRow (currRow);
		}
	}

	currRow->rowEls[1]="0";
	for (l=1;l<=numberLocations;l++)
	{
		currKey=toString<int>(l)+"_0";
		currDistance=distances[currKey];
		currRow->rowEls[0]=toString <int>(l);	
		currRow->rowEls[2]=toString <double>(currDistance);
		distancesFile->writeRow (currRow);
	}

	currRow->numberRowEls=2;
	
	currRow->rowEls[0]=consignationFileHeadLine[0];
	currRow->rowEls[1]=consignationFileHeadLine[1];
	consListsFile->writeRow (currRow);
	for (cL=0;cL<numberConsLists;cL++)
	{
		currRow->rowEls[0]=toString <int>(cL);
		for (p=0;p<numberProducts;p++)
		{
			if (consListsBool->m_A[cL][p])
			{
				currRow->rowEls[1]=toString <int>(p);
				consListsFile ->writeRow (currRow);
			}
		}
	}

	currRow->rowEls[0]=consignationListsMetaDataFileHeadLine[0];
	currRow->rowEls[1]=consignationListsMetaDataFileHeadLine[1];
	consListsMetaDataFile->writeRow (currRow);
	for (cL=0;cL<numberConsLists;cL++)
	{
		currRow->rowEls[0]=toString <int>(cL);
		currRow->rowEls[1]=toString <int>(freqConsLists[cL]);
		consListsMetaDataFile->writeRow (currRow);
	}

	delete currRow;
	delete productFile;
	delete locationsFile;
	delete distancesFile;
	delete consListsFile;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int i,j;
	srand(0);
	consListsBool=new Matrix_2d<bool>(numberConsLists,numberProducts );
	productsInConsLists=new bool [numberProducts];
	freqConsLists=new int [numberConsLists ];
	currConsList =new bool[numberProducts];
	for (i=0;i<numberProducts ;i++)
	{
		productsInConsLists [i]=false;
		for (j=0;j<numberConsLists;j++)
			consListsBool->m_A[j][i]=false;
	}

	sampleDistances ();
	sampleConsLists ();
	writefiles();

	return 0;
}

