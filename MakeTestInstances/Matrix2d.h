
#ifndef MATRIX_2D

#define MATRIX_2D

template <typename T>class Matrix_2d {

protected:
	unsigned length_X;
	unsigned length_Y;
	unsigned *vLength_Y;
	T* orig_A;
	void basicInit ()
	{
		orig_A =NULL;
		m_A=NULL;
		vLength_Y =NULL;
		length_X=length_Y =-1;
	}

public:
	T** m_A;

	Matrix_2d()
	{
		basicInit();
	}

	void build(unsigned lX,unsigned lY)
	{
		unsigned i,p;

		length_X=lX;
		length_Y=lY;

		orig_A=new T[length_X*lY];
		m_A=new T*[length_X];
		p=0;
		for (i=0;i<length_X;i++)
		{
			m_A[i]=&orig_A[p];
			p+=lY;
		}
	}

	Matrix_2d (unsigned lX,unsigned lY)
	{
		basicInit();
		build(lX,lY);
	}

	Matrix_2d (unsigned lX,unsigned *lY)
	{
		basicInit();
		build(lX,lY);
	}

	~Matrix_2d()
	{
		if (orig_A!=NULL)
			delete [] orig_A;

		if (m_A!=NULL)
			delete [] m_A;
	}

	void build (unsigned lX,unsigned *lY)
	{
		unsigned i,p;
		unsigned tLength;

		length_X=lX;
		vLength_Y =lY;

		tLength=0;
		for (i=0;i<length_X;i++)
			tLength+=lY[i];

		orig_A=new T[tLength];
			
		m_A=new T*[length_X];
		p=0;
		for (i=0;i<length_X;i++)
		{
			m_A[i]=&orig_A[p];
			p+=lY[i];
		}
	}

	unsigned LengthY(unsigned x)
	{
		if (vLength_Y !=NULL)
			return vLength_Y[x];

		return length_Y;
	}

	unsigned LengthX()
	{
		return length_X;
	}
};

#endif