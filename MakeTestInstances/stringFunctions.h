#include <string>
#include<sstream>

#ifndef STRINGFUNCTIONS

#define STRINGFUNCTIONS
using namespace std;
//using namespace System;

string *charDelete (string s,char c);
string *charReplace (string s,char oldChar,char newChar);

//char *fromSystemStringToCChar(String ^s);

//void fromSystemStringToStlString(String ^s, string & stlStr);

template<class T> T fromString(const string& s)
{
     istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

template <typename T> string toString (const T &t) {
	    ostringstream os;
	    os << t;
	    return os.str();
}

//template <> string toString<int> (const int &t);

template <> string toString<double> (const double &t);
//string doubleConvert (double v);

void setNumberDigits(int n);
string doubleToString(const double t);
#endif