﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Data;
using Logging;
using Optimizer_Data;

namespace KZO_Main
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            protected string[,] baseDataCSVSpec ={
                                            {"CurrentDate",CSV_File.DATETIME_TYPE,"dd.MM.yyyy"},
                                            {"NumberOfCovariates",CSV_File.INT_TYPE,""}
            */
            String configFileName="kzo_config.csv";
            String[,] inputColSpecs = {
                                        {"InputDataPath",CSV_File.STRING_TYPE,""}, //0
                                        {"OutputDataPath",CSV_File.STRING_TYPE,""}, //1
                                        {"MinimumNumberOfPicks",CSV_File.INT_TYPE,""}, //2
                                        {"MinDistanceForShift",CSV_File.DOUBLE_TYPE,""}, //3
                                        {"MaxDistanceForShift",CSV_File.DOUBLE_TYPE,""}, //4
                                        {"CostPerShift",CSV_File.DOUBLE_TYPE,""},      //5
                                        {"MaxNumberOfImprovements",CSV_File.INT_TYPE,""}, //6
                                        {"MaxNumberOfThreads",CSV_File.INT_TYPE,""},   //7
                                        {"Heuristic",CSV_File.STRING_TYPE,""},  //8
                                        {"ConsignmentArea",CSV_File.STRING_TYPE,""},  //9
                                        {"StartingPointLine",CSV_File.INT_TYPE,""},  //10
                                        {"StartingPointColumn",CSV_File.INT_TYPE,""},  //11
                                        };
            Int32 minimumNumberOfPicks;
            bool isIgnoreNotConsignedProducts = false;
            KZO_Data.DataProvider_Excel usedDP;
            KZO_Data.ConsignationProblem currProblem;
            KZO_Data.ConsignationSolution existingSolution,newSolution,initialSolution;
            KZO_Problem.HeuristicParams usedParams;
            DataTable configDataTab;
            CSV_File configFile;
            String consignmentAreaName;
            Int32 startingPointLine,startingPointCol;
            String usedDataPath;// = Environment.CurrentDirectory+"\\Stammdaten";//"C:\\PM\\KTO\\TestWorkingDir\\Stammdaten";//"C:\\PM\\KTO\\TestWorkingDir\\Stammdaten";
            String usedOutputPath;// = Environment.CurrentDirectory + "\\Ergebnis";//"C:\\PM\\KTO\\TestWorkingDir\\Stammdaten\\storedData3";

            configFile = new CSV_File(Environment.CurrentDirectory + Path .DirectorySeparatorChar +configFileName);
            configFile.IsHeadLine = true;
            configFile.setInputColSpecsByStrings(inputColSpecs);
            configFile.InputCultureForConversions = CultureInfo.CreateSpecificCulture("en-US");
            configFile.CSVDelimiter = ";";
            configFile.openForReading();
            configDataTab = configFile.getData();
            configFile.closeDoc();

            ScreenLogger sL;
            LoggerCollection lC;

            lC = new LoggerCollection();

            sL = new ScreenLogger();
            lC.addLogger(sL);

            usedParams = new KZO_Problem.HeuristicParams();
            //usedParams.ImprovementHeuristic = KZO_Problem.HeuristicParams.ImprovementHeuristicType.H2_OPT_GREEDY ;
            usedDataPath = Path.GetFullPath ((String) configDataTab.Rows[0][inputColSpecs[0, 0]]);//Environment.CurrentDirectory+"\\Stammdaten";
            usedOutputPath = Path.GetFullPath((String)configDataTab.Rows[0][inputColSpecs[1, 0]]);//Environment.CurrentDirectory + "\\Ergebnis";
            consignmentAreaName=(String) configDataTab.Rows[0][inputColSpecs[9, 0]];
            switch ((String) configDataTab.Rows[0][inputColSpecs[8, 0]])
            {
                case "H2_OPT_GREEDY":
                    usedParams.ImprovementHeuristic = KZO_Problem.HeuristicParams.ImprovementHeuristicType.H2_OPT_GREEDY;
                    break;
                case "H2_OPT_BEST":
                    usedParams.ImprovementHeuristic = KZO_Problem.HeuristicParams.ImprovementHeuristicType.H2_OPT_BEST;
                    break;
                case "H2_OPT_GREEDY_SA":
                    usedParams.ImprovementHeuristic = KZO_Problem.HeuristicParams.ImprovementHeuristicType.H2_OPT_GREEDY_SA;
                    break;
            }
            usedParams.MaxNumberOfImprovements = (Int32) configDataTab.Rows[0][inputColSpecs[6, 0]];//30000;
            usedParams.MinDistanceForShift = (Double)configDataTab.Rows[0][inputColSpecs[3, 0]];// 10;
            usedParams.MaxDistanceForShift = (Double)configDataTab.Rows[0][inputColSpecs[4, 0]];//10000;

            usedParams.ShiftCost = (Double)configDataTab.Rows[0][inputColSpecs[5, 0]];//0;
            usedParams.MaxNumberOfThreads = (Int32)configDataTab.Rows[0][inputColSpecs[7, 0]];//10;
            usedDP = new KZO_Data.DataProvider_Excel(usedDataPath,CultureInfo.CurrentCulture,lC);
            minimumNumberOfPicks = (Int32) configDataTab.Rows[0][inputColSpecs[2, 0]];//100;
            startingPointLine = (Int32)configDataTab.Rows[0][inputColSpecs[10, 0]];
            startingPointCol = (Int32)configDataTab.Rows[0][inputColSpecs[11, 0]];
            usedDP.setProblemToGet(consignmentAreaName, startingPointLine, startingPointCol);//("K51");//("K01");
            usedDP.load();
            List<String> pIds = usedDP.getAllProblemIds();

            
            
            currProblem = usedDP.getConsignationProblem(isIgnoreNotConsignedProducts, minimumNumberOfPicks);
            existingSolution = usedDP.getExistingSolution(currProblem, isIgnoreNotConsignedProducts, minimumNumberOfPicks);

            KZO_Problem.ConsignmentProblemSolver t;

            t = new KZO_Problem.ConsignmentProblemSolver();
            t.Path_s(usedOutputPath);
            t.setHeuristicOptimizer(usedParams);
            t.setProblem(currProblem);
            //t.evaluateSolution(existingSolution);
            t.setStartingSolution(existingSolution);
            newSolution=t.optimize();
            initialSolution = t.getStartSolution();
           // usedParams.ImprovementHeuristic = KZO_Problem.HeuristicParams.ImprovementHeuristicType.H2_OPT_GREEDY_SA;
            //t.setHeuristicOptimizer(usedParams);
            //t.setProblem(currProblem);
            //t.setStartingSolution(newSolution);
            //newSolution = t.optimize();
            //intialSolution=.
            usedDP.outputResult(usedOutputPath, currProblem, newSolution, initialSolution);//("C:\\PM\\KTO\\TestWorkingDir\\Stammdaten\\storedData2",currProblem, newSolution,initialSolution);
        }
    }
}
