#include <fstream>
#include <string>
#include "DataTable.h"

//using namespace std;

#ifndef CSVFILE
#define CSVFILE

#define MAXLINELENGTH 1024
#define CSVSEPARATOR ';'
/*
class CSVLine {

public:
	string *csvEls;
	int numberCSVEls;

	~CSVLine();
};*/

class CSVFile : public GeneralDataSource {

private: 
	std::ifstream f;
	std::ofstream of;
	DatasourceTable *dT;
	bool isFirstLine;
	bool isTitleLine;
public: 
	CSVFile (char* fileName,bool isWrite);
	~CSVFile();
	virtual void resetRead();
	virtual DatasourceRow *getRow();
	DatasourceRow *getSingleRow();
	virtual int writeRow (DatasourceRow *l);
	virtual DatasourceTable *getDatasourceTable();
	virtual bool isDataSourceWithTitleLine ();
	void setIsTitleLine (bool v);
};
#endif