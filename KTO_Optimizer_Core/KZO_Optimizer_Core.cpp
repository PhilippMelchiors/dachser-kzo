// KTO_Optimizer_Core.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "CSVFile.h"
#include "KZO_Opt_Core_CPLEX.h"
#include "KZO_Opt_Core_Heuristic.h"
#include "stringFunctions.h"

const char *productsFileName="products.csv";
const char *locationsFileName="locations.csv";
const char *distancesFileName="distances.csv";
const char *consignationListsFileName="consignationlists.csv";
const char *consignationListsMetaDataFileName="consignationlistsMetaData.csv";

KZO::Product *products;
KZO::ConsignmentLocation *locations;
KZO::ConsignmentLocationsDistance *distances;
KZO::ConsignmentListType *consLists;

void loadDataFromDataSources (KZO::KZO_OptimizationEngine *currEngine,
							  GeneralDataSource *productsDS,
							  GeneralDataSource *locationsDS,
							  GeneralDataSource *distancesDS,
							  GeneralDataSource *consignationListsDS,
							  GeneralDataSource *consignationListsMetaDataDS
							  )
{
	DatasourceTable *prodData,*locsData,*distsData,*consListsData,*consListsMetaData;
	//KZO::Product *currProduct;
	
	string *currCellContent;
	int i;
	int currNum;
	int numberOfConsLists;
	prodData=productsDS->getDatasourceTable();
	products=new KZO::Product [prodData->getNumberOfRows ()];
	for (i=0;i<prodData->getNumberOfRows ();i++)
	{
		//currProduct=new KZO::Product ();
		currCellContent=prodData->getCellContent (i,0);
		products[i].Number_s (fromString <int>(*currCellContent));

		currEngine->addProduct (&products[i]);
	}

	locsData=locationsDS->getDatasourceTable();
	locations =new KZO::ConsignmentLocation [locsData->getNumberOfRows ()];

	for (i=0;i<locsData->getNumberOfRows ();i++)
	{
		//currLocation=new KZO::ConsignmentLocation ();
		currCellContent =locsData->getCellContent (i,0);
		locations[i].Number_s (fromString<int>(*currCellContent));
		//currCellContent =locsData->getCellContent (i,1);
		//locations[i].Cap_s (fromString <double>(*currCellContent));

		currEngine->addConsignmentLocation (&locations [i]);
	}

	distsData =distancesDS->getDatasourceTable ();
	distances=new KZO::ConsignmentLocationsDistance [distsData->getNumberOfRows ()];
	for (i=0;i<distsData->getNumberOfRows ();i++)
	{
		//currDistance=new KZO::ConsignmentLocationsDistance ();
		currCellContent =distsData->getCellContent (i,0);
		distances[i].Loc1_s (fromString <int>(*currCellContent ));
		currCellContent =distsData->getCellContent (i,1);
		distances[i].Loc2_s (fromString <int>(*currCellContent ));
		currCellContent =distsData->getCellContent (i,2);
		distances[i].Cost_s (fromString <double>(*currCellContent ));

		currEngine->addDistance (&distances[i]);
	}

	consListsMetaData =consignationListsMetaDataDS ->getDatasourceTable ();
	consListsData=consignationListsDS->getDatasourceTable ();
	
	consLists=new KZO::ConsignmentListType [consListsMetaData->getNumberOfRows ()];
	for (i=0;i<consListsMetaData->getNumberOfRows ();i++)
	{
		currCellContent=consListsMetaData->getCellContent (i,0);
		currNum=fromString <int>(*currCellContent );
		consLists[currNum].Number_s (currNum);
		currCellContent=consListsMetaData->getCellContent (i,1);
		consLists [currNum].Frequency_s (fromString <int>(*currCellContent));
	}
	
	for (i=0;i<consListsData->getNumberOfRows ();i++)
	{
		currCellContent =consListsData->getCellContent (i,0);
		currNum =fromString <int>(*currCellContent);
		currCellContent =consListsData->getCellContent (i,1);
		consLists [currNum].addProduct (fromString <int>(*currCellContent));
	}
	for (i=0;i<consListsMetaData->getNumberOfRows ();i++)
	{
		currEngine->addConsignmentList(&consLists [i]);
	}
}

void loadData (KZO::KZO_OptimizationEngine *currEngine)
{
	CSVFile *productsFile,*locationsFile,*distancesFile,*consignationListsFile,*consignationListsMetaDataFile;

	productsFile=new CSVFile ((char *)productsFileName,false);

	locationsFile=new CSVFile ((char *)locationsFileName,false);

	distancesFile=new CSVFile ((char *)distancesFileName ,false);
	
	consignationListsFile =new CSVFile ((char *)consignationListsFileName,false);

	consignationListsMetaDataFile=new CSVFile((char *) consignationListsMetaDataFileName,false);
	
	productsFile->setIsTitleLine (true);
	locationsFile->setIsTitleLine (true);
	distancesFile->setIsTitleLine (true);
	consignationListsFile ->setIsTitleLine (true);
	consignationListsMetaDataFile ->setIsTitleLine (true);

	loadDataFromDataSources (currEngine,productsFile,locationsFile,distancesFile,consignationListsFile,consignationListsMetaDataFile);
	
}

int _tmain(int argc, _TCHAR* argv[])
{
	KZO::KZO_OptimizationEngine_CPLEX *currOptimizer;
	KZO::KZO_OptimizationEngine_Heuristic *currHeuristicOptimizer;
	KZO::ConsignmentListType l1,l2,l3,l4,l5;
	KZO::Product p1,p2,p3,p4;
	KZO::ConsignmentLocation cL1,cL2,cL3,cL4;

	KZO::ConsignmentLocationsDistance d01,d02,d03,d12,d13,d23,d10,d20,d30;
	KZO::KZO_OptimizationEngine_Heuristic_Params currParams;
	/*l1=new KZO::ConsignmentListType ();
	l2=new KZO::ConsignmentListType ();
	l3=new KZO::ConsignmentListType ();
	l4=new KZO::ConsignmentListType ();
	l5=new KZO::ConsignmentListType ();

	p1=new KZO::Product ();
	p2=new KZO::Product ();
	p3=new KZO::Product ();
	p4=new KZO::Product ();*/

	//p1.Number_s (0);
	//p2.Number_s (1);
	//p3.Number_s (2);
	////p4.Number_s (3);

	//l1.addProduct (0);
	//l1.Number_s (0);
	//l1.Frequency_s (1);

	//l2.addProduct (0);
	//l2.addProduct (2);
	//l2.Number_s (1);
	//l2.Frequency_s (1);

	//l3.addProduct (2);
	////l3.addProduct (3);
	//l3.Number_s (2);
	//l3.Frequency_s (1);

	//l4.addProduct (0);
	//l4.addProduct (1);
	//l4.addProduct (2);
	//l4.Number_s (3);
	//l4.Frequency_s (1);

	//l5.addProduct (1);
	//l5.addProduct (2);
	//l5.Number_s (4);
	//l5.Frequency_s (1);

	/*
	cL1=new KZO::ConsignmentLocation (0,0);
	cL2=new KZO::ConsignmentLocation (0,1);
	cL3=new KZO::ConsignmentLocation (0,2);
	cL4=new KZO::ConsignmentLocation (0,3);
	*/
	//cL1.ConsigmentCost_s (0);
	//cL1.Number_s (0);

	////cL2.ConsigmentCost_s (0);
	//cL2.Number_s (1);

	////cL3.ConsigmentCost_s (0);
	//cL3.Number_s (2);

	////cL4.ConsigmentCost_s (0);
	//cL4.Number_s (3);


	//d01.Loc1_s (0);
	//d01.Loc2_s (1);
	//d01.Cost_s (1.0);

	//d12.Loc1_s (1);
	//d12.Loc2_s (2);
	//d12.Cost_s (2.5);

	//d23.Loc1_s (2);
	//d23.Loc2_s (3);
	//d23.Cost_s (1.5);

	//d13.Loc1_s (1);
	//d13.Loc2_s (3);
	//d13.Cost_s (3.5);

	//d02.Loc1_s (0);
	//d02.Loc2_s (2);
	//d02.Cost_s (2.8);

	//d03.Loc1_s (0);
	//d03.Loc2_s (3);
	//d03.Cost_s (5);

	//d10.Loc1_s (1);
	//d10.Loc2_s (0);
	//d10.Cost_s (0.8);

	//d20.Loc1_s (2);
	//d20.Loc2_s (0);
	//d20.Cost_s (2.75);

	//d30.Loc1_s (3);
	//d30.Loc2_s (0);
	//d30.Cost_s (4.75);

	
	currHeuristicOptimizer =new KZO::KZO_OptimizationEngine_Heuristic (&currParams);

	loadData (currHeuristicOptimizer);
	currHeuristicOptimizer->buildStructures ();
	
	currHeuristicOptimizer->optimize ();
	
	
	currOptimizer=new KZO::KZO_OptimizationEngine_CPLEX ();
	loadData (currOptimizer);
	currOptimizer->buildStructures ();
	
	currOptimizer->optimize ();
	
	/*currHeuristicOptimizer->addProduct (&p1);
	currHeuristicOptimizer->addProduct (&p2);
	currHeuristicOptimizer->addProduct (&p3);
	
	currHeuristicOptimizer->addConsignmentLocation (&cL1);
	currHeuristicOptimizer->addConsignmentLocation (&cL2);
	currHeuristicOptimizer->addConsignmentLocation (&cL3);
	currHeuristicOptimizer->addConsignmentLocation (&cL4);

	currHeuristicOptimizer->addDistance (&d01);
	currHeuristicOptimizer->addDistance (&d02);
	currHeuristicOptimizer->addDistance (&d03);
	currHeuristicOptimizer->addDistance (&d12);
	currHeuristicOptimizer->addDistance (&d23);
	currHeuristicOptimizer->addDistance (&d13);
	currHeuristicOptimizer->addDistance (&d10);
	currHeuristicOptimizer->addDistance (&d20);
	currHeuristicOptimizer->addDistance (&d30);

	currHeuristicOptimizer->addConsignmentList (&l1);
	currHeuristicOptimizer->addConsignmentList (&l2);
	currHeuristicOptimizer->addConsignmentList (&l3);
	currHeuristicOptimizer->addConsignmentList (&l4);
	currHeuristicOptimizer->addConsignmentList (&l5);*/

	
	
	/*
	currOptimizer=new KZO::KZO_OptimizationEngine_CPLEX ();

	currOptimizer->addProduct (&p1);
	currOptimizer->addProduct (&p2);
	currOptimizer->addProduct (&p3);
	//currOptimizer->addProduct (&p4);
	
	currOptimizer->addConsignmentLocation (&cL1);
	currOptimizer->addConsignmentLocation (&cL2);
	currOptimizer->addConsignmentLocation (&cL3);
	currOptimizer->addConsignmentLocation (&cL4);

	currOptimizer->addDistance (&d01);
	currOptimizer->addDistance (&d02);
	currOptimizer->addDistance (&d03);
	currOptimizer->addDistance (&d12);
	currOptimizer->addDistance (&d23);
	currOptimizer->addDistance (&d13);
	currOptimizer->addDistance (&d10);
	currOptimizer->addDistance (&d20);
	currOptimizer->addDistance (&d30);

	currOptimizer->addConsignmentList (&l1);
	currOptimizer->addConsignmentList (&l2);
	currOptimizer->addConsignmentList (&l3);
	currOptimizer->addConsignmentList (&l4);
	currOptimizer->addConsignmentList (&l5);
	currOptimizer->buildStructures ();
	
	currOptimizer->optimize ();
	*/
	return 0;
}

