#include "stdafx.h"
#include "CSVFile.h"
#include<iostream>
CSVFile::CSVFile (char* filename,bool isWrite)
{
	std::cout<<filename<<std::endl;
	dT=NULL;
	if (!isWrite)
		f.open(filename,std::fstream::in);
	else
		of.open(filename,std::fstream::out);

	isFirstLine=true;
	isTitleLine=true;
}
CSVFile::~CSVFile()
{
	f.close();
}

void CSVFile::resetRead()
{
	f.clear();
	f.seekg(0);
	isFirstLine=true;
}

DatasourceRow * CSVFile::getRow()
{
	DatasourceRow *cLine;
	if (isFirstLine)
	{
		cLine=getSingleRow(); //Überlesen der Titelzeile, um Kompatibilität zu Datenbanken herzustellen
		if (cLine!=NULL)
			isFirstLine=false;
	}
	return getSingleRow();
}

DatasourceRow * CSVFile::getSingleRow()
{
	char line[MAXLINELENGTH],subStr[MAXLINELENGTH];
	DatasourceRow *cLine;
	int numberLineEls,i,j,oldJ;
	
	cLine=new DatasourceRow();
	if (f.eof())
	{
		delete cLine;
		return NULL;
	}
	f.getline(line, MAXLINELENGTH);

	numberLineEls=1;
	for (i=0;i<MAXLINELENGTH;i++)
	{
		if (line[i]==0)
			break;
		else if (line[i]==CSVSEPARATOR)
			numberLineEls++;
	}
	cLine->numberRowEls=numberLineEls;
	cLine->rowEls=new std::string [numberLineEls];

	j=0;
	for (i=0;i<numberLineEls;i++)
	{
		oldJ=j;
		while (line[j]!=CSVSEPARATOR && line[j]!=0)
		{
			subStr[j-oldJ]=line[j];
			j++;
		}
		subStr[j-oldJ]=0;

		cLine->rowEls[i].append(subStr);
		j++; //Separator überspringen
	}

	return cLine;
}

int CSVFile::writeRow (DatasourceRow *l)
{
	int i;
	for (i=0;i<l->numberRowEls;i++)
	{
		of<<l->rowEls[i];
		if (i<l->numberRowEls-1)
			of<<";";
	}
	of<<std::endl;
	return 0;
}

DatasourceTable *CSVFile::getDatasourceTable()
{
	//DatasourceTable *nT;
	DatasourceRow *currDatasourceRow;
	
	if (dT!=NULL)
		delete dT;
	
	dT=new DatasourceTable ();

	bool isNotEOF;
	int i;
	do {
		 isNotEOF=false;
		 currDatasourceRow=getRow();
		 if (currDatasourceRow!=NULL && !(currDatasourceRow->numberRowEls==0 || (currDatasourceRow->numberRowEls==1 && currDatasourceRow->rowEls[0]=="")))
		 {
			dT->addRow(currDatasourceRow);
			isNotEOF=true;
		 }
	}
	while (isNotEOF);

	return dT;
}

bool CSVFile::isDataSourceWithTitleLine () 
{ 
	return isTitleLine; //DB-Relation braucht keine Titelzeile
}

void CSVFile::setIsTitleLine (bool v)
{
	isTitleLine=v;
	isFirstLine=v;
}