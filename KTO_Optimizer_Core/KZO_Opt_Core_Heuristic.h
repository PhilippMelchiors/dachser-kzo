#include "KZO_Opt_Core.h"
#include "Matrix2d.h"
#include<string>
#include<set>
#include <unordered_map>

namespace KZO
{
	class KZO_ExchangeCombination 
	{
	public:
		int p1;
		int p2;
		double v;
	};
	enum OptimizationScheme
	{
		STARTHEURISTIC_IMPROVEMENTHEURISTIC
	};
	enum StartHeuristic
	{
		SIMPLE_FREQUENCY_ORDER,
		MODIFIED_CONSIGMENT_LIST_TYPES
	};
	enum ImprovementHeuristic
	{
		H2_OPT
	};
	class KZO_OptimizationEngine_Heuristic_Params
	{
	public:
		OptimizationScheme usedScheme;
		StartHeuristic usedStartHeuristic;
		ImprovementHeuristic usedImprovementHeuristic;
		int maxNumberOfImprovementIterations;
		
		KZO_OptimizationEngine_Heuristic_Params();
	};
	class KZO_OptimizationEngine_Heuristic : public KZO_OptimizationEngine 
	{
		protected:
			std::list<KZO::Product *> pList;
			std::list<KZO::ConsignmentLocation *>cLocations;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			std::list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;
			KZO_OptimizationEngine_Heuristic_Params *usedParams;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			int *locationsByProduct;
			int *productsByLocation;
			std::list<int>*sortedProductsByLocation;
			std::list<int>*consListsByProduct;
			int *numberOfConsignationsByProduct;

			void basicInitialize();
			void simpleSortInitialization();
			void improveBy2Opt();
			void prepareVariables();
			double objFunction();
			void assignProductToLocation (int p, int l);
			void unassignProductToLocation (int p,int l);
			double exchangeCostDiff(int p1,int l1,int p2,int l2);
			KZO::Optimization_Result *getResult();
			bool isSolution;
		public:
			void resetSolution ();
			KZO_OptimizationEngine_Heuristic();
			KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params);
			~KZO_OptimizationEngine_Heuristic();
			void Params_s(KZO_OptimizationEngine_Heuristic_Params *params);
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (std::list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (std::list<ProductionLocationAssignment> &assignments);
	};
}