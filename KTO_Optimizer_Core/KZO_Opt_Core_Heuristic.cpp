#include "stdafx.h"
#include "KZO_Opt_Core_Heuristic.h"
#include "stringFunctions.h"
#include <iostream>

KZO::KZO_OptimizationEngine_Heuristic_Params::KZO_OptimizationEngine_Heuristic_Params()
{
	maxNumberOfImprovementIterations=-1;
	usedScheme =OptimizationScheme::STARTHEURISTIC_IMPROVEMENTHEURISTIC;
	usedStartHeuristic=StartHeuristic::SIMPLE_FREQUENCY_ORDER;
	usedImprovementHeuristic=ImprovementHeuristic::H2_OPT;
}

void KZO::KZO_OptimizationEngine_Heuristic::basicInitialize()
{
	xVars =NULL;
	locationsByNums=NULL;
	productsByNums =NULL;
	consListsByNums =NULL;
	numberOfConsignationsByProduct=NULL;
	isSolution =false;
}
KZO::KZO_OptimizationEngine_Heuristic::KZO_OptimizationEngine_Heuristic()
{	
	basicInitialize();
}

KZO::KZO_OptimizationEngine_Heuristic::KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params)
{
	basicInitialize ();
	usedParams =params;
}

KZO::KZO_OptimizationEngine_Heuristic::~KZO_OptimizationEngine_Heuristic()
{
	if (xVars!=NULL)
		delete xVars;
	if (locationsByNums!=NULL)
		delete [] locationsByNums;
	if (productsByNums !=NULL)
		delete [] productsByNums;
	if (consListsByNums !=NULL)
		delete [] consListsByNums;
}

void KZO::KZO_OptimizationEngine_Heuristic::Params_s(KZO_OptimizationEngine_Heuristic_Params *params)
{
	usedParams =params;
}

void KZO::KZO_OptimizationEngine_Heuristic::addProduct(Product *p)
{
	pList.push_back (p);
}
void KZO::KZO_OptimizationEngine_Heuristic::addConsignmentLocation (ConsignmentLocation *cL)
{
	cLocations.push_back (cL);
}
void KZO::KZO_OptimizationEngine_Heuristic::addDistance (ConsignmentLocationsDistance  *d)
{
	std::string key;

	key=toString<int>(d->Loc1_g())+"_"+toString<int>(d->Loc2_g());
	cLDistances.insert(pair<string,ConsignmentLocationsDistance  *>(key,d));
}
void KZO::KZO_OptimizationEngine_Heuristic::addConsignmentList(ConsignmentListType *cListType)
{
	consLists.push_back (cListType);
}

double KZO::KZO_OptimizationEngine_Heuristic::exchangeCostDiff(int p1,int l1,int p2,int l2)
{
	list<int>::iterator itP,itPEnd;
	int lastLoc,currCL;
	int oldPredLocDeltaP,newPredLocDeltaP;
	int oldSuccLocDeltaP,newSuccLocDeltaP;
	bool isDeltaPLocLast;
	bool isGreaterPosFound;
	bool isP1,isP2;
	string key;
	int deltaProduct;
	int deltaPNewLoc,deltaPOldLoc;
	double distDiff;
	double totalDiffCost;
	KZO::ConsignmentLocationsDistance *currDistance;

	totalDiffCost=0;
	for (currCL=0;currCL<consLists.size();currCL++)
	{
		if (consListsByNums[currCL]->Frequency_g ()>0)
		{
			isP1=consListsByNums[currCL]->isProductInList (p1);
			isP2=consListsByNums[currCL]->isProductInList(p2);

			if ((isP1 || isP2) && !(isP1 && isP2) )
			{
				if (isP1)
				{
					deltaProduct=p1;
					deltaPOldLoc=l1;
					deltaPNewLoc=l2;
				}
				else
				{
					deltaProduct=p2;
					deltaPOldLoc=l2;
					deltaPNewLoc=l1;
				}

				isDeltaPLocLast =false;
				isGreaterPosFound=false;
				itP=sortedProductsByLocation[currCL].begin();
				itPEnd=sortedProductsByLocation[currCL].end();

				newSuccLocDeltaP=newPredLocDeltaP=oldSuccLocDeltaP=oldPredLocDeltaP=0;

				lastLoc=0;
				while (itP!=itPEnd)
				{
					if (*itP==deltaProduct)
					{
						oldPredLocDeltaP=lastLoc ;
						isDeltaPLocLast=true;
					}
					else
					{
						if (isDeltaPLocLast)
							oldSuccLocDeltaP =locationsByProduct[*itP];
						isDeltaPLocLast =false;
					}

					if (lastLoc!=deltaPOldLoc && !isGreaterPosFound)
						newPredLocDeltaP =lastLoc;

					if (locationsByProduct [*itP]>deltaPNewLoc && *itP!=deltaProduct && !isGreaterPosFound)
					{	
						isGreaterPosFound=true;
						newSuccLocDeltaP=locationsByProduct[*itP];
						//newPredLocDeltaP =lastLoc;
					}

					if (newSuccLocDeltaP>0 && oldSuccLocDeltaP>0)
						break;
				
					lastLoc=locationsByProduct [*itP];
					itP++;
				}
		
				if (lastLoc!=deltaPOldLoc && !isGreaterPosFound)
						newPredLocDeltaP =lastLoc;

				key=toString<int>(newPredLocDeltaP)+"_"+toString (deltaPNewLoc);

				currDistance=cLDistances [key];
				distDiff=currDistance->Cost_g ();

				key=toString<int>(deltaPNewLoc)+"_"+toString<int>(newSuccLocDeltaP);
				currDistance=cLDistances[key];
				distDiff+=currDistance->Cost_g ();

				if (oldPredLocDeltaP !=oldSuccLocDeltaP && oldPredLocDeltaP !=newPredLocDeltaP && oldSuccLocDeltaP !=newSuccLocDeltaP)
				{
					key=toString<int>(oldPredLocDeltaP)+"_"+toString<int>(oldSuccLocDeltaP);
					currDistance=cLDistances[key];
					distDiff+=currDistance->Cost_g ();
				}
				key=toString<int>(oldPredLocDeltaP)+"_"+toString (deltaPOldLoc);

				currDistance=cLDistances [key];
				distDiff-=currDistance->Cost_g ();

				key=toString<int>(deltaPOldLoc)+"_"+toString<int>(oldSuccLocDeltaP);
				currDistance=cLDistances[key];
				distDiff-=currDistance->Cost_g ();

				if (newPredLocDeltaP !=newSuccLocDeltaP && oldPredLocDeltaP !=newPredLocDeltaP && oldSuccLocDeltaP !=newSuccLocDeltaP)
				{
					key=toString<int>(newPredLocDeltaP)+"_"+toString<int>(newSuccLocDeltaP);
					currDistance=cLDistances[key];
					distDiff-=currDistance->Cost_g ();
				}
				totalDiffCost+=distDiff*consListsByNums[currCL]->Frequency_g ();
			}
		}
		//itCL++;
	}

	return totalDiffCost;
}

double KZO::KZO_OptimizationEngine_Heuristic::objFunction()
{
	int currCL;
	double objVal,currCLDistanceCost;
	int lastLoc;
	list<int>::iterator itP,itPEnd;
	string currDistanceKey;
	ConsignmentLocationsDistance *currDistance;
	objVal=0;
	bool isFirst;

	for (currCL=0;currCL<consLists.size();currCL++)
	{
		itP=sortedProductsByLocation[currCL].begin ();
		itPEnd=sortedProductsByLocation[currCL].end ();
		isFirst=true;
		currCLDistanceCost=0;
		while (itP!=itPEnd)
		{
			if (isFirst)
			{	
				currDistanceKey="0_"+toString<int>(locationsByProduct[*itP]);
				currDistance=cLDistances[currDistanceKey];

				isFirst=false;
			}
			else
			{
				currDistanceKey =toString <int>(lastLoc)+"_"+toString<int>(locationsByProduct[*itP]);
				currDistance=cLDistances[currDistanceKey];
			}
			currCLDistanceCost+=currDistance->Cost_g ();
			lastLoc=locationsByProduct[*itP];

			itP++;
		}

		currDistanceKey=toString<int>(lastLoc)+"_0";
		currDistance=cLDistances[currDistanceKey];
		currCLDistanceCost +=currDistance->Cost_g ();

		objVal+=currCLDistanceCost*consListsByNums [currCL]->Frequency_g();
	}

	return objVal;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::getResult()
{
	int p;
	double objVal;
	KZO::ProductionLocationAssignment currAssignment;
	KZO::Optimization_Result *currResult;

	currResult=new Optimization_Result ();

	for (p=0;p<pList.size();p++)
	{
		currAssignment.p =productsByNums[p];
		currAssignment.l=locationsByNums [locationsByProduct [p]];
		currResult->addAssignemnt (currAssignment);
	}

	currResult ->Cost_s (objFunction ());

	return currResult;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::optimize()
{	
	double objVal;

	if (usedParams->usedScheme ==OptimizationScheme::STARTHEURISTIC_IMPROVEMENTHEURISTIC)
	{
		if (!isSolution)
		{
			switch (usedParams->usedStartHeuristic) {
				case StartHeuristic::SIMPLE_FREQUENCY_ORDER :
					simpleSortInitialization();
					break;
				case StartHeuristic::MODIFIED_CONSIGMENT_LIST_TYPES :
					break;
			}
		}
		objVal=objFunction ();
		std::cout<<"Zielfunktionswert:"<<objVal <<endl;
		switch (usedParams->usedImprovementHeuristic) {
			case ImprovementHeuristic ::H2_OPT :
				improveBy2Opt ();
		}

		return getResult ();
	}

	return NULL;
}
void KZO::KZO_OptimizationEngine_Heuristic::clear()
{
	pList.clear ();
	cLocations.clear ();
	cLDistances.clear ();
}
void KZO::KZO_OptimizationEngine_Heuristic::prepareVariables()
{
}
void KZO::KZO_OptimizationEngine_Heuristic::resetSolution ()
{
	int i;

	for (i=0;i<pList.size();i++)
		locationsByProduct [i]=-1;
	
	for (i=0;i<cLocations.size();i++)
	{
		productsByLocation [i]=-1;
		sortedProductsByLocation [i].clear ();
	}

	isSolution =false;
}
void KZO::KZO_OptimizationEngine_Heuristic::assignProductToLocation (int p, int l)
{
	list<int>::iterator itCL,itCLEnd,itPL,itPLEnd;
	int currCL;
	locationsByProduct[p]=l;
	productsByLocation[l]=p;

	itCL=consListsByProduct[p].begin ();
	itCLEnd =consListsByProduct[p].end();

	while(itCL!=itCLEnd)
	{
		currCL=*itCL;

		itPL=sortedProductsByLocation[currCL].begin ();
		itPLEnd=sortedProductsByLocation[currCL].end();

		while (itPL!=itPLEnd)
		{
			if (locationsByProduct[*itPL]>locationsByProduct[p])
				break;

			itPL++;
		}

		if (itPL==itPLEnd)
			sortedProductsByLocation[currCL].push_back (p);
		else
			sortedProductsByLocation[currCL].insert (itPL,p);

		itCL++;
	}
}

void KZO::KZO_OptimizationEngine_Heuristic::unassignProductToLocation (int p,int l)
{
	list<int>::iterator itCL,itCLEnd;
	int currCL;
	locationsByProduct[p]=-1;
	productsByLocation[l]=-1;

	itCL=consListsByProduct[p].begin ();
	itCLEnd =consListsByProduct[p].end();

	while(itCL!=itCLEnd)
	{
		currCL=*itCL;

		sortedProductsByLocation [currCL].remove (p);
		itCL++;
	}
}

void KZO::KZO_OptimizationEngine_Heuristic::buildStructures()
{
	unsigned l,p,currCl;
	list<ConsignmentLocation *>::iterator locIt,locItEnd;
	list<Product *>::iterator pIt,pItEnd;
	list<ConsignmentListType *>::iterator cLIt,cLItEnd;

	if (locationsByNums !=NULL)
		delete [] locationsByNums;

	locationsByNums =new KZO::ConsignmentLocation *[cLocations.size()];

	locIt=cLocations .begin ();
	locItEnd=cLocations .end ();

	while(locIt !=locItEnd )
	{
		locationsByNums [(*locIt)->Number_g ()]=*locIt;
		locIt++;
	}

	if (productsByNums !=NULL)
		delete [] productsByNums;

	productsByNums =new KZO::Product *[pList.size ()];

	pIt=pList.begin ();
	pItEnd =pList.end ();

	while (pIt!=pItEnd )
	{
		productsByNums[(*pIt)->Number_g ()]=*pIt;
		pIt++;
	}

	if (consListsByNums !=NULL)
		delete [] consListsByNums;

	consListsByNums =new ConsignmentListType *[consLists .size()];
	cLIt=consLists .begin ();
	cLItEnd =consLists .end();

	while(cLIt !=cLItEnd )
	{
		consListsByNums[(*cLIt)->Number_g ()]=*cLIt;
		cLIt++;
	}

	locationsByProduct=new int [pList.size ()];
	productsByLocation=new int [cLocations.size()];
	sortedProductsByLocation=new std::list<int>[consLists.size()];    
	consListsByProduct=new std::list<int>[pList.size()];
	for (p=0;p<pList.size();p++)
	{
		locationsByProduct[p]=-1;
		for (currCl=0;currCl<consLists.size();currCl++)
		{
			if (consListsByNums[currCl]->isProductInList (p))
				consListsByProduct[p].push_back (currCl);
		}
	}

	for (l=0;l<cLocations.size();l++)
		productsByLocation[l]=-1;

	if (numberOfConsignationsByProduct!=NULL)
		delete [] numberOfConsignationsByProduct;

	numberOfConsignationsByProduct=new int [pList.size ()];
}



void KZO::KZO_OptimizationEngine_Heuristic::simpleSortInitialization()
{
	int p;
	int lmin,l;
	unsigned pMax;
	int pNumClMax;
	list<int>::iterator itCL,itCLEnd;

	for (p=0;p<pList .size ();p++)
	{
		itCL=consListsByProduct[p].begin ();
		itCLEnd=consListsByProduct[p].end();
		numberOfConsignationsByProduct[p]=0;
		while (itCL!=itCLEnd )
		{
			numberOfConsignationsByProduct[p]+=consListsByNums[*itCL]->Frequency_g ();
			itCL++;
		}
	}
	
	lmin=1;
	do
	{
		pNumClMax=-1;

		for (p=0;p<pList.size ();p++)
		{
			if (numberOfConsignationsByProduct[p]>pNumClMax)
			{
				pMax=p;
				pNumClMax=numberOfConsignationsByProduct[p];
			}
		}
		if (pNumClMax>=0)
		{
			l=lmin;
			while (productsByLocation[l]!=-1 || 
					!productsByNums[pMax]->isLocationInConstrainedLocationSet (l)
					)
				l++;

			assignProductToLocation (pMax,l);
			numberOfConsignationsByProduct[pMax]=-1;

			lmin++;
		}
	}
	while (pNumClMax>=0);

	isSolution=true;
}


/*void KZO::KZO_OptimizationEngine_Heuristic::improveBy2Opt()
{
	int p1,p2,p1Max,p2Max,l1,l2;
	double minDiff,diff;
	double diffObj;
	int currIt;
	int pBCombi;
	KZO_ExchangeCombination *bestCombis;
	bestCombis=new KZO_ExchangeCombination [400];
	pBCombi=0;

	currIt=0;
	do {
		minDiff=0;
		for (p1=0;p1<pList.size()-1;p1++)
		{
			for (p2=p1+1;p2<pList.size();p2++)
			{
				if (productsByNums[p1]->isLocationInConstrainedLocationSet (locationsByProduct[p2]) &&
					productsByNums[p2]->isLocationInConstrainedLocationSet (locationsByProduct[p1])
					)
				{	
					diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					if (diff<minDiff)
					{
						p1Max=p1;
						p2Max=p2;
						minDiff=diff;
					}
				}
			}
		}

		if (minDiff<0)
		{
			l1=locationsByProduct[p1Max];
			l2=locationsByProduct[p2Max];

			unassignProductToLocation (p1Max,l1);
			unassignProductToLocation (p2Max,l2);
			assignProductToLocation (p1Max,l2);
			assignProductToLocation (p2Max,l1);
		}

		cout<<"Zielfunktion:"<<objFunction ()<<endl;
	}
	while (minDiff<-0.01);
}
*/

void KZO::KZO_OptimizationEngine_Heuristic::improveBy2Opt()
{
	int p1,p2,p1Max,p2Max,l1,l2;
	double minDiff,diff;
	double diffObj;
	int currIt;
	int pBCombi;
	KZO_ExchangeCombination *bestCombis;
	bestCombis=new KZO_ExchangeCombination [400];
	pBCombi=0;

	currIt=0;
	do {
		minDiff=0;
		for (p1=0;p1<pList.size()-1;p1++)
		{
			for (p2=p1+1;p2<pList.size();p2++)
			{
				if (productsByNums[p1]->isLocationInConstrainedLocationSet (locationsByProduct[p2]) &&
					productsByNums[p2]->isLocationInConstrainedLocationSet (locationsByProduct[p1])
					)
				{	
					diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					if (diff<0)//(diff<minDiff)
					{
						p1Max=p1;
						p2Max=p2;
						minDiff=diff;
						
									l1=locationsByProduct[p1];
									l2=locationsByProduct[p2];

									unassignProductToLocation (p1,l1);
									unassignProductToLocation (p2,l2);
									assignProductToLocation (p1,l2);
									assignProductToLocation (p2,l1);
									cout<<"Zielfunktion:"<<objFunction ()<<endl;
								
						/*
						bestCombis[pBCombi].p1 =p1;
						bestCombis[pBCombi].p2=p2;
						bestCombis [pBCombi].v =minDiff;

						pBCombi++;
						cout<<"CurrCombi:"<<pBCombi <<endl;
						if (pBCombi ==400)
						{
							pBCombi--;
							while (pBCombi>=0)
							{
								diff=exchangeCostDiff (bestCombis[pBCombi].p1 ,locationsByProduct [bestCombis[pBCombi].p1],bestCombis[pBCombi].p2 ,locationsByProduct [bestCombis[pBCombi].p2 ]);
								if (diff<0)
								{
									l1=locationsByProduct[bestCombis[pBCombi].p1];
									l2=locationsByProduct[bestCombis[pBCombi].p2];

									unassignProductToLocation (bestCombis[pBCombi].p1,l1);
									unassignProductToLocation (bestCombis[pBCombi].p2,l2);
									assignProductToLocation (bestCombis[pBCombi].p1,l2);
									assignProductToLocation (bestCombis[pBCombi].p2,l1);
								}
								pBCombi--;
							}
						}*/
						//std::cout<<"Current Mindiff"<<minDiff<<endl;
					}
				}
			}
		}
		/*
		if (minDiff<0)
		{
			l1=locationsByProduct[p1Max];
			l2=locationsByProduct[p2Max];

			unassignProductToLocation (p1Max,l1);
			unassignProductToLocation (p2Max,l2);
			assignProductToLocation (p1Max,l2);
			assignProductToLocation (p2Max,l1);
		}
		*/
		cout<<"Zielfunktion:"<<objFunction ()<<endl;
	}
	while (minDiff<-0.01);
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::evaluateSolution (list<ProductionLocationAssignment> &assignments)
{
	KZO::Optimization_Result *currResult;
	setInitialSolution (assignments);

	if (isSolution )
	{
		currResult=getResult ();
		resetSolution ();
	}
	else
	{
		currResult =new KZO::Optimization_Result ();
		currResult->Status_s (KZO::Optimization_Status ::NO_SOLUTION_FOUND );
		currResult->Cost_s (0);
	}

	return currResult;
}
void KZO::KZO_OptimizationEngine_Heuristic::setInitialSolution (list<ProductionLocationAssignment> &assignments)
{
	list<ProductionLocationAssignment>::iterator it,itEnd;
	int p,l;

	if (isSolution)
		resetSolution ();

	isSolution =true;
	it=assignments.begin ();
	itEnd=assignments.end ();

	while (it!=itEnd)
	{
		p=it->p->Number_g ();
		l=it->l->Number_g ();

		if (p<pList.size() && l<cLocations.size() &&
			it->p->isLocationInConstrainedLocationSet (l))
		{
			assignProductToLocation (p,l);
		}
		else
		{
			resetSolution ();
			return;
		}
		it++;
	}
}
