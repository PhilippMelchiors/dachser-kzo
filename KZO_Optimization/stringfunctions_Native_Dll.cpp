#include "stdafx.h"
#include "stringFunctions_Native_Dll.h"
#include<iostream>
#include <list>


int numberDigits=4;
int oc=0;
string *charDelete (string s,char c)
{
	const char *sc;
	char *nsc;
	string *nstr;
	int length,i,j;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length];

	j=0;
	for (i=0;i<length;i++)
	{
		if (sc[i]!=c)
		{
			nsc[j]=sc[i];
			j++;
		}
	}
	nsc[j]=0;
	
	nstr=new string;
	nstr->append(nsc);
	return nstr;
}

string *charReplace (string s,char oldChar,char newChar)
{
	const char *sc;
	char *nsc;
	string *nstr;
	int length,i;

	sc= s.c_str();

	length=0;
	while (sc[length]!=0)
		length++;

	nsc=new char [length+1];

	for (i=0;i<length;i++)
	{
		if (sc[i]!=oldChar)
		{
			nsc[i]=sc[i];
		}
		else
		{
			nsc[i]=newChar;
		}
	}

	nsc[i]=0;
	
	nstr=new string;
	nstr->append(nsc);
	return nstr;
}

void setNumberDigits(int n)
{
	numberDigits=n;
}

template <> string toString<double> (const double &t)
{
	  ostringstream os;
	    
	  os.precision(numberDigits);
	
		os <<fixed<< t;
	    return os.str();
}