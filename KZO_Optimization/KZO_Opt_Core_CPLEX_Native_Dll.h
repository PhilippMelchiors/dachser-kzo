#include "KZO_Opt_Core_Native_Dll.h"
#include "Matrix2d_Native_Dll.h"
#include "cplex_model_Native_Dll.h"

namespace KZO
{
	/*
	//const char *usedSense="min";
	class __declspec( dllexport ) KZO_Test
	{
	protected:
		std::list<string> l;
		CPLEX *usedCPLEX;
			CPLEX_Params usedCPLEXParams;
			list<KZO::Product *> pList;
			list<KZO::ConsignmentLocation *>cLocations;
			//list<KZO::ConsignmentLocationsDistance *>cLDistances;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;

			bool isSetFixations;
			//int *nbArr;
			Matrix_2d <int> *nbArr;
			int *nArr;
			int **nArrByOrderList;

			int *locAssignConstraints;
			int *productAssignConstraints;
			int *productAssignmentsDistanceRelationsConstraints;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
	public:
		 KZO_Test();
		 ~KZO_Test();
	};*/

	class __declspec( dllexport ) KZO_OptimizationEngine_CPLEX : public KZO_OptimizationEngine 
	{
		protected:
			CPLEX *usedCPLEX;
			CPLEX_Params usedCPLEXParams;
			list<KZO::Product *> pList;
			list<KZO::ConsignmentLocation *>cLocations;
			//list<KZO::ConsignmentLocationsDistance *>cLDistances;
			std::unordered_map <std::string,KZO::ConsignmentLocationsDistance *> cLDistances;
			list<KZO::ConsignmentListType*>consLists;
			Matrix_2d <int> *xVars;
			KZO::ConsignmentLocation **locationsByNums;
			KZO::Product **productsByNums;
			KZO::ConsignmentListType **consListsByNums;

			bool isSetFixations;
			//int *nbArr;
			Matrix_2d <int> *nbArr;
			int *nArr;
			int **nArrByOrderList;

			int *locAssignConstraints;
			int *productAssignConstraints;
			int *productAssignmentsDistanceRelationsConstraints;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointStart;
			Matrix_2d <int> *productAssignmentsDistanceRelationsConstraintsConsignationPointEnd;
			//int nLocTuples;
			void createCPLEX();
			void prepareVariables();
			void prepareConstraints();
			KZO::Optimization_Result *runSolver();
		public:
			KZO_OptimizationEngine_CPLEX();
			~KZO_OptimizationEngine_CPLEX();
			virtual void addProduct(Product *p);
			virtual void addConsignmentLocation (ConsignmentLocation *cL);
			virtual void addDistance (ConsignmentLocationsDistance  *d);
			virtual void addConsignmentList(ConsignmentListType *cListType);
			virtual KZO::Optimization_Result *optimize();
			virtual KZO::Optimization_Result *getStartSolution();
			virtual void clear();
			virtual void buildStructures();
			virtual KZO::Optimization_Result *evaluateSolution (list<ProductionLocationAssignment> &assignments);
			virtual void setInitialSolution (list<ProductionLocationAssignment> &assignments);
			virtual void resetSolution ();
	};
}