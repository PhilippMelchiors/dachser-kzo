#include "stdafx.h"
#include "KZO_Opt_Core_Native_Dll.h"


KZO::Product::Product()
{
	isConstrainedConsignedLocations=false;
}

int KZO::Product::Number_g()
{
	return number;
}
void KZO::Product::Number_s(int n)
{
	number=n;
}

void KZO::Product::addLocation (int l)
{
	isConstrainedConsignedLocations =true;
	
	if (locations.find(l)==locations.end())
		locations.insert (l);
}

void KZO::Product::removeLocation (int l)
{
	if (locations.find(l)!=locations.end ())
		locations.erase (l);
}

bool KZO::Product::isLocationInConstrainedLocationSet(int l)
{
	if (isConstrainedConsignedLocations)
	{
		if (locations.find (l)==locations.end())
			return false;
	}
	return true;
}

KZO::ConsignmentLocation::ConsignmentLocation ()
{
}

KZO::ConsignmentLocation::ConsignmentLocation (int c,int n)
{
	cap=c;
	number=n;
}

int KZO::ConsignmentLocation::Cap_g()
{
	return cap;
}
		
void KZO::ConsignmentLocation::Cap_s(int v)
{
	cap=v;
}
		
int KZO::ConsignmentLocation::Number_g()
{
	return number;
}
		
void KZO::ConsignmentLocation::Number_s(int n)
{
	number=n;
}

//double KZO::ConsignmentLocation::ConsignmentCost_g()
//{
//	return consignmentCost;
//}
//
//void KZO::ConsignmentLocation::ConsigmentCost_s(double c)
//{
//	consignmentCost =c;
//}

KZO::ConsignmentLocationsDistance::ConsignmentLocationsDistance ()
{
}
		
KZO::ConsignmentLocationsDistance::ConsignmentLocationsDistance (int oNL1,int oNL2,double c)
{
	loc1 =oNL1;
	loc2=oNL2;
	cost=c;
}
		
int KZO::ConsignmentLocationsDistance::Loc1_g()
{
	return loc1;
}
		
void KZO::ConsignmentLocationsDistance::Loc1_s(int l)
{
	loc1=l;
}
		
int KZO::ConsignmentLocationsDistance::Loc2_g()
{
	return loc2;
}
		
void KZO::ConsignmentLocationsDistance::Loc2_s(int l)
{
	loc2=l;
}

double KZO::ConsignmentLocationsDistance::Cost_g()
{
	return cost;
}
void KZO::ConsignmentLocationsDistance::Cost_s(double c)
{
	cost=c;
}

void KZO::ConsignmentListType::addProduct (int p)
{
	if (products.find (p)==products.end ())
		products.insert (p);
}

bool KZO::ConsignmentListType::isProductInList(int p)
{
	if (products.find (p)==products.end())
		return false;

	return true;
}

std::list<int> *KZO::ConsignmentListType::getProducts()
{
	std::unordered_set <int>::iterator it,itEnd;
	std::list<int> *usedList;

	usedList =new std::list<int>();
	itEnd=products.end ();
	it=products.begin ();

	while (it!=itEnd)
	{
		usedList->push_back (*it);
		it++;
	}

	return usedList;
}
int KZO::ConsignmentListType::NumberOfProducts()
{
	return products.size ();
}
void KZO::ConsignmentListType::Number_s(int n)
{
	number=n;
}

int KZO::ConsignmentListType::Number_g()
{
	return number;
}


void KZO::ConsignmentListType::Frequency_s(int f)
{
	frequency =f;
}
int KZO::ConsignmentListType::Frequency_g()
{
	return frequency;
}

std::list<KZO::ProductionLocationAssignment> *KZO::Optimization_Result::Assigments_g()
{
	return &assignments;
}
		
double KZO::Optimization_Result::Cost_g()
{
	return cost;
}

void KZO::Optimization_Result::Cost_s(double c)
{
	cost=c;
}

KZO::Optimization_Status KZO::Optimization_Result::Status_g()
{
	return status;
}
void KZO::Optimization_Result::Status_s(Optimization_Status stat)
{
	status=stat;
}

void KZO::Optimization_Result::addAssignemnt (ProductionLocationAssignment a)
{
	assignments .push_back (a);
}