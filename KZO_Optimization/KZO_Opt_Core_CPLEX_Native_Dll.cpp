#include "stdafx.h"
#include "KZO_Opt_Core_CPLEX_Native_Dll.h"
#include "stringFunctions_Native_Dll.h"
/*
KZO::KZO_Test::KZO_Test()
{
	usedCPLEX =NULL;
	xVars =NULL;
	locationsByNums=NULL;
	productsByNums =NULL;
	consListsByNums =NULL;
	nArr=NULL;
	nbArr=NULL;
	locAssignConstraints=NULL;
	productAssignConstraints=NULL;
	isSetFixations=false;
	if (usedCPLEX!=NULL)
		delete usedCPLEX;

	usedCPLEXParams.SCRIND=CPX_ON;
	usedCPLEX =new CPLEX (usedCPLEXParams,"KZO_Problem");
}
KZO::KZO_Test::~KZO_Test()
{
}
*/
KZO::KZO_OptimizationEngine_CPLEX::KZO_OptimizationEngine_CPLEX()
{
	usedCPLEX =NULL;
	xVars =NULL;
	locationsByNums=NULL;
	productsByNums =NULL;
	consListsByNums =NULL;
	nArr=NULL;
	nbArr=NULL;
	locAssignConstraints=NULL;
	productAssignConstraints=NULL;
	isSetFixations=false;
	createCPLEX ();
}

KZO::KZO_OptimizationEngine_CPLEX::~KZO_OptimizationEngine_CPLEX()
{
	delete usedCPLEX;
	if (xVars!=NULL)
		delete xVars;
	if (locationsByNums!=NULL)
		delete [] locationsByNums;
	if (productsByNums !=NULL)
		delete [] productsByNums;
	if (consListsByNums !=NULL)
		delete [] consListsByNums;
	if (nArr !=NULL)
		delete [] nArr;
	if (nbArr!=NULL)
		delete nbArr;
	if (locAssignConstraints !=NULL)
		delete [] locAssignConstraints;
	if (productAssignConstraints !=NULL)
		delete [] productAssignConstraints;
}

void KZO::KZO_OptimizationEngine_CPLEX::addProduct(Product *p)
{
	pList.push_back (p);
}

void KZO::KZO_OptimizationEngine_CPLEX::addConsignmentLocation (ConsignmentLocation *cL)
{
	cLocations.push_back (cL);
}

void KZO::KZO_OptimizationEngine_CPLEX::addDistance (ConsignmentLocationsDistance  *d)
{
	string key;

	key=toString<int>(d->Loc1_g())+"_"+toString<int>(d->Loc2_g());
	cLDistances.insert(pair<string,ConsignmentLocationsDistance  *>(key,d));
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_CPLEX::runSolver()
{
	int p,l;
	double epgap=0.005;
	double epsilon = 0.0001;
	bool isPolish=false;
	int returnCode;
	CPLEX_Result *currResult;
	KZO::Optimization_Result *currOptResult;
	KZO::ProductionLocationAssignment currPLA;
	usedCPLEX->write_problem ("debug.mip");

	currResult=usedCPLEX->solve_mip (epgap,isPolish);
	
	returnCode=currResult->solveStatus;

	currOptResult=new KZO::Optimization_Result ();

	if (currResult->resultType==CPLEX_RESULT_TYPE::OK)
	{
		for (p=0;p<pList.size ();p++)
		{
			for (l=1;l<cLocations.size ();l++)
			{
				if (usedCPLEX->get_varsol(xVars->m_A[p][l])>epsilon)
				{
					currPLA.p=productsByNums [p];//*pIt;
					currPLA.l=locationsByNums [l];

					currOptResult->addAssignemnt (currPLA);
				}
			}
		}

		currOptResult->Cost_s (usedCPLEX->get_objval ());

		currOptResult->Status_s (KZO::Optimization_Status::SOLUTION_FOUND);
	}
	else
	{
		currOptResult->Status_s (KZO::Optimization_Status::NO_SOLUTION_FOUND);
	}

	delete currResult;

	return currOptResult;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_CPLEX::getStartSolution()
{
	return NULL;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_CPLEX::optimize()
{	
	int p,l;

	if (isSetFixations)
	{
		for (p=0;p<pList.size();p++)
		{
			for (l=1;l<cLocations.size ();l++)
			{
				if (xVars->m_A[p][l]!=-1)
				{
					usedCPLEX->change_bound (xVars->m_A[p][l],0,'L');
					usedCPLEX->change_bound (xVars->m_A[p][l],CPX_INFBOUND,'U');
				}
			}
		}
		isSetFixations =false;
	}
	return runSolver ();
}

void KZO::KZO_OptimizationEngine_CPLEX::clear()
{
	pList.clear ();
	cLocations.clear ();
	cLDistances.clear ();

	createCPLEX ();
}

void KZO::KZO_OptimizationEngine_CPLEX::buildStructures()
{
	unsigned l;
	list<ConsignmentLocation *>::iterator locIt,locItEnd;
	list<Product *>::iterator pIt,pItEnd;
	list<ConsignmentListType *>::iterator cLIt,cLItEnd;

	usedCPLEX->setObjSense ("min");
	
	if (locationsByNums !=NULL)
		delete [] locationsByNums;

	locationsByNums =new KZO::ConsignmentLocation *[cLocations.size()];

	locIt=cLocations .begin ();
	locItEnd=cLocations .end ();

	while(locIt !=locItEnd )
	{
		locationsByNums [(*locIt)->Number_g ()]=*locIt;
		locIt++;
	}

	if (productsByNums !=NULL)
		delete [] productsByNums;

	productsByNums =new KZO::Product *[pList.size ()];

	pIt=pList.begin ();
	pItEnd =pList.end ();

	while (pIt!=pItEnd )
	{
		productsByNums[(*pIt)->Number_g ()]=*pIt;
		pIt++;
	}

	if (consListsByNums !=NULL)
		delete [] consListsByNums;

	consListsByNums =new ConsignmentListType *[consLists .size()];
	cLIt=consLists .begin ();
	cLItEnd =consLists .end();

	while(cLIt !=cLItEnd )
	{
		consListsByNums[(*cLIt)->Number_g ()]=*cLIt;
		cLIt++;
	}


	if (nArr!=NULL)
	{
		delete [] nArr;
		delete [] nArrByOrderList;
		delete xVars;
		delete nbArr;
		delete locAssignConstraints;
	}

	prepareVariables ();
	prepareConstraints ();
}

void KZO::KZO_OptimizationEngine_CPLEX::prepareVariables()
{
	int p,l,l1,l2,cL;
	int pos;
	double consCost;
	string currVarName;
	string currKey;
	KZO::ConsignmentLocationsDistance *currDistance;
	list<KZO::ConsignmentListType*>::iterator itCL,itCLEnd;
	list<int>::iterator it,itEnd;

	xVars =new Matrix_2d <int>(pList.size(),cLocations.size ());

	nArr =new int [consLists.size ()*cLocations.size ()*cLocations.size ()];

	for (cL=0;cL<consLists.size ()*cLocations.size ()*cLocations.size ();cL++)
		nArr[cL]=-1;
	
	nbArr=new Matrix_2d <int>(consLists.size(),cLocations.size ());

	for (p=0;p<pList.size();p++)
	{
		for (l=1;l<cLocations.size ();l++)
		{
			if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
			{
				consCost=0;
				
				currVarName="X_";
				currVarName+=toString<int>(p);
				currVarName+="_";
				currVarName+=toString<int>(l);
				
				usedCPLEX->add_var_binary (currVarName.c_str(),0.0,xVars->m_A[p][l]);
			}
			else
				xVars->m_A[p][l]=-1;
		}
	}

	for (cL=0;cL<consLists.size();cL++)
	{
		for (l1=0;l1<cLocations.size ()-1;l1++)
		{
			for (l2=l1+1;l2<cLocations.size();l2++)
			{
				pos=cL*cLocations.size()*cLocations.size()+l1*cLocations.size()+l2;
				currVarName="N_";
				currVarName+=toString<int>(cL);
				currVarName+="_";
				currVarName+=toString<int>(l1);
				currVarName+="_";
				currVarName+=toString<int>(l2);
				currKey=toString <int>(l1)+"_"+toString<int>(l2);
				currDistance=cLDistances[currKey];
				usedCPLEX->add_var_binary (currVarName.c_str(),consListsByNums[cL]->Frequency_g ()*currDistance->Cost_g (),nArr[pos]);
			}
			if (l1>0)
			{
				currVarName="N_";
				currVarName+=toString<int>(cL);
				currVarName+="_";
				currVarName+=toString<int>(l1);
				currVarName+="_";
				currVarName+="0";
				currKey=toString <int>(l1)+"_"+"0";
				currDistance=cLDistances[currKey];
				usedCPLEX->add_var_binary (currVarName.c_str(),consListsByNums[cL]->Frequency_g ()*currDistance->Cost_g (),nbArr->m_A[cL][l1]);
			}
		}

		currVarName="N_";
				currVarName+=toString<int>(cL);
				currVarName+="_";
				currVarName+=toString<int>(l1);
				currVarName+="_";
				currVarName+="0";
				currKey=toString <int>(l1)+"_"+"0";
				currDistance=cLDistances[currKey];
				usedCPLEX->add_var_binary (currVarName.c_str(),consListsByNums[cL]->Frequency_g ()*currDistance->Cost_g (),nbArr->m_A[cL][l1]);
	}
}

void KZO::KZO_OptimizationEngine_CPLEX::prepareConstraints()
{	
	string currConstraintName;
	int i,p,l,l1,l2,pos,currCL;

	locAssignConstraints =new int [cLocations.size()];
	
	for (l=1;l<cLocations.size ();l++)
	{
		currConstraintName="CONSTRAINT_ASSIGN_LOCATION_";
		currConstraintName+=toString<int>(l);

		usedCPLEX->add_cons (currConstraintName.c_str(),'L',1.0,locAssignConstraints[l]);

		for (p=0;p<pList.size ();p++)
		{
			if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
			{
				usedCPLEX->add_coeff_linear(locAssignConstraints[l],xVars->m_A[p][l],1);
			}
		}
	}

	productAssignConstraints =new int [pList.size ()];
	
	for (p=0;p<pList .size ();p++)
	{
		currConstraintName="CONSTRAINT_ASSIGN_PRODUCT_";
		currConstraintName+=toString<int>(p);

		usedCPLEX->add_cons (currConstraintName.c_str (),'E',1.0,productAssignConstraints[p]);

		for (l=1;l<cLocations.size ();l++)
		{
			if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
			{
				usedCPLEX->add_coeff_linear (productAssignConstraints[p],xVars->m_A[p][l],1.0);
			}
		}
	}

	productAssignmentsDistanceRelationsConstraints=new int [consLists.size()*cLocations.size()*cLocations.size()];
	productAssignmentsDistanceRelationsConstraintsConsignationPointStart=new Matrix_2d <int>(consLists.size (),cLocations.size ());
	productAssignmentsDistanceRelationsConstraintsConsignationPointEnd=new Matrix_2d <int>(consLists.size (),cLocations.size ());

	for (i=0;i<consLists.size()*cLocations.size()*cLocations.size();i++)
		productAssignmentsDistanceRelationsConstraints[i]=-1;

	for (currCL=0;currCL<consLists .size();currCL++)
	{
		for (l1=1;l1<cLocations.size()-1;l1++)
		{
			for (l2=l1+1;l2<cLocations.size();l2++)
			{
				pos=currCL*cLocations.size ()*cLocations.size ()+l1*cLocations.size()+l2;
				currConstraintName="CONSTRAINT_ASSIGN_DISTANCE_RELATION_";
				currConstraintName+=toString <int>(currCL);
				currConstraintName+="_"+toString <int>(l1);
				currConstraintName+="_"+toString <int>(l2);
					
				usedCPLEX->add_cons (currConstraintName.c_str(),'L',1,productAssignmentsDistanceRelationsConstraints[pos]);

				for (p=0;p<pList .size();p++)
				{
					if (consListsByNums[currCL]->isProductInList (p))
					{
						if (productsByNums[p]->isLocationInConstrainedLocationSet (l1))
						{	
							usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraints[pos],xVars->m_A[p][l1],1);
						}

						if (productsByNums[p]->isLocationInConstrainedLocationSet (l2))
						{	
							usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraints[pos],xVars->m_A[p][l2],1);
						}

						for (l=l1+1;l<=l2-1;l++)
						{
							if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
							{	
								usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraints[pos],xVars->m_A[p][l],-1);
							}
						}
					}
				}

				usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraints[pos],nArr[pos],-1);
			}
		}

		for (l1=1;l1<cLocations.size();l1++)
		{
			currConstraintName="CONSTRAINT_ASSIGN_DISTANCE_RELATION_CONS_POINT_START";
			currConstraintName+=toString <int>(currCL);
			currConstraintName+="_"+toString <int>(l1);
						
			usedCPLEX->add_cons (currConstraintName.c_str(),'L',0,productAssignmentsDistanceRelationsConstraintsConsignationPointStart->m_A[currCL][l1]);

			currConstraintName="CONSTRAINT_ASSIGN_DISTANCE_RELATION_CONS_POINT_END";
			currConstraintName+=toString <int>(currCL);
			currConstraintName+="_"+toString <int>(l1);
						
			usedCPLEX->add_cons (currConstraintName.c_str(),'L',0,productAssignmentsDistanceRelationsConstraintsConsignationPointEnd->m_A[currCL][l1]);

			for (p=0;p<pList.size();p++)
			{
				if (consListsByNums [currCL]->isProductInList (p))
				{
					if (productsByNums[p]->isLocationInConstrainedLocationSet (l1))
					{	
						usedCPLEX->add_coeff_linear (
													 productAssignmentsDistanceRelationsConstraintsConsignationPointStart->m_A[currCL][l1],
													 xVars->m_A[p][l1],
													 1
													 );
						usedCPLEX->add_coeff_linear (
													 productAssignmentsDistanceRelationsConstraintsConsignationPointEnd->m_A[currCL][l1],
													 xVars->m_A[p][l1],
													 1
													 );
					}

					for (l=1;l<=l1-1;l++)
					{
						if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
						{	
							usedCPLEX->add_coeff_linear (
													 productAssignmentsDistanceRelationsConstraintsConsignationPointStart->m_A[currCL][l1],
													 xVars->m_A[p][l],
													 -1
													 );
						
						}
					}
				
					for (l=l1+1;l<cLocations.size ();l++)
					{
						if (productsByNums[p]->isLocationInConstrainedLocationSet (l))
						{
							usedCPLEX->add_coeff_linear (
													 productAssignmentsDistanceRelationsConstraintsConsignationPointEnd->m_A[currCL][l1],
													 xVars->m_A[p][l],
													 -1
													 );
						}
					}
				}
			}

			pos=currCL*cLocations .size ()*cLocations.size ()+l1;
			usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraintsConsignationPointStart->m_A[currCL][l1],nArr[pos],-1);
			usedCPLEX->add_coeff_linear (productAssignmentsDistanceRelationsConstraintsConsignationPointEnd->m_A[currCL][l1],nbArr->m_A[currCL][l1],-1);
		}
	}
}

void KZO::KZO_OptimizationEngine_CPLEX::createCPLEX()
{
	if (usedCPLEX!=NULL)
		delete usedCPLEX;

	usedCPLEXParams.SCRIND=CPX_ON;
	usedCPLEX =new CPLEX (usedCPLEXParams,"KZO_Problem");
}

void KZO::KZO_OptimizationEngine_CPLEX::addConsignmentList(ConsignmentListType *cListType)
{
	consLists.push_back (cListType);
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_CPLEX::evaluateSolution (list<ProductionLocationAssignment> &assignments)
{
	list<ProductionLocationAssignment>::iterator it,itEnd;
	KZO::Optimization_Result *currResult;
	int p,l;

	isSetFixations=true;
	
	for (p=0;p<pList.size();p++)
	{
		for (l=1;l<cLocations.size ();l++)
		{
			if (xVars->m_A[p][l]!=-1)
			{
				usedCPLEX->change_bound (xVars->m_A [p][l],0,'B');
			}
		}
	}
	

	it=assignments.begin ();
	itEnd =assignments.end();

	while (it!=itEnd)
	{
		p=it->p->Number_g ();
		l=it->l->Number_g ();
		if (xVars->m_A [p][l]!=-1)
		{
			usedCPLEX->change_bound (xVars->m_A [p][l],1,'B');
		}
		else
		{
			currResult=new KZO::Optimization_Result ();
			currResult->Status_s (KZO::Optimization_Status::NO_SOLUTION_FOUND );
			currResult->Cost_s (0);
			return currResult;
		}
		it++;
	}

	return runSolver ();
}

void KZO::KZO_OptimizationEngine_CPLEX::setInitialSolution (list<ProductionLocationAssignment> &assignments)
{
	
}

void KZO::KZO_OptimizationEngine_CPLEX::resetSolution ()
{
}