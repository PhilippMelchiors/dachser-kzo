#include "stdafx.h"
#include<time.h>
#include "KZO_Opt_Core_Heuristic_Native_Dll.h"
#include "stringFunctions_Native_Dll.h"
#include <iostream>

KZO::KZO_OptimizationEngine_Heuristic_Params::KZO_OptimizationEngine_Heuristic_Params()
{
	maxNumberOfImprovementIterations=-1;
	usedScheme =OptimizationScheme::STARTHEURISTIC_IMPROVEMENTHEURISTIC;
	usedStartHeuristic=StartHeuristic::SIMPLE_FREQUENCY_ORDER;
	usedImprovementHeuristic=ImprovementHeuristic::H2_OPT_GREEDY;
	this->maxLocDistanceForShift =pow(2,32)-1;
	this->minLocDistanceForShift =0;
	this->shiftCostPerMeter =0;
	maxNumberOfThreads=1;
}

void KZO::KZO_OptimizationEngine_Heuristic::basicInitialize()
{
	isSolution =false;
	initialObjValue=0;

	xVars =NULL;
	locationsByNums=NULL;
	productsByNums =NULL;
	consListsByNums =NULL;
	numberOfConsignationsByProduct=NULL;
	sortedProductsByConsListVisit=NULL;
	distances=NULL;
	distanceMatrix =NULL;
	consignmentListsBoolean =NULL;
	consLBEntries =NULL;
	findThreads =NULL;
	InitializeCriticalSection (&crit);
}
KZO::KZO_OptimizationEngine_Heuristic::KZO_OptimizationEngine_Heuristic()
{	
	basicInitialize();
}

KZO::KZO_OptimizationEngine_Heuristic::KZO_OptimizationEngine_Heuristic(KZO_OptimizationEngine_Heuristic_Params *params)
{
	basicInitialize ();
	usedParams =params;
	if (usedParams->maxNumberOfThreads>1)
	{
		//this->threadHandles =new HANDLE[usedParams->maxNumberOfThreads-1];
		this->findThreads =new KZO_ThreadInfos [usedParams->maxNumberOfThreads];
	}
}

KZO::KZO_OptimizationEngine_Heuristic::~KZO_OptimizationEngine_Heuristic()
{
	if (xVars!=NULL)
		delete xVars;

	if (locationsByNums!=NULL)
		delete [] locationsByNums;

	if (productsByNums !=NULL)
		delete [] productsByNums;

	if (consListsByNums !=NULL)
		delete [] consListsByNums;

	if (numberOfConsignationsByProduct!=NULL)
		delete [] numberOfConsignationsByProduct ;

	if (sortedProductsByConsListVisit!=NULL)
		delete sortedProductsByConsListVisit;

	if (distances!=NULL)
		delete [] distances;

	if (distanceMatrix!=NULL)
		delete [] distanceMatrix;

	if (consignmentListsBoolean!=NULL)
		delete [] consignmentListsBoolean;

	if (consLBEntries!=NULL)
		delete [] consLBEntries;

	if (findThreads !=NULL)
		delete [] findThreads;

   DeleteCriticalSection (&crit);
}

void KZO::KZO_OptimizationEngine_Heuristic::Params_s(KZO_OptimizationEngine_Heuristic_Params *params)
{
	usedParams =params;
}

void KZO::KZO_OptimizationEngine_Heuristic::addProduct(Product *p)
{
	pList.push_back (p);
}
void KZO::KZO_OptimizationEngine_Heuristic::addConsignmentLocation (ConsignmentLocation *cL)
{
	cLocations.push_back (cL);
}
void KZO::KZO_OptimizationEngine_Heuristic::addDistance (ConsignmentLocationsDistance  *d)
{
	std::string key;

	key=toString<int>(d->Loc1_g())+"_"+toString<int>(d->Loc2_g());
	cLDistances.insert(pair<string,ConsignmentLocationsDistance  *>(key,d));
	cLDistanceList .push_back (d);
}
void KZO::KZO_OptimizationEngine_Heuristic::addConsignmentList(ConsignmentListType *cListType)
{
	consLists.push_back (cListType);
}

double KZO::KZO_OptimizationEngine_Heuristic::exchangeCostDiffSingleProduct (int p1,int l1, int p2,int l2)
{
	//list<int>::iterator itP,itPEnd;
	int i;
	list<int>::iterator itCP,itCPEnd;
	int lastLoc,currCL;
	int oldPredLocDeltaP,newPredLocDeltaP;
	int oldSuccLocDeltaP,newSuccLocDeltaP;
	bool isDeltaPLocLast;
	bool isGreaterPosFound;
	string key;
	double distDiff;
	double totalDiffCost;
	KZO::ConsignmentLocationsDistance *currDistance;
	
	totalDiffCost=0;

	itCP=consListsByProduct [p1].begin ();
	itCPEnd=consListsByProduct [p1].end();

	while (itCP!=itCPEnd)
	{
		currCL=*itCP;
		
		if (consListsByNums[currCL]->Frequency_g ()>0)
		{
			if (!consignmentListsBoolean [currCL][p2])
			{
				isDeltaPLocLast =false;
				isGreaterPosFound=false;

				//itP=sortedProductsByLocation[currCL].begin();
				//itPEnd=sortedProductsByLocation[currCL].end();
				int p1OldPos,p1NewPos;
				newSuccLocDeltaP=newPredLocDeltaP=oldSuccLocDeltaP=oldPredLocDeltaP=0;

				numberDiffCalls++;
				lastLoc=0;
				//while (itP!=itPEnd)
				p1OldPos=productPositionInSortedList->m_A [currCL][p1];
				if (p1OldPos>0)
					oldPredLocDeltaP=locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][ p1OldPos-1]];

				if (p1OldPos<consListsByNums[currCL]->NumberOfProducts ()-1)
					oldSuccLocDeltaP =locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][ p1OldPos+1]];
				
				if (this->findThreads!=NULL)
				{
					p1NewPos=findNewPositionForProduct (currCL,p1,l2);
				}
				else
				{
				//EnterCriticalSection(&crit);
				if (candidateLocationPositionInSortedList->m_A[currCL][l2].iteration==currImprovementIteration)
					p1NewPos=candidateLocationPositionInSortedList->m_A[currCL][l2].position;
				else
				{	
					//EnterCriticalSection(&crit);
					p1NewPos=findNewPositionForProduct (currCL,p1,l2);
					candidateLocationPositionInSortedList->m_A[currCL][l2].position=p1NewPos;
					candidateLocationPositionInSortedList->m_A[currCL][l2].iteration=currImprovementIteration;
					//LeaveCriticalSection (&crit);
				}
				}
				//

				if (p1NewPos>0)
				{
					if (locationsByProduct[sortedProductsByConsListVisit ->m_A [currCL][p1NewPos-1]]!=l1)
						newPredLocDeltaP=locationsByProduct[sortedProductsByConsListVisit ->m_A [currCL][p1NewPos-1]];
					else if (p1NewPos>1)
						newPredLocDeltaP=locationsByProduct[sortedProductsByConsListVisit ->m_A [currCL][p1NewPos-2]];
				}

				if (p1NewPos<consListsByNums[currCL]->NumberOfProducts ())
				{
					if (locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][p1NewPos]] !=l1)
						newSuccLocDeltaP =locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][p1NewPos]];
					else if(p1NewPos <consListsByNums[currCL]->NumberOfProducts ()-1)
						newSuccLocDeltaP =locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][p1NewPos+1]];
				}
				/*
				int newPredLocDeltaPTest,newSuccLocDeltaPTest;

				newSuccLocDeltaPTest=newPredLocDeltaPTest=0;
				for (i=0;i<consListsByNums[currCL]->NumberOfProducts ();i++) 
				{
					if (lastLoc!=l1 &&
						!isGreaterPosFound
						)
						newPredLocDeltaPTest =lastLoc;

					if(locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]]>l2 &&//if (locationsByProduct [*itP]>l2 &&
						sortedProductsByConsListVisit->m_A[currCL][i]!=p1 &&//*itP!=p1 && 
						!isGreaterPosFound
						)
					{	
						isGreaterPosFound=true;
						newSuccLocDeltaPTest=locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					}

					if (newSuccLocDeltaPTest>0)
						break;
				
					lastLoc=locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					//itP++;
					totalNumberIts++;
				}
				if (lastLoc!=l1 &&
					!isGreaterPosFound
					)
						newPredLocDeltaPTest =lastLoc;

				if (newPredLocDeltaP != newPredLocDeltaPTest ||
					newSuccLocDeltaP !=newSuccLocDeltaPTest)
					p1+=0;
					*/

				/*
				for (i=0;i<consListsByNums[currCL]->NumberOfProducts ();i++) 
				{
					if (lastLoc!=l1 &&
						!isGreaterPosFound
						)
						newPredLocDeltaP =lastLoc;

					if(locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]]>l2 &&//if (locationsByProduct [*itP]>l2 &&
						sortedProductsByConsListVisit->m_A[currCL][i]!=p1 &&//*itP!=p1 && 
						!isGreaterPosFound
						)
					{	
						isGreaterPosFound=true;
						newSuccLocDeltaP=locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					}

					if (newSuccLocDeltaP>0)
						break;
				
					lastLoc=locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					//itP++;
					totalNumberIts++;
				}
				if (lastLoc!=l1 &&
					!isGreaterPosFound
					)
						newPredLocDeltaP =lastLoc;
				*/
				/*
				for (i=0;i<consListsByNums[currCL]->NumberOfProducts ();i++) 
				{
					//sortedProductsByConsListVisit [currCL]
					if (sortedProductsByConsListVisit->m_A[currCL][i]==p1)//if (*itP==p1)
					{
						oldPredLocDeltaP=lastLoc ;
						isDeltaPLocLast=true;
					}
					else
					{
						if (isDeltaPLocLast)
							oldSuccLocDeltaP =locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];

						isDeltaPLocLast =false;
					}

					if (lastLoc!=l1 &&
						!isGreaterPosFound
						)
						newPredLocDeltaP =lastLoc;

					if(locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]]>l2 &&//if (locationsByProduct [*itP]>l2 &&
						sortedProductsByConsListVisit->m_A[currCL][i]!=p1 &&//*itP!=p1 && 
						!isGreaterPosFound
						)
					{	
						isGreaterPosFound=true;
						newSuccLocDeltaP=locationsByProduct[sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					}

					if (newSuccLocDeltaP>0 && oldSuccLocDeltaP>0)
						break;
				
					lastLoc=locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]];//[*itP];
					//itP++;
					totalNumberIts++;
				}
				if (lastLoc!=l1 &&
					!isGreaterPosFound
					)
						newPredLocDeltaP =lastLoc;
				*/
				
				
				distDiff=distanceMatrix[newPredLocDeltaP][l2];

				distDiff+=distanceMatrix[l2][newSuccLocDeltaP];

				if (
					oldPredLocDeltaP !=oldSuccLocDeltaP && 
					oldPredLocDeltaP !=newPredLocDeltaP && 
					oldSuccLocDeltaP !=newSuccLocDeltaP
					)
				{	
					distDiff+=distanceMatrix[oldPredLocDeltaP][oldSuccLocDeltaP];
				}
				
				distDiff-=distanceMatrix[oldPredLocDeltaP][l1];

				distDiff-=distanceMatrix[l1][oldSuccLocDeltaP];

				if (newPredLocDeltaP !=newSuccLocDeltaP && 
					oldPredLocDeltaP !=newPredLocDeltaP && 
					oldSuccLocDeltaP !=newSuccLocDeltaP
					)
				{	
					distDiff-=distanceMatrix[newPredLocDeltaP][newSuccLocDeltaP];
				}

				totalDiffCost+=distDiff*consListsByNums[currCL]->Frequency_g ();
			}
		}

		itCP++;
	}

	totalDiffCost +=(getAbsDistance (l2,locationsByProductRefSolution[p1])-getAbsDistance(l1,locationsByProductRefSolution[p1]))*usedParams->shiftCostPerMeter; //getAbsDistance (l1,l2)*usedParams->shiftCostPerMeter;

	return totalDiffCost;
}


double KZO::KZO_OptimizationEngine_Heuristic::exchangeCostDiff(int p1,int l1,int p2,int l2)
{
	double totalDiffCost;

	totalDiffCost=exchangeCostDiffSingleProduct (p1,l1,p2,l2);
	totalDiffCost+=exchangeCostDiffSingleProduct (p2,l2,p1,l1);
	//objFunction ();
	return totalDiffCost;
}

/*
double KZO::KZO_OptimizationEngine_Heuristic::exchangeCostDiff(int p1,int l1,int p2,int l2)
{
	list<int>::iterator itP,itPEnd;
	int lastLoc,currCL;
	int oldPredLocDeltaP,newPredLocDeltaP;
	int oldSuccLocDeltaP,newSuccLocDeltaP;
	bool isDeltaPLocLast;
	bool isGreaterPosFound;
	bool isP1,isP2;
	string key;
	int deltaProduct;
	int deltaPNewLoc,deltaPOldLoc;
	double distDiff;
	double totalDiffCost;
	KZO::ConsignmentLocationsDistance *currDistance;

	
	totalDiffCost=0;
	for (currCL=0;currCL<consLists.size();currCL++)
	{
		if (consListsByNums[currCL]->Frequency_g ()>0)
		{
			isP1=consListsByNums[currCL]->isProductInList (p1);
			isP2=consListsByNums[currCL]->isProductInList(p2);

			if ((isP1 || isP2) && !(isP1 && isP2) )
			{
				if (isP1)
				{
					deltaProduct=p1;
					deltaPOldLoc=l1;
					deltaPNewLoc=l2;
				}
				else
				{
					deltaProduct=p2;
					deltaPOldLoc=l2;
					deltaPNewLoc=l1;
				}

				isDeltaPLocLast =false;
				isGreaterPosFound=false;
				itP=sortedProductsByLocation[currCL].begin();
				itPEnd=sortedProductsByLocation[currCL].end();

				newSuccLocDeltaP=newPredLocDeltaP=oldSuccLocDeltaP=oldPredLocDeltaP=0;

				lastLoc=0;
				while (itP!=itPEnd)
				{
					if (*itP==deltaProduct)
					{
						oldPredLocDeltaP=lastLoc ;
						isDeltaPLocLast=true;
					}
					else
					{
						if (isDeltaPLocLast)
							oldSuccLocDeltaP =locationsByProduct[*itP];
						isDeltaPLocLast =false;
					}

					if (lastLoc!=deltaPOldLoc && !isGreaterPosFound)
						newPredLocDeltaP =lastLoc;

					if (locationsByProduct [*itP]>deltaPNewLoc && *itP!=deltaProduct && !isGreaterPosFound)
					{	
						isGreaterPosFound=true;
						newSuccLocDeltaP=locationsByProduct[*itP];
						//newPredLocDeltaP =lastLoc;
					}

					if (newSuccLocDeltaP>0 && oldSuccLocDeltaP>0)
						break;
				
					lastLoc=locationsByProduct [*itP];
					itP++;
				}
		
				if (lastLoc!=deltaPOldLoc && !isGreaterPosFound)
						newPredLocDeltaP =lastLoc;

				key=toString<int>(newPredLocDeltaP)+"_"+toString (deltaPNewLoc);

				currDistance=cLDistances [key];
				distDiff=currDistance->Cost_g ();

				key=toString<int>(deltaPNewLoc)+"_"+toString<int>(newSuccLocDeltaP);
				currDistance=cLDistances[key];
				distDiff+=currDistance->Cost_g ();

				if (oldPredLocDeltaP !=oldSuccLocDeltaP && oldPredLocDeltaP !=newPredLocDeltaP && oldSuccLocDeltaP !=newSuccLocDeltaP)
				{
					key=toString<int>(oldPredLocDeltaP)+"_"+toString<int>(oldSuccLocDeltaP);
					currDistance=cLDistances[key];
					distDiff+=currDistance->Cost_g ();
				}
				key=toString<int>(oldPredLocDeltaP)+"_"+toString (deltaPOldLoc);

				currDistance=cLDistances [key];
				distDiff-=currDistance->Cost_g ();

				key=toString<int>(deltaPOldLoc)+"_"+toString<int>(oldSuccLocDeltaP);
				currDistance=cLDistances[key];
				distDiff-=currDistance->Cost_g ();

				if (newPredLocDeltaP !=newSuccLocDeltaP && oldPredLocDeltaP !=newPredLocDeltaP && oldSuccLocDeltaP !=newSuccLocDeltaP)
				{
					key=toString<int>(newPredLocDeltaP)+"_"+toString<int>(newSuccLocDeltaP);
					currDistance=cLDistances[key];
					distDiff-=currDistance->Cost_g ();
				}
				totalDiffCost+=distDiff*consListsByNums[currCL]->Frequency_g ();
			}
		}
		//itCL++;
	}

	//std::cout<<"DiffCost:"<<totalDiffCost <<endl;
	//std::cin.ignore();
	return totalDiffCost;
}
*/
double KZO::KZO_OptimizationEngine_Heuristic::objFunction()
{
	int currCL;
	int i;
	double objVal,currCLDistanceCost;
	double currDistVal;
	int lastLoc;
	list<int>::iterator itP,itPEnd;
	string currDistanceKey;
	ConsignmentLocationsDistance *currDistance;
	objVal=0;
	bool isFirst;

	
	for (currCL=0;currCL<consLists.size();currCL++)
	{
		currCLDistanceCost=0;
		isFirst=true;
		for (i=0;i<consListsByNums [currCL]->NumberOfProducts ();i++)
		{
			if (isFirst)
			{
				currDistVal=distanceMatrix[0][locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]]];
				if (currDistVal <0)
					currDistVal +=0;

				isFirst =false;
			}
			else
			{
				currDistVal=distanceMatrix[lastLoc][locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]]];

				if (currDistVal <0)
					currDistVal +=0;

			}

			currCLDistanceCost+=currDistVal;
			lastLoc=locationsByProduct [sortedProductsByConsListVisit->m_A[currCL][i]];
		}

		currCLDistanceCost+=distanceMatrix[lastLoc][0];

		objVal+=currCLDistanceCost*consListsByNums [currCL]->Frequency_g ();
	}

	//cout<<"ObjVal1:"<<objVal<<endl;
	/*
	objVal=0;
	for (currCL=0;currCL<consLists.size();currCL++)
	{
		itP=sortedProductsByLocation[currCL].begin ();
		itPEnd=sortedProductsByLocation[currCL].end ();
		isFirst=true;
		currCLDistanceCost=0;
		while (itP!=itPEnd)
		{
			if (isFirst)
			{	
				currDistanceKey="0_"+toString<int>(locationsByProduct[*itP]);
				currDistance=cLDistances[currDistanceKey];

				isFirst=false;
			}
			else
			{
				currDistanceKey =toString <int>(lastLoc)+"_"+toString<int>(locationsByProduct[*itP]);
				currDistance=cLDistances[currDistanceKey];
			}
			currCLDistanceCost+=currDistance->Cost_g ();
			lastLoc=locationsByProduct[*itP];

			itP++;
		}

		currDistanceKey=toString<int>(lastLoc)+"_0";
		currDistance=cLDistances[currDistanceKey];
		currCLDistanceCost +=currDistance->Cost_g ();

		objVal+=currCLDistanceCost*consListsByNums [currCL]->Frequency_g();
	}

	cout<<"ObjVal2:"<<objVal<<endl;
	*/
	return objVal;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::getResult()
{
	int p;
	KZO::ProductionLocationAssignment currAssignment;
	KZO::Optimization_Result *currResult;

	currResult=new Optimization_Result ();

	for (p=0;p<pList.size();p++)
	{
		currAssignment.p =productsByNums[p];
		currAssignment.l=locationsByNums [locationsByProduct [p]];
		currAssignment.isFixed =isFixedAssignmentRefSolutionByProduct [p];

		currResult->addAssignemnt (currAssignment);
	}

	currResult ->Cost_s (objFunction ());
	currResult->Status_s (KZO::Optimization_Status::SOLUTION_FOUND);
	return currResult;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::getStartSolution()
{
	int p;
	KZO::ProductionLocationAssignment currAssignment;
	KZO::Optimization_Result *currResult;

	currResult=new Optimization_Result ();

	for (p=0;p<pList.size();p++)
	{
		currAssignment.p =productsByNums [p];
		currAssignment.l=locationsByNums [locationsByProductRefSolution [p]];
		currAssignment.isFixed =isFixedAssignmentRefSolutionByProduct [p];

		currResult->addAssignemnt (currAssignment);
	}

	currResult ->Cost_s (initialObjValue);
	currResult->Status_s (KZO::Optimization_Status::SOLUTION_FOUND);
	return currResult;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::optimize()
{
	if (usedParams->usedScheme ==OptimizationScheme::STARTHEURISTIC_IMPROVEMENTHEURISTIC)
	{
		if (!isSolution)
		{
			switch (usedParams->usedStartHeuristic) {
				case StartHeuristic::SIMPLE_FREQUENCY_ORDER :
					std::cout<<"Startlösung berechnen"<<std::endl;
					simpleSortInitialization();
					break;
				case StartHeuristic::MODIFIED_CONSIGMENT_LIST_TYPES :
					break;
			}
		}

		initialObjValue=objFunction ();
		std::cout<<"Zielfunktionswert:"<<initialObjValue <<endl;
		numberDiffCalls =0;
		totalNumberIts =0;
		switch (usedParams->usedImprovementHeuristic) {
		case ImprovementHeuristic ::H2_OPT_BEST  :
				storeReferenceSolution ();
				improveBy2Opt_Best ();
				break;
		case ImprovementHeuristic ::H2_OPT_GREEDY:
				storeReferenceSolution ();
				improveBy2Opt_Greedy ();
				break;
		case ImprovementHeuristic::H2_OPT_GREEDY_SA :
				storeReferenceSolution ();
				improveBy2Opt_Greedy_SA ();
		}

		return getResult ();
	}

	return NULL;
}
void KZO::KZO_OptimizationEngine_Heuristic::clear()
{
	pList.clear ();
	cLocations.clear ();
	cLDistances.clear ();
}
void KZO::KZO_OptimizationEngine_Heuristic::prepareVariables()
{
}
void KZO::KZO_OptimizationEngine_Heuristic::resetSolution ()
{
	int i;

	for (i=0;i<pList.size();i++)
	{
		locationsByProduct [i]=-1;
		isFixedAssignmentRefSolutionByProduct[i]=false;
	}

	for (i=0;i<cLocations.size();i++)
	{
		productsByLocation [i]=-1;
		//sortedProductsByLocation [i].clear ();
	}
	
	isSolution =false;
}


int KZO::KZO_OptimizationEngine_Heuristic::findNewPositionForProduct(int cL,int p, int l)
{
	int newPosition,lP,uP,mP;
	bool posFound;

	if (l<locationsByProduct [sortedProductsByConsListVisit->m_A [cL][0]])
		newPosition=0;
	else if (l>locationsByProduct [sortedProductsByConsListVisit->m_A [cL][consListsByNums [cL]->NumberOfProducts ()-1]])
		newPosition=consListsByNums [cL]->NumberOfProducts ();
	else
	{
		lP=0;

		uP=consListsByNums [cL]->NumberOfProducts ()-1;

		posFound=false;

		do
		{	
			if (uP-lP==0)
			{
				mP=lP;
				posFound=true;
			}
			else
			{
				mP=(lP+uP)/2;

				if (l<locationsByProduct [sortedProductsByConsListVisit->m_A [cL][mP]])//locationsByProduct[mP])
				{
					uP=mP;
				}
				else
				{
					lP=mP+1;
				}
			}
		}
		while (!posFound);
		newPosition=mP;
	}

	return newPosition;
}

void KZO::KZO_OptimizationEngine_Heuristic::sortedListChangePositionSingleProduct(int cL,int p,int newL)
{
	int lP,uP,mP,i;
	int startPos;
	bool posFound;
	int newPosition;
/*
	if (newL<locationsByProduct [sortedProductsByConsListVisit->m_A [cL][0]])
		newPosition=0;
	else if (newL>locationsByProduct [sortedProductsByConsListVisit->m_A [cL][consListsByNums [cL]->NumberOfProducts ()-1]])
		newPosition=consListsByNums [cL]->NumberOfProducts ();
	else
	{
		lP=0;

		uP=consListsByNums [cL]->NumberOfProducts ()-1;

		posFound=false;

		do
		{	
			if (uP-lP==0)
			{
				mP=lP;
				posFound=true;
			}
			else
			{
				mP=(lP+uP)/2;

				if (newL<locationsByProduct [sortedProductsByConsListVisit->m_A [cL][mP]])//locationsByProduct[mP])
				{
					uP=mP;
				}
				else
				{
					lP=mP+1;
				}
			}
		}
		while (!posFound);
		newPosition=mP;
	}*/
	
	//newPosition =findNewPositionForProduct (cL,p,newL);
	if (candidateLocationPositionInSortedList ->m_A [cL][newL].iteration==currImprovementIteration)
		newPosition=candidateLocationPositionInSortedList ->m_A [cL][newL].position;
	else
		newPosition =findNewPositionForProduct (cL,p,newL);
	/*
	int newPosition2=candidateLocationPositionInSortedList ->m_A [cL][newL].position;
	if (newPosition != candidateLocationPositionInSortedList ->m_A [cL][newL].position || candidateLocationPositionInSortedList ->m_A [cL][newL].iteration !=currImprovementIteration)
	{
		cout<<"CurrIt:"<<currImprovementIteration 
			<<" Pos:"<<newPosition
			<<" PosC:"<<candidateLocationPositionInSortedList ->m_A [cL][newL].position
			<<" Iteration:"<<candidateLocationPositionInSortedList ->m_A [cL][newL].iteration 
			<<endl;
	}
	*/
	if (productPositionInSortedList->m_A [cL][p]<newPosition)
	{
		newPosition--;
		//memcpy (&sortedProductsByConsListVisit->m_A[cL][productPositionInSortedList->m_A [cL][p]],
		//	&sortedProductsByConsListVisit->m_A[cL][productPositionInSortedList->m_A [cL][p]+1],
		//sizeof(int)*(newPosition-productPositionInSortedList->m_A [cL][p]));

		startPos=productPositionInSortedList->m_A [cL][p] ;
		for (i=startPos;i<newPosition;i++)
		{
			sortedProductsByConsListVisit->m_A[cL][i]=sortedProductsByConsListVisit->m_A[cL][i+1];
			productPositionInSortedList->m_A[cL][sortedProductsByConsListVisit->m_A[cL][i]]=i;
		}
		//sortedProductsByConsListVisit->m_A[cL][newPosition]=p;
		//productPositionInSortedList->m_A [cL][p]=newPosition;
	}
	else if (productPositionInSortedList->m_A [cL][p]>newPosition)
	{
		//memcpy (&sortedProductsByConsListVisit->m_A[cL][newPosition+1],
		//	&sortedProductsByConsListVisit->m_A[cL][newPosition],
		//sizeof(int)*(productPositionInSortedList->m_A [cL][p]-newPosition));
		for (i=productPositionInSortedList->m_A [cL][p];i>newPosition;i--)
		{
			sortedProductsByConsListVisit->m_A[cL][i]=sortedProductsByConsListVisit->m_A[cL][i-1];
			productPositionInSortedList->m_A[cL][sortedProductsByConsListVisit->m_A[cL][i]]=i;
		}
	}

	sortedProductsByConsListVisit->m_A[cL][newPosition]=p;
	productPositionInSortedList->m_A [cL][p]=newPosition;
}


void KZO::KZO_OptimizationEngine_Heuristic::sortedListChangePosition(int p1,int newL1,int p2,int newL2)//sortedListChangePosition(int p,int newL,int p2)
{
	int newPositionP1,cL;
	int uP,lP,mP;
	bool posFound;
	list<int>::iterator cLIt,cLItEnd;

	cLIt=consListsByProduct[p1].begin ();
	cLItEnd =consListsByProduct [p1].end();

	while (cLIt!=cLItEnd)
	{
		cL=*cLIt;

		if (consignmentListsBoolean[cL][p2])
		{
			newPositionP1=productPositionInSortedList->m_A [cL][p2];

			sortedProductsByConsListVisit->m_A[cL][productPositionInSortedList->m_A [cL][p1]]=p2;
			productPositionInSortedList->m_A [cL][p2]=productPositionInSortedList->m_A [cL][p1];

			sortedProductsByConsListVisit->m_A[cL][newPositionP1]=p1;
			productPositionInSortedList->m_A [cL][p1]=newPositionP1;
		}
		else
			sortedListChangePositionSingleProduct(cL,p1,newL1);

		cLIt++;
	}

	cLIt=consListsByProduct[p2].begin ();
	cLItEnd =consListsByProduct [p2].end();

	while (cLIt!=cLItEnd)
	{
		cL=*cLIt;

		if (!consignmentListsBoolean[cL][p1])
		{
			sortedListChangePositionSingleProduct(cL,p2,newL2);
		}

		cLIt++;
	}
}

void KZO::KZO_OptimizationEngine_Heuristic::checkSortedLists ()
{
	int currCL,i;
	int lastLoc;

	for (currCL=0;currCL<consLists.size ();currCL++)
	{
		lastLoc=-1;
		for (i=0;i<consListsByNums[currCL]->NumberOfProducts ();i++)
		{
			if (i>0)
			{
				if (locationsByProduct [sortedProductsByConsListVisit->m_A [currCL][i]]<lastLoc)
					cout<<"Fehler:"<<currCL<<" "<<i<<endl;
			}

			lastLoc=locationsByProduct [sortedProductsByConsListVisit->m_A [currCL][i]];
		}
	}
}
void KZO::KZO_OptimizationEngine_Heuristic::initializeSortedLists()
{
	int currCL, p,i,cLLength,pMin;
	list<int> pCandidateList;
	list<int>::iterator it,itEnd;

	//pCandidateList=new int [pList.size ()];
	cout<<"Init SL."<<endl;
	for (currCL=0;currCL<consLists.size ();currCL++)
	{
		cLLength=consListsByNums [currCL]->NumberOfProducts ();
		pCandidateList .clear ();
		i=0;
		for (p=0;p<pList.size ();p++)
		{
			if (consignmentListsBoolean[currCL][p])
			{
				//sortedProductsByConsListVisit->m_A [currCL][i]=p;
				//pCandidateList[i]=p;
				pCandidateList.push_back (p);
				i++;
			}
		}

		for (i=0;i<cLLength;i++)
		{
			it=pCandidateList .begin ();
			itEnd=pCandidateList .end ();
			pMin=*it;

			while(it!=itEnd)
			{
				if (locationsByProduct [*it]<locationsByProduct[pMin])
					pMin=*it;
				it++;
			}
			pCandidateList.remove(pMin);
			sortedProductsByConsListVisit->m_A [currCL][i]=pMin;
			productPositionInSortedList->m_A[currCL][pMin]=i;
		}
	}

	cout<<"Init SL fertig."<<endl;
}

void KZO::KZO_OptimizationEngine_Heuristic::assignProductToLocation (int p, int l)
{
	list<int>::iterator itCL,itCLEnd,itPL,itPLEnd;
	int currCL;
	locationsByProduct[p]=l;
	productsByLocation[l]=p;

	/*
	itCL=consListsByProduct[p].begin ();
	itCLEnd =consListsByProduct[p].end();

	while(itCL!=itCLEnd)
	{
		currCL=*itCL;

		itPL=sortedProductsByLocation[currCL].begin ();
		itPLEnd=sortedProductsByLocation[currCL].end();

		while (itPL!=itPLEnd)
		{
			if (locationsByProduct[*itPL]>locationsByProduct[p])
				break;

			itPL++;
		}

		if (itPL==itPLEnd)
			sortedProductsByLocation[currCL].push_back (p);
		else
			sortedProductsByLocation[currCL].insert (itPL,p);

		itCL++;
	}*/
}

void KZO::KZO_OptimizationEngine_Heuristic::unassignProductToLocation (int p,int l)
{
	list<int>::iterator itCL,itCLEnd;
	int currCL;
	locationsByProduct[p]=-1;
	productsByLocation[l]=-1;
	/*
	itCL=consListsByProduct[p].begin ();
	itCLEnd =consListsByProduct[p].end();

	while(itCL!=itCLEnd)
	{
		currCL=*itCL;

		sortedProductsByLocation [currCL].remove (p);
		itCL++;
	}*/
}

void KZO::KZO_OptimizationEngine_Heuristic::buildStructures()
{
	unsigned l,p,currCL;
	list<ConsignmentLocation *>::iterator locIt,locItEnd;
	list<Product *>::iterator pIt,pItEnd;
	list<ConsignmentListType *>::iterator cLIt,cLItEnd;
	int nCLBE;
	unsigned *consListLengths;
	list<KZO::ConsignmentLocationsDistance*>::iterator itD,itDEnd;
	KZO::ConsignmentLocationsDistance* currD;

	std::cout<<"Standorte"<<std::endl;
	if (locationsByNums !=NULL)
		delete [] locationsByNums;

	locationsByNums =new KZO::ConsignmentLocation *[cLocations.size()];

	locIt=cLocations .begin ();
	locItEnd=cLocations .end ();

	while(locIt !=locItEnd )
	{
		locationsByNums [(*locIt)->Number_g ()]=*locIt;
		locIt++;
	}
	std::cout<<"Produkte"<<std::endl;
	if (productsByNums !=NULL)
		delete [] productsByNums;

	productsByNums =new KZO::Product *[pList.size ()];

	pIt=pList.begin ();
	pItEnd =pList.end ();

	while (pIt!=pItEnd )
	{
		productsByNums[(*pIt)->Number_g ()]=*pIt;
		pIt++;
	}
	std::cout<<"Komissionierungslisten"<<std::endl;
	if (consListsByNums !=NULL)
		delete [] consListsByNums;

	consListsByNums =new ConsignmentListType *[consLists .size()];
	cLIt=consLists .begin ();
	cLItEnd =consLists .end();
	std::cout<<"Anzahl Komissionierungslisten:"<<consLists .size()<<std::endl;
	while(cLIt !=cLItEnd )
	{
		consListsByNums[(*cLIt)->Number_g ()]=*cLIt;
		cLIt++;
	}
	std::cout<<"Detaillierte Datenstrukturen"<<std::endl;

	locationsByProduct=new int [pList.size ()];
	productsByLocation=new int [cLocations.size()];
	locationsByProductRefSolution =new int [pList.size()];
	isFixedAssignmentRefSolutionByProduct=new bool [pList.size()];
	productsByLocationRefSolution =new int [cLocations.size()];
	locationsByProductBestSolution=new int [pList.size()];
	productsByLocationBestSolution=new int [cLocations.size()];

	consignmentListsBoolean =new bool *[consLists.size()];
	
	nCLBE=consLists .size()*pList.size();
	consLBEntries =new bool[nCLBE];
	for (l=0;l<nCLBE;l++)
		consLBEntries [l]=false;
	l=0;
	for (currCL=0;currCL<consLists .size();currCL++)
	{
		consignmentListsBoolean[currCL]=&consLBEntries [l];
		l+=pList.size();
	}
	std::cout<<"L_Final:"<<l<< "nCLBE:"<<nCLBE<<" consLists.size()"<<consLists.size()<<" currCL:"<<currCL<<endl;
	
	//sortedProductsByLocation=new std::list<int>[consLists.size()]; 

	consListLengths =new unsigned [consLists.size()];

	productPositionInSortedList=new Matrix_2d<int>(consLists.size(),pList.size ());
	candidateLocationPositionInSortedList =new Matrix_2d <KZO_LocationPositionEntry>(consLists.size (),cLocations.size ());

	for (currCL=0;currCL<consLists.size();currCL++)
	{
		consListLengths[currCL]=consListsByNums[currCL]->NumberOfProducts ();
		for (l=0;l<cLocations.size();l++)
			candidateLocationPositionInSortedList->m_A [currCL][l].iteration=-1;
	}

	sortedProductsByConsListVisit=new Matrix_2d <int>(consLists.size (),consListLengths);

	consListsByProduct=new std::list<int>[pList.size()];
	for (p=0;p<pList.size();p++)
	{
		locationsByProduct[p]=-1;
		for (currCL=0;currCL<consLists.size();currCL++)
		{
			if (consListsByNums[currCL]->isProductInList (p))
			{
				consListsByProduct[p].push_back (currCL);
				consignmentListsBoolean[currCL][p]=true;
			}
		}
	}
	std::cout<<"Zuordnungen"<<std::endl;

	for (l=0;l<cLocations.size();l++)
		productsByLocation[l]=-1;

	if (numberOfConsignationsByProduct!=NULL)
		delete [] numberOfConsignationsByProduct;

	std::cout<<"Anzahl Kommissierungen nach Produkt"<<std::endl;
	numberOfConsignationsByProduct=new int [pList.size ()];
	
	distances=new double [cLocations.size()*cLocations.size()];
	distanceMatrix =new double*[cLocations.size()];
	
	for (currCL=0;currCL<cLocations .size();currCL++)
	{
		distanceMatrix[currCL]=&distances[currCL*cLocations.size()];
	}

	itD=cLDistanceList .begin();
	itDEnd=cLDistanceList .end();
	
	while (itD!=itDEnd )
	{
		distanceMatrix[(*itD)->Loc1_g()][(*itD)->Loc2_g()]=(*itD)->Cost_g();
		itD++;
	}

	delete [] consListLengths;

	std::cout<<"Fertig."<<std::endl;
}

void KZO::KZO_OptimizationEngine_Heuristic::backupCurrentSolution()
{
	memmove(locationsByProductBestSolution,locationsByProduct,sizeof(int)*pList.size ());
	memmove(productsByLocationBestSolution,productsByLocation,sizeof(int)*cLocations.size());
}

void KZO::KZO_OptimizationEngine_Heuristic::restoreCurrentSolution()
{
	memmove(locationsByProduct,locationsByProductBestSolution,sizeof(int)*pList.size());
	memmove(productsByLocation,productsByLocationBestSolution,sizeof(int)*cLocations.size());
	initializeSortedLists ();
}

void KZO::KZO_OptimizationEngine_Heuristic::simpleSortInitialization()
{
	int p;
	int lmin,l;
	unsigned pMax;
	int pNumClMax;
	list<int>::iterator itCL,itCLEnd;

	for (p=0;p<pList .size ();p++)
	{
		itCL=consListsByProduct[p].begin ();
		itCLEnd=consListsByProduct[p].end();
		numberOfConsignationsByProduct[p]=0;
		while (itCL!=itCLEnd )
		{
			numberOfConsignationsByProduct[p]+=consListsByNums[*itCL]->Frequency_g ();
			itCL++;
		}
	}
	
	lmin=1;
	do
	{
		pNumClMax=-1;

		for (p=0;p<pList.size ();p++)
		{
			if (numberOfConsignationsByProduct[p]>pNumClMax)
			{
				pMax=p;
				pNumClMax=numberOfConsignationsByProduct[p];
			}
		}
		if (pNumClMax>=0)
		{
			l=lmin;
			while (productsByLocation[l]!=-1 || 
					!productsByNums[pMax]->isLocationInConstrainedLocationSet (l)
					)
				l++;

			assignProductToLocation (pMax,l);
			numberOfConsignationsByProduct[pMax]=-1;

			lmin++;
		}
	}
	while (pNumClMax>=0);

	initializeSortedLists ();
	isSolution=true;
}
void KZO::KZO_OptimizationEngine_Heuristic::setInitialSolution (list<ProductionLocationAssignment> &assignments)
{
	list<ProductionLocationAssignment>::iterator it,itEnd;
	int p,l;

	if (isSolution)
		resetSolution ();

	isSolution =true;
	it=assignments.begin ();
	itEnd=assignments.end ();

	while (it!=itEnd)
	{
		p=it->p->Number_g ();
		l=it->l->Number_g ();

		if (p<pList.size() && l<cLocations.size() &&
			it->p->isLocationInConstrainedLocationSet (l))
		{
			assignProductToLocation (p,l);
			if (it->isFixed)
				isFixedAssignmentRefSolutionByProduct[p]=true;
			else
				isFixedAssignmentRefSolutionByProduct[p]=false;
		}
		else
		{
			cout<<"Infeasible assignment:"<<" Product:"<<p<<" Location:"<<l<<endl;
			resetSolution ();
			return;
		}
		it++;
	}
	initializeSortedLists ();
}

void KZO::KZO_OptimizationEngine_Heuristic::storeReferenceSolution()
{
	int p,l;

	for (p=0;p<pList.size();p++)
	{
		locationsByProductRefSolution[p]=locationsByProduct[p];
	}
	for (l=0;l<cLocations .size();l++)
		productsByLocationRefSolution[l]=productsByLocation[l];

	initializeSortedLists ();
}

bool KZO::KZO_OptimizationEngine_Heuristic::isValidLocDiff(int p,int newLoc)
{	
	double currDist;
	
	currDist =getAbsDistance (locationsByProductRefSolution [p],newLoc);
	
	if (currDist<usedParams->minLocDistanceForShift ||
		currDist >usedParams->maxLocDistanceForShift)
		return false;
	/*
	int firstLocP,secondLocP;
	if (locationsByProductRefSolution [p]>newLoc)
	{
		firstLocP=newLoc;
		secondLocP=locationsByProduct[p];
	}
	else
	{
		firstLocP=locationsByProduct[p];
		secondLocP=newLoc;
	}
	if (distanceMatrix[firstLocP][secondLocP]<usedParams->minLocDistanceForShift)
		return false;
		*/
	/*
	int firstLocP,secondLocP;
	if (locationsByProductRefSolution [p]>newLoc)
	{
		firstLocP=newLoc;
		secondLocP=locationsByProductRefSolution[p];
	}
	else
	{
		firstLocP=locationsByProductRefSolution[p];
		secondLocP=newLoc;
	}
	if (distanceMatrix[firstLocP][secondLocP]<usedParams->minLocDistanceForShift)
		return false;*/
	return true;
}

double  KZO::KZO_OptimizationEngine_Heuristic::getAbsDistance (int l1,int l2)
{
	int firstLoc,secondLoc;

	if (l1<l2)
	{
		firstLoc=l1;
		secondLoc=l2;
	}
	else
	{
		firstLoc=l2;
		secondLoc=l1;
	}

	return distanceMatrix [firstLoc][secondLoc];
}

bool KZO::KZO_OptimizationEngine_Heuristic::isValidExchange (int p1,int p2)
{
	if (isFixedAssignmentRefSolutionByProduct [p1] || isFixedAssignmentRefSolutionByProduct [p2])
		return false;

	if (!productsByNums[p1]->isLocationInConstrainedLocationSet (locationsByProduct[p2]) || 
		!productsByNums[p2]->isLocationInConstrainedLocationSet (locationsByProduct[p1])
		)
	{
	//	cout<<"Infeasible locations."<<endl;
		return false;
	}
	if (
		!isValidLocDiff (p1,locationsByProduct[p2]) || 
		!isValidLocDiff (p2,locationsByProduct[p1])
		)
	{
//		cout<<"Distance too small."<<endl;
		return false;
	}
//	cout<<"ValidExchange ok!"<<endl;
	return true;
}

void KZO::KZO_OptimizationEngine_Heuristic::findBestExchange (int &p1Min,int &p2Min,double &minDiff,int p1Start,int p1End,int p2Start,int p2End)
{
	int p1,p2;
	int p2s;
	double diff;

	//cout<<"In FindMin"<<" P1Start:"<<p1Start<<" P2Start:"<<p2Start<<" P1End:"<<p1End<<" P2End:"<<p2End<<endl;
	for (p1=p1Start;p1<=p1End;p1++)		
	{
				if (p1>p2Start)
					p2s=p1;
				else
					p2s=p2Start;
				for (p2=p2s;p2<=p2End;p2++)
				{
					
					if (p1!=p2 && isValidExchange (p1,p2)
						)
					{	
						//cout<<"P1: "<<p1<<" P2:"<<p2<<" Loc1:"<<locationsByProduct[p1]<<" Loc2:"<<locationsByProduct[p2]<<endl;
						diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					
						if (diff<minDiff)
						{
							p1Min=p1;
							p2Min=p2;
							minDiff=diff;
						}
					}
				}
			}
}

unsigned KZO::KZO_OptimizationEngine_Heuristic::threadProcedure (void *index)
{
	int usedIndex;

	usedIndex =*(int *)index;
	//cout<<"Thread:<<"<<usedIndex<<" begonnnen."<<endl;
	findThreads[usedIndex].minDiff=0;
	findBestExchange (findThreads[usedIndex].p1Min ,
					  findThreads[usedIndex].p2Min,
					  findThreads[usedIndex].minDiff,
					  findThreads[usedIndex].p1Start,
					  findThreads[usedIndex].p1End,
					  findThreads[usedIndex].p2Start,
					  findThreads[usedIndex].p2End);
					  //(p1Max,p2Max,minDiff,0,pList.size ()-2,1,pList.size()-1);
	findThreads[usedIndex].isFinished=true;
	//cout<<"Thread:<<"<<usedIndex<<" fertig."<<endl;
	return 0;
}

class XThreadInfo {

public:
	KZO::KZO_OptimizationEngine_Heuristic *obj;
	int index;
};

//KZO::KZO_OptimizationEngine_Heuristic *usedObj;

unsigned __stdcall xthreadProcedure (void *info)
{
	XThreadInfo *usedInfo;

	usedInfo=(XThreadInfo *)info;

	//usedObj->threadProcedure (index);
	usedInfo->obj->threadProcedure (&usedInfo->index );
	return 0;
}

void KZO::KZO_OptimizationEngine_Heuristic::improveBy2Opt_Best()
{
	int p1,p2,p1Max,p2Max,l1,l2,p1Start,p1End;
	int i;
	double minDiff,diff;
	XThreadInfo *thrInfos;
	int numberOfProductsPerThread;
	bool isAllThreadsFinished;
	double currObjVal;
	thrInfos=new XThreadInfo [usedParams->maxNumberOfThreads];
	currObjVal =objFunction ();
	currImprovementIteration=0;
	clock_t t1,t2;
	bool isSwap;
	t1=clock();
	//usedObj=this;
	do {
		totalNumberIts=0;
		numberDiffCalls =0;
		minDiff=0;
		isSwap=false;
		if (findThreads!=NULL)
		{
			cout <<"Starte Threads."<<endl;
			//_beginthreadex (NULL,0,
			numberOfProductsPerThread =pList.size()/usedParams->maxNumberOfThreads ;
			p1Start=0;
			for (i=0;i<usedParams->maxNumberOfThreads-1;i++)
			{
				thrInfos[i].obj =this;
				thrInfos[i].index =i;
				//threadIndex[i]=i;
				findThreads[i].p1Start=p1Start;
				findThreads[i].p1End=p1Start+numberOfProductsPerThread-1;
				findThreads[i].p2Start=0;
				findThreads[i].p2End =pList.size()-1;
				findThreads[i].isFinished =false;
				findThreads[i].threadHandle=(HANDLE)_beginthreadex (NULL,0,xthreadProcedure,&thrInfos[i],0,0);
				//xthreadProcedure (&thrInfos[i]);
				//findThreads[i].threadHandle =
				p1Start+=numberOfProductsPerThread;
				
			}

			//threadIndex[i]=i;
			thrInfos[i].obj =this;
			thrInfos[i].index =i;
			findThreads[i].p1Start=p1Start;
			findThreads[i].p1End=pList.size()-1;
			findThreads[i].p2Start=0;
			findThreads[i].isFinished =false;
			findThreads[i].p2End =pList.size()-1;
			xthreadProcedure (&thrInfos[i]);//xthreadProcedure (&threadIndex[i]);

			
			do {
				isAllThreadsFinished=true;
				Sleep (1);
				for (i=0;i<usedParams->maxNumberOfThreads-1;i++)
				{
					if (!findThreads[i].isFinished )
						isAllThreadsFinished =false;
				}
			}
			while (!isAllThreadsFinished);

			cout <<"Alle Threads fertig."<<endl;

			for (i=0;i<usedParams->maxNumberOfThreads;i++)
			{
				if (findThreads[i].minDiff <minDiff)
				{
					minDiff=findThreads[i].minDiff;
					p1Max=findThreads[i].p1Min;
					p2Max=findThreads[i].p2Min;
				}
			}
		}
		else
		{
			//cout<<"Vor FindMin!"<<endl;
			findBestExchange (p1Max,p2Max,minDiff,0,pList.size ()-2,1,pList.size()-1);
			/*
			for (p1=0;p1<pList.size()-1;p1++)
			{	
				for (p2=p1+1;p2<pList.size();p2++)
				{
					if (isValidExchange (p1,p2)
						)
					{	
						diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					
						if (diff<minDiff)
						{
							p1Max=p1;
							p2Max=p2;
							minDiff=diff;
						}
					}
				}
			}
			*/
		}

		if (minDiff<-0.01)
		{
			//cout<<"Vor Exchange!:"<<" P1:"<<p1Max<<" P2:"<<p2Max<<endl;
			l1=locationsByProduct[p1Max];
			l2=locationsByProduct[p2Max];

			sortedListChangePosition (p1Max,l2,p2Max,l1);
			
			unassignProductToLocation (p1Max,l1);
			unassignProductToLocation (p2Max,l2);
			assignProductToLocation (p1Max,l2);
			assignProductToLocation (p2Max,l1);
			currObjVal+=minDiff;
			isSwap=true;
		}

		cout<<"Zielfunktion:"<<currObjVal<<endl;//objFunction ()<<endl;
		cout<<"Aktuelle Verbesserungsiteration:"<<currImprovementIteration
			<<" Maximale Anzahl Verbesserungsiterationen:"<<usedParams->maxNumberOfImprovementIterations
			<<"Min-Abstand:"<<usedParams->minLocDistanceForShift
			<<"Max-Abstand:"<<usedParams->maxLocDistanceForShift
			<<endl;
		cout<<"Total_Number_Its:"<<totalNumberIts<<"Total_Number_Diff_Calls:"<<numberDiffCalls <<"Avg_ConsList_its:"<<((double)totalNumberIts )/((double)numberDiffCalls)<<endl;
		currImprovementIteration++;
	}
	while (isSwap && currImprovementIteration<usedParams->maxNumberOfImprovementIterations);
	t2=clock();
	double secs;
	secs=((double)(t2-t1))/CLOCKS_PER_SEC;
	cout<<"Finale Zielfunktion:"<<objFunction ()<<" Benötigte Laufzeit:"<<secs<< endl;
	
}

void KZO::KZO_OptimizationEngine_Heuristic::improveBy2Opt_Greedy()
{
	int p1,p2,p1Max,p2Max,l1,l2;
	double minDiff,diff;
	double currObjFunction;
	int pBCombi;
	KZO_ExchangeCombination *bestCombis;
	clock_t t1,t2;

	bestCombis=new KZO_ExchangeCombination [400];
	pBCombi=0;
	t1=clock();
	currObjFunction =objFunction ();
	currImprovementIteration=0;
	do {
		minDiff=0;
		for (p1=0;p1<pList.size()-1;p1++)
		{
			for (p2=p1+1;p2<pList.size();p2++)
			{
				/*if (productsByNums[p1]->isLocationInConstrainedLocationSet (locationsByProduct[p2]) &&
					productsByNums[p2]->isLocationInConstrainedLocationSet (locationsByProduct[p1]) &&
					abs (locationsByProductRefSolution [p1]-locationsByProduct[p2])>=usedParams->minLocDistanceForShift && 
					abs (locationsByProductRefSolution [p2]-locationsByProduct[p1])>=usedParams->minLocDistanceForShift
					)*/
				if (isValidExchange (p1,p2)
						)
						
				{	
					diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					if (diff<-0.01)
					{
						p1Max=p1;
						p2Max=p2;
						if (minDiff>diff)
							minDiff=diff;
			
						l1=locationsByProduct[p1];
						l2=locationsByProduct[p2];
						
						sortedListChangePosition (p1,l2,p2,l1);
						
						unassignProductToLocation (p1,l1);
						unassignProductToLocation (p2,l2);
						assignProductToLocation (p1,l2);
						assignProductToLocation (p2,l1);
						currObjFunction +=diff;
						//checkSortedLists ();
						cout<<"Zielfunktion:"<<currObjFunction<<endl;

						currImprovementIteration++;
						
					}
				}
				if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
			}
			if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
		}
		if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
		cout<<"MinDiff:"<<minDiff<<"CurrIt"<<currImprovementIteration<<" MaxNumberIt:"<<usedParams->maxNumberOfImprovementIterations<<endl;
	}
	while (minDiff<-0.01 && currImprovementIteration<usedParams->maxNumberOfImprovementIterations);
	t2=clock();
	double secs;
	secs=((double)(t2-t1))/CLOCKS_PER_SEC;
	cout<<"Finale Zielfunktion:"<<objFunction ()<<" Benötigte Laufzeit:"<<secs<< endl;
}
double zeroOneRNG()
{
	int rv1=rand();
	int rv2=(rv1 % 10000);
	double rv=((double)rv2/10000);
	return rv;
}
void KZO::KZO_OptimizationEngine_Heuristic::improveBy2Opt_Greedy_SA()
{
	int p1,p2,p1Max,p2Max,l1,l2;
	double minDiff,diff;
	double currObjFunction,bestObjFunction;
	int pBCombi;
	KZO_ExchangeCombination *bestCombis;
	clock_t t1,t2;
	double startTemperature=80;
	double currTemperature;
	double currProbVal;
	bool isAccept;
	int maxItPerTemperature=10000,currItTemp;
	bestCombis=new KZO_ExchangeCombination [400];
	pBCombi=0;

	srand(0);
	t1=clock();
	bestObjFunction=currObjFunction =objFunction ();
	currItTemp=0;
	currTemperature =startTemperature;
	currImprovementIteration=0;
	bool isTotAccept;
	do {
		isTotAccept=false;
		minDiff=0;
		for (p1=0;p1<pList.size()-1;p1++)
		{
			for (p2=p1+1;p2<pList.size();p2++)
			{	
				if (isValidExchange (p1,p2)
						)
				{
					isAccept =false;

					diff=exchangeCostDiff (p1,locationsByProduct[p1],p2,locationsByProduct[p2]);
					
					if (diff<-0.01)
					{
						
						isAccept =true;
						currImprovementIteration++;
					}
					else if (diff>0.01)
					{
						currProbVal=exp (-(diff)/currTemperature );
						if (zeroOneRNG ()<currProbVal)
							isAccept=true;
					}

					if (isAccept )
					{
						isTotAccept=true;
						p1Max=p1;
						p2Max=p2;
						if (minDiff>diff)
							minDiff=diff;
			
						l1=locationsByProduct[p1];
						l2=locationsByProduct[p2];
						
						sortedListChangePosition (p1,l2,p2,l1);
						
						unassignProductToLocation (p1,l1);
						unassignProductToLocation (p2,l2);
						assignProductToLocation (p1,l2);
						assignProductToLocation (p2,l1);
						currObjFunction +=diff;
						//checkSortedLists ();
						cout<<"Zielfunktion:"<<currObjFunction<<" Beste Zielfunktion:"<<bestObjFunction<<" Temperatur:"<<currTemperature<<endl;

						if (currObjFunction <bestObjFunction)
						{
							backupCurrentSolution ();
							bestObjFunction =currObjFunction;
						}
						
						currItTemp++;
						if (currItTemp==maxItPerTemperature )
						{
							currItTemp=0;
							currTemperature=currTemperature*0.95;

							if (currObjFunction >bestObjFunction*1.02)
							{
								restoreCurrentSolution ();
								currObjFunction =bestObjFunction;//objFunction ();
							}
						}
					}
					

					
				}
				if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
			}
			if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
		}
		if (currImprovementIteration>=usedParams->maxNumberOfImprovementIterations)
						break;
		
		cout<<"MinDiff:"<<minDiff<<"CurrIt"<<currImprovementIteration<<" MaxNumberIt:"<<usedParams->maxNumberOfImprovementIterations<<endl;
	}
	while (isTotAccept && currImprovementIteration<usedParams->maxNumberOfImprovementIterations);
	t2=clock();
	double secs;
	secs=((double)(t2-t1))/CLOCKS_PER_SEC;
	cout<<"Finale Zielfunktion:"<<objFunction ()<<" Benötigte Laufzeit:"<<secs<< endl;
}

KZO::Optimization_Result *KZO::KZO_OptimizationEngine_Heuristic::evaluateSolution (list<ProductionLocationAssignment> &assignments)
{
	KZO::Optimization_Result *currResult;
	setInitialSolution (assignments);

	if (isSolution )
	{
		currResult=getResult ();
		resetSolution ();
	}
	else
	{
		currResult =new KZO::Optimization_Result ();
		currResult->Status_s (KZO::Optimization_Status ::NO_SOLUTION_FOUND );
		currResult->Cost_s (0);
	}

	return currResult;
}
