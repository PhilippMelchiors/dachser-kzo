﻿namespace Optimizer_Messages
{
    public interface UserMessageGenerator
    {
        string getMessage(int msgId,params string[] parameter);
    }
}