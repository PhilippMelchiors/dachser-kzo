﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimizer_Data;
using System.Data;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization;
using System.Drawing;
using System.Globalization;
namespace Optimizer_Data
{
    public class FilterTableCollection
    {
        public enum SelectionColumnOperatorType
        {
            LEQ,
            EQ,
            GEQ,
            L,
            G
        }
        public delegate void CallReturnVoidParamDataTableString(DataTable entries, string colName);
        public delegate void CallReturnVoidParamDataTable(DataTable dt);
        public delegate DataTable CallReturnDataTableVoidParams();
        public delegate void CallReturnVoidParamDataTableDataTableListOfString(DataTable entries, DataTable availableEntries, List<string> selection);
        public delegate void CallReturnVoidParamBool(bool isEnable);
        protected const string LEQ_STR = "<=";
        protected const string GEQ_STR = ">=";
        protected const string EQ_STR = "=";
        protected const string L_STR = "<";
        protected const string G_STR = ">";

        //public interface OutputAdapter
        //{
        //    void setFilterTableCollection(FilterTableCollection uFTC);
        //    void update();
        //    void initialize();
        //}

        public enum Behavior
        {
            AVAILABLE_BY_COLOR,
            SHOW_ONLY_AVAILABLE,
            IGNORE_AVAILABILITY
        }

        public class ColumnSpec
        {
            public enum ColAggrType
            {
                SUM,
                AVG,
                COUNT,
                MIN,
                MAX,
                NONE
            }
            public bool IsEnabled = true;

            public string ColumnName;
            public string NewColumnName;
            public string SQL_Type;
            public bool IsDimensionColumn;
            public bool IsNonDimensionColumn;
            public bool IsVisibleColumn;
            public bool IsSortColumn;
            public bool IsFilteredColumn;
            public bool IsFilterEntryColumn;
            public bool IsZeroForNull = false;
            public string DateTimeFormat = "%Y-%m-%dT%H:%M:%S";
            public ColAggrType AggrType;
            public static string AGGRTYPE_SUM_STR = "SUM";
            public static string AGGRTYPE_AVG_STR = "AVG";
            public static string AGGRTYPE_COUNT_STR = "COUNT";
            public static string AGGRTYPE_MIN_STR = "MIN";
            public static string AGGRTYPE_MAX_STR = "MAX";
            public void fillColumnSpecFromString(string[] colSpec)
            {
                ColumnName = colSpec[0];
                SQL_Type = colSpec[1];

                IsVisibleColumn = Convert.ToBoolean(colSpec[2]);

                IsDimensionColumn = Convert.ToBoolean(colSpec[3]);

                IsNonDimensionColumn = Convert.ToBoolean(colSpec[4]);

                IsSortColumn = Convert.ToBoolean(colSpec[5]);

                IsFilteredColumn = Convert.ToBoolean(colSpec[6]);

                IsFilterEntryColumn = Convert.ToBoolean(colSpec[7]);

                NewColumnName = colSpec[8];

                if (colSpec[9] == AGGRTYPE_AVG_STR)
                    AggrType = ColumnSpec.ColAggrType.AVG;
                else if (colSpec[9] == AGGRTYPE_COUNT_STR)
                    AggrType = ColumnSpec.ColAggrType.COUNT;
                else if (colSpec[9] == AGGRTYPE_SUM_STR)
                    AggrType = ColumnSpec.ColAggrType.SUM;
                else if (colSpec[9] == AGGRTYPE_MIN_STR)
                    AggrType = ColumnSpec.ColAggrType.MIN;
                else if (colSpec[9] == AGGRTYPE_MAX_STR)
                    AggrType = ColumnSpec.ColAggrType.MAX;
                else
                    AggrType = ColumnSpec.ColAggrType.NONE;

                if (colSpec[10] != "")
                    DateTimeFormat = colSpec[10];
            }
        }
        protected class SelectionColumn
        {
            public string columnName;
            public string orderColumnName;
            public string usedOperatorStr;
            public DataTable explicitlySelectedItems;
            public List<Selection_Adapter> selectionAdapters;
            public SelectionColumn()
            {
                selectionAdapters = new List<Selection_Adapter>();
                explicitlySelectedItems = null;
            }
        }
        protected class SelectionGroup
        {
            public Dictionary<string, List<Data_Adapter>> dataAdapters;
            public Dictionary<string, SelectionColumn> selectionColumns;
            public List<FilterEntries> filterList;
            //public Dictionary<string, TableSpec> assignedTableSpecs;

            public SelectionGroup()
            {
                dataAdapters = new Dictionary<string, List<Data_Adapter>>();
                selectionColumns = new Dictionary<string, SelectionColumn>();
                filterList = new List<FilterEntries>();
            }
        }

        public class TableJoin
        {
            public enum JoinType
            {
                JOIN,
                LEFT_JOIN,
                INNER_JOIN,
                OUTER_JOIN,
                CROSS_JOIN
            };
            public string tableName;
            public JoinType jType;
        }

        public class TableSpec
        {
            public static string JOINTYPE_JOIN_STR = "JOIN";
            public static string JOINTYPE_LEFTJOIN_STR = "LEFT_JOIN";
            public static string JOINTYPE_INNERJOIN_STR = "INNER_JOIN";
            public static string JOINTYPE_OUTERJOIN_STR = "OUTER_JOIN";
            public static string JOINTYPE_CROSSJOIN_STR = "CROSS_JOIN";

            public String SelectionGroupName;

            public string TableName;

            public bool IsUseSQLCmd;
            public string BaseSQLCmd;

            public bool IsUseCalculatedTable;
            public string CalcTableName;

            public bool IsUseDimensions;

            public bool IsDistinctRows;
            public string addFilterExpression = "";
            public Dictionary<string, ColumnSpec> ColumnSpecs;//ColumnSpec[] ColumnSpecs;

            public TableJoin[] TablesToJoin;

            public static string SQL_DATETIME = "datetime";
            public static string SQL_DATE = "date";
            public static string SQL_VARCHAR = "varchar";
            public static string SQL_TEXT = "text";
            public static string SQL_INTEGER = "integer";
            public static string SQL_DOUBLE = "double";
            //public int getColumnNum(string columnName)
            //{
            //    int i, tL;

            //    tL = ColumnSpecs.Length;
            //    for (i = 0; i < tL; i++)
            //    {
            //        if (ColumnSpecs[i].NewColumnName  ==columnName)
            //            return i;
            //    }
            //    return -1;
            //}

            public ColumnSpec getColSpecByName(String name)
            {
                if (ColumnSpecs.ContainsKey(name))
                    return ColumnSpecs[name];
                else
                    return null;
            }

            public string getDimensionColumnsEnum()
            {
                int i, tL;
                bool isFirst;
                string enumStr = "";
                isFirst = true;


                //tL = ColumnSpecs.Length;
                //for (i = 0; i < tL; i++)
                //{
                //    if (ColumnSpecs[i].isDimensionColumn)
                //    {
                //        if (!isFirst)
                //            enumStr += ",";
                //        else
                //            isFirst = false;

                //        enumStr += ColumnSpecs[i].ColumnName;
                //    }
                //}

                foreach (KeyValuePair<String, ColumnSpec> currColSpecKV in ColumnSpecs)
                {
                    if (currColSpecKV.Value.IsDimensionColumn)
                    {
                        if (!isFirst)
                            enumStr += ",";
                        else
                            isFirst = false;

                        enumStr += currColSpecKV.Value.ColumnName;//ColumnSpecs[i].ColumnName;
                    }
                }

                return enumStr;
            }



            public void fillTableSpecFromString(string[] globalSpecs, string[,] colSpecs, string[,] tabJoinSpecs)
            {
                int i;
                TableName = globalSpecs[0];
                IsUseSQLCmd = Convert.ToBoolean(globalSpecs[1]);
                BaseSQLCmd = globalSpecs[2];
                IsUseCalculatedTable = Convert.ToBoolean(globalSpecs[3]);
                CalcTableName = globalSpecs[4];
                IsUseDimensions = Convert.ToBoolean(globalSpecs[5]);
                IsDistinctRows = Convert.ToBoolean(globalSpecs[6]);
                addFilterExpression = globalSpecs[7];
                SelectionGroupName = globalSpecs[8];
                //ColumnSpecs = new ColumnSpec[colSpecs.GetLength (0)];

                //for (i = 0; i < colSpecs.GetLength (0); i++)
                //{
                //    ColumnSpecs[i] = new ColumnSpec();
                //    ColumnSpecs[i] = new FilterTableCollection.ColumnSpec();
                //    ColumnSpecs[i].ColumnName = colSpecs[i, 0];
                //    ColumnSpecs[i].SQL_Type = colSpecs[i, 1];

                //    ColumnSpecs[i].isVisibleColumn = Convert.ToBoolean(colSpecs[i, 2]);

                //    ColumnSpecs[i].isDimensionColumn = Convert.ToBoolean(colSpecs[i, 3]);

                //    ColumnSpecs[i].isNonDimensionColumn = Convert.ToBoolean(colSpecs[i, 4]);

                //    ColumnSpecs[i].isSortColumn = Convert.ToBoolean(colSpecs[i, 5]);

                //    ColumnSpecs[i].isFilteredColumn = Convert.ToBoolean(colSpecs[i, 6]);

                //    ColumnSpecs[i].isFilterEntryColumn = Convert.ToBoolean(colSpecs[i, 7]);

                //    ColumnSpecs[i].NewColumnName = colSpecs[i, 8];

                //    if (colSpecs[i, 9] == AGGRTYPE_AVG_STR)
                //        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.AVG;
                //    else if (colSpecs[i, 9] == AGGRTYPE_COUNT_STR)
                //        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.COUNT;
                //    else if (colSpecs[i, 9] == AGGRTYPE_SUM_STR)
                //        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.SUM;
                //    else if (colSpecs[i, 9] == AGGRTYPE_MIN_STR)
                //        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.MIN;
                //    else if (colSpecs[i, 9] == AGGRTYPE_MAX_STR)
                //        ColumnSpecs[i].AggrType = ColumnSpec .ColAggrType .MAX;
                //    else
                //        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.NONE;

                //    if (colSpecs[i, 10]!="")
                //     ColumnSpecs[i].DateTimeFormat = colSpecs[i, 10];

                //    //if (ColumnSpecs[i, 2] == "true")
                //    //    sTabSQLColSpecs[i].isVisibleColumn = true;
                //    //else
                //    //    sTabSQLColSpecs[i].isVisibleColumn = false;

                //    //if (sTabSpec[i, 3] == "true")
                //    //    sTabSQLColSpecs[i].isDimensionColumn = true;
                //    //else
                //    //    sTabSQLColSpecs[i].isDimensionColumn = false;

                //    //if (sTabSpec[i, 4] == "true")
                //    //    sTabSQLColSpecs[i].isNonDimensionColumn = true;
                //    //else
                //    //    sTabSQLColSpecs[i].isNonDimensionColumn = false;

                //    //if (sTabSpec[i, 5] == "true")
                //    //    sTabSQLColSpecs[i].isSortColumn = true;
                //    //else
                //    //    sTabSQLColSpecs[i].isSortColumn = false;
                //    //if (sTabSpec[i, 6] == "true")
                //    //    sTabSQLColSpecs[i].isFilteredColumn = true;
                //    //else
                //    //    sTabSQLColSpecs[i].isFilteredColumn = false;

                //    //sTabSQLColSpecs[i].NewColumnName = sTabSpec[i, 7];
                //}
                ColumnSpecs = new Dictionary<string, ColumnSpec>();//ColumnSpec[colSpecs.GetLength(0)];
                ColumnSpec currColSpec;
                for (i = 0; i < colSpecs.GetLength(0); i++)
                {

                    if (!ColumnSpecs.ContainsKey(colSpecs[i, 8]))
                    {
                        currColSpec = new ColumnSpec();

                        currColSpec.ColumnName = colSpecs[i, 0];
                        currColSpec.SQL_Type = colSpecs[i, 1];

                        currColSpec.IsVisibleColumn = Convert.ToBoolean(colSpecs[i, 2]);

                        currColSpec.IsDimensionColumn = Convert.ToBoolean(colSpecs[i, 3]);

                        currColSpec.IsNonDimensionColumn = Convert.ToBoolean(colSpecs[i, 4]);

                        currColSpec.IsSortColumn = Convert.ToBoolean(colSpecs[i, 5]);

                        currColSpec.IsFilteredColumn = Convert.ToBoolean(colSpecs[i, 6]);

                        currColSpec.IsFilterEntryColumn = Convert.ToBoolean(colSpecs[i, 7]);

                        currColSpec.IsEnabled = true;

                        currColSpec.NewColumnName = colSpecs[i, 8];

                        if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_AVG_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.AVG;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_COUNT_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.COUNT;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_SUM_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.SUM;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_MIN_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.MIN;
                        else if (colSpecs[i, 9] == ColumnSpec.AGGRTYPE_MAX_STR)
                            currColSpec.AggrType = ColumnSpec.ColAggrType.MAX;
                        else
                            currColSpec.AggrType = ColumnSpec.ColAggrType.NONE;

                        if (colSpecs[i, 10] != "")
                            currColSpec.DateTimeFormat = colSpecs[i, 10];

                        ColumnSpecs.Add(currColSpec.NewColumnName, currColSpec);
                    }
                }


                TablesToJoin = new TableJoin[tabJoinSpecs.GetLength(0)];

                for (i = 0; i < tabJoinSpecs.GetLength(0); i++)
                {
                    TablesToJoin[i] = new TableJoin();
                    TablesToJoin[i].tableName = tabJoinSpecs[i, 0];
                    if (tabJoinSpecs[i, 1] == JOINTYPE_JOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_LEFTJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.LEFT_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_INNERJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.INNER_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_OUTERJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.OUTER_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_CROSSJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.CROSS_JOIN;
                    else
                        TablesToJoin[i].jType = TableJoin.JoinType.JOIN;
                }
            }
        }

        public interface Selection_Adapter
        {
            void setFilterTableCollection(FilterTableCollection uFTC);
            //void fillColumnEntries(DataTable entries,DataTable availableEntries,List<string> selection);
            void setTotalColumnEntries(DataTable entries, string colName);
            void setAvailableColumnEntries(DataTable entries, string colName);
            void setSelectedEntries(DataTable selectedEntries, string colName);
            DataTable getSelectedEntries();
            bool verifySender(Object sender);
            void enableDisable(bool isEnabled);
        };

        //protected class SelectionAdapterInfo
        //{
        //    public Selection_Adapter sA;
        //    public string orderColumn;
        //}

        public interface Data_Adapter
        {
            void setData(DataTable data);
            void setFilterTableCollection(FilterTableCollection uFTC);
        }

        public class GridViewAdapter : Data_Adapter
        {
            DataGridView usedDGV;
            //FilterTableCollection usedFilterTableCollection;

            protected string usedSelectionGroup;
            protected string usedTableName;
            protected bool isAutoAdjustColumns = false;

            public delegate void CallVoidReturnParamsObject(Object sender);
            public bool IsAutoAdjustColumns
            {
                get { return isAutoAdjustColumns; }
                set
                {
                    isAutoAdjustColumns = value;
                    if (isAutoAdjustColumns)
                        usedDGV.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(checkColumnWidth);
                    else
                    {
                        usedDGV.DataBindingComplete -= new DataGridViewBindingCompleteEventHandler(checkColumnWidth);
                        //usedDGV.DataBindingComplete += null;
                        usedDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    }
                }
            }

            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String TableName
            {
                get { return usedTableName; }
                set { usedTableName = value; }
            }

            public GridViewAdapter(DataGridView uDGV)
            {
                usedDGV = uDGV;

                usedDGV.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(checkColumnWidth);
            }

            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                //usedFilterTableCollection = uFTC;
            }

            public void checkColumnWidth(Object sender, EventArgs args)
            {
                int currentColsWidth;
                // if (isAutoAdjustColumns)
                //{
                currentColsWidth = 0;
                foreach (DataGridViewColumn dGVC in usedDGV.Columns)
                {
                    currentColsWidth += dGVC.Width;
                }

                if (currentColsWidth < usedDGV.Width)
                    usedDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                else
                    usedDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                //   }

            }
            public void setData(DataTable data)
            {
                int i;

                if (usedDGV.InvokeRequired)
                {
                    usedDGV.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    usedDGV.DataSource = data;
                }
            }


            //public void update()
            //{
            //    usedDGV.DataSource = usedFilterTableCollection.getFilteredData(usedSelectionGroup, usedTableName);
            //}
            //public void initialize()
            //{
            //    update();
            //}
        }
        //public class ListBoxOutputAdapter : Data_Adapter
        //{
        //    ListBox usedLB;
        //    Dictionary<string, int> elementIndices;
        //    public ListBoxOutputAdapter(ListBox lB)
        //    {
        //        usedLB = lB;
        //    }

        //    public void setFilterTableCollection(FilterTableCollection uFTC)
        //    {
        //        // usedFilterTableCollection = uFTC;
        //    }
        //    public void setData(DataTable data)
        //    {
        //        if (usedLB.InvokeRequired)
        //        {
        //            usedLB.Invoke(new CallReturnVoidParamDataTable(setData), data);
        //        }
        //        else
        //        {
        //            usedLB.BindingContext = new BindingContext();
        //            usedLB.DataSource = data;
        //            usedLB.DisplayMember = data.Columns[0].ColumnName;
        //            usedLB.ValueMember = data.Columns[0].ColumnName;
        //        }
        //    }
        //}
        public class ChartAdapter : Data_Adapter
        {
            System.Windows.Forms.DataVisualization.Charting.Chart usedChart;
            //FilterTableCollection.TableSpec usedTabSpec;
            FilterTableCollection usedFilterTableCollection;
            protected string ISO_8601_FORMAT_STRING = "yyyy-MM-ddTHH:mm:ss";
            protected string usedSelectionGroup;
            protected string usedTableName;
            protected CultureInfo usedC;
            Dictionary<string, SeriesInfo> usedSeries;

            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String TableName
            {
                get { return usedTableName; }
                set { usedTableName = value; }
            }

            public CultureInfo Culture
            {
                get { return usedC; }
                set { usedC = value; }

            }

            public ChartAdapter(System.Windows.Forms.DataVisualization.Charting.Chart uC)
            {
                usedC = CultureInfo.InvariantCulture;
                usedChart = uC;
                usedSeries = new Dictionary<string, SeriesInfo>();//List<SeriesInfo>();//new List<System.Windows.Forms.DataVisualization.Charting.Series>();
            }

            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }

            public void clearSeries()
            {
                usedSeries.Clear();
            }

            public void setTabSpec(TableSpec tabSpec)
            {
                //usedTabSpec = tabSpec;

            }
            public System.Windows.Forms.DataVisualization.Charting.ChartValueType transLateSQLTypeToChartType(string sqlType)
            {
                if (sqlType == TableSpec.SQL_DATE || sqlType == TableSpec.SQL_DATETIME)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
                else if (sqlType == TableSpec.SQL_TEXT || sqlType == TableSpec.SQL_VARCHAR)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
                else if (sqlType == TableSpec.SQL_INTEGER)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
                else if (sqlType == TableSpec.SQL_DOUBLE)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            }


            public Type transLateSQLTypeToDataType(string sqlType)
            {
                if (sqlType == TableSpec.SQL_DATE || sqlType == TableSpec.SQL_DATETIME)
                    return typeof(DateTime);
                else if (sqlType == TableSpec.SQL_TEXT || sqlType == TableSpec.SQL_VARCHAR)
                    return typeof(String);
                else if (sqlType == TableSpec.SQL_INTEGER)
                    return typeof(Int32);
                else if (sqlType == TableSpec.SQL_DOUBLE)
                    return typeof(Double);

                return typeof(String);
            }

            //List<System.Windows.Forms.DataVisualization.Charting.Series> usedSeries;


            protected class SeriesInfo
            {
                public System.Windows.Forms.DataVisualization.Charting.Series s;
                public string yColName;
                public string rangeColName;
                public object rangeMin;
                public object rangeMax;
                public string dateTimeFormat;
            }

            public void setRangeData(string yColName, string rangeColName, object rangeMin, object rangeMax, string dateTimeFormat)
            {
                SeriesInfo currSInfo;

                currSInfo = usedSeries[yColName];
                currSInfo.rangeColName = rangeColName;
                currSInfo.rangeMin = rangeMin;
                currSInfo.rangeMax = rangeMax;
                currSInfo.dateTimeFormat = dateTimeFormat;
            }
            public void addSeries(string xColName, bool isSecondaryX, string yColName, bool isSecondaryY, Color usedColor, string rangeColName, object rangeMin, object rangeMax, string dateTimeFormat)
            {
                System.Windows.Forms.DataVisualization.Charting.Series currSeries;
                //int currColNum;
                ColumnSpec currColSpec;
                TableSpec usedTabSpec;
                SeriesInfo currSInfo;

                usedTabSpec = usedFilterTableCollection.getTablSpec(usedTableName);
                currSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                currColSpec = usedTabSpec.getColSpecByName(xColName);
                //currColNum=usedTabSpec .getColumnNum (xColName );
                currSeries.Name = yColName;
                currSeries.XValueMember = xColName;

                currSeries.XValueType = transLateSQLTypeToChartType(currColSpec.SQL_Type);//(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

                if (isSecondaryX)
                    currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
                else
                    currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

                //currColNum=usedTabSpec .getColumnNum (yColName );
                currColSpec = usedTabSpec.getColSpecByName(yColName);
                currSeries.YValueMembers = yColName;
                currSeries.YValueType = transLateSQLTypeToChartType(currColSpec.SQL_Type);//transLateSQLTypeToChartType(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

                if (isSecondaryY)
                    currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
                else
                    currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                currSeries.ChartArea = "Default";

                currSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                currSeries.Color = usedColor;

                currSInfo = new SeriesInfo();
                currSInfo.s = currSeries;
                currSInfo.rangeColName = rangeColName;
                currSInfo.rangeMin = rangeMin;
                currSInfo.rangeMax = rangeMax;
                currSInfo.yColName = yColName;
                currSInfo.dateTimeFormat = dateTimeFormat;
                usedSeries.Add(yColName, currSInfo);
            }

            public void setPrimaryXAxsis(string label)
            {
                usedXAxis1Label = label;
            }
            public void setSecondaryXAxsis(string label)
            {
                usedXAxis2Label = label;
            }
            public void setSecondaryYAxsis(string label)
            {
                usedYAxis1Label = label;
            }
            public void setPrimaryYAxsis(string label)
            {
                usedYAxis2Label = label;
            }

            protected string usedXAxis1Label;
            protected string usedXAxis2Label;
            protected string usedYAxis1Label;
            protected string usedYAxis2Label;
            public void setData(DataTable data)
            {
                DataTable cData;
                double minDbl, maxDbl, cDbl;
                int minInt, maxInt, cInt;
                DateTime minDate, maxDate, cDate;
                TableSpec usedTabSpec;
                //int rangeColSpecNum;
                ColumnSpec rangeColSpec;
                if (usedChart.InvokeRequired)
                {
                    usedChart.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    usedChart.Series.Clear();
                    usedTabSpec = usedFilterTableCollection.getTablSpec(usedTableName);
                    cData = data.Copy();

                    foreach (KeyValuePair<string, SeriesInfo> currSInfo in usedSeries)//(System.Windows.Forms.DataVisualization.Charting.Series currSeries in usedSeries)
                    {
                        if (currSInfo.Value.rangeColName != null && cData.Columns.Contains(currSInfo.Value.rangeColName))
                        {
                            //rangeColSpecNum = usedTabSpec.getColumnNum(currSInfo.Value.rangeColName);
                            rangeColSpec = usedTabSpec.getColSpecByName(currSInfo.Value.rangeColName);
                            if (transLateSQLTypeToDataType(rangeColSpec.SQL_Type) == typeof(Int32))//(usedTabSpec .ColumnSpecs [rangeColSpecNum].SQL_Type) == typeof(int))
                            {
                                if (currSInfo.Value.rangeMin == null)
                                    minInt = Int32.MinValue;
                                else
                                    minInt = (int)currSInfo.Value.rangeMin;

                                if (currSInfo.Value.rangeMax == null)
                                    maxInt = Int32.MaxValue;
                                else
                                    maxInt = (int)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cInt = (int)r[currSInfo.Value.rangeColName];//Convert.ToInt32  (r[rangeColNum].ToString (),usedC );//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cInt <= minInt || cInt >= maxInt)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                }
                            }
                            else if (transLateSQLTypeToDataType(rangeColSpec.SQL_Type) == typeof(double))//(usedTabSpec.ColumnSpecs[rangeColSpecNum].SQL_Type) == typeof(double))
                            {
                                if (currSInfo.Value.rangeMin == null)
                                    minDbl = Double.MinValue;
                                else
                                    minDbl = (double)currSInfo.Value.rangeMin;

                                if (currSInfo.Value.rangeMax == null)
                                    maxDbl = Double.MaxValue;
                                else
                                    maxDbl = (double)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cDbl = (double)r[currSInfo.Value.rangeColName];//Convert.ToDouble(r[rangeColNum].ToString(), usedC);//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cDbl <= minDbl || cDbl >= maxDbl)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                }
                            }
                            else if (transLateSQLTypeToDataType(rangeColSpec.SQL_Type) == typeof(DateTime))//(transLateSQLTypeToDataType(usedTabSpec.ColumnSpecs[rangeColSpecNum].SQL_Type) == typeof(DateTime))
                            {
                                if (currSInfo.Value.rangeMin == null)
                                    minDate = Date.MinValue;
                                else
                                    minDate = (DateTime)currSInfo.Value.rangeMin;

                                if (currSInfo.Value.rangeMax == null)
                                    maxDate = Date.MaxValue;
                                else
                                    maxDate = (DateTime)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cDate = DateTime.ParseExact(r[currSInfo.Value.rangeColName].ToString(), currSInfo.Value.dateTimeFormat, usedC);//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cDate <= minDate || cDate >= maxDate)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                }
                            }
                        }

                        usedChart.Series.Add(currSInfo.Value.s);
                    }
                    usedChart.DataSource = cData;
                    usedChart.ChartAreas[0].AxisX.Title = usedXAxis1Label;
                    usedChart.ChartAreas[0].AxisY.Title = usedYAxis1Label;
                    usedChart.ChartAreas[0].AxisX2.Title = usedXAxis2Label;
                    usedChart.ChartAreas[0].AxisY2.Title = usedYAxis2Label;
                }
            }

            //public void update()
            //{   
            //    usedChart.DataSource = usedFilterTableCollection.getFilteredData(usedSelectionGroup, usedTableName);
            //}

            //public void initialize()
            //{
            //    usedChart.Series.Clear();
            //    foreach (System.Windows.Forms.DataVisualization.Charting.Series currSeries in usedSeries)
            //    {
            //        usedChart.Series.Add(currSeries);
            //    }
            //    update();
            //}
        }

        public class ListBoxAdapter : FilterTableCollection.Selection_Adapter, Data_Adapter//,OutputAdapter 
        {
            ListBox usedLB;
            protected string usedSelectionGroup;
            protected string usedColumnName;
            protected bool isPreserveSelection = true;
            FilterTableCollection usedFilterTableCollection;
            Dictionary<string, int> elementIndices;

            public ListBoxAdapter()
            {
                elementIndices = new Dictionary<string, int>();
            }

            public bool IsPreserveSelection
            {
                get { return isPreserveSelection; }
                set { isPreserveSelection = value; }
            }

            public void enableDisable(bool isEnable)
            {
                if (usedLB.InvokeRequired)
                    usedLB.Invoke(new CallReturnVoidParamBool(enableDisable), isEnable);
                else
                {
                    usedLB.Enabled = isEnable;
                }
            }

            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String ColumnName
            {
                get { return usedColumnName; }
                set { usedColumnName = value; }
            }

            public FilterTableCollection UsedFilterTableCollection
            {
                get { return usedFilterTableCollection; }
                set { usedFilterTableCollection = value; }
            }

            public bool verifySender(Object sender)
            {
                ListBox sLB;

                if (sender != null && sender.GetType() == typeof(ListBox))
                {
                    sLB = (ListBox)sender;

                    if (sLB == usedLB)
                        return true;
                }

                return false;
            }

            //protected string usedColName;
            //public String ColName
            //{
            //    get { return usedColName; }
            //    set { usedColName = value; }
            //}

            public void setData(DataTable data)
            {
                List<string> selectedDataItems;
                DataRowView currDRV;
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    selectedDataItems = null;

                    storeElementIndices(data, usedColumnName);

                    if (isPreserveSelection && usedLB.ValueMember == usedColumnName)
                    {
                        selectedDataItems = new List<string>();

                        foreach (object o in usedLB.SelectedItems)
                        {
                            currDRV = (DataRowView)o;
                            selectedDataItems.Add(currDRV.Row[usedColumnName].ToString());
                        }
                    }

                    usedLB.BindingContext = new BindingContext();
                    usedLB.DataSource = data;
                    usedLB.DisplayMember = usedColumnName;//data.Columns[usedColumnName].ColumnName;
                    usedLB.ValueMember = usedColumnName;//data.Columns[usedColumnName].ColumnName;

                    if (isPreserveSelection && selectedDataItems != null)
                    {
                        foreach (string item in selectedDataItems)
                        {
                            if (elementIndices.ContainsKey(item))
                                usedLB.SetSelected(elementIndices[item], true);
                        }
                    }
                }
            }
            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }

            public ListBoxAdapter(ListBox lB)
                : this()
            {
                usedLB = lB;
            }
            //int getColNum(string colNames, DataTable entries)
            //{
            //    int i;

            //    for (i = 0; i < entries.Columns.Count; i++)
            //    {
            //        if (entries.Columns [i]
            //    }
            //}
            void storeElementIndices(DataTable entries, string colName)
            {
                int i;

                elementIndices.Clear();

                for (i = 0; i < entries.Rows.Count; i++)
                {
                    elementIndices.Add(entries.Rows[i][colName].ToString(), i);
                }
            }

            public void setTotalColumnEntries(DataTable entries, string colName)
            {
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setTotalColumnEntries), entries, colName);
                }
                else
                {
                    storeElementIndices(entries, colName);
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[colName].ColumnName;
                    usedLB.ValueMember = entries.Columns[colName].ColumnName;
                }
            }
            public void setAvailableColumnEntries(DataTable entries, string colName)
            {
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setAvailableColumnEntries), entries, colName);
                }
                else
                {
                    storeElementIndices(entries, colName);
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[colName].ColumnName;
                    usedLB.ValueMember = entries.Columns[colName].ColumnName;
                }
            }
            public void setSelectedEntries(DataTable selectedEntries, string colName)
            {
                int currIndex;
                string currElementContent;

                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setSelectedEntries), selectedEntries, colName);
                }
                else
                {
                    foreach (DataRow r in selectedEntries.Rows)
                    {
                        currElementContent = r[colName].ToString();
                        if (elementIndices.ContainsKey(currElementContent))
                        {
                            currIndex = elementIndices[currElementContent];
                            usedLB.SetSelected(currIndex, true);
                        }
                    }
                }
            }
            public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
            {
                int i, j;
                DataRowView currItem;

                if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY ||
                    usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR
                    )
                {
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[0].ColumnName;
                    usedLB.ValueMember = entries.Columns[0].ColumnName;
                }
                else if (usedFilterTableCollection.UsedBehavior == Behavior.SHOW_ONLY_AVAILABLE)
                {
                    usedLB.DataSource = availableEntries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[0].ColumnName;
                    usedLB.ValueMember = entries.Columns[0].ColumnName;
                }

                for (i = 0; i < usedLB.Items.Count; i++)
                {
                    currItem = (DataRowView)usedLB.Items[i];
                    if (selection.Contains(currItem.Row.ItemArray[0].ToString()))
                        usedLB.SetSelected(i, true);
                    else
                        usedLB.SetSelected(i, false);
                }
            }

            public DataTable getSelectedEntries()
            {
                DataTable entries;
                string[] currRow;

                if (usedLB.InvokeRequired)
                {
                    entries = (DataTable)usedLB.Invoke(new CallReturnDataTableVoidParams(getSelectedEntries));
                }
                else
                {
                    entries = new DataTable();
                    entries.Columns.Add();

                    foreach (DataRowView currItemRV in usedLB.SelectedItems)
                    {
                        currRow = new string[1];
                        currRow[0] = currItemRV.Row[0].ToString();
                        entries.Rows.Add(currRow);
                    }
                }
                return entries;
            }
            //void update()
            //{
            //    if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY ||
            //       usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR
            //       )
            //    {get
            //        usedLB.DataSource = usedFilterTableCollection .getColumnEntries (usedSelectionGroup ,usedColumnName ,;
            //        usedLB.BindingContext = new BindingContext();
            //        usedLB.DisplayMember = entries.Columns[0].ColumnName;
            //        usedLB.ValueMember = entries.Columns[0].ColumnName;
            //    }
            //    else if (usedFilterTableCollection.UsedBehavior == Behavior.SHOW_ONLY_AVAILABLE)
            //    {
            //        usedLB.DataSource = availableEntries;
            //        usedLB.BindingContext = new BindingContext();
            //        usedLB.DisplayMember = entries.Columns[0].ColumnName;
            //        usedLB.ValueMember = entries.Columns[0].ColumnName;
            //    }

            //    for (i = 0; i < usedLB.Items.Count; i++)
            //    {
            //        currItem = (DataRowView)usedLB.Items[i];
            //        if (selection.Contains(currItem.Row.ItemArray[0].ToString()))
            //            usedLB.SetSelected(i, true);
            //        else
            //            usedLB.SetSelected(i, false);
            //    }

            //}
            //void initialize()
            //{
            //}
        }

        public class ListViewAdapter : FilterTableCollection.Selection_Adapter
        {
            ListView usedLV;
            protected string usedSelectionGroup;
            protected string usedTableName;
            protected Color availColor = Color.White;
            protected Color notAvailColor = Color.Gray;
            Dictionary<string, int> elementIndices;
            List<int> previousAvailableIndices;
            List<int> previousSelectedIndices;
            FilterTableCollection usedFilterTableCollection;

            public ListViewAdapter()
            {
                elementIndices = new Dictionary<string, int>();
                previousAvailableIndices = new List<int>();
                previousSelectedIndices = new List<int>();
            }

            public FilterTableCollection UsedFilterTableCollection
            {
                get { return usedFilterTableCollection; }
                set { usedFilterTableCollection = value; }
            }

            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }
            public void enableDisable(bool isEnable)
            {
                if (usedLV.InvokeRequired)
                    usedLV.Invoke(new CallReturnVoidParamBool(enableDisable), isEnable);
                else
                    usedLV.Enabled = isEnable;
            }
            public bool verifySender(Object sender)
            {
                ListView sLB;

                if (sender != null && sender.GetType() == typeof(ListView))
                {
                    sLB = (ListView)sender;

                    if (sLB == usedLV)
                        return true;
                }

                return false;
            }

            public Color AvailColor
            {
                set { availColor = value; }
                get { return availColor; }
            }

            public Color NotAvailColor
            {
                set { notAvailColor = value; }
                get { return notAvailColor; }
            }

            public ListViewAdapter(ListView lV)
                : this()
            {
                usedLV = lV;
            }
            public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
            {
                int i, j;
                DataTable usedDT;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableDataTableListOfString(setColumnEntries), entries, availableEntries, selection);
                }
                else
                {

                    if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY || usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
                    {
                        usedDT = entries;
                    }
                    else
                    {
                        usedDT = availableEntries;
                    }

                    if (selection.Count == 0)
                    {
                        usedLV.Items.Clear();
                        for (i = 0; i < usedDT.Rows.Count; i++)
                        {
                            usedLV.Items.Add(new ListViewItem(usedDT.Rows[i].ItemArray[0].ToString()));
                        }
                    }

                    if (usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
                    {
                        for (i = 0; i < usedLV.Items.Count; i++)
                        {
                            for (j = 0; j < availableEntries.Rows.Count; j++)
                            {
                                if (availableEntries.Rows[j].ItemArray[0].ToString() == usedLV.Items[i].Text)
                                    break;
                            }

                            if (j < availableEntries.Rows.Count)
                                usedLV.Items[i].BackColor = availColor;
                            else
                            {
                                usedLV.Items[i].BackColor = notAvailColor;
                            }

                            usedLV.Items[i].Selected = false;

                            foreach (string selectedItem in selection)
                            {
                                if (usedLV.Items[i].Text == selectedItem)
                                    usedLV.Items[i].Selected = true;
                            }
                        }
                    }
                }
            }

            void storeElementIndices(DataTable entries, string colName)
            {
                int i;

                elementIndices.Clear();

                for (i = 0; i < entries.Rows.Count; i++)
                {
                    elementIndices.Add(entries.Rows[i][colName].ToString(), i);
                }
            }

            public void setTotalColumnEntries(DataTable entries, string colName)
            {
                int i;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setTotalColumnEntries), entries, colName);
                }
                else
                {
                    usedLV.Items.Clear();
                    for (i = 0; i < entries.Rows.Count; i++)
                    {
                        usedLV.Items.Add(new ListViewItem(entries.Rows[i][colName].ToString()));
                    }

                    storeElementIndices(entries, colName);
                    previousAvailableIndices.Clear();
                    previousSelectedIndices.Clear();
                }
            }
            public void setAvailableColumnEntries(DataTable entries, string colName)
            {
                int currIndex;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setAvailableColumnEntries), entries, colName);
                }
                else
                {

                    foreach (int pAI in previousAvailableIndices)
                    {
                        usedLV.Items[pAI].BackColor = notAvailColor;
                    }
                    previousAvailableIndices.Clear();

                    foreach (DataRow r in entries.Rows)
                    {
                        currIndex = elementIndices[r[colName].ToString()];
                        usedLV.Items[currIndex].BackColor = availColor;
                        previousAvailableIndices.Add(currIndex);
                    }
                }
            }
            public void setSelectedEntries(DataTable entries, string colName)
            {
                int currIndex;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setSelectedEntries), entries, colName);
                }
                else
                {
                    foreach (int pSI in previousSelectedIndices)
                    {
                        usedLV.Items[pSI].Selected = false;
                    }

                    previousSelectedIndices.Clear();
                    foreach (DataRow r in entries.Rows)
                    {
                        currIndex = elementIndices[r[colName].ToString()];
                        usedLV.Items[currIndex].Selected = true;
                        previousSelectedIndices.Add(currIndex);
                    }
                }
            }

            public DataTable getSelectedEntries()
            {
                DataTable entries;
                string[] currRow;

                if (usedLV.InvokeRequired)
                {
                    entries = (DataTable)usedLV.Invoke(new CallReturnDataTableVoidParams(getSelectedEntries));
                }
                else
                {
                    entries = new DataTable();
                    entries.Columns.Add();

                    foreach (ListViewItem currItem in usedLV.SelectedItems)
                    {
                        currRow = new string[1];
                        currRow[0] = currItem.Text;
                        entries.Rows.Add(currRow);
                    }
                }
                return entries;
            }
        }

        protected Behavior usedBehavior;
        public Behavior UsedBehavior
        {
            get { return usedBehavior; }
            set { usedBehavior = value; }
        }

        protected DataBase usedDb;

        protected bool isChangeColumnEntriesOnSelection;
        // protected bool isInResponse;
        protected bool isSortByCols = false;

        public class FilterEntries
        {

            public string colName;
            //public string orderColName;
            public List<string> filterEntries;
        }
        protected bool isBlockHandling;
        Dictionary<string, TableSpec> tableSpecs;
        Dictionary<string, SelectionGroup> selectionGroups;

        protected bool isEnableSAs = true;

        public bool IsEnabledAllSelectionAdapters
        {
            set
            {
                foreach (KeyValuePair<string, SelectionGroup> sGKV in selectionGroups)
                {
                    foreach (KeyValuePair<string, SelectionColumn> sCKV in sGKV.Value.selectionColumns)
                    {
                        foreach (Selection_Adapter sA in sCKV.Value.selectionAdapters)
                        {
                            sA.enableDisable(value);
                        }
                    }
                }

                isEnableSAs = value;
            }

            get { return isEnableSAs; }
        }

        public void addTable(TableSpec tabSpec)
        {
            SelectionGroup currSelectionGroup;

            //if (tabSpec.SelectionGroupName != "")
            //{
            //    currSelectionGroup = selectionGroups[tabSpec.SelectionGroupName];
            //    currSelectionGroup.assignedTableSpecs.Add(tabSpec.TableName, tabSpec);
            //    //unAssignedTableSpecs.Add(tabSpec.TableName, tabSpec);
            //}
            //else
            //{
                tableSpecs.Add(tabSpec.TableName, tabSpec);
                //unAssignedTableSpecs.Add(tabSpec.TableName, tabSpec);
            //}
        }

        public TableSpec getTablSpec(string tabName)//,string selectionGroupName)
        {
            SelectionGroup currSelectionGroup;

            //if (selectionGroupName != "")
            //{
            //    currSelectionGroup = selectionGroups[selectionGroupName];
            //    if (currSelectionGroup.assignedTableSpecs.ContainsKey(tabName))
            //        return currSelectionGroup.assignedTableSpecs[tabName];
            //    else
            //        return null;
            //}
            //else
            //{
                if (tableSpecs.ContainsKey(tabName))
                    return tableSpecs[tabName];
                else
                    return null;
            //}
        }

        protected SelectionGroup getSelectionGroup(string sGroupName)
        {
            SelectionGroup currGroup;

            if (!selectionGroups.ContainsKey(sGroupName))
            {
                currGroup = new SelectionGroup();
                selectionGroups.Add(sGroupName, currGroup);
            }
            else
                currGroup = selectionGroups[sGroupName];

            return currGroup;
        }

        public void addSelectionColumn(string sGroupName, string columnName, string orderColumnName, SelectionColumnOperatorType sCOperator)
        {
            SelectionGroup currSG;
            SelectionColumn sCol;
            currSG = getSelectionGroup(sGroupName);///selectionGroups[sGroupName];

            sCol = new SelectionColumn();
            sCol.orderColumnName = orderColumnName;

            switch (sCOperator)
            {
                case SelectionColumnOperatorType.EQ:
                    sCol.usedOperatorStr = EQ_STR;
                    break;
                case SelectionColumnOperatorType.G:
                    sCol.usedOperatorStr = G_STR;
                    break;
                case SelectionColumnOperatorType.GEQ:
                    sCol.usedOperatorStr = GEQ_STR;
                    break;
                case SelectionColumnOperatorType.L:
                    sCol.usedOperatorStr = L_STR;
                    break;
                case SelectionColumnOperatorType.LEQ:
                    sCol.usedOperatorStr = LEQ_STR;
                    break;
            }

            currSG.selectionColumns.Add(columnName, sCol);

        }

        public void addSelectionAdapter(string sGroupName, string columnName, Selection_Adapter sA)
        {
            SelectionGroup currGroup;
            SelectionColumn currSC;
            currGroup = getSelectionGroup(sGroupName);

            sA.setFilterTableCollection(this);
            currSC = currGroup.selectionColumns[columnName];
            /*
            if (!currGroup.selectionAdapters.ContainsKey(columnName))
            {
                currSAs = new List<Selection_Adapter>();
                currGroup.selectionAdapters.Add(columnName, currSAs);
            }
            else
                currSAs = currGroup.selectionAdapters[columnName];
            currSAI = new SelectionAdapterInfo();
            currSAI.sA = sA;
            currSAI.orderColumn = orderColName;
            currSAs.Add(currSAI);
             */
            //currGroup.selectionAdapters.Add(columnName, sA);

            currSC.selectionAdapters.Add(sA);
        }

        public void addDataAdapter(string sGroupName, string tableName, Data_Adapter dA)
        {
            SelectionGroup currGroup;

            currGroup = getSelectionGroup(sGroupName);

            if (!currGroup.dataAdapters.ContainsKey(tableName))
            {
                currGroup.dataAdapters.Add(tableName, new List<Data_Adapter>());
            }
            dA.setFilterTableCollection(this);
            currGroup.dataAdapters[tableName].Add(dA);
        }

        public FilterTableCollection(DataBase db, bool iCCOS)
        {
            usedDb = db;

            tableSpecs = new Dictionary<string, TableSpec>();
            //dataAdapters = new Dictionary<string, List<Data_Adapter>>();
            //selectionAdapters = new Dictionary<string, Selection_Adapter>();
            selectionGroups = new Dictionary<string, SelectionGroup>();
            isChangeColumnEntriesOnSelection = iCCOS;
        }

       

        public DataTable getColumnEntryList(string sGroupName, string colName, string orderColName, bool isOnlyAvailable)
        {
            DataTable resultTab;
            SelectionGroup currSelectionGroup;
            string filterCondition;
            string sqlCmdStr;
            bool isFirstTab;
            //int currColNum;
            ColumnSpec currColSpec;
            sqlCmdStr = "";
            isFirstTab = true;
            currSelectionGroup=selectionGroups [sGroupName ];
            foreach (KeyValuePair<string, TableSpec> tSp in tableSpecs ) //tableSpecs)
            {
                if (tSp.Value.SelectionGroupName == sGroupName)
                {
                    currColSpec = tSp.Value.getColSpecByName(colName);
                    if (currColSpec != null && currColSpec.IsFilterEntryColumn)
                    {
                        if (!isFirstTab)
                            sqlCmdStr += SQLTable.SQL_UNION;
                        else
                            isFirstTab = false;
                        sqlCmdStr += SQLTable.SQL_SELECTDISTINCT + colName;
                        if (orderColName != "")
                            sqlCmdStr += "," + orderColName;
                        sqlCmdStr += SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(//sGroupName, 
                                                                                                        tSp.Key, false, true) + SQLTable.SQL_BRACKET_CLOSE + tSp.Key;//XBaseTableCmds[tSp.Key];
                        filterCondition = getFilterExpression(//sGroupName,
                                                                tSp.Key);//getFilterCondition(sGroupName, tSp.Key);
                        sqlCmdStr += SQLTable.SQL_WHERE + colName + " is not NULL";
                        if (isOnlyAvailable && filterCondition != "")
                            sqlCmdStr += SQLTable.SQL_AND + filterCondition;
                    }
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + "almostFinishedTab";
            if (orderColName != "")
                sqlCmdStr += SQLTable.SQL_ORDERBY + orderColName;
            else
                sqlCmdStr += SQLTable.SQL_ORDERBY + colName;

            resultTab = usedDb.getData(sqlCmdStr);
            return resultTab;
        }

        public DataTable getColumnEntries(string sGroupName, string colName, string orderColName, bool isOnlyAvailable)
        {
            DataTable resultTab;
            string filterCondition;
            string sqlCmdStr;
            bool isFirstTab;
            ColumnSpec currColSpec;

            sqlCmdStr = "";
            isFirstTab = true;
            foreach (KeyValuePair<string, TableSpec> tSp in tableSpecs)
            {
                currColSpec = tSp.Value.getColSpecByName(colName);
                if (currColSpec != null && currColSpec.IsFilterEntryColumn && currColSpec.IsEnabled)
                {
                    if (!isFirstTab)
                        sqlCmdStr += SQLTable.SQL_UNION;
                    else
                        isFirstTab = false;
                    sqlCmdStr += SQLTable.SQL_SELECTDISTINCT + colName;
                    if (orderColName != "")
                        sqlCmdStr += "," + orderColName;
                    sqlCmdStr += SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(//sGroupName, 
                                                                                                  tSp.Key, 
                                                                                                  false, 
                                                                                                  true) 
                                                                               + SQLTable.SQL_BRACKET_CLOSE 
                                                                               + tSp.Key;//XBaseTableCmds[tSp.Key];
                    filterCondition = getFilterCondition(sGroupName, tSp.Key);
                    sqlCmdStr += SQLTable.SQL_WHERE + colName + " is not NULL";
                    if (isOnlyAvailable && filterCondition != "")
                        sqlCmdStr += SQLTable.SQL_AND + filterCondition;
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + "almostFinishedTab";
            if (orderColName != "")
                sqlCmdStr += SQLTable.SQL_ORDERBY + orderColName;
            else
                sqlCmdStr += SQLTable.SQL_ORDERBY + colName;

            resultTab = usedDb.getData(sqlCmdStr);
            return resultTab;
        }

        protected string getColumnsEnum(List<string> columnNames)
        {
            int i, nCols;
            string enumStr = "";

            nCols = columnNames.Count - 1;
            i = 0;
            foreach (string colName in columnNames)
            {
                enumStr += colName;
                if (i < nCols)
                    enumStr += ",";
            }

            return enumStr;
        }

        //protected int getColumnNum(string columnName,string tableName)
        //{
        //    int i,tL;
        //    TableSpec currTabSpec;

        //    currTabSpec = tableSpecs[tableName];

        //    tL = currTabSpec .ColumnSpecs.Length;
        //    for (i = 0; i < tL; i++)
        //    {
        //        if (currTabSpec.ColumnSpecs[i].NewColumnName == columnName)
        //            return i;
        //    }
        //    return -1;
        //}

        public bool IsSortByColumns
        {
            get { return isSortByCols; }

            set { isSortByCols = value; }
        }

        public void clearSelection(string sGroupName)
        {
            SelectionGroup currSGroup;

            isBlockHandling = true;

            currSGroup = selectionGroups[sGroupName];

            foreach (KeyValuePair<string, SelectionColumn> sColKV in currSGroup.selectionColumns)
            {
                if (sColKV.Value.explicitlySelectedItems != null)
                {
                    sColKV.Value.explicitlySelectedItems.Clear();
                    foreach (Selection_Adapter sAToChange in sColKV.Value.selectionAdapters)
                    {
                        sAToChange.setSelectedEntries(sColKV.Value.explicitlySelectedItems, sColKV.Value.explicitlySelectedItems.Columns[0].ColumnName);
                    }
                }
            }

            //fList = new List<FilterEntries>();

            //setFilter(null, fList, sGroupName);
            setFilter(sGroupName);
            isBlockHandling = false;
        }

        public void partialReInitialize(string sGroupName, string colName)
        {
            Dictionary<string, DataTable> colEntries;
            DataTable currCOLEntries;
            SelectionGroup currSelectionGroup;
            SelectionColumn currSColumn;
            colEntries = new Dictionary<string, DataTable>();

            isBlockHandling = true;

            currSelectionGroup = selectionGroups[sGroupName];
            currSColumn = currSelectionGroup.selectionColumns[colName];

            //foreach (KeyValuePair<string, SelectionGroup> currGroupKV in selectionGroups)
            //{
            //  foreach (KeyValuePair<string, SelectionColumn> sC in currGroupKV.Value.selectionColumns)
            //{
            if (colEntries.ContainsKey(colName))
            {
                colEntries.Remove(colName);
            }

            currCOLEntries = getColumnEntries(sGroupName, colName, currSColumn.orderColumnName, false);
            colEntries.Add(colName, currCOLEntries);

            foreach (Selection_Adapter sA in currSColumn.selectionAdapters)
            {
                //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                //sA.setSelectedEntries(currCOLEntries);
                sA.setTotalColumnEntries(currCOLEntries, colName);
            }
            // }
            //}

            isBlockHandling = false;
        }
        protected bool isInitialized = false;
        public void initialize()
        {
            Dictionary<string, DataTable> colEntries;
            DataTable currCOLEntries;
            colEntries = new Dictionary<string, DataTable>();
            isBlockHandling = true;
            foreach (KeyValuePair<string, SelectionGroup> currGroupKV in selectionGroups)
            {
                foreach (KeyValuePair<string, SelectionColumn> sC in currGroupKV.Value.selectionColumns)
                {
                    if (!colEntries.ContainsKey(sC.Key))
                    {
                        currCOLEntries = getColumnEntries(currGroupKV.Key, sC.Key, sC.Value.orderColumnName, false);
                        colEntries.Add(sC.Key, currCOLEntries);
                    }
                    else
                        currCOLEntries = colEntries[sC.Key];

                    foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                    {
                        //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                        //sA.setSelectedEntries(currCOLEntries);
                        sA.setTotalColumnEntries(currCOLEntries, sC.Key);
                    }
                }
            }
            isInitialized = true;
            refreshAll();
            isBlockHandling = false;
        }

        public void selectionHandler(Object sender)
        {
            if (isBlockHandling)
                return;

            isBlockHandling = true;
            foreach (KeyValuePair<string, SelectionGroup> sGKV in selectionGroups)
            {
                foreach (KeyValuePair<string, SelectionColumn> sCKV in sGKV.Value.selectionColumns)
                {
                    foreach (Selection_Adapter sA in sCKV.Value.selectionAdapters)
                    {
                        if (sA.verifySender(sender))
                        {
                            sCKV.Value.explicitlySelectedItems = sA.getSelectedEntries();
                            setFilter(sGKV.Key);
                            foreach (Selection_Adapter sAToChange in sCKV.Value.selectionAdapters)
                            {
                                sAToChange.setSelectedEntries(sCKV.Value.explicitlySelectedItems, sCKV.Value.explicitlySelectedItems.Columns[0].ColumnName);
                            }

                            isBlockHandling = false;
                            return;
                        }
                    }
                }
            }

            isBlockHandling = false;
        }

        public void refreshAll()
        {
            if (isInitialized)
            {
                foreach (KeyValuePair<string, SelectionGroup> currSGKV in selectionGroups)
                {
                    setFilter(currSGKV.Key);
                }
            }
        }

        public void setFilter(string sGroupName)
        {
            int j;
            SelectionGroup currGroup;
            DataTable currAvailableCOLEntries;
            List<string> currSelection;
            List<Data_Adapter> currDAs;
            DataTable currDTab;
            DataTable currSTab;

            currGroup = selectionGroups[sGroupName];
            // isBlockHandling = true;
            //currGroup.filterList.Clear();
            //currGroup.filterList.AddRange(fList);

            if (isChangeColumnEntriesOnSelection)
            {
                foreach (KeyValuePair<string, SelectionColumn> sColKV in currGroup.selectionColumns)
                {
                    currSTab = new DataTable();
                    currSTab.Columns.Add();
                    currSelection = new List<string>();
                    //currSC = currGroup.selectionColumns[fE.colName];

                    if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                    {
                        currAvailableCOLEntries = getColumnEntryList(sGroupName, sColKV.Key, sColKV.Value.orderColumnName, true);//getColumnEntries(sGroupName, sColKV .Key, sColKV .Value .orderColumnName, true);

                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                        {
                            foreach (Selection_Adapter sA in sColKV.Value.selectionAdapters)
                            {
                                sA.setAvailableColumnEntries(currAvailableCOLEntries, sColKV.Key);
                            }
                        }
                        else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            foreach (Selection_Adapter sA in sColKV.Value.selectionAdapters)
                            {
                                sA.setTotalColumnEntries(currAvailableCOLEntries, sColKV.Key);
                            }
                        }
                    }
                    //currCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName , false);
                    //currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                    //foreach (string currFE in fE.filterEntries)
                    //{
                    //    currSRow = new string[1];
                    //    //for (j = 0; j < currCOLEntries.Rows.Count; j++)
                    //    //{
                    //    //    if (currCOLEntries.Rows[j].ItemArray[0].ToString() == currFE)
                    //    //        break;
                    //    //}

                    //    //if (j < currCOLEntries.Rows.Count)
                    //    //currSelection.Add(currFE);
                    //    currSRow[0] = currFE;
                    //    currSTab.Rows.Add(currSRow);
                    //}

                    ////currSAs = currGroup.selectionAdapters[fE.colName];

                    //foreach (Selection_Adapter sA in currSC.selectionAdapters)
                    //{
                    //    //sA.setColumnEntries (currCOLEntries ,currAvailableCOLEntries ,currSelection );
                    //    sA.setSelectedEntries(currSTab);
                    //}
                }

                //currSelection = new List<string>();
                //currSTab = new DataTable();
                //currSTab.Columns.Add();
                //foreach (KeyValuePair<string, SelectionColumn> sC in currGroup.selectionColumns)
                //{
                //    isRelevantColumn = false;
                //    foreach (FilterEntries fE in fList)
                //    {
                //        if (fE.colName == sC.Key)
                //        {
                //            isRelevantColumn = true;
                //            break;
                //        }
                //    }

                //    if (!isRelevantColumn)
                //    {
                //        //currCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, false);
                //        //currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);
                //        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                //        {
                //            currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);

                //            if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                //            {
                //                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //                {
                //                    sA.setAvailableColumnEntries(currAvailableCOLEntries);
                //                }
                //            }
                //            else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                //            {
                //                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //                {
                //                    sA.setTotalColumnEntries(currAvailableCOLEntries);
                //                }
                //            }
                //        }

                //        foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //        {
                //            //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                //            sA.setSelectedEntries(currSTab);
                //        }
                //    }
                //}
            }

            foreach (KeyValuePair<string, TableSpec> kTSp in tableSpecs)
            {
                //isRelevantTable=false;
                //foreach (FilterEntries fE in fList)
                //{
                //    if (getColumnNum (fE.colName,kTSp.Key)!=-1)
                //    {
                //        isRelevantTable =true;
                //        break;
                //    }
                //}
                //if (isRelevantTable )
                //{
                if (currGroup.dataAdapters.ContainsKey(kTSp.Key))
                {
                    currDTab = getFilteredData(sGroupName, kTSp.Key);
                    currDAs = currGroup.dataAdapters[kTSp.Key];

                    foreach (Data_Adapter currDA in currDAs)
                    {
                        currDA.setData(currDTab);
                    }
                }
                //}
            }

            //isBlockHandling = false;
        }


        public void setFilterA(Object sender, List<FilterEntries> fList, string sGroupName)
        {
            int j;
            SelectionGroup currGroup;
            DataTable currAvailableCOLEntries;
            List<string> currSelection;
            List<Data_Adapter> currDAs;
            SelectionColumn currSC;
            DataTable currDTab;
            DataTable currSTab;
            bool isRelevantColumn;
            string[] currSRow;

            if (isBlockHandling)
                return;

            currGroup = selectionGroups[sGroupName];
            isBlockHandling = true;
            currGroup.filterList.Clear();
            currGroup.filterList.AddRange(fList);

            if (isChangeColumnEntriesOnSelection)
            {
                foreach (FilterEntries fE in fList)
                {
                    currSTab = new DataTable();
                    currSTab.Columns.Add();
                    currSelection = new List<string>();
                    currSC = currGroup.selectionColumns[fE.colName];

                    if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                    {
                        currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                        {
                            foreach (Selection_Adapter sA in currSC.selectionAdapters)
                            {
                                sA.setAvailableColumnEntries(currAvailableCOLEntries, currSC.columnName);
                            }
                        }
                        else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            foreach (Selection_Adapter sA in currSC.selectionAdapters)
                            {
                                sA.setTotalColumnEntries(currAvailableCOLEntries, currSC.columnName);
                            }
                        }
                    }


                    //currCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName , false);
                    //currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                    foreach (string currFE in fE.filterEntries)
                    {
                        currSRow = new string[1];
                        //for (j = 0; j < currCOLEntries.Rows.Count; j++)
                        //{
                        //    if (currCOLEntries.Rows[j].ItemArray[0].ToString() == currFE)
                        //        break;
                        //}

                        //if (j < currCOLEntries.Rows.Count)
                        //currSelection.Add(currFE);
                        currSRow[0] = currFE;
                        currSTab.Rows.Add(currSRow);
                    }

                    //currSAs = currGroup.selectionAdapters[fE.colName];

                    foreach (Selection_Adapter sA in currSC.selectionAdapters)
                    {
                        //sA.setColumnEntries (currCOLEntries ,currAvailableCOLEntries ,currSelection );
                        sA.setSelectedEntries(currSTab, currSC.columnName);
                    }
                }

                //currSelection = new List<string>();
                currSTab = new DataTable();
                currSTab.Columns.Add();
                foreach (KeyValuePair<string, SelectionColumn> sC in currGroup.selectionColumns)
                {
                    isRelevantColumn = false;
                    foreach (FilterEntries fE in fList)
                    {
                        if (fE.colName == sC.Key)
                        {
                            isRelevantColumn = true;
                            break;
                        }
                    }
                    if (!isRelevantColumn)
                    {
                        //currCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, false);
                        //currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);
                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);

                            if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                            {
                                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                                {
                                    sA.setAvailableColumnEntries(currAvailableCOLEntries, sC.Key);
                                }
                            }
                            else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                            {
                                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                                {
                                    sA.setTotalColumnEntries(currAvailableCOLEntries, sC.Key);
                                }
                            }
                        }

                        foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                        {
                            //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                            sA.setSelectedEntries(currSTab, sC.Key);
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, TableSpec> kTSp in tableSpecs)
            {
                //isRelevantTable=false;
                //foreach (FilterEntries fE in fList)
                //{
                //    if (getColumnNum (fE.colName,kTSp.Key)!=-1)
                //    {
                //        isRelevantTable =true;
                //        break;
                //    }
                //}
                //if (isRelevantTable )
                //{
                if (currGroup.dataAdapters.ContainsKey(kTSp.Key))
                {
                    currDTab = getFilteredData(sGroupName, kTSp.Key);
                    currDAs = currGroup.dataAdapters[kTSp.Key];

                    foreach (Data_Adapter currDA in currDAs)
                    {
                        currDA.setData(currDTab);
                    }
                }
                //}
            }

            isBlockHandling = false;
        }

        public string getFilterExpression(//string sGroupName,
                                        string tableName)
        {
            int i, j;
            bool isStr, isFirst;
            //int colNum;
            ColumnSpec currColSpec;
            TableSpec currTabSpec;
            string sqlCmdStr;
            SelectionGroup currGroup;

            currTabSpec = tableSpecs[tableName];

            sqlCmdStr = "";

            if (currTabSpec.SelectionGroupName != "")
            {
                currGroup = selectionGroups[currTabSpec.SelectionGroupName];//[sGroupName];

                if (currGroup.filterList == null)
                    return sqlCmdStr;

                j = 0;
                isFirst = true;

                foreach (KeyValuePair<string, SelectionColumn> sCKV in currGroup.selectionColumns)
                {
                    if (sCKV.Value.explicitlySelectedItems != null &&
                        sCKV.Value.explicitlySelectedItems.Rows.Count > 0)//(fE.filterEntries.Count > 0)
                    {
                        //colNum = getColumnNum(sCKV.Key, tableName);
                        currColSpec = currTabSpec.getColSpecByName(sCKV.Key);
                        if (currColSpec != null && currColSpec.IsFilteredColumn && currColSpec.IsEnabled)//(colNum != -1 && currTabSpec.ColumnSpecs[colNum].isFilteredColumn)
                        {
                            if (!isFirst)
                            {
                                sqlCmdStr += SQLTable.SQL_AND;
                            }
                            if (isFirst)
                                isFirst = false;

                            sqlCmdStr += " (";
                            //if (currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATETIME ||
                            //    currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATE ||
                            //     currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_TEXT ||
                            //     currTabSpec.ColumnSpecs[colNum].SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                            if (currColSpec.SQL_Type == SQLTable.SQL_DATETIME ||
                                currColSpec.SQL_Type == SQLTable.SQL_DATE ||
                                 currColSpec.SQL_Type == SQLTable.SQL_TEXT ||
                                 currColSpec.SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                                )
                            {
                                isStr = true;
                            }
                            else
                                isStr = false;
                            i = 0;
                            foreach (DataRow currRow in sCKV.Value.explicitlySelectedItems.Rows)//(string entry in fE.filterEntries)
                            {
                                sqlCmdStr += currColSpec.NewColumnName + sCKV.Value.usedOperatorStr;//currTabSpec.ColumnSpecs[colNum].NewColumnName + sCKV.Value.usedOperatorStr; // "=";
                                if (isStr)
                                    sqlCmdStr += SQLTable.SQL_STR_MARKER;

                                sqlCmdStr += currRow[0].ToString();//entry;

                                if (isStr)
                                    sqlCmdStr += SQLTable.SQL_STR_MARKER;

                                if (i < sCKV.Value.explicitlySelectedItems.Rows.Count - 1)//fE.filterEntries.Count - 1)//if (i < fE.filterEntries.Count - 1)
                                    sqlCmdStr += SQLTable.SQL_OR;
                                i++;
                            }
                            sqlCmdStr += ")";
                        }
                    }
                    j++;
                }
            }
            return sqlCmdStr;
        }

        public string getFilterCondition(string sGroupName, string tableName)
        {
            int i, j;
            bool isStr, isFirst;
            //int colNum;
            ColumnSpec currColSpec;
            TableSpec currTabSpec;
            string sqlCmdStr;
            SelectionGroup currGroup;

            currGroup = selectionGroups[sGroupName];

            sqlCmdStr = "";
            if (currGroup.filterList == null)
                return sqlCmdStr;
            j = 0;
            isFirst = true;

            currTabSpec = tableSpecs[tableName];

            foreach (FilterEntries fE in currGroup.filterList)
            {
                if (fE.filterEntries.Count > 0)
                {
                    //colNum =getColumnNum (fE.colName ,tableName );
                    currColSpec = currTabSpec.getColSpecByName(fE.colName);
                    if (currColSpec != null && currColSpec.IsFilteredColumn) //if (colNum!=-1 && currTabSpec .ColumnSpecs [colNum].isFilteredColumn)
                    {
                        if (!isFirst)
                        {
                            sqlCmdStr += SQLTable.SQL_AND;
                        }
                        if (isFirst)
                            isFirst = false;

                        sqlCmdStr += " (";
                        //if (currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATETIME ||
                        //    currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATE ||
                        //     currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_TEXT ||
                        //     currTabSpec.ColumnSpecs[colNum].SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                        if (currColSpec.SQL_Type == SQLTable.SQL_DATETIME ||
                            currColSpec.SQL_Type == SQLTable.SQL_DATE ||
                            currColSpec.SQL_Type == SQLTable.SQL_TEXT ||
                            currColSpec.SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                            )
                        {
                            isStr = true;
                        }
                        else
                            isStr = false;
                        i = 0;
                        foreach (string entry in fE.filterEntries)
                        {
                            sqlCmdStr += currColSpec.ColumnName + "=";//currTabSpec.ColumnSpecs [colNum].ColumnName + "=";
                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;

                            sqlCmdStr += entry;

                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;
                            if (i < fE.filterEntries.Count - 1)
                                sqlCmdStr += SQLTable.SQL_OR;
                            i++;
                        }
                        sqlCmdStr += ")";
                    }
                }
                j++;
            }
            return sqlCmdStr;
        }

        protected string buildTableSQLCmd(//string sGroupName,
                                            string tableName, bool isFinalTable, bool isUnFiltered)
        {
            int i, j, k;
            string sqlCmdStr;
            string aggrSqlCmdStr;
            TableSpec currTabSpec, jTabSpec;
            string filterCondition;
            string[] tNs;
            string[] ccs;
            string baseTableName, aggrTableName, disaggrTableName, renTableName;
            string usedTableName;
            bool isFirstVCol, isFirstAFCol, isFirstECol, isFirstDCol;
            string vColEnum, oColEnum, vColEnumNoAlias;
            string gDEnum;
            string colStr;
            HashSet<string> nonUniqueColumns;
            //ColumnSpec currColSpec;

            nonUniqueColumns = new HashSet<string>();
            baseTableName = " base" + tableName;
            aggrTableName = " aggr" + tableName;
            renTableName = " rename" + tableName;
            disaggrTableName = " disaggr" + tableName;
            tNs = new string[2];
            
            currTabSpec = tableSpecs[tableName];

            if (currTabSpec.IsUseSQLCmd)
                sqlCmdStr = currTabSpec.BaseSQLCmd;
            else
                sqlCmdStr = buildTableSQLCmd(//sGroupName,
                                            currTabSpec.CalcTableName, false, isUnFiltered);

            sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + tableName;

            for (i = 0; i < currTabSpec.TablesToJoin.Length; i++)
            {
                tNs[0] = tableName;
                tNs[1] = currTabSpec.TablesToJoin[i].tableName;
                ccs = getCommonColunms(tNs);

                for (j = 0; j < ccs.Length; j++)
                {
                    if (!nonUniqueColumns.Contains(ccs[j]))
                        nonUniqueColumns.Add(ccs[j]);
                }
            }

            isFirstAFCol = true;
            vColEnum = "";
            if (currTabSpec.IsUseDimensions)
            {
                aggrSqlCmdStr = SQLTable.SQL_SELECT;
                isFirstVCol = true;
                isFirstDCol = true;

                gDEnum = "";
                foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in currTabSpec.ColumnSpecs)//for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
                {
                    if (currColSpecKV.Value.IsEnabled)
                    {
                        if (currColSpecKV.Value.IsDimensionColumn || currColSpecKV.Value.IsNonDimensionColumn)//(currTabSpec.ColumnSpecs[i].isDimensionColumn || currTabSpec.ColumnSpecs[i].isNonDimensionColumn)
                        {
                            if (isFirstVCol)
                            {
                                isFirstVCol = false;
                                isFirstAFCol = false;
                            }
                            else
                            {
                                aggrSqlCmdStr += ",";
                                vColEnum += ",";
                            }

                            //if (currColSpecKV.Value.isDimensionColumn)//(currTabSpec.ColumnSpecs[i].isDimensionColumn)
                            //{
                            //    if (isFirstDCol)
                            //        isFirstDCol = false;
                            //    else
                            //        gDEnum += ",";

                            //    if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                            //    {
                            //        aggrSqlCmdStr += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                            //        gDEnum += currTabSpec.ColumnSpecs[i].ColumnName;
                            //    }
                            //    else
                            //    {
                            //        aggrSqlCmdStr += currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                            //        gDEnum += currTabSpec.ColumnSpecs[i].ColumnName;
                            //    }

                            //}
                            //else if (currTabSpec.ColumnSpecs[i].isNonDimensionColumn)
                            //{

                            //    colStr = "";

                            //    switch (currTabSpec.ColumnSpecs[i].AggrType)
                            //    {
                            //        case  ColumnSpec.ColAggrType.AVG :
                            //            colStr = SQLTable.SQL_AVG;
                            //            break;
                            //        case ColumnSpec.ColAggrType.COUNT :
                            //            colStr = SQLTable.SQL_COUNT;
                            //            break;
                            //        case ColumnSpec.ColAggrType.MAX :
                            //            colStr = SQLTable.SQL_MAX;
                            //            break;
                            //        case ColumnSpec.ColAggrType.MIN :
                            //            colStr = SQLTable.SQL_MIN;
                            //            break;
                            //        case ColumnSpec.ColAggrType.SUM :
                            //            colStr = SQLTable.SQL_SUM;
                            //            break;
                            //        default:
                            //            break;

                            //    }
                            //    //aggrSqlCmdStr += SQLTable.SQL_SUM;
                            //    colStr += SQLTable.SQL_BRACKET_OPEN + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_BRACKET_CLOSE;

                            //    if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                            //    {
                            //        colStr = "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + colStr + ")";//'%Y-%m-%d',tTab.Date)
                            //    }
                            //    colStr+=SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                            //    aggrSqlCmdStr += colStr;

                            //}

                            //if (!nonUniqueColumns.Contains(currTabSpec.ColumnSpecs[i].NewColumnName))
                            //    vColEnum += currTabSpec.ColumnSpecs[i].NewColumnName;
                            //else
                            //    vColEnum += aggrTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;
                            if (currColSpecKV.Value.IsDimensionColumn)//(currTabSpec.ColumnSpecs[i].isDimensionColumn)
                            {
                                if (isFirstDCol)
                                    isFirstDCol = false;
                                else
                                    gDEnum += ",";

                                if (currColSpecKV.Value.SQL_Type == SQLTable.SQL_DATETIME)
                                {
                                    aggrSqlCmdStr += "strftime(" + "'" + currColSpecKV.Value.DateTimeFormat + "'" + "," + currColSpecKV.Value.ColumnName + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;
                                    gDEnum += currColSpecKV.Value.ColumnName;
                                }
                                else
                                {
                                    aggrSqlCmdStr += currColSpecKV.Value.ColumnName + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;
                                    gDEnum += currColSpecKV.Value.ColumnName;
                                }

                            }
                            else if (currColSpecKV.Value.IsNonDimensionColumn)
                            {

                                colStr = "";

                                switch (currColSpecKV.Value.AggrType)
                                {
                                    case ColumnSpec.ColAggrType.AVG:
                                        colStr = SQLTable.SQL_AVG;
                                        break;
                                    case ColumnSpec.ColAggrType.COUNT:
                                        colStr = SQLTable.SQL_COUNT;
                                        break;
                                    case ColumnSpec.ColAggrType.MAX:
                                        colStr = SQLTable.SQL_MAX;
                                        break;
                                    case ColumnSpec.ColAggrType.MIN:
                                        colStr = SQLTable.SQL_MIN;
                                        break;
                                    case ColumnSpec.ColAggrType.SUM:
                                        colStr = SQLTable.SQL_SUM;
                                        break;
                                    default:
                                        break;

                                }
                                //aggrSqlCmdStr += SQLTable.SQL_SUM;
                                colStr += SQLTable.SQL_BRACKET_OPEN + currColSpecKV.Value.ColumnName + SQLTable.SQL_BRACKET_CLOSE;

                                if (currColSpecKV.Value.SQL_Type == SQLTable.SQL_DATETIME)
                                {
                                    colStr = "strftime(" + "'" + currColSpecKV.Value.DateTimeFormat + "'" + "," + colStr + ")";//'%Y-%m-%d',tTab.Date)
                                }
                                colStr += SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;
                                aggrSqlCmdStr += colStr;

                            }

                            if (!nonUniqueColumns.Contains(currColSpecKV.Value.NewColumnName))
                                vColEnum += currColSpecKV.Value.NewColumnName;
                            else
                                vColEnum += aggrTableName + "." + currColSpecKV.Value.NewColumnName;


                            //vColEnum += aggrTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;//currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                        }
                    }
                }
                sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + aggrSqlCmdStr + SQLTable.SQL_FROM + sqlCmdStr;
                if (gDEnum != "")
                    sqlCmdStr += SQLTable.SQL_GROUPBY + gDEnum;
                sqlCmdStr += SQLTable.SQL_BRACKET_CLOSE + aggrTableName;
                usedTableName = aggrTableName;
            }
            else
            {
                // usedTableName = tableName;
                string colRenameSQLCmd;
                usedTableName = renTableName;
                colRenameSQLCmd = SQLTable.SQL_SELECT;
                //for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
                //{
                //    if (isFirstAFCol)
                //        isFirstAFCol = false;
                //    else
                //    {
                //        vColEnum += ",";
                //        colRenameSQLCmd += ",";
                //    }
                //    if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                //        colRenameSQLCmd += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + currTabSpec.ColumnSpecs[i].ColumnName + ")" + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                //    else
                //        colRenameSQLCmd += currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;

                //    if (!nonUniqueColumns.Contains(currTabSpec.ColumnSpecs[i].NewColumnName))
                //    {
                //        vColEnum += currTabSpec.ColumnSpecs[i].NewColumnName;
                //    }
                //    else
                //    {
                //        vColEnum += usedTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;

                //        //if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                //        //    colRenameSQLCmd += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + ")" + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                //        //else
                //        //    colRenameSQLCmd += usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                //    }
                //}
                foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in currTabSpec.ColumnSpecs)
                {
                    if (currColSpecKV.Value.IsEnabled)
                    {
                        if (isFirstAFCol)
                            isFirstAFCol = false;
                        else
                        {
                            vColEnum += ",";
                            colRenameSQLCmd += ",";
                        }
                        if (currColSpecKV.Value.SQL_Type == SQLTable.SQL_DATETIME)
                            colRenameSQLCmd += "strftime(" + "'" + currColSpecKV.Value.DateTimeFormat + "'" + "," + currColSpecKV.Value.ColumnName + ")" + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;
                        else
                            colRenameSQLCmd += currColSpecKV.Value.ColumnName + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;

                        if (!nonUniqueColumns.Contains(currColSpecKV.Value.NewColumnName))
                        {
                            vColEnum += currColSpecKV.Value.NewColumnName;
                        }
                        else
                        {
                            vColEnum += usedTableName + "." + currColSpecKV.Value.NewColumnName;

                            //if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                            //    colRenameSQLCmd += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + ")" + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                            //else
                            //    colRenameSQLCmd += usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                        }
                    }
                }

                colRenameSQLCmd += SQLTable.SQL_FROM + sqlCmdStr;
                sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + colRenameSQLCmd + SQLTable.SQL_BRACKET_CLOSE + renTableName;

                // sqlCmdStr = SQLTable.SQL_SELECT + vColEnum + SQLTable.SQL_FROM + sqlCmdStr;
            }
            vColEnumNoAlias = oColEnum = "";
            isFirstVCol = true;
            isFirstECol = true;
            //for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
            //{  
            //    if (currTabSpec.ColumnSpecs[i].isVisibleColumn)
            //    {
            //        if (isFirstVCol)
            //            isFirstVCol = false;
            //        else
            //        {
            //            vColEnumNoAlias += ",";
            //        }

            //        vColEnumNoAlias += currTabSpec.ColumnSpecs[i].NewColumnName;
            //    }

            //    if (currTabSpec.ColumnSpecs[i].isSortColumn )
            //    {
            //        if (isFirstECol)
            //            isFirstECol = false;
            //        else
            //            oColEnum += ",";
            //        oColEnum +=currTabSpec.ColumnSpecs[i].NewColumnName;
            //    }
            //}
            foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in currTabSpec.ColumnSpecs)
            {
                if (currColSpecKV.Value.IsEnabled)
                {
                    if (currColSpecKV.Value.IsVisibleColumn)
                    {
                        if (isFirstVCol)
                            isFirstVCol = false;
                        else
                        {
                            vColEnumNoAlias += ",";
                        }

                        vColEnumNoAlias += currColSpecKV.Value.NewColumnName;
                    }

                    if (currColSpecKV.Value.IsSortColumn)
                    {
                        if (isFirstECol)
                            isFirstECol = false;
                        else
                            oColEnum += ",";
                        oColEnum += currColSpecKV.Value.NewColumnName;
                    }
                }
            }
            for (i = 0; i < currTabSpec.TablesToJoin.Length; i++)
            {
                tNs[0] = tableName;
                tNs[1] = currTabSpec.TablesToJoin[i].tableName;
                ccs = getCommonColunms(tNs);

                jTabSpec = tableSpecs[currTabSpec.TablesToJoin[i].tableName];

                switch (currTabSpec.TablesToJoin[i].jType)
                {
                    case TableJoin.JoinType.JOIN:
                        sqlCmdStr += SQLTable.SQL_JOIN;
                        break;
                    case TableJoin.JoinType.LEFT_JOIN:
                        sqlCmdStr += SQLTable.SQL_LEFTJOIN;
                        break;
                    case TableJoin.JoinType.INNER_JOIN:
                        sqlCmdStr += SQLTable.SQL_INNERJOIN;
                        break;
                    case TableJoin.JoinType.OUTER_JOIN:
                        sqlCmdStr += SQLTable.SQL_OUTTERJOIN;
                        break;
                    case TableJoin.JoinType.CROSS_JOIN:
                        sqlCmdStr += SQLTable.SQL_CROSS_JOIN;
                        break;
                    default:
                        sqlCmdStr += SQLTable.SQL_JOIN;
                        break;
                }

                sqlCmdStr += SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(//sGroupName, 
                                                                          currTabSpec.TablesToJoin[i].tableName, false, isUnFiltered) + SQLTable.SQL_BRACKET_CLOSE + currTabSpec.TablesToJoin[i].tableName;

                if (currTabSpec.TablesToJoin[i].jType != TableJoin.JoinType.CROSS_JOIN)
                {
                    sqlCmdStr += SQLTable.SQL_ON;

                    for (j = 0; j < ccs.Length; j++)
                    {
                        if (j > 0)
                            sqlCmdStr += SQLTable.SQL_AND;
                        sqlCmdStr += usedTableName + "." + ccs[j] + "=" + currTabSpec.TablesToJoin[i].tableName + "." + ccs[j];
                    }
                }
                //for (j = 0; j < jTabSpec.ColumnSpecs.Length; j++)
                //{
                //    for (k = 0; k < ccs.Length; k++)
                //    {
                //        if (jTabSpec.ColumnSpecs[j].NewColumnName  == ccs[k])
                //            break;
                //    }

                //    if (k == ccs.Length)
                //    {
                //        if (!isFirstAFCol)
                //        {
                //            vColEnum += ",";
                //        }
                //        else
                //            isFirstAFCol = false;

                //        if ((jTabSpec.ColumnSpecs[j].SQL_Type == "double" || jTabSpec.ColumnSpecs[j].SQL_Type == "int") && jTabSpec.ColumnSpecs[j].isZeroForNull)
                //        {
                //            vColEnum += SQLTable.SQL_ISNULL + SQLTable.SQL_BRACKET_OPEN + currTabSpec.TablesToJoin[i].tableName + "." + jTabSpec.ColumnSpecs[j].NewColumnName + ",0" + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + jTabSpec.ColumnSpecs[j].NewColumnName;
                //        }
                //        else
                //            vColEnum += currTabSpec.TablesToJoin[i].tableName + "." + jTabSpec.ColumnSpecs[j].NewColumnName + SQLTable.SQL_AS + jTabSpec.ColumnSpecs[j].NewColumnName;

                //        if (jTabSpec.ColumnSpecs[j].isVisibleColumn)
                //        {
                //            if (!isFirstVCol)
                //            {
                //                vColEnumNoAlias += ",";
                //            }
                //            else
                //                isFirstVCol = false;

                //            vColEnumNoAlias += jTabSpec.ColumnSpecs[j].NewColumnName;
                //        }
                //    }
                //    if (jTabSpec.ColumnSpecs[i].isSortColumn)
                //    {
                //        if (!isFirstECol)
                //            oColEnum += ",";
                //        else
                //            isFirstECol = false;

                //        oColEnum += jTabSpec.ColumnSpecs[i].NewColumnName;
                //    }
                //}
                foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in jTabSpec.ColumnSpecs) //(j = 0; j < jTabSpec.ColumnSpecs.Length; j++)
                {
                    if (currColSpecKV.Value.IsEnabled)
                    {
                        for (k = 0; k < ccs.Length; k++)
                        {
                            if (currColSpecKV.Value.NewColumnName == ccs[k])
                                break;
                        }

                        if (k == ccs.Length)
                        {
                            if (!isFirstAFCol)
                            {
                                vColEnum += ",";
                            }
                            else
                                isFirstAFCol = false;

                            if ((currColSpecKV.Value.SQL_Type == "double" || currColSpecKV.Value.SQL_Type == "int") && currColSpecKV.Value.IsZeroForNull)
                            {
                                vColEnum += SQLTable.SQL_ISNULL + SQLTable.SQL_BRACKET_OPEN + currTabSpec.TablesToJoin[i].tableName + "." + currColSpecKV.Value.NewColumnName + ",0" + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;
                            }
                            else
                                vColEnum += currTabSpec.TablesToJoin[i].tableName + "." + currColSpecKV.Value.NewColumnName + SQLTable.SQL_AS + currColSpecKV.Value.NewColumnName;

                            if (currColSpecKV.Value.IsVisibleColumn)
                            {
                                if (!isFirstVCol)
                                {
                                    vColEnumNoAlias += ",";
                                }
                                else
                                    isFirstVCol = false;

                                vColEnumNoAlias += currColSpecKV.Value.NewColumnName;
                            }
                        }
                        if (currColSpecKV.Value.IsSortColumn)
                        {
                            if (!isFirstECol)
                                oColEnum += ",";
                            else
                                isFirstECol = false;

                            oColEnum += currColSpecKV.Value.NewColumnName;
                        }
                    }
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECT + vColEnum + SQLTable.SQL_FROM + sqlCmdStr;

            if (currTabSpec.IsDistinctRows)
                sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE;
            else
                sqlCmdStr = SQLTable.SQL_SELECT + vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE;

            // sqlCmdStr+=vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + " almostFinalTab ";//finalTableSQLCmdStr;
            sqlCmdStr += " almostFinalTab ";
            if (!isUnFiltered)
            {
                filterCondition = getFilterExpression(//sGroupName,
                                                        tableName);//getFilterCondition(sGroupName, tableName);
                if (filterCondition != "")
                    sqlCmdStr += SQLTable.SQL_WHERE + SQLTable.SQL_BRACKET_OPEN + filterCondition + SQLTable.SQL_BRACKET_CLOSE;
                if (currTabSpec.addFilterExpression != "")
                {
                    if (filterCondition != "")
                        sqlCmdStr += SQLTable.SQL_AND;
                    else
                        sqlCmdStr += SQLTable.SQL_WHERE;

                    sqlCmdStr += SQLTable.SQL_BRACKET_OPEN + currTabSpec.addFilterExpression + SQLTable.SQL_BRACKET_CLOSE;
                }
            }
            if (isFinalTable && oColEnum != "")
            {
                sqlCmdStr += SQLTable.SQL_ORDERBY + oColEnum;
            }

            return sqlCmdStr;
        }


        public string[] getCommonColunms(string[] tabNames)
        {
            int i, j, nCC;
            string[] cc;
            Dictionary<string, int> hs;
            TableSpec currTabSpec;

            hs = new Dictionary<string, int>();

            //for (i=0;i<tabNames .Length;i++)
            //{
            //    currTabSpec =tableSpecs [tabNames [i]];

            //    if (currTabSpec.IsUseDimensions)
            //    {
            //        for (j = 0; j < currTabSpec.ColumnSpecs.Length; j++)
            //        {
            //            if (currTabSpec.ColumnSpecs[j].isDimensionColumn || currTabSpec.ColumnSpecs[j].isNonDimensionColumn)
            //            {
            //                if (hs.ContainsKey(currTabSpec.ColumnSpecs[j].NewColumnName))
            //                {
            //                    hs[currTabSpec.ColumnSpecs[j].NewColumnName]++;
            //                }
            //                else
            //                    hs.Add(currTabSpec.ColumnSpecs[j].NewColumnName, 1);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        for (j = 0; j < currTabSpec.ColumnSpecs.Length; j++)
            //        {
            //            if (hs.ContainsKey(currTabSpec.ColumnSpecs[j].NewColumnName))
            //            {
            //                hs[currTabSpec.ColumnSpecs[j].NewColumnName]++;
            //            }
            //            else
            //                hs.Add(currTabSpec.ColumnSpecs[j].NewColumnName, 1);
            //        }
            //    }
            //}
            for (i = 0; i < tabNames.Length; i++)
            {
                currTabSpec = tableSpecs[tabNames[i]];

                if (currTabSpec.IsUseDimensions)
                {
                    foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in currTabSpec.ColumnSpecs)
                    {
                        if (currColSpecKV.Value.IsEnabled)
                        {
                            if (currColSpecKV.Value.IsDimensionColumn || currColSpecKV.Value.IsNonDimensionColumn)
                            {
                                if (hs.ContainsKey(currColSpecKV.Value.NewColumnName))
                                {
                                    hs[currColSpecKV.Value.NewColumnName]++;
                                }
                                else
                                    hs.Add(currColSpecKV.Value.NewColumnName, 1);
                            }
                        }
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, ColumnSpec> currColSpecKV in currTabSpec.ColumnSpecs)
                    {
                        if (currColSpecKV.Value.IsEnabled)
                        {
                            if (hs.ContainsKey(currColSpecKV.Value.NewColumnName))
                            {
                                hs[currColSpecKV.Value.NewColumnName]++;
                            }
                            else
                                hs.Add(currColSpecKV.Value.NewColumnName, 1);
                        }
                    }
                }
            }

            nCC = 0;

            foreach (KeyValuePair<string, int> kV in hs)
            {
                if (kV.Value == tabNames.Length)
                    nCC++;
            }

            cc = new string[nCC];
            i = 0;
            foreach (KeyValuePair<string, int> kV in hs)
            {
                if (kV.Value == tabNames.Length)
                {
                    cc[i] = kV.Key;
                    i++;
                }
            }

            return cc;
        }

        public DataTable getFilteredData(string sGroupName, string tableName)
        {
            DataTable resultTab;
            string baseSQLCmd;

            baseSQLCmd = buildTableSQLCmd(//sGroupName, 
                                            tableName, true, false);

            resultTab = usedDb.getData(baseSQLCmd);

            return resultTab;
        }
    }
    /*
    public class FilterTableCollection
    {
        public enum SelectionColumnOperatorType
        {
            LEQ,
            EQ,
            GEQ,
            L,
            G
        }
        public delegate void CallReturnVoidParamDataTableString(DataTable entries, string colName);
        public delegate void CallReturnVoidParamDataTable(DataTable dt);
        public delegate DataTable CallReturnDataTableVoidParams();
        public delegate void CallReturnVoidParamDataTableDataTableListOfString(DataTable entries, DataTable availableEntries, List<string> selection);
        public delegate void CallReturnVoidParamBool(bool isEnable);
        protected const string LEQ_STR = "<=";
        protected const string GEQ_STR = ">=";
        protected const string EQ_STR = "=";
        protected const string L_STR = "<";
        protected const string G_STR = ">";

        //public interface OutputAdapter
        //{
        //    void setFilterTableCollection(FilterTableCollection uFTC);
        //    void update();
        //    void initialize();
        //}

        public enum Behavior
        {
            AVAILABLE_BY_COLOR,
            SHOW_ONLY_AVAILABLE,
            IGNORE_AVAILABILITY
        }

        public class ColumnSpec
        {
            public enum ColAggrType
            {
                SUM,
                AVG,
                COUNT,
                MIN,
                MAX,
                NONE
            }
            public string ColumnName;
            public string NewColumnName;
            public string SQL_Type;
            public bool isDimensionColumn;
            public bool isNonDimensionColumn;
            public bool isVisibleColumn;
            public bool isSortColumn;
            public bool isFilteredColumn;
            public bool isFilterEntryColumn;
            public bool isZeroForNull = false;
            public string DateTimeFormat = "%Y-%m-%dT%H:%M:%S";
            public ColAggrType AggrType;

        }
        protected class SelectionColumn
        {
            public string columnName;
            public string orderColumnName;
            public string usedOperatorStr;
            public DataTable explicitlySelectedItems;
            public List<Selection_Adapter> selectionAdapters;
            public SelectionColumn()
            {
                selectionAdapters = new List<Selection_Adapter>();
                explicitlySelectedItems = null;
            }
        }
        protected class SelectionGroup
        {
            public Dictionary<string, List<Data_Adapter>> dataAdapters;
            public Dictionary<string, SelectionColumn> selectionColumns;
            public List<FilterEntries> filterList;

            public SelectionGroup()
            {
                dataAdapters = new Dictionary<string, List<Data_Adapter>>();
                selectionColumns = new Dictionary<string, SelectionColumn>();
                filterList = new List<FilterEntries>();
            }
        }

        public class TableJoin
        {
            public enum JoinType
            {
                JOIN,
                LEFT_JOIN,
                INNER_JOIN,
                OUTER_JOIN,
                CROSS_JOIN
            };
            public string tableName;
            public JoinType jType;
        }

        public class TableSpec
        {
            public static string AGGRTYPE_SUM_STR = "SUM";
            public static string AGGRTYPE_AVG_STR = "AVG";
            public static string AGGRTYPE_COUNT_STR = "COUNT";
            public static string AGGRTYPE_MIN_STR = "MIN";
            public static string AGGRTYPE_MAX_STR = "MAX";

            public static string JOINTYPE_JOIN_STR = "JOIN";
            public static string JOINTYPE_LEFTJOIN_STR = "LEFT_JOIN";
            public static string JOINTYPE_INNERJOIN_STR = "INNER_JOIN";
            public static string JOINTYPE_OUTERJOIN_STR = "OUTER_JOIN";
            public static string JOINTYPE_CROSSJOIN_STR = "CROSS_JOIN";

            public string TableName;

            public bool IsUseSQLCmd;
            public string BaseSQLCmd;

            public bool IsUseCalculatedTable;
            public string CalcTableName;

            public bool IsUseDimensions;

            public bool IsDistinctRows;
            public string addFilterExpression = "";

            public ColumnSpec[] ColumnSpecs;

            public TableJoin[] TablesToJoin;

            public static string SQL_DATETIME = "datetime";
            public static string SQL_DATE = "date";
            public static string SQL_VARCHAR = "varchar";
            public static string SQL_TEXT = "text";
            public static string SQL_INTEGER = "integer";
            public static string SQL_DOUBLE = "double";
            public int getColumnNum(string columnName)
            {
                int i, tL;

                tL = ColumnSpecs.Length;
                for (i = 0; i < tL; i++)
                {
                    if (ColumnSpecs[i].NewColumnName == columnName)
                        return i;
                }
                return -1;
            }

            public string getDimensionColumnsEnum()
            {
                int i, tL;
                bool isFirst;
                string enumStr = "";
                isFirst = true;
                tL = ColumnSpecs.Length;
                for (i = 0; i < tL; i++)
                {
                    if (ColumnSpecs[i].isDimensionColumn)
                    {
                        if (!isFirst)
                            enumStr += ",";
                        else
                            isFirst = false;

                        enumStr += ColumnSpecs[i].ColumnName;
                    }
                }

                return enumStr;
            }
            //protected string[] globalRawTabSQLSpec = { "rawData",//tableIdentifier
            //                                         "true",//IsUseSQLCmd
            //                                       "Select Product"
            //                                            + ",WGruppe"
            //                                            + ",Date"
            //                                            + ",Quantity"
            //                                            + ",Covariate"
            //                                            + ",mdTab.Calendarweek as CW"
            //                                            + ",Cast(mdTab.Calendarweek/100 as text)||'-'||Cast(mdTab.Calendarweek%100 as text) as Calendarweek"
            //                                            + " from IN_MASTERDATA mdTab"
            //                                            + " join IN_MONTHS mTab on mTab.MonthNumber=strftime('%m',Date)",//BaseSQLCmd
            //                                           "false",//isUseCalculatedTable
            //                                           "",//TableName
            //                                           "true",//IsUseDimensions
            //                                           "false" //isDistinctRows
            //                                       };
            public void fillTableSpecFromString(string[] globalSpecs, string[,] colSpecs, string[,] tabJoinSpecs)
            {
                int i;
                TableName = globalSpecs[0];
                IsUseSQLCmd = Convert.ToBoolean(globalSpecs[1]);
                BaseSQLCmd = globalSpecs[2];
                IsUseCalculatedTable = Convert.ToBoolean(globalSpecs[3]);
                CalcTableName = globalSpecs[4];
                IsUseDimensions = Convert.ToBoolean(globalSpecs[5]);
                IsDistinctRows = Convert.ToBoolean(globalSpecs[6]);
                addFilterExpression = globalSpecs[7];

                ColumnSpecs = new ColumnSpec[colSpecs.GetLength(0)];

                for (i = 0; i < colSpecs.GetLength(0); i++)
                {
                    ColumnSpecs[i] = new ColumnSpec();
                    ColumnSpecs[i] = new FilterTableCollection.ColumnSpec();
                    ColumnSpecs[i].ColumnName = colSpecs[i, 0];
                    ColumnSpecs[i].SQL_Type = colSpecs[i, 1];

                    ColumnSpecs[i].isVisibleColumn = Convert.ToBoolean(colSpecs[i, 2]);

                    ColumnSpecs[i].isDimensionColumn = Convert.ToBoolean(colSpecs[i, 3]);

                    ColumnSpecs[i].isNonDimensionColumn = Convert.ToBoolean(colSpecs[i, 4]);

                    ColumnSpecs[i].isSortColumn = Convert.ToBoolean(colSpecs[i, 5]);

                    ColumnSpecs[i].isFilteredColumn = Convert.ToBoolean(colSpecs[i, 6]);

                    ColumnSpecs[i].isFilterEntryColumn = Convert.ToBoolean(colSpecs[i, 7]);

                    ColumnSpecs[i].NewColumnName = colSpecs[i, 8];

                    if (colSpecs[i, 9] == AGGRTYPE_AVG_STR)
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.AVG;
                    else if (colSpecs[i, 9] == AGGRTYPE_COUNT_STR)
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.COUNT;
                    else if (colSpecs[i, 9] == AGGRTYPE_SUM_STR)
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.SUM;
                    else if (colSpecs[i, 9] == AGGRTYPE_MIN_STR)
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.MIN;
                    else if (colSpecs[i, 9] == AGGRTYPE_MAX_STR)
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.MAX;
                    else
                        ColumnSpecs[i].AggrType = ColumnSpec.ColAggrType.NONE;

                    if (colSpecs[i, 10] != "")
                        ColumnSpecs[i].DateTimeFormat = colSpecs[i, 10];
                }

                TablesToJoin = new TableJoin[tabJoinSpecs.GetLength(0)];

                for (i = 0; i < tabJoinSpecs.GetLength(0); i++)
                {
                    TablesToJoin[i] = new TableJoin();
                    TablesToJoin[i].tableName = tabJoinSpecs[i, 0];
                    if (tabJoinSpecs[i, 1] == JOINTYPE_JOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_LEFTJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.LEFT_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_INNERJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.INNER_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_OUTERJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.OUTER_JOIN;
                    else if (tabJoinSpecs[i, 1] == JOINTYPE_CROSSJOIN_STR)
                        TablesToJoin[i].jType = TableJoin.JoinType.CROSS_JOIN;
                    else
                        TablesToJoin[i].jType = TableJoin.JoinType.JOIN;
                }
            }
        }

        public interface Selection_Adapter
        {
            void setFilterTableCollection(FilterTableCollection uFTC);
            //void fillColumnEntries(DataTable entries,DataTable availableEntries,List<string> selection);
            void setTotalColumnEntries(DataTable entries, string colName);
            void setAvailableColumnEntries(DataTable entries, string colName);
            void setSelectedEntries(DataTable selectedEntries, string colName);
            DataTable getSelectedEntries();
            bool verifySender(Object sender);
            void enableDisable(bool isEnabled);
        };

        //protected class SelectionAdapterInfo
        //{
        //    public Selection_Adapter sA;
        //    public string orderColumn;
        //}

        public interface Data_Adapter
        {
            void setData(DataTable data);
            void setFilterTableCollection(FilterTableCollection uFTC);
        }

        public class GridViewAdapter : Data_Adapter
        {
            DataGridView usedDGV;
            //FilterTableCollection usedFilterTableCollection;

            protected string usedSelectionGroup;
            protected string usedTableName;
            protected bool isAutoAdjustColumns = true;

            public delegate void CallVoidReturnParamsObject(Object sender);
            public bool IsAutoAdjustColumns
            {
                get { return isAutoAdjustColumns; }
                set
                {
                    isAutoAdjustColumns = value;
                    if (isAutoAdjustColumns)
                        usedDGV.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(checkColumnWidth);
                    else
                        usedDGV.DataBindingComplete += null;
                }
            }



            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String TableName
            {
                get { return usedTableName; }
                set { usedTableName = value; }
            }

            public GridViewAdapter(DataGridView uDGV)
            {
                usedDGV = uDGV;

                usedDGV.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(checkColumnWidth);
            }
            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                //usedFilterTableCollection = uFTC;
            }

            public void checkColumnWidth(Object sender, EventArgs args)
            {
                int currentColsWidth;
                // if (isAutoAdjustColumns)
                //{
                currentColsWidth = 0;
                foreach (DataGridViewColumn dGVC in usedDGV.Columns)
                {
                    currentColsWidth += dGVC.Width;
                }

                if (currentColsWidth < usedDGV.Width)
                    usedDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
                else
                    usedDGV.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                //   }

            }
            public void setData(DataTable data)
            {
                int i;

                if (usedDGV.InvokeRequired)
                {
                    usedDGV.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    usedDGV.DataSource = data;
                }
            }


            //public void update()
            //{
            //    usedDGV.DataSource = usedFilterTableCollection.getFilteredData(usedSelectionGroup, usedTableName);
            //}
            //public void initialize()
            //{
            //    update();
            //}
        }
        //public class ListBoxOutputAdapter : Data_Adapter
        //{
        //    ListBox usedLB;
        //    Dictionary<string, int> elementIndices;
        //    public ListBoxOutputAdapter(ListBox lB)
        //    {
        //        usedLB = lB;
        //    }

        //    public void setFilterTableCollection(FilterTableCollection uFTC)
        //    {
        //        // usedFilterTableCollection = uFTC;
        //    }
        //    public void setData(DataTable data)
        //    {
        //        if (usedLB.InvokeRequired)
        //        {
        //            usedLB.Invoke(new CallReturnVoidParamDataTable(setData), data);
        //        }
        //        else
        //        {
        //            usedLB.BindingContext = new BindingContext();
        //            usedLB.DataSource = data;
        //            usedLB.DisplayMember = data.Columns[0].ColumnName;
        //            usedLB.ValueMember = data.Columns[0].ColumnName;
        //        }
        //    }
        //}
        public class ChartAdapter : Data_Adapter
        {
            System.Windows.Forms.DataVisualization.Charting.Chart usedChart;
            //FilterTableCollection.TableSpec usedTabSpec;
            FilterTableCollection usedFilterTableCollection;
            protected string ISO_8601_FORMAT_STRING = "yyyy-MM-ddTHH:mm:ss";
            protected string usedSelectionGroup;
            protected string usedTableName;
            protected CultureInfo usedC;
            Dictionary<string, SeriesInfo> usedSeries;

            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String TableName
            {
                get { return usedTableName; }
                set { usedTableName = value; }
            }

            public CultureInfo Culture
            {
                get { return usedC; }
                set { usedC = value; }

            }

            public ChartAdapter(System.Windows.Forms.DataVisualization.Charting.Chart uC)
            {
                usedC = CultureInfo.InvariantCulture;
                usedChart = uC;
                usedSeries = new Dictionary<string, SeriesInfo>();//List<SeriesInfo>();//new List<System.Windows.Forms.DataVisualization.Charting.Series>();
            }

            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }

            public void clearSeries()
            {
                usedSeries.Clear();
            }

            public void setTabSpec(TableSpec tabSpec)
            {
                //usedTabSpec = tabSpec;

            }
            public System.Windows.Forms.DataVisualization.Charting.ChartValueType transLateSQLTypeToChartType(string sqlType)
            {
                if (sqlType == TableSpec.SQL_DATE || sqlType == TableSpec.SQL_DATETIME)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
                else if (sqlType == TableSpec.SQL_TEXT || sqlType == TableSpec.SQL_VARCHAR)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
                else if (sqlType == TableSpec.SQL_INTEGER)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
                else if (sqlType == TableSpec.SQL_DOUBLE)
                    return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
            }


            public Type transLateSQLTypeToDataType(string sqlType)
            {
                if (sqlType == TableSpec.SQL_DATE || sqlType == TableSpec.SQL_DATETIME)
                    return typeof(DateTime);
                else if (sqlType == TableSpec.SQL_TEXT || sqlType == TableSpec.SQL_VARCHAR)
                    return typeof(String);
                else if (sqlType == TableSpec.SQL_INTEGER)
                    return typeof(Int32);
                else if (sqlType == TableSpec.SQL_DOUBLE)
                    return typeof(Double);

                return typeof(String);
            }

            //List<System.Windows.Forms.DataVisualization.Charting.Series> usedSeries;


            protected class SeriesInfo
            {
                public System.Windows.Forms.DataVisualization.Charting.Series s;
                public string yColName;
                public string rangeColName;
                public object rangeMin;
                public object rangeMax;
                public string dateTimeFormat;
            }

            public void setRangeData(string yColName, string rangeColName, object rangeMin, object rangeMax, string dateTimeFormat)
            {
                SeriesInfo currSInfo;

                currSInfo = usedSeries[yColName];
                currSInfo.rangeColName = rangeColName;
                currSInfo.rangeMin = rangeMin;
                currSInfo.rangeMax = rangeMax;
                currSInfo.dateTimeFormat = dateTimeFormat;
            }
            public void addSeries(string xColName, bool isSecondaryX, string yColName, bool isSecondaryY, Color usedColor, string rangeColName, object rangeMin, object rangeMax, string dateTimeFormat)
            {
                System.Windows.Forms.DataVisualization.Charting.Series currSeries;
                int currColNum;
                TableSpec usedTabSpec;
                SeriesInfo currSInfo;
                usedTabSpec = usedFilterTableCollection.getTablSpec(usedTableName);
                currSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
                currColNum = usedTabSpec.getColumnNum(xColName);
                currSeries.Name = yColName;
                currSeries.XValueMember = xColName;

                currSeries.XValueType = transLateSQLTypeToChartType(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

                if (isSecondaryX)
                    currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
                else
                    currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

                currColNum = usedTabSpec.getColumnNum(yColName);
                currSeries.YValueMembers = yColName;
                currSeries.YValueType = transLateSQLTypeToChartType(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

                if (isSecondaryY)
                    currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
                else
                    currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
                currSeries.ChartArea = "Default";

                currSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                currSeries.Color = usedColor;

                currSInfo = new SeriesInfo();
                currSInfo.s = currSeries;
                currSInfo.rangeColName = rangeColName;
                currSInfo.rangeMin = rangeMin;
                currSInfo.rangeMax = rangeMax;
                currSInfo.yColName = yColName;
                currSInfo.dateTimeFormat = dateTimeFormat;
                usedSeries.Add(yColName, currSInfo);
            }

            public void setPrimaryXAxsis(string label)
            {
                usedXAxis1Label = label;
            }
            public void setSecondaryXAxsis(string label)
            {
                usedXAxis2Label = label;
            }
            public void setSecondaryYAxsis(string label)
            {
                usedYAxis1Label = label;
            }
            public void setPrimaryYAxsis(string label)
            {
                usedYAxis2Label = label;
            }

            protected string usedXAxis1Label;
            protected string usedXAxis2Label;
            protected string usedYAxis1Label;
            protected string usedYAxis2Label;
            public void setData(DataTable data)
            {
                DataTable cData;
                double minDbl, maxDbl, cDbl;
                int minInt, maxInt, cInt;
                DateTime minDate, maxDate, cDate;
                TableSpec usedTabSpec;
                int rangeColSpecNum;
                if (usedChart.InvokeRequired)
                {
                    usedChart.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    usedChart.Series.Clear();
                    usedTabSpec = usedFilterTableCollection.getTablSpec(usedTableName);
                    cData = data.Copy();

                    foreach (KeyValuePair<string, SeriesInfo> currSInfo in usedSeries)//(System.Windows.Forms.DataVisualization.Charting.Series currSeries in usedSeries)
                    {

                        if (currSInfo.Value.rangeColName != null && cData.Columns.Contains(currSInfo.Value.rangeColName))
                        {
                            rangeColSpecNum = usedTabSpec.getColumnNum(currSInfo.Value.rangeColName);

                            if (transLateSQLTypeToDataType(usedTabSpec.ColumnSpecs[rangeColSpecNum].SQL_Type) == typeof(int))
                            {
                                minInt = (int)currSInfo.Value.rangeMin;
                                maxInt = (int)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cInt = (int)r[currSInfo.Value.rangeColName];//Convert.ToInt32  (r[rangeColNum].ToString (),usedC );//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cInt < minInt || cInt > maxInt)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                }
                            }
                            else if (transLateSQLTypeToDataType(usedTabSpec.ColumnSpecs[rangeColSpecNum].SQL_Type) == typeof(double))
                            {
                                minDbl = (double)currSInfo.Value.rangeMin;
                                maxDbl = (double)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cDbl = (double)r[currSInfo.Value.rangeColName];//Convert.ToDouble(r[rangeColNum].ToString(), usedC);//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cDbl < minDbl || cDbl > maxDbl)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                }
                            }
                            else if (transLateSQLTypeToDataType(usedTabSpec.ColumnSpecs[rangeColSpecNum].SQL_Type) == typeof(DateTime))
                            {
                                minDate = (DateTime)currSInfo.Value.rangeMin;
                                maxDate = (DateTime)currSInfo.Value.rangeMax;

                                foreach (DataRow r in cData.Rows)
                                {
                                    cDate = DateTime.ParseExact(r[currSInfo.Value.rangeColName].ToString(), currSInfo.Value.dateTimeFormat, usedC);//Convert.ChangeType (r[rangeColNum ],typeof(int));
                                    if (cDate < minDate || cDate > maxDate)
                                    {
                                        r.BeginEdit();
                                        r[currSInfo.Value.yColName] = DBNull.Value;
                                        r.EndEdit();
                                    }
                                    else
                                    {
                                        cDate = cDate;
                                    }
                                }
                            }
                        }

                        usedChart.Series.Add(currSInfo.Value.s);
                    }
                    usedChart.DataSource = cData;
                    usedChart.ChartAreas[0].AxisX.Title = usedXAxis1Label;
                    usedChart.ChartAreas[0].AxisY.Title = usedYAxis1Label;
                    usedChart.ChartAreas[0].AxisX2.Title = usedXAxis2Label;
                    usedChart.ChartAreas[0].AxisY2.Title = usedYAxis2Label;
                }
            }

            //public void update()
            //{   
            //    usedChart.DataSource = usedFilterTableCollection.getFilteredData(usedSelectionGroup, usedTableName);
            //}

            //public void initialize()
            //{
            //    usedChart.Series.Clear();
            //    foreach (System.Windows.Forms.DataVisualization.Charting.Series currSeries in usedSeries)
            //    {
            //        usedChart.Series.Add(currSeries);
            //    }
            //    update();
            //}
        }

        public class ListBoxAdapter : FilterTableCollection.Selection_Adapter, Data_Adapter//,OutputAdapter 
        {
            ListBox usedLB;
            protected string usedSelectionGroup;
            protected string usedColumnName;
            protected bool isPreserveSelection = true;
            FilterTableCollection usedFilterTableCollection;
            Dictionary<string, int> elementIndices;

            public ListBoxAdapter()
            {
                elementIndices = new Dictionary<string, int>();
            }

            public bool IsPreserveSelection
            {
                get { return isPreserveSelection; }
                set { isPreserveSelection = value; }
            }

            public void enableDisable(bool isEnable)
            {
                if (usedLB.InvokeRequired)
                    usedLB.Invoke(new CallReturnVoidParamBool(enableDisable), isEnable);
                else
                {
                    usedLB.Enabled = isEnable;
                }
            }

            public String SelectionGroup
            {
                get { return usedSelectionGroup; }
                set { usedSelectionGroup = value; }
            }

            public String ColumnName
            {
                get { return usedColumnName; }
                set { usedColumnName = value; }
            }

            public FilterTableCollection UsedFilterTableCollection
            {
                get { return usedFilterTableCollection; }
                set { usedFilterTableCollection = value; }
            }

            public bool verifySender(Object sender)
            {
                ListBox sLB;

                if (sender != null && sender.GetType() == typeof(ListBox))
                {
                    sLB = (ListBox)sender;

                    if (sLB == usedLB)
                        return true;
                }

                return false;
            }

            //protected string usedColName;
            //public String ColName
            //{
            //    get { return usedColName; }
            //    set { usedColName = value; }
            //}

            public void setData(DataTable data)
            {
                List<string> selectedDataItems;
                DataRowView currDRV;
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTable(setData), data);
                }
                else
                {
                    selectedDataItems = null;

                    storeElementIndices(data, usedColumnName);

                    if (isPreserveSelection && usedLB.ValueMember == usedColumnName)
                    {
                        selectedDataItems = new List<string>();

                        foreach (object o in usedLB.SelectedItems)
                        {
                            currDRV = (DataRowView)o;
                            selectedDataItems.Add(currDRV.Row[usedColumnName].ToString());
                        }
                    }

                    usedLB.BindingContext = new BindingContext();
                    usedLB.DataSource = data;
                    usedLB.DisplayMember = usedColumnName;//data.Columns[usedColumnName].ColumnName;
                    usedLB.ValueMember = usedColumnName;//data.Columns[usedColumnName].ColumnName;

                    if (isPreserveSelection && selectedDataItems != null)
                    {
                        foreach (string item in selectedDataItems)
                        {
                            if (elementIndices.ContainsKey(item))
                                usedLB.SetSelected(elementIndices[item], true);
                        }
                    }
                }
            }
            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }

            public ListBoxAdapter(ListBox lB)
                : this()
            {
                usedLB = lB;
            }
            //int getColNum(string colNames, DataTable entries)
            //{
            //    int i;

            //    for (i = 0; i < entries.Columns.Count; i++)
            //    {
            //        if (entries.Columns [i]
            //    }
            //}
            void storeElementIndices(DataTable entries, string colName)
            {
                int i;

                elementIndices.Clear();

                for (i = 0; i < entries.Rows.Count; i++)
                {
                    elementIndices.Add(entries.Rows[i][colName].ToString(), i);
                }
            }

            public void setTotalColumnEntries(DataTable entries, string colName)
            {
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setTotalColumnEntries), entries, colName);
                }
                else
                {
                    storeElementIndices(entries, colName);
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[colName].ColumnName;
                    usedLB.ValueMember = entries.Columns[colName].ColumnName;
                }
            }
            public void setAvailableColumnEntries(DataTable entries, string colName)
            {
                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setAvailableColumnEntries), entries, colName);
                }
                else
                {
                    storeElementIndices(entries, colName);
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[colName].ColumnName;
                    usedLB.ValueMember = entries.Columns[colName].ColumnName;
                }
            }
            public void setSelectedEntries(DataTable selectedEntries, string colName)
            {
                int currIndex;
                string currElementContent;

                if (usedLB.InvokeRequired)
                {
                    usedLB.Invoke(new CallReturnVoidParamDataTableString(setSelectedEntries), selectedEntries, colName);
                }
                else
                {
                    foreach (DataRow r in selectedEntries.Rows)
                    {
                        currElementContent = r[colName].ToString();
                        if (elementIndices.ContainsKey(currElementContent))
                        {
                            currIndex = elementIndices[currElementContent];
                            usedLB.SetSelected(currIndex, true);
                        }
                    }
                }
            }
            public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
            {
                int i, j;
                DataRowView currItem;

                if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY ||
                    usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR
                    )
                {
                    usedLB.DataSource = entries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[0].ColumnName;
                    usedLB.ValueMember = entries.Columns[0].ColumnName;
                }
                else if (usedFilterTableCollection.UsedBehavior == Behavior.SHOW_ONLY_AVAILABLE)
                {
                    usedLB.DataSource = availableEntries;
                    usedLB.BindingContext = new BindingContext();
                    usedLB.DisplayMember = entries.Columns[0].ColumnName;
                    usedLB.ValueMember = entries.Columns[0].ColumnName;
                }

                for (i = 0; i < usedLB.Items.Count; i++)
                {
                    currItem = (DataRowView)usedLB.Items[i];
                    if (selection.Contains(currItem.Row.ItemArray[0].ToString()))
                        usedLB.SetSelected(i, true);
                    else
                        usedLB.SetSelected(i, false);
                }
            }

            public DataTable getSelectedEntries()
            {
                DataTable entries;
                string[] currRow;

                if (usedLB.InvokeRequired)
                {
                    entries = (DataTable)usedLB.Invoke(new CallReturnDataTableVoidParams(getSelectedEntries));
                }
                else
                {
                    entries = new DataTable();
                    entries.Columns.Add();

                    foreach (DataRowView currItemRV in usedLB.SelectedItems)
                    {
                        currRow = new string[1];
                        currRow[0] = currItemRV.Row[0].ToString();
                        entries.Rows.Add(currRow);
                    }
                }
                return entries;
            }
            //void update()
            //{
            //    if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY ||
            //       usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR
            //       )
            //    {get
            //        usedLB.DataSource = usedFilterTableCollection .getColumnEntries (usedSelectionGroup ,usedColumnName ,;
            //        usedLB.BindingContext = new BindingContext();
            //        usedLB.DisplayMember = entries.Columns[0].ColumnName;
            //        usedLB.ValueMember = entries.Columns[0].ColumnName;
            //    }
            //    else if (usedFilterTableCollection.UsedBehavior == Behavior.SHOW_ONLY_AVAILABLE)
            //    {
            //        usedLB.DataSource = availableEntries;
            //        usedLB.BindingContext = new BindingContext();
            //        usedLB.DisplayMember = entries.Columns[0].ColumnName;
            //        usedLB.ValueMember = entries.Columns[0].ColumnName;
            //    }

            //    for (i = 0; i < usedLB.Items.Count; i++)
            //    {
            //        currItem = (DataRowView)usedLB.Items[i];
            //        if (selection.Contains(currItem.Row.ItemArray[0].ToString()))
            //            usedLB.SetSelected(i, true);
            //        else
            //            usedLB.SetSelected(i, false);
            //    }

            //}
            //void initialize()
            //{
            //}
        }

        public class ListViewAdapter : FilterTableCollection.Selection_Adapter
        {
            ListView usedLV;
            protected string usedSelectionGroup;
            protected string usedTableName;
            protected Color availColor = Color.White;
            protected Color notAvailColor = Color.Gray;
            Dictionary<string, int> elementIndices;
            List<int> previousAvailableIndices;
            List<int> previousSelectedIndices;
            FilterTableCollection usedFilterTableCollection;

            public ListViewAdapter()
            {
                elementIndices = new Dictionary<string, int>();
                previousAvailableIndices = new List<int>();
                previousSelectedIndices = new List<int>();
            }

            public FilterTableCollection UsedFilterTableCollection
            {
                get { return usedFilterTableCollection; }
                set { usedFilterTableCollection = value; }
            }

            public void setFilterTableCollection(FilterTableCollection uFTC)
            {
                usedFilterTableCollection = uFTC;
            }
            public void enableDisable(bool isEnable)
            {
                if (usedLV.InvokeRequired)
                    usedLV.Invoke(new CallReturnVoidParamBool(enableDisable), isEnable);
                else
                    usedLV.Enabled = isEnable;
            }
            public bool verifySender(Object sender)
            {
                ListView sLB;

                if (sender != null && sender.GetType() == typeof(ListView))
                {
                    sLB = (ListView)sender;

                    if (sLB == usedLV)
                        return true;
                }

                return false;
            }

            public Color AvailColor
            {
                set { availColor = value; }
                get { return availColor; }
            }

            public Color NotAvailColor
            {
                set { notAvailColor = value; }
                get { return notAvailColor; }
            }

            public ListViewAdapter(ListView lV)
                : this()
            {
                usedLV = lV;
            }
            public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
            {
                int i, j;
                DataTable usedDT;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableDataTableListOfString(setColumnEntries), entries, availableEntries, selection);
                }
                else
                {

                    if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY || usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
                    {
                        usedDT = entries;
                    }
                    else
                    {
                        usedDT = availableEntries;
                    }

                    if (selection.Count == 0)
                    {
                        usedLV.Items.Clear();
                        for (i = 0; i < usedDT.Rows.Count; i++)
                        {
                            usedLV.Items.Add(new ListViewItem(usedDT.Rows[i].ItemArray[0].ToString()));
                        }
                    }

                    if (usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
                    {
                        for (i = 0; i < usedLV.Items.Count; i++)
                        {
                            for (j = 0; j < availableEntries.Rows.Count; j++)
                            {
                                if (availableEntries.Rows[j].ItemArray[0].ToString() == usedLV.Items[i].Text)
                                    break;
                            }

                            if (j < availableEntries.Rows.Count)
                                usedLV.Items[i].BackColor = availColor;
                            else
                            {
                                usedLV.Items[i].BackColor = notAvailColor;
                            }

                            usedLV.Items[i].Selected = false;

                            foreach (string selectedItem in selection)
                            {
                                if (usedLV.Items[i].Text == selectedItem)
                                    usedLV.Items[i].Selected = true;
                            }
                        }
                    }
                }
            }

            void storeElementIndices(DataTable entries, string colName)
            {
                int i;

                elementIndices.Clear();

                for (i = 0; i < entries.Rows.Count; i++)
                {
                    elementIndices.Add(entries.Rows[i][colName].ToString(), i);
                }
            }

            public void setTotalColumnEntries(DataTable entries, string colName)
            {
                int i;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setTotalColumnEntries), entries, colName);
                }
                else
                {
                    usedLV.Items.Clear();
                    for (i = 0; i < entries.Rows.Count; i++)
                    {
                        usedLV.Items.Add(new ListViewItem(entries.Rows[i][colName].ToString()));
                    }
                    if (colName == "Product")
                        colName = colName;
                    storeElementIndices(entries, colName);
                    previousAvailableIndices.Clear();
                    previousSelectedIndices.Clear();
                }
            }
            public void setAvailableColumnEntries(DataTable entries, string colName)
            {
                int currIndex;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setAvailableColumnEntries), entries, colName);
                }
                else
                {

                    foreach (int pAI in previousAvailableIndices)
                    {
                        usedLV.Items[pAI].BackColor = notAvailColor;
                    }
                    previousAvailableIndices.Clear();
                    if (colName == "Product")
                        colName = colName;
                    foreach (DataRow r in entries.Rows)
                    {
                        currIndex = elementIndices[r[colName].ToString()];
                        usedLV.Items[currIndex].BackColor = availColor;
                        previousAvailableIndices.Add(currIndex);
                    }
                }
            }
            public void setSelectedEntries(DataTable entries, string colName)
            {
                int currIndex;

                if (usedLV.InvokeRequired)
                {
                    usedLV.Invoke(new CallReturnVoidParamDataTableString(setSelectedEntries), entries, colName);
                }
                else
                {
                    foreach (int pSI in previousSelectedIndices)
                    {
                        usedLV.Items[pSI].Selected = false;
                    }

                    previousSelectedIndices.Clear();
                    foreach (DataRow r in entries.Rows)
                    {
                        currIndex = elementIndices[r[colName].ToString()];
                        usedLV.Items[currIndex].Selected = true;
                        previousSelectedIndices.Add(currIndex);
                    }
                }
            }

            public DataTable getSelectedEntries()
            {
                DataTable entries;
                string[] currRow;

                if (usedLV.InvokeRequired)
                {
                    entries = (DataTable)usedLV.Invoke(new CallReturnDataTableVoidParams(getSelectedEntries));
                }
                else
                {
                    entries = new DataTable();
                    entries.Columns.Add();

                    foreach (ListViewItem currItem in usedLV.SelectedItems)
                    {
                        currRow = new string[1];
                        currRow[0] = currItem.Text;
                        entries.Rows.Add(currRow);
                    }
                }
                return entries;
            }
        }

        protected Behavior usedBehavior;
        public Behavior UsedBehavior
        {
            get { return usedBehavior; }
            set { usedBehavior = value; }
        }

        protected DataBase usedDb;

        protected bool isChangeColumnEntriesOnSelection;
        // protected bool isInResponse;
        protected bool isSortByCols = false;

        public class FilterEntries
        {

            public string colName;
            //public string orderColName;
            public List<string> filterEntries;
        }
        protected bool isBlockHandling;
        Dictionary<string, TableSpec> tableSpecs;
        Dictionary<string, SelectionGroup> selectionGroups;

        protected bool isEnableSAs = true;

        public bool IsEnabledAllSelectionAdapters
        {
            set
            {
                foreach (KeyValuePair<string, SelectionGroup> sGKV in selectionGroups)
                {
                    foreach (KeyValuePair<string, SelectionColumn> sCKV in sGKV.Value.selectionColumns)
                    {
                        foreach (Selection_Adapter sA in sCKV.Value.selectionAdapters)
                        {
                            sA.enableDisable(value);
                        }
                    }
                }

                isEnableSAs = value;
            }

            get { return isEnableSAs; }
        }

        public void addTable(TableSpec tabSpec)
        {
            tableSpecs.Add(tabSpec.TableName, tabSpec);
        }

        public TableSpec getTablSpec(string tabName)
        {
            return tableSpecs[tabName];
        }

        protected SelectionGroup getSelectionGroup(string sGroupName)
        {
            SelectionGroup currGroup;

            if (!selectionGroups.ContainsKey(sGroupName))
            {
                currGroup = new SelectionGroup();
                selectionGroups.Add(sGroupName, currGroup);
            }
            else
                currGroup = selectionGroups[sGroupName];

            return currGroup;
        }

        public void addSelectionColumn(string sGroupName, string columnName, string orderColumnName, SelectionColumnOperatorType sCOperator)
        {
            SelectionGroup currSG;
            SelectionColumn sCol;
            currSG = getSelectionGroup(sGroupName);///selectionGroups[sGroupName];

            sCol = new SelectionColumn();
            sCol.orderColumnName = orderColumnName;
            switch (sCOperator)
            {
                case SelectionColumnOperatorType.EQ:
                    sCol.usedOperatorStr = EQ_STR;
                    break;
                case SelectionColumnOperatorType.G:
                    sCol.usedOperatorStr = G_STR;
                    break;

                case SelectionColumnOperatorType.GEQ:
                    sCol.usedOperatorStr = GEQ_STR;
                    break;
                case SelectionColumnOperatorType.L:
                    sCol.usedOperatorStr = L_STR;
                    break;
                case SelectionColumnOperatorType.LEQ:
                    sCol.usedOperatorStr = LEQ_STR;
                    break;
            }

            currSG.selectionColumns.Add(columnName, sCol);

        }

        public void addSelectionAdapter(string sGroupName, string columnName, Selection_Adapter sA)
        {
            SelectionGroup currGroup;
            List<Selection_Adapter> currSAs;
            Selection_Adapter currSAI;
            SelectionColumn currSC;
            currGroup = getSelectionGroup(sGroupName);

            sA.setFilterTableCollection(this);
            currSC = currGroup.selectionColumns[columnName];
         
            //currGroup.selectionAdapters.Add(columnName, sA);

            currSC.selectionAdapters.Add(sA);
        }

        public void addDataAdapter(string sGroupName, string tableName, Data_Adapter dA)
        {
            SelectionGroup currGroup;

            currGroup = getSelectionGroup(sGroupName);

            if (!currGroup.dataAdapters.ContainsKey(tableName))
            {
                currGroup.dataAdapters.Add(tableName, new List<Data_Adapter>());
            }
            dA.setFilterTableCollection(this);
            currGroup.dataAdapters[tableName].Add(dA);
        }

        public FilterTableCollection(DataBase db, bool iCCOS)
        {
            usedDb = db;
            tableSpecs = new Dictionary<string, TableSpec>();
            //dataAdapters = new Dictionary<string, List<Data_Adapter>>();
            //selectionAdapters = new Dictionary<string, Selection_Adapter>();
            selectionGroups = new Dictionary<string, SelectionGroup>();
            isChangeColumnEntriesOnSelection = iCCOS;
        }

        public DataTable getColumnEntryList(string sGroupName, string colName, string orderColName, bool isOnlyAvailable)
        {
            DataTable resultTab;
            string filterCondition;
            string sqlCmdStr;
            bool isFirstTab;
            int currColNum;
            sqlCmdStr = "";
            isFirstTab = true;
            foreach (KeyValuePair<string, TableSpec> tSp in tableSpecs)
            {
                currColNum = getColumnNum(colName, tSp.Key);
                if (currColNum != -1 && tSp.Value.ColumnSpecs[currColNum].isFilterEntryColumn)
                {
                    if (!isFirstTab)
                        sqlCmdStr += SQLTable.SQL_UNION;
                    else
                        isFirstTab = false;
                    sqlCmdStr += SQLTable.SQL_SELECTDISTINCT + colName;
                    if (orderColName != "")
                        sqlCmdStr += "," + orderColName;
                    sqlCmdStr += SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(sGroupName, tSp.Key, false, true) + SQLTable.SQL_BRACKET_CLOSE + tSp.Key;//XBaseTableCmds[tSp.Key];
                    filterCondition = getFilterExpression(sGroupName, tSp.Key);//getFilterCondition(sGroupName, tSp.Key);
                    sqlCmdStr += SQLTable.SQL_WHERE + colName + " is not NULL";
                    if (isOnlyAvailable && filterCondition != "")
                        sqlCmdStr += SQLTable.SQL_AND + filterCondition;
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + "almostFinishedTab";
            if (orderColName != "")
                sqlCmdStr += SQLTable.SQL_ORDERBY + orderColName;
            else
                sqlCmdStr += SQLTable.SQL_ORDERBY + colName;

            resultTab = usedDb.getData(sqlCmdStr);
            return resultTab;
        }

        public DataTable getColumnEntries(string sGroupName, string colName, string orderColName, bool isOnlyAvailable)
        {
            DataTable resultTab;
            string filterCondition;
            string sqlCmdStr;
            bool isFirstTab;
            int currColNum;
            sqlCmdStr = "";
            isFirstTab = true;
            foreach (KeyValuePair<string, TableSpec> tSp in tableSpecs)
            {
                currColNum = getColumnNum(colName, tSp.Key);
                if (currColNum != -1 && tSp.Value.ColumnSpecs[currColNum].isFilterEntryColumn)
                {
                    if (!isFirstTab)
                        sqlCmdStr += SQLTable.SQL_UNION;
                    else
                        isFirstTab = false;
                    sqlCmdStr += SQLTable.SQL_SELECTDISTINCT + colName;
                    if (orderColName != "")
                        sqlCmdStr += "," + orderColName;
                    sqlCmdStr += SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(sGroupName, tSp.Key, false, true) + SQLTable.SQL_BRACKET_CLOSE + tSp.Key;//XBaseTableCmds[tSp.Key];
                    filterCondition = getFilterCondition(sGroupName, tSp.Key);
                    sqlCmdStr += SQLTable.SQL_WHERE + colName + " is not NULL";
                    if (isOnlyAvailable && filterCondition != "")
                        sqlCmdStr += SQLTable.SQL_AND + filterCondition;
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + "almostFinishedTab";
            if (orderColName != "")
                sqlCmdStr += SQLTable.SQL_ORDERBY + orderColName;
            else
                sqlCmdStr += SQLTable.SQL_ORDERBY + colName;

            resultTab = usedDb.getData(sqlCmdStr);
            return resultTab;
        }

        protected string getColumnsEnum(List<string> columnNames)
        {
            int i, nCols;
            string enumStr = "";

            nCols = columnNames.Count - 1;
            i = 0;
            foreach (string colName in columnNames)
            {
                enumStr += colName;
                if (i < nCols)
                    enumStr += ",";
            }

            return enumStr;
        }

        protected int getColumnNum(string columnName, string tableName)
        {
            int i, tL;
            TableSpec currTabSpec;

            currTabSpec = tableSpecs[tableName];

            tL = currTabSpec.ColumnSpecs.Length;
            for (i = 0; i < tL; i++)
            {
                if (currTabSpec.ColumnSpecs[i].NewColumnName == columnName)
                    return i;
            }
            return -1;
        }

        public bool IsSortByColumns
        {
            get { return isSortByCols; }

            set { isSortByCols = value; }
        }

        public void clearSelection(string sGroupName)
        {
            List<FilterEntries> fList;
            SelectionGroup currSGroup;

            isBlockHandling = true;

            currSGroup = selectionGroups[sGroupName];

            foreach (KeyValuePair<string, SelectionColumn> sColKV in currSGroup.selectionColumns)
            {
                if (sColKV.Value.explicitlySelectedItems != null)
                {
                    sColKV.Value.explicitlySelectedItems.Clear();
                    foreach (Selection_Adapter sAToChange in sColKV.Value.selectionAdapters)
                    {
                        sAToChange.setSelectedEntries(sColKV.Value.explicitlySelectedItems, sColKV.Value.explicitlySelectedItems.Columns[0].ColumnName);
                    }
                }
            }

            //fList = new List<FilterEntries>();

            //setFilter(null, fList, sGroupName);
            setFilter(sGroupName);
            isBlockHandling = false;
        }

        public void partialReInitialize(string sGroupName, string colName)
        {
            Dictionary<string, DataTable> colEntries;
            DataTable currCOLEntries;
            SelectionGroup currSelectionGroup;
            SelectionColumn currSColumn;
            colEntries = new Dictionary<string, DataTable>();

            isBlockHandling = true;

            currSelectionGroup = selectionGroups[sGroupName];
            currSColumn = currSelectionGroup.selectionColumns[colName];

            //foreach (KeyValuePair<string, SelectionGroup> currGroupKV in selectionGroups)
            //{
            //  foreach (KeyValuePair<string, SelectionColumn> sC in currGroupKV.Value.selectionColumns)
            //{
            if (colEntries.ContainsKey(colName))
            {
                colEntries.Remove(colName);
            }

            currCOLEntries = getColumnEntries(sGroupName, colName, currSColumn.orderColumnName, false);
            colEntries.Add(colName, currCOLEntries);

            foreach (Selection_Adapter sA in currSColumn.selectionAdapters)
            {
                //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                //sA.setSelectedEntries(currCOLEntries);
                sA.setTotalColumnEntries(currCOLEntries, colName);
            }
            // }
            //}
            refreshAll();
            isBlockHandling = false;
        }

        public void initialize()
        {
            Dictionary<string, DataTable> colEntries;
            DataTable currCOLEntries;
            colEntries = new Dictionary<string, DataTable>();
            isBlockHandling = true;
            foreach (KeyValuePair<string, SelectionGroup> currGroupKV in selectionGroups)
            {
                foreach (KeyValuePair<string, SelectionColumn> sC in currGroupKV.Value.selectionColumns)
                {
                    if (!colEntries.ContainsKey(sC.Key))
                    {
                        currCOLEntries = getColumnEntries(currGroupKV.Key, sC.Key, sC.Value.orderColumnName, false);
                        colEntries.Add(sC.Key, currCOLEntries);
                    }
                    else
                        currCOLEntries = colEntries[sC.Key];

                    foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                    {
                        //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                        //sA.setSelectedEntries(currCOLEntries);
                        sA.setTotalColumnEntries(currCOLEntries, sC.Key);
                    }
                }
            }
            refreshAll();
            isBlockHandling = false;
        }

        public void selectionHandler(Object sender)
        {
            if (isBlockHandling)
                return;

            isBlockHandling = true;
            foreach (KeyValuePair<string, SelectionGroup> sGKV in selectionGroups)
            {
                foreach (KeyValuePair<string, SelectionColumn> sCKV in sGKV.Value.selectionColumns)
                {
                    foreach (Selection_Adapter sA in sCKV.Value.selectionAdapters)
                    {
                        if (sA.verifySender(sender))
                        {
                            sCKV.Value.explicitlySelectedItems = sA.getSelectedEntries();
                            setFilter(sGKV.Key);
                            foreach (Selection_Adapter sAToChange in sCKV.Value.selectionAdapters)
                            {
                                sAToChange.setSelectedEntries(sCKV.Value.explicitlySelectedItems, sCKV.Value.explicitlySelectedItems.Columns[0].ColumnName);
                            }

                            isBlockHandling = false;
                            return;
                        }
                    }
                }
            }

            isBlockHandling = false;
        }

        public void refreshAll()
        {
            foreach (KeyValuePair<string, SelectionGroup> currSGKV in selectionGroups)
            {
                setFilter(currSGKV.Key);
            }
        }

        public void setFilter(string sGroupName)
        {
            int j;
            SelectionGroup currGroup;
            DataTable currAvailableCOLEntries;
            List<string> currSelection;
            List<Data_Adapter> currDAs;
            DataTable currDTab;
            DataTable currSTab;

            currGroup = selectionGroups[sGroupName];
            // isBlockHandling = true;
            //currGroup.filterList.Clear();
            //currGroup.filterList.AddRange(fList);

            if (isChangeColumnEntriesOnSelection)
            {
                foreach (KeyValuePair<string, SelectionColumn> sColKV in currGroup.selectionColumns)
                {
                    currSTab = new DataTable();
                    currSTab.Columns.Add();
                    currSelection = new List<string>();
                    //currSC = currGroup.selectionColumns[fE.colName];

                    if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                    {
                        currAvailableCOLEntries = getColumnEntryList(sGroupName, sColKV.Key, sColKV.Value.orderColumnName, true);//getColumnEntries(sGroupName, sColKV .Key, sColKV .Value .orderColumnName, true);

                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                        {
                            foreach (Selection_Adapter sA in sColKV.Value.selectionAdapters)
                            {
                                sA.setAvailableColumnEntries(currAvailableCOLEntries, sColKV.Key);
                            }
                        }
                        else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            foreach (Selection_Adapter sA in sColKV.Value.selectionAdapters)
                            {
                                sA.setTotalColumnEntries(currAvailableCOLEntries, sColKV.Key);
                            }
                        }
                    }
                    //currCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName , false);
                    //currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                    //foreach (string currFE in fE.filterEntries)
                    //{
                    //    currSRow = new string[1];
                    //    //for (j = 0; j < currCOLEntries.Rows.Count; j++)
                    //    //{
                    //    //    if (currCOLEntries.Rows[j].ItemArray[0].ToString() == currFE)
                    //    //        break;
                    //    //}

                    //    //if (j < currCOLEntries.Rows.Count)
                    //    //currSelection.Add(currFE);
                    //    currSRow[0] = currFE;
                    //    currSTab.Rows.Add(currSRow);
                    //}

                    ////currSAs = currGroup.selectionAdapters[fE.colName];

                    //foreach (Selection_Adapter sA in currSC.selectionAdapters)
                    //{
                    //    //sA.setColumnEntries (currCOLEntries ,currAvailableCOLEntries ,currSelection );
                    //    sA.setSelectedEntries(currSTab);
                    //}
                }

                //currSelection = new List<string>();
                //currSTab = new DataTable();
                //currSTab.Columns.Add();
                //foreach (KeyValuePair<string, SelectionColumn> sC in currGroup.selectionColumns)
                //{
                //    isRelevantColumn = false;
                //    foreach (FilterEntries fE in fList)
                //    {
                //        if (fE.colName == sC.Key)
                //        {
                //            isRelevantColumn = true;
                //            break;
                //        }
                //    }

                //    if (!isRelevantColumn)
                //    {
                //        //currCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, false);
                //        //currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);
                //        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                //        {
                //            currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);

                //            if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                //            {
                //                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //                {
                //                    sA.setAvailableColumnEntries(currAvailableCOLEntries);
                //                }
                //            }
                //            else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                //            {
                //                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //                {
                //                    sA.setTotalColumnEntries(currAvailableCOLEntries);
                //                }
                //            }
                //        }

                //        foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                //        {
                //            //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                //            sA.setSelectedEntries(currSTab);
                //        }
                //    }
                //}
            }

            foreach (KeyValuePair<string, TableSpec> kTSp in tableSpecs)
            {
                //isRelevantTable=false;
                //foreach (FilterEntries fE in fList)
                //{
                //    if (getColumnNum (fE.colName,kTSp.Key)!=-1)
                //    {
                //        isRelevantTable =true;
                //        break;
                //    }
                //}
                //if (isRelevantTable )
                //{
                if (currGroup.dataAdapters.ContainsKey(kTSp.Key))
                {
                    currDTab = getFilteredData(sGroupName, kTSp.Key);
                    currDAs = currGroup.dataAdapters[kTSp.Key];

                    foreach (Data_Adapter currDA in currDAs)
                    {
                        currDA.setData(currDTab);
                    }
                }
                //}
            }

            //isBlockHandling = false;
        }


        public void setFilterA(Object sender, List<FilterEntries> fList, string sGroupName)
        {
            int j;
            SelectionGroup currGroup;
            DataTable currCOLEntries, currAvailableCOLEntries;
            List<string> currSelection;
            bool isRelevantTable;
            List<Data_Adapter> currDAs;
            SelectionColumn currSC;
            DataTable currDTab;
            DataTable currSTab;
            bool isRelevantColumn;
            string[] currSRow;
            if (isBlockHandling)
                return;
            currGroup = selectionGroups[sGroupName];
            isBlockHandling = true;
            currGroup.filterList.Clear();
            currGroup.filterList.AddRange(fList);

            if (isChangeColumnEntriesOnSelection)
            {
                foreach (FilterEntries fE in fList)
                {
                    currSTab = new DataTable();
                    currSTab.Columns.Add();
                    currSelection = new List<string>();
                    currSC = currGroup.selectionColumns[fE.colName];

                    if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                    {
                        currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                        {
                            foreach (Selection_Adapter sA in currSC.selectionAdapters)
                            {
                                sA.setAvailableColumnEntries(currAvailableCOLEntries, currSC.columnName);
                            }
                        }
                        else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            foreach (Selection_Adapter sA in currSC.selectionAdapters)
                            {
                                sA.setTotalColumnEntries(currAvailableCOLEntries, currSC.columnName);
                            }
                        }
                    }


                    //currCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName , false);
                    //currAvailableCOLEntries = getColumnEntries(sGroupName, fE.colName, currSC.orderColumnName, true);

                    foreach (string currFE in fE.filterEntries)
                    {
                        currSRow = new string[1];
                        //for (j = 0; j < currCOLEntries.Rows.Count; j++)
                        //{
                        //    if (currCOLEntries.Rows[j].ItemArray[0].ToString() == currFE)
                        //        break;
                        //}

                        //if (j < currCOLEntries.Rows.Count)
                        //currSelection.Add(currFE);
                        currSRow[0] = currFE;
                        currSTab.Rows.Add(currSRow);
                    }

                    //currSAs = currGroup.selectionAdapters[fE.colName];

                    foreach (Selection_Adapter sA in currSC.selectionAdapters)
                    {
                        //sA.setColumnEntries (currCOLEntries ,currAvailableCOLEntries ,currSelection );
                        sA.setSelectedEntries(currSTab, currSC.columnName);
                    }
                }

                //currSelection = new List<string>();
                currSTab = new DataTable();
                currSTab.Columns.Add();
                foreach (KeyValuePair<string, SelectionColumn> sC in currGroup.selectionColumns)
                {
                    isRelevantColumn = false;
                    foreach (FilterEntries fE in fList)
                    {
                        if (fE.colName == sC.Key)
                        {
                            isRelevantColumn = true;
                            break;
                        }
                    }
                    if (!isRelevantColumn)
                    {
                        //currCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, false);
                        //currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);
                        if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR || usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                        {
                            currAvailableCOLEntries = getColumnEntries(sGroupName, sC.Key, sC.Value.orderColumnName, true);

                            if (usedBehavior == FilterTableCollection.Behavior.AVAILABLE_BY_COLOR)
                            {
                                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                                {
                                    sA.setAvailableColumnEntries(currAvailableCOLEntries, sC.Key);
                                }
                            }
                            else if (usedBehavior == FilterTableCollection.Behavior.SHOW_ONLY_AVAILABLE)
                            {
                                foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                                {
                                    sA.setTotalColumnEntries(currAvailableCOLEntries, sC.Key);
                                }
                            }
                        }

                        foreach (Selection_Adapter sA in sC.Value.selectionAdapters)
                        {
                            //sA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
                            sA.setSelectedEntries(currSTab, sC.Key);
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, TableSpec> kTSp in tableSpecs)
            {
                //isRelevantTable=false;
                //foreach (FilterEntries fE in fList)
                //{
                //    if (getColumnNum (fE.colName,kTSp.Key)!=-1)
                //    {
                //        isRelevantTable =true;
                //        break;
                //    }
                //}
                //if (isRelevantTable )
                //{
                if (currGroup.dataAdapters.ContainsKey(kTSp.Key))
                {
                    currDTab = getFilteredData(sGroupName, kTSp.Key);
                    currDAs = currGroup.dataAdapters[kTSp.Key];

                    foreach (Data_Adapter currDA in currDAs)
                    {
                        currDA.setData(currDTab);
                    }
                }
                //}
            }

            isBlockHandling = false;
        }

        public string getFilterExpression(string sGroupName, string tableName)
        {
            int i, j;
            bool isStr, isFirst;
            int colNum;
            TableSpec currTabSpec;
            string sqlCmdStr;
            SelectionGroup currGroup;

            currGroup = selectionGroups[sGroupName];

            sqlCmdStr = "";
            if (currGroup.filterList == null)
                return sqlCmdStr;
            j = 0;
            isFirst = true;

            currTabSpec = tableSpecs[tableName];

            foreach (KeyValuePair<string, SelectionColumn> sCKV in currGroup.selectionColumns)
            {
                if (sCKV.Value.explicitlySelectedItems != null &&
                    sCKV.Value.explicitlySelectedItems.Rows.Count > 0)//(fE.filterEntries.Count > 0)
                {
                    colNum = getColumnNum(sCKV.Key, tableName);
                    if (colNum != -1 && currTabSpec.ColumnSpecs[colNum].isFilteredColumn)
                    {
                        if (!isFirst)
                        {
                            sqlCmdStr += SQLTable.SQL_AND;
                        }
                        if (isFirst)
                            isFirst = false;

                        sqlCmdStr += " (";
                        if (currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATETIME ||
                            currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATE ||
                             currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_TEXT ||
                             currTabSpec.ColumnSpecs[colNum].SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                            )
                        {
                            isStr = true;
                        }
                        else
                            isStr = false;
                        i = 0;
                        foreach (DataRow currRow in sCKV.Value.explicitlySelectedItems.Rows)//(string entry in fE.filterEntries)
                        {
                            sqlCmdStr += currTabSpec.ColumnSpecs[colNum].NewColumnName + sCKV.Value.usedOperatorStr; // "=";
                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;

                            sqlCmdStr += currRow[0].ToString();//entry;

                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;

                            if (i < sCKV.Value.explicitlySelectedItems.Rows.Count - 1)//fE.filterEntries.Count - 1)//if (i < fE.filterEntries.Count - 1)
                                sqlCmdStr += SQLTable.SQL_OR;
                            i++;
                        }
                        sqlCmdStr += ")";
                    }
                }
                j++;
            }
            return sqlCmdStr;
        }

        public string getFilterCondition(string sGroupName, string tableName)
        {
            int i, j;
            bool isStr, isFirst;
            int colNum;
            TableSpec currTabSpec;
            string sqlCmdStr;
            SelectionGroup currGroup;

            currGroup = selectionGroups[sGroupName];

            sqlCmdStr = "";
            if (currGroup.filterList == null)
                return sqlCmdStr;
            j = 0;
            isFirst = true;

            currTabSpec = tableSpecs[tableName];

            foreach (FilterEntries fE in currGroup.filterList)
            {
                if (fE.filterEntries.Count > 0)
                {
                    colNum = getColumnNum(fE.colName, tableName);
                    if (colNum != -1 && currTabSpec.ColumnSpecs[colNum].isFilteredColumn)
                    {
                        if (!isFirst)
                        {
                            sqlCmdStr += SQLTable.SQL_AND;
                        }
                        if (isFirst)
                            isFirst = false;

                        sqlCmdStr += " (";
                        if (currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATETIME ||
                            currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATE ||
                             currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_TEXT ||
                             currTabSpec.ColumnSpecs[colNum].SQL_Type.Contains(SQLTable.SQL_VARCHAR)
                            )
                        {
                            isStr = true;
                        }
                        else
                            isStr = false;
                        i = 0;
                        foreach (string entry in fE.filterEntries)
                        {
                            sqlCmdStr += currTabSpec.ColumnSpecs[colNum].ColumnName + "=";
                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;

                            sqlCmdStr += entry;

                            if (isStr)
                                sqlCmdStr += SQLTable.SQL_STR_MARKER;
                            if (i < fE.filterEntries.Count - 1)
                                sqlCmdStr += SQLTable.SQL_OR;
                            i++;
                        }
                        sqlCmdStr += ")";
                    }
                }
                j++;
            }
            return sqlCmdStr;
        }

        protected string buildTableSQLCmd(string sGroupName, string tableName, bool isFinalTable, bool isUnFiltered)
        {
            int i, j, k;
            string sqlCmdStr;
            string aggrSqlCmdStr;
            TableSpec currTabSpec, jTabSpec;
            string filterCondition;
            string[] tNs;
            string[] ccs;
            string baseTableName, aggrTableName, disaggrTableName, renTableName;
            string usedTableName;
            bool isFirstVCol, isFirstAFCol, isFirstECol, isFirstDCol;
            string vColEnum, oColEnum, vColEnumNoAlias;
            string gDEnum;
            string colStr;
            HashSet<string> nonUniqueColumns;

            nonUniqueColumns = new HashSet<string>();




            baseTableName = " base" + tableName;
            aggrTableName = " aggr" + tableName;
            renTableName = " rename" + tableName;
            disaggrTableName = " disaggr" + tableName;
            tNs = new string[2];
            currTabSpec = tableSpecs[tableName];

            if (currTabSpec.IsUseSQLCmd)
                sqlCmdStr = currTabSpec.BaseSQLCmd;
            else
                sqlCmdStr = buildTableSQLCmd(sGroupName, currTabSpec.CalcTableName, false, isUnFiltered);

            sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + tableName;

            for (i = 0; i < currTabSpec.TablesToJoin.Length; i++)
            {
                tNs[0] = tableName;
                tNs[1] = currTabSpec.TablesToJoin[i].tableName;
                ccs = getCommonColunms(tNs);

                for (j = 0; j < ccs.Length; j++)
                {
                    if (!nonUniqueColumns.Contains(ccs[j]))
                        nonUniqueColumns.Add(ccs[j]);
                }
            }

            isFirstAFCol = true;
            vColEnum = "";
            if (currTabSpec.IsUseDimensions)
            {
                aggrSqlCmdStr = SQLTable.SQL_SELECT;
                isFirstVCol = true;
                isFirstDCol = true;

                gDEnum = "";
                for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
                {
                    if (currTabSpec.ColumnSpecs[i].isDimensionColumn || currTabSpec.ColumnSpecs[i].isNonDimensionColumn)
                    {
                        if (isFirstVCol)
                        {
                            isFirstVCol = false;
                            isFirstAFCol = false;
                        }
                        else
                        {
                            aggrSqlCmdStr += ",";
                            vColEnum += ",";
                        }

                        if (currTabSpec.ColumnSpecs[i].isDimensionColumn)
                        {
                            if (isFirstDCol)
                                isFirstDCol = false;
                            else
                                gDEnum += ",";

                            if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                            {
                                aggrSqlCmdStr += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                                gDEnum += currTabSpec.ColumnSpecs[i].ColumnName;
                            }
                            else
                            {
                                aggrSqlCmdStr += currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                                gDEnum += currTabSpec.ColumnSpecs[i].ColumnName;
                            }

                        }
                        else if (currTabSpec.ColumnSpecs[i].isNonDimensionColumn)
                        {

                            colStr = "";

                            switch (currTabSpec.ColumnSpecs[i].AggrType)
                            {
                                case ColumnSpec.ColAggrType.AVG:
                                    colStr = SQLTable.SQL_AVG;
                                    break;
                                case ColumnSpec.ColAggrType.COUNT:
                                    colStr = SQLTable.SQL_COUNT;
                                    break;
                                case ColumnSpec.ColAggrType.MAX:
                                    colStr = SQLTable.SQL_MAX;
                                    break;
                                case ColumnSpec.ColAggrType.MIN:
                                    colStr = SQLTable.SQL_MIN;
                                    break;
                                case ColumnSpec.ColAggrType.SUM:
                                    colStr = SQLTable.SQL_SUM;
                                    break;
                                default:
                                    break;

                            }
                            //aggrSqlCmdStr += SQLTable.SQL_SUM;
                            colStr += SQLTable.SQL_BRACKET_OPEN + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_BRACKET_CLOSE;

                            if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                            {
                                colStr = "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + colStr + ")";//'%Y-%m-%d',tTab.Date)
                            }
                            colStr += SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                            aggrSqlCmdStr += colStr;

                        }

                        if (!nonUniqueColumns.Contains(currTabSpec.ColumnSpecs[i].NewColumnName))
                            vColEnum += currTabSpec.ColumnSpecs[i].NewColumnName;
                        else
                            vColEnum += aggrTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;


                        //vColEnum += aggrTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;//currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                    }
                }

                sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + aggrSqlCmdStr + SQLTable.SQL_FROM + sqlCmdStr;
                if (gDEnum != "")
                    sqlCmdStr += SQLTable.SQL_GROUPBY + gDEnum;
                sqlCmdStr += SQLTable.SQL_BRACKET_CLOSE + aggrTableName;
                usedTableName = aggrTableName;
            }
            else
            {
                // usedTableName = tableName;
                string colRenameSQLCmd;
                usedTableName = renTableName;
                colRenameSQLCmd = SQLTable.SQL_SELECT;
                for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
                {
                    if (isFirstAFCol)
                        isFirstAFCol = false;
                    else
                    {
                        vColEnum += ",";
                        colRenameSQLCmd += ",";
                    }
                    if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                        colRenameSQLCmd += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + currTabSpec.ColumnSpecs[i].ColumnName + ")" + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                    else
                        colRenameSQLCmd += currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;

                    if (!nonUniqueColumns.Contains(currTabSpec.ColumnSpecs[i].NewColumnName))
                    {
                        vColEnum += currTabSpec.ColumnSpecs[i].NewColumnName;
                    }
                    else
                    {
                        vColEnum += usedTableName + "." + currTabSpec.ColumnSpecs[i].NewColumnName;

                        //if (currTabSpec.ColumnSpecs[i].SQL_Type == SQLTable.SQL_DATETIME)
                        //    colRenameSQLCmd += "strftime(" + "'" + currTabSpec.ColumnSpecs[i].DateTimeFormat + "'" + "," + usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + ")" + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                        //else
                        //    colRenameSQLCmd += usedTableName + "." + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].NewColumnName;
                    }
                }

                colRenameSQLCmd += SQLTable.SQL_FROM + sqlCmdStr;
                sqlCmdStr = SQLTable.SQL_BRACKET_OPEN + colRenameSQLCmd + SQLTable.SQL_BRACKET_CLOSE + renTableName;

                // sqlCmdStr = SQLTable.SQL_SELECT + vColEnum + SQLTable.SQL_FROM + sqlCmdStr;
            }
            vColEnumNoAlias = oColEnum = "";
            isFirstVCol = true;
            isFirstECol = true;
            for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
            {
                if (currTabSpec.ColumnSpecs[i].isVisibleColumn)
                {
                    if (isFirstVCol)
                        isFirstVCol = false;
                    else
                    {
                        vColEnumNoAlias += ",";
                    }

                    vColEnumNoAlias += currTabSpec.ColumnSpecs[i].NewColumnName;
                }

                if (currTabSpec.ColumnSpecs[i].isSortColumn)
                {
                    if (isFirstECol)
                        isFirstECol = false;
                    else
                        oColEnum += ",";
                    oColEnum += currTabSpec.ColumnSpecs[i].NewColumnName;
                }
            }

            for (i = 0; i < currTabSpec.TablesToJoin.Length; i++)
            {
                tNs[0] = tableName;
                tNs[1] = currTabSpec.TablesToJoin[i].tableName;
                ccs = getCommonColunms(tNs);

                jTabSpec = tableSpecs[currTabSpec.TablesToJoin[i].tableName];

                switch (currTabSpec.TablesToJoin[i].jType)
                {
                    case TableJoin.JoinType.JOIN:
                        sqlCmdStr += SQLTable.SQL_JOIN;
                        break;
                    case TableJoin.JoinType.LEFT_JOIN:
                        sqlCmdStr += SQLTable.SQL_LEFTJOIN;
                        break;
                    case TableJoin.JoinType.INNER_JOIN:
                        sqlCmdStr += SQLTable.SQL_INNERJOIN;
                        break;
                    case TableJoin.JoinType.OUTER_JOIN:
                        sqlCmdStr += SQLTable.SQL_OUTTERJOIN;
                        break;
                    case TableJoin.JoinType.CROSS_JOIN:
                        sqlCmdStr += SQLTable.SQL_CROSS_JOIN;
                        break;
                    default:
                        sqlCmdStr += SQLTable.SQL_JOIN;
                        break;
                }

                sqlCmdStr += SQLTable.SQL_BRACKET_OPEN + buildTableSQLCmd(sGroupName, currTabSpec.TablesToJoin[i].tableName, false, isUnFiltered) + SQLTable.SQL_BRACKET_CLOSE + currTabSpec.TablesToJoin[i].tableName;

                if (currTabSpec.TablesToJoin[i].jType != TableJoin.JoinType.CROSS_JOIN)
                {
                    sqlCmdStr += SQLTable.SQL_ON;

                    for (j = 0; j < ccs.Length; j++)
                    {
                        if (j > 0)
                            sqlCmdStr += SQLTable.SQL_AND;
                        sqlCmdStr += usedTableName + "." + ccs[j] + "=" + currTabSpec.TablesToJoin[i].tableName + "." + ccs[j];
                    }
                }
                for (j = 0; j < jTabSpec.ColumnSpecs.Length; j++)
                {
                    for (k = 0; k < ccs.Length; k++)
                    {
                        if (jTabSpec.ColumnSpecs[j].NewColumnName == ccs[k])
                            break;
                    }

                    if (k == ccs.Length)
                    {
                        if (!isFirstAFCol)
                        {
                            vColEnum += ",";
                        }
                        else
                            isFirstAFCol = false;

                        if ((jTabSpec.ColumnSpecs[j].SQL_Type == "double" || jTabSpec.ColumnSpecs[j].SQL_Type == "int") && jTabSpec.ColumnSpecs[j].isZeroForNull)
                        {
                            vColEnum += SQLTable.SQL_ISNULL + SQLTable.SQL_BRACKET_OPEN + currTabSpec.TablesToJoin[i].tableName + "." + jTabSpec.ColumnSpecs[j].NewColumnName + ",0" + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + jTabSpec.ColumnSpecs[j].NewColumnName;
                        }
                        else
                            vColEnum += currTabSpec.TablesToJoin[i].tableName + "." + jTabSpec.ColumnSpecs[j].NewColumnName + SQLTable.SQL_AS + jTabSpec.ColumnSpecs[j].NewColumnName;

                        if (jTabSpec.ColumnSpecs[j].isVisibleColumn)
                        {
                            if (!isFirstVCol)
                            {
                                vColEnumNoAlias += ",";
                            }
                            else
                                isFirstVCol = false;

                            vColEnumNoAlias += jTabSpec.ColumnSpecs[j].NewColumnName;
                        }
                    }
                    if (jTabSpec.ColumnSpecs[i].isSortColumn)
                    {
                        if (!isFirstECol)
                            oColEnum += ",";
                        else
                            isFirstECol = false;

                        oColEnum += jTabSpec.ColumnSpecs[i].NewColumnName;
                    }
                }
            }

            sqlCmdStr = SQLTable.SQL_SELECT + vColEnum + SQLTable.SQL_FROM + sqlCmdStr;

            if (currTabSpec.IsDistinctRows)
                sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE;
            else
                sqlCmdStr = SQLTable.SQL_SELECT + vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE;

            // sqlCmdStr+=vColEnumNoAlias + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + " almostFinalTab ";//finalTableSQLCmdStr;
            sqlCmdStr += " almostFinalTab ";
            if (!isUnFiltered)
            {
                filterCondition = getFilterExpression(sGroupName, tableName);//getFilterCondition(sGroupName, tableName);
                if (filterCondition != "")
                    sqlCmdStr += SQLTable.SQL_WHERE + SQLTable.SQL_BRACKET_OPEN + filterCondition + SQLTable.SQL_BRACKET_CLOSE;
                if (currTabSpec.addFilterExpression != "")
                {
                    if (filterCondition != "")
                        sqlCmdStr += SQLTable.SQL_AND;
                    else
                        sqlCmdStr += SQLTable.SQL_WHERE;

                    sqlCmdStr += SQLTable.SQL_BRACKET_OPEN + currTabSpec.addFilterExpression + SQLTable.SQL_BRACKET_CLOSE;
                }
            }
            if (isFinalTable && oColEnum != "")
            {
                sqlCmdStr += SQLTable.SQL_ORDERBY + oColEnum;
            }

            return sqlCmdStr;
        }


        public string[] getCommonColunms(string[] tabNames)
        {
            int i, j, nCC;
            string[] cc;
            Dictionary<string, int> hs;
            TableSpec currTabSpec;

            hs = new Dictionary<string, int>();

            for (i = 0; i < tabNames.Length; i++)
            {
                currTabSpec = tableSpecs[tabNames[i]];

                if (currTabSpec.IsUseDimensions)
                {
                    for (j = 0; j < currTabSpec.ColumnSpecs.Length; j++)
                    {
                        if (currTabSpec.ColumnSpecs[j].isDimensionColumn || currTabSpec.ColumnSpecs[j].isNonDimensionColumn)
                        {
                            if (hs.ContainsKey(currTabSpec.ColumnSpecs[j].NewColumnName))
                            {
                                hs[currTabSpec.ColumnSpecs[j].NewColumnName]++;
                            }
                            else
                                hs.Add(currTabSpec.ColumnSpecs[j].NewColumnName, 1);
                        }
                    }
                }
                else
                {
                    for (j = 0; j < currTabSpec.ColumnSpecs.Length; j++)
                    {
                        if (hs.ContainsKey(currTabSpec.ColumnSpecs[j].NewColumnName))
                        {
                            hs[currTabSpec.ColumnSpecs[j].NewColumnName]++;
                        }
                        else
                            hs.Add(currTabSpec.ColumnSpecs[j].NewColumnName, 1);
                    }
                }
            }
            nCC = 0;

            foreach (KeyValuePair<string, int> kV in hs)
            {
                if (kV.Value == tabNames.Length)
                    nCC++;
            }

            cc = new string[nCC];
            i = 0;
            foreach (KeyValuePair<string, int> kV in hs)
            {
                if (kV.Value == tabNames.Length)
                {
                    cc[i] = kV.Key;
                    i++;
                }
            }

            return cc;
        }

        public DataTable getFilteredData(string sGroupName, string tableName)
        {
            DataTable resultTab;
            string baseSQLCmd;

            baseSQLCmd = buildTableSQLCmd(sGroupName, tableName, true, false);

            resultTab = usedDb.getData(baseSQLCmd);

            return resultTab;
        }
    }
        //public class FilterTableCollection
        //{
        //    public enum Behavior
        //    {
        //        AVAILABLE_BY_COLOR,
        //        SHOW_ONLY_AVAILABLE,
        //        IGNORE_AVAILABILITY
        //    }

        //    public class ColumnSpec
        //    {
        //        public string ColumnName;
        //        public string SQL_Type;
        //        public bool isDimensionColumn;
        //        public bool isNonDimensionColumn;
        //        public bool isVisibleColumn;
        //    }
        //    public class TableSpec
        //    {

        //        public string BaseSQLCmd;
        //        public ColumnSpec[] ColumnSpecs;
        //        public bool isUseDimensions;
        //        public const string SQL_DATETIME = "datetime";
        //        public const string SQL_DATE = "date";
        //        public const string SQL_VARCHAR = "varchar";
        //        public const string SQL_TEXT = "text";
        //        public const string SQL_INTEGER = "integer";
        //        public const string SQL_DOUBLE = "double";
        //        public int getColumnNum(string columnName)
        //        {
        //            int i, tL;

        //            tL = ColumnSpecs.Length;
        //            for (i = 0; i < tL; i++)
        //            {
        //                if (ColumnSpecs[i].ColumnName == columnName)
        //                    return i;
        //            }
        //            return -1;
        //        }

        //        public string getDimensionColumnsEnum()
        //        {
        //            int i, tL;
        //            bool isFirst;
        //            string enumStr = "";
        //            isFirst = true;
        //            tL = ColumnSpecs.Length;
        //            for (i = 0; i < tL; i++)
        //            {
        //                if (ColumnSpecs[i].isDimensionColumn)
        //                {
        //                    if (!isFirst)
        //                        enumStr += ",";
        //                    else
        //                        isFirst = false;

        //                    enumStr += ColumnSpecs[i].ColumnName;
        //                }
        //            }

        //            return enumStr;
        //        }
        //    }

        //    public interface Selection_Adapter
        //    {
        //        void setFilterTableCollection(FilterTableCollection uFTC);
        //        void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection);
        //        bool verifySender(Object sender);
        //    };

        //    public interface Data_Adapter
        //    {
        //        void setData(DataTable data);
        //        void setFilterTableCollection(FilterTableCollection uFTC);
        //    }

        //    public class GridViewAdapter : Data_Adapter
        //    {
        //        DataGridView usedDGV;
        //        FilterTableCollection usedFilterTableCollection;
        //        public GridViewAdapter(DataGridView uDGV)
        //        {
        //            usedDGV = uDGV;
        //        }
        //        public void setFilterTableCollection(FilterTableCollection uFTC)
        //        {
        //            usedFilterTableCollection = uFTC;
        //        }
        //        public void setData(DataTable data)
        //        {
        //            usedDGV.DataSource = data;
        //        }
        //    }

        //    public class ChartAdapter : Data_Adapter
        //    {
        //        System.Windows.Forms.DataVisualization.Charting.Chart usedChart;
        //        FilterTableCollection.TableSpec usedTabSpec;
        //        FilterTableCollection usedFilterTableCollection;

        //        public ChartAdapter(System.Windows.Forms.DataVisualization.Charting.Chart uC)
        //        {
        //            usedChart = uC;
        //            usedSeries = new List<System.Windows.Forms.DataVisualization.Charting.Series>();
        //        }

        //        public void setFilterTableCollection(FilterTableCollection uFTC)
        //        {
        //            usedFilterTableCollection = uFTC;
        //        }

        //        public void setTabSpec(TableSpec tabSpec)
        //        {
        //            usedTabSpec = tabSpec;

        //        }
        //        public System.Windows.Forms.DataVisualization.Charting.ChartValueType transLateSQLTypeToChartType(string sqlType)
        //        {
        //            if (sqlType == TableSpec.SQL_DATE)
        //                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.DateTime;
        //            else if (sqlType == TableSpec.SQL_TEXT || sqlType == TableSpec.SQL_VARCHAR)
        //                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
        //            else if (sqlType == TableSpec.SQL_INTEGER)
        //                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
        //            else if (sqlType == TableSpec.SQL_DOUBLE)
        //                return System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

        //            return System.Windows.Forms.DataVisualization.Charting.ChartValueType.String;
        //        }
        //        List<System.Windows.Forms.DataVisualization.Charting.Series> usedSeries;
        //        public void addSeries(string xColName, bool isSecondaryX, string yColName, bool isSecondaryY, Color usedColor)
        //        {
        //            System.Windows.Forms.DataVisualization.Charting.Series currSeries;
        //            int currColNum;

        //            currSeries = new System.Windows.Forms.DataVisualization.Charting.Series();
        //            currColNum = usedTabSpec.getColumnNum(xColName);
        //            currSeries.Name = yColName;
        //            currSeries.XValueMember = xColName;

        //            currSeries.XValueType = transLateSQLTypeToChartType(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

        //            if (isSecondaryX)
        //                currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
        //            else
        //                currSeries.XAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;

        //            currColNum = usedTabSpec.getColumnNum(yColName);
        //            currSeries.YValueMembers = yColName;
        //            currSeries.YValueType = transLateSQLTypeToChartType(usedTabSpec.ColumnSpecs[currColNum].SQL_Type);

        //            if (isSecondaryY)
        //                currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
        //            else
        //                currSeries.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Primary;
        //            currSeries.ChartArea = "Default";

        //            currSeries.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
        //            currSeries.Color = usedColor;
        //            usedSeries.Add(currSeries);
        //        }

        //        public void setData(DataTable data)
        //        {
        //            System.Windows.Forms.DataVisualization.Charting.ChartArea cArea;

        //            usedChart.Series.Clear();
        //            usedChart.DataSource = data;
        //            foreach (System.Windows.Forms.DataVisualization.Charting.Series currSeries in usedSeries)
        //            {
        //                usedChart.Series.Add(currSeries);
        //            }

        //        }
        //    }

        //    public class ListBoxAdapter : FilterTableCollection.Selection_Adapter
        //    {
        //        ListBox usedLB;

        //        FilterTableCollection usedFilterTableCollection;
        //        public FilterTableCollection UsedFilterTableCollection
        //        {
        //            get { return usedFilterTableCollection; }
        //            set { usedFilterTableCollection = value; }
        //        }

        //        public bool verifySender(Object sender)
        //        {
        //            ListBox sLB;

        //            if (sender != null && sender.GetType() == typeof(ListBox))
        //            {
        //                sLB = (ListBox)sender;

        //                if (sLB == usedLB)
        //                    return true;
        //            }

        //            return false;
        //        }

        //        public void setFilterTableCollection(FilterTableCollection uFTC)
        //        {
        //            usedFilterTableCollection = uFTC;
        //        }

        //        public ListBoxAdapter(ListBox lB)
        //        {
        //            usedLB = lB;
        //        }

        //        public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
        //        {
        //            int i, j;
        //            DataRowView currItem;

        //            if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY || usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
        //            {
        //                usedLB.DataSource = entries;
        //                usedLB.DisplayMember = entries.Columns[0].ColumnName;
        //                usedLB.ValueMember = entries.Columns[0].ColumnName;
        //            }
        //            else if (usedFilterTableCollection.UsedBehavior == Behavior.SHOW_ONLY_AVAILABLE)
        //            {
        //                usedLB.DataSource = availableEntries;
        //                usedLB.DisplayMember = entries.Columns[0].ColumnName;
        //                usedLB.ValueMember = entries.Columns[0].ColumnName;
        //            }

        //            for (i = 0; i < usedLB.Items.Count; i++)
        //            {
        //                currItem = (DataRowView)usedLB.Items[i];
        //                if (selection.Contains(currItem.Row.ItemArray[0].ToString()))
        //                    usedLB.SetSelected(i, true);
        //                else
        //                    usedLB.SetSelected(i, false);
        //            }
        //        }
        //    }

        //    public class ListViewAdapter : FilterTableCollection.Selection_Adapter
        //    {
        //        ListView usedLV;
        //        protected Color availColor = Color.White;
        //        protected Color notAvailColor = Color.Gray;

        //        FilterTableCollection usedFilterTableCollection;
        //        public FilterTableCollection UsedFilterTableCollection
        //        {
        //            get { return usedFilterTableCollection; }
        //            set { usedFilterTableCollection = value; }
        //        }

        //        public void setFilterTableCollection(FilterTableCollection uFTC)
        //        {
        //            usedFilterTableCollection = uFTC;
        //        }

        //        public bool verifySender(Object sender)
        //        {
        //            ListView sLB;

        //            if (sender != null && sender.GetType() == typeof(ListView))
        //            {
        //                sLB = (ListView)sender;

        //                if (sLB == usedLV)
        //                    return true;
        //            }

        //            return false;
        //        }

        //        public Color AvailColor
        //        {
        //            set { availColor = value; }
        //            get { return availColor; }
        //        }

        //        public Color NotAvailColor
        //        {
        //            set { notAvailColor = value; }
        //            get { return notAvailColor; }
        //        }

        //        public ListViewAdapter(ListView lV)
        //        {
        //            usedLV = lV;
        //        }
        //        public void setColumnEntries(DataTable entries, DataTable availableEntries, List<string> selection)
        //        {
        //            int i, j;
        //            DataTable usedDT;

        //            if (usedFilterTableCollection.UsedBehavior == Behavior.IGNORE_AVAILABILITY || usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
        //            {
        //                usedDT = entries;
        //            }
        //            else
        //            {
        //                usedDT = availableEntries;
        //            }

        //            if (selection.Count == 0)
        //            {
        //                usedLV.Items.Clear();
        //                for (i = 0; i < usedDT.Rows.Count; i++)
        //                {
        //                    usedLV.Items.Add(new ListViewItem(usedDT.Rows[i].ItemArray[0].ToString()));
        //                }
        //            }

        //            if (usedFilterTableCollection.UsedBehavior == Behavior.AVAILABLE_BY_COLOR)
        //            {
        //                for (i = 0; i < usedLV.Items.Count; i++)
        //                {
        //                    for (j = 0; j < availableEntries.Rows.Count; j++)
        //                    {
        //                        if (availableEntries.Rows[j].ItemArray[0].ToString() == usedLV.Items[i].Text)
        //                            break;
        //                    }

        //                    if (j < availableEntries.Rows.Count)
        //                        usedLV.Items[i].BackColor = availColor;
        //                    else
        //                        usedLV.Items[i].BackColor = notAvailColor;
        //                }
        //            }

        //        }
        //    }



        //    protected Behavior usedBehavior;
        //    public Behavior UsedBehavior
        //    {
        //        get { return usedBehavior; }
        //        set { usedBehavior = value; }
        //    }

        //    DataBase usedDb;

        //    protected bool isChangeColumnEntriesOnSelection;
        //    protected bool isInResponse;

        //    public class FilterEntries
        //    {

        //        public string colName;
        //        public List<string> filterEntries;
        //    }

        //    Dictionary<string, string> XBaseTableCmds;
        //    Dictionary<string, TableSpec> tableSpecs;
        //    Dictionary<string, List<Data_Adapter>> dataAdapters;
        //    Dictionary<string, Selection_Adapter> selectionAdapters;

        //    public void addTable(string tableName, TableSpec tabSpec)
        //    {
        //        string currXBaseTableCmd;

        //        tableSpecs.Add(tableName, tabSpec);
        //        currXBaseTableCmd = SQLTable.SQL_BRACKET_OPEN + tabSpec.BaseSQLCmd + SQLTable.SQL_BRACKET_CLOSE + " baseTab ";

        //        XBaseTableCmds.Add(tableName, currXBaseTableCmd);
        //    }

        //    public void addSelectionAdapter(string columnName, Selection_Adapter sA)
        //    {
        //        sA.setFilterTableCollection(this);
        //        selectionAdapters.Add(columnName, sA);
        //    }

        //    public void addDataAdapter(string tableName, Data_Adapter dA)
        //    {
        //        if (!dataAdapters.ContainsKey(tableName))
        //        {
        //            dataAdapters.Add(tableName, new List<Data_Adapter>());
        //        }
        //        dA.setFilterTableCollection(this);
        //        dataAdapters[tableName].Add(dA);
        //    }

        //    public FilterTableCollection(DataBase db, bool isChangeColEntriesOnSelection)
        //    {
        //        usedDb = db;
        //        tableSpecs = new Dictionary<string, TableSpec>();
        //        XBaseTableCmds = new Dictionary<string, string>();
        //        dataAdapters = new Dictionary<string, List<Data_Adapter>>();
        //        selectionAdapters = new Dictionary<string, Selection_Adapter>();

        //        isChangeColumnEntriesOnSelection = isChangeColEntriesOnSelection;
        //    }



        //    public DataTable getColumnEntries(string colName, bool isOnlyAvailable)
        //    {
        //        DataTable resultTab;
        //        string filterCondition;
        //        string sqlCmdStr;
        //        bool isFirstTab;

        //        sqlCmdStr = "";
        //        isFirstTab = true;
        //        foreach (KeyValuePair<string, TableSpec> tSp in tableSpecs)
        //        {
        //            if (getColumnNum(colName, tSp.Key) != -1)
        //            {
        //                if (!isFirstTab)
        //                    sqlCmdStr += SQLTable.SQL_UNION;
        //                else
        //                    isFirstTab = false;
        //                sqlCmdStr += SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + XBaseTableCmds[tSp.Key];
        //                filterCondition = getFilterCondition(tSp.Key);
        //                if (isOnlyAvailable && filterCondition != "")
        //                    sqlCmdStr += SQLTable.SQL_WHERE + filterCondition;
        //            }
        //        }

        //        sqlCmdStr = SQLTable.SQL_SELECTDISTINCT + colName + SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + "almostFinishedTab";
        //        sqlCmdStr += SQLTable.SQL_ORDERBY + colName;

        //        resultTab = usedDb.getData(sqlCmdStr);
        //        return resultTab;
        //    }



        //    protected string getColumnsEnum(string tableName)
        //    {
        //        int i, tL;
        //        bool isFirst;
        //        TableSpec currTabSpec;
        //        string enumStr = "";

        //        currTabSpec = tableSpecs[tableName];
        //        tL = currTabSpec.ColumnSpecs.Length;

        //        isFirst = true;
        //        for (i = 0; i < tL; i++)
        //        {
        //            if (currTabSpec.ColumnSpecs[i].isVisibleColumn)
        //            {
        //                if (isFirst)
        //                    isFirst = false;
        //                else
        //                    enumStr += ",";
        //                enumStr += currTabSpec.ColumnSpecs[i].ColumnName;
        //            }
        //        }

        //        return enumStr;
        //    }

        //    protected string getColumnsEnum(List<string> columnNames)
        //    {
        //        int i, nCols;
        //        string enumStr = "";

        //        nCols = columnNames.Count - 1;
        //        i = 0;
        //        foreach (string colName in columnNames)
        //        {
        //            enumStr += colName;
        //            if (i < nCols)
        //                enumStr += ",";
        //        }

        //        return enumStr;
        //    }

        //    protected int getColumnNum(string columnName, string tableName)
        //    {
        //        int i, tL;
        //        TableSpec currTabSpec;

        //        currTabSpec = tableSpecs[tableName];

        //        tL = currTabSpec.ColumnSpecs.Length;
        //        for (i = 0; i < tL; i++)
        //        {
        //            if (currTabSpec.ColumnSpecs[i].ColumnName == columnName)
        //                return i;
        //        }
        //        return -1;
        //    }

        //    protected bool isSortByCols = false;
        //    List<FilterEntries> filterList;

        //    public bool IsSortByColumns
        //    {
        //        get { return isSortByCols; }

        //        set { isSortByCols = value; }
        //    }

        //    public void setFilter(Object sender, List<FilterEntries> fList)
        //    {
        //        int i, j;
        //        DataTable currCOLEntries, currAvailableCOLEntries;
        //        Selection_Adapter currSA;
        //        List<string> currSelection;
        //        if (isInResponse)
        //            return;

        //        isInResponse = true;
        //        filterList = fList;

        //        if (isChangeColumnEntriesOnSelection)
        //        {
        //            foreach (FilterEntries fE in fList)
        //            {
        //                currSelection = new List<string>();
        //                currCOLEntries = getColumnEntries(fE.colName, false);
        //                currAvailableCOLEntries = getColumnEntries(fE.colName, true);

        //                foreach (string currFE in fE.filterEntries)
        //                {
        //                    for (j = 0; j < currCOLEntries.Rows.Count; j++)
        //                    {
        //                        if (currCOLEntries.Rows[j].ItemArray[0].ToString() == currFE)
        //                            break;
        //                    }

        //                    if (j < currCOLEntries.Rows.Count)
        //                        currSelection.Add(currFE);
        //                }

        //                currSA = selectionAdapters[fE.colName];
        //                currSA.setColumnEntries(currCOLEntries, currAvailableCOLEntries, currSelection);
        //            }
        //        }
        //        bool isRelevantTable;
        //        List<Data_Adapter> currDAs;
        //        DataTable currDTab;
        //        foreach (KeyValuePair<string, TableSpec> kTSp in tableSpecs)
        //        {
        //            isRelevantTable = false;
        //            foreach (FilterEntries fE in fList)
        //            {
        //                if (getColumnNum(fE.colName, kTSp.Key) != -1)
        //                {
        //                    isRelevantTable = true;
        //                    break;
        //                }
        //            }
        //            if (isRelevantTable)
        //            {
        //                if (dataAdapters.ContainsKey(kTSp.Key))
        //                {

        //                    currDTab = getFilteredData(kTSp.Key);

        //                    currDAs = dataAdapters[kTSp.Key];

        //                    foreach (Data_Adapter currDA in currDAs)
        //                    {
        //                        currDA.setData(currDTab);
        //                    }
        //                }
        //            }
        //        }

        //        isInResponse = false;
        //    }

        //    public string getFilterCondition(string tableName)
        //    {
        //        int i, j;
        //        bool isStr, isFirst;
        //        int colNum;
        //        TableSpec currTabSpec;
        //        string sqlCmdStr;

        //        sqlCmdStr = "";
        //        if (filterList == null)
        //            return sqlCmdStr;
        //        j = 0;
        //        isFirst = true;

        //        currTabSpec = tableSpecs[tableName];

        //        foreach (FilterEntries fE in filterList)
        //        {
        //            if (fE.filterEntries.Count > 0)
        //            {
        //                colNum = getColumnNum(fE.colName, tableName);
        //                if (colNum != -1)
        //                {
        //                    if (!isFirst)
        //                    {
        //                        sqlCmdStr += SQLTable.SQL_AND;
        //                    }
        //                    if (isFirst)
        //                        isFirst = false;

        //                    sqlCmdStr += " (";
        //                    if (currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATETIME ||
        //                        currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_DATE ||
        //                         currTabSpec.ColumnSpecs[colNum].SQL_Type == SQLTable.SQL_TEXT ||
        //                         currTabSpec.ColumnSpecs[colNum].SQL_Type.Contains(SQLTable.SQL_VARCHAR)
        //                        )
        //                    {
        //                        isStr = true;
        //                    }
        //                    else
        //                        isStr = false;
        //                    i = 0;
        //                    foreach (string entry in fE.filterEntries)
        //                    {
        //                        sqlCmdStr += currTabSpec.ColumnSpecs[colNum].ColumnName + "=";
        //                        if (isStr)
        //                            sqlCmdStr += SQLTable.SQL_STR_MARKER;

        //                        sqlCmdStr += entry;

        //                        if (isStr)
        //                            sqlCmdStr += SQLTable.SQL_STR_MARKER;
        //                        if (i < fE.filterEntries.Count - 1)
        //                            sqlCmdStr += SQLTable.SQL_OR;
        //                        i++;
        //                    }
        //                    sqlCmdStr += ")";
        //                }
        //            }
        //            j++;
        //        }
        //        return sqlCmdStr;
        //    }

        //    public DataTable getFilteredData(string tableName)
        //    {
        //        int i, j;
        //        string sqlCmdStr;
        //        string aggrSqlCmdStr;
        //        string columnsEnum;
        //        string dimensionsEnum;
        //        TableSpec currTabSpec;
        //        DataTable resultTab;
        //        string filterCondition;

        //        columnsEnum = getColumnsEnum(tableName);

        //        sqlCmdStr = SQLTable.SQL_SELECT + columnsEnum + SQLTable.SQL_FROM + XBaseTableCmds[tableName];
        //        filterCondition = getFilterCondition(tableName);
        //        if (filterCondition != "")
        //            sqlCmdStr += SQLTable.SQL_WHERE + filterCondition;

        //        currTabSpec = tableSpecs[tableName];

        //        if (currTabSpec.isUseDimensions)
        //        {
        //            dimensionsEnum = currTabSpec.getDimensionColumnsEnum();
        //            aggrSqlCmdStr = SQLTable.SQL_SELECT + dimensionsEnum;

        //            for (i = 0; i < currTabSpec.ColumnSpecs.Length; i++)
        //            {
        //                if (currTabSpec.ColumnSpecs[i].isNonDimensionColumn)
        //                {
        //                    aggrSqlCmdStr += "," + SQLTable.SQL_SUM + SQLTable.SQL_BRACKET_OPEN + currTabSpec.ColumnSpecs[i].ColumnName + SQLTable.SQL_BRACKET_CLOSE + SQLTable.SQL_AS + currTabSpec.ColumnSpecs[i].ColumnName;
        //                }
        //            }

        //            aggrSqlCmdStr += SQLTable.SQL_FROM + SQLTable.SQL_BRACKET_OPEN + sqlCmdStr + SQLTable.SQL_BRACKET_CLOSE + " rawTable " + SQLTable.SQL_GROUPBY + dimensionsEnum;
        //            if (isSortByCols)
        //                aggrSqlCmdStr += SQLTable.SQL_ORDERBY + dimensionsEnum;
        //            resultTab = usedDb.getData(aggrSqlCmdStr);
        //        }
        //        else
        //        {
        //            if (isSortByCols)
        //                sqlCmdStr += SQLTable.SQL_ORDERBY + columnsEnum;
        //            resultTab = usedDb.getData(sqlCmdStr);
        //        }

        //        return resultTab;
        //    }
        //}
    */
}
